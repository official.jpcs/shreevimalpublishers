
<!DOCTYPE html>
<html lang="en">


<!-- index 06:41:43 GMT -->
<head>
    <meta charset="UTF-8">
    <title>Shree Vimal Publishers Pvt. Ltd.</title>

    <!-- responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <meta charset="utf-8"><meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
    <!-- master stylesheet -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive stylesheet -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Fontawesome -->
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="images/favicon/favicon-16x16.png" sizes="16x16">
	
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



</head>

<body>
    <div class="boxed_wrapper">

        <div class="preloader"></div>

        <!-- Start Top Bar style1 -->
        <section class="top-bar-style1">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">

                    </div>
                </div>
            </div>
        </section>
        <!-- End Top Bar style1 -->

        <!--Start Main Header-->
        <header class="main-header header-style1">

            <div class="header-upper-style1">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="inner-container clearfix">
                                <div class="logo-box-style1 float-left">
                                    <a href="index.php">
                                        <img src="images/resources/logo.png" alt="Awesome Logo">
                                    </a>
                                </div>
                                <div class="main-menu-box float-right">
                                    <nav class="main-menu clearfix">
                                        <div class="navbar-header clearfix">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        </div>
                                        <div class="navbar-collapse collapse clearfix">
                                            <ul class="navigation clearfix">
                                                <li ><a href="index.php">Home</a>
												</li>
												<li><a href="About.php">About Us</a>
                                                </li>
												
												<li class="dropdown" ><a href="" class="dropdown-toggle" data-toggle="dropdown">BOOK CATEGORIES<b class="caret"></b></a>
												
													<ul class="dropdown-menu dropdown-content multi-column columns-3">
														<div class="row ">
																
															<?php
													
			include('includes/dbcon.php');
			$query = "SELECT count(*) as count,category_title,tbl_book_categories.category_id from tbl_books  
			INNER JOIN tbl_book_categories ON (tbl_books.category_id=tbl_book_categories.category_id) group by category_title";
				$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
				
												if(mysqli_num_rows($fire)>0)
											{
													while($user= mysqli_fetch_assoc($fire))
												{
												?>
															<div class="col-sm-4">
															<!--fetch count of catagories-->
															<?php include('includes/catagoriescount.php'); ?>
								
													
																<li><a href="Book_Category.php?bk=<?php echo $user['category_id']?>"><?php echo $user['category_title']?>(<?php echo $user['count']?>)</a></li>
														</div>
																<?php
											}}
												?>	
														</div>
													</ul>
												</li>
                                                <li><a href="Distributers.php">OUR DISTRIBUTORES</a>
                                                </li>
                                                
                                                <li><a href="ContactUs.php">Contact US</a></li>
                                            </ul>
                                        </div>
                                    </nav>
                                    <div class="mainmenu-right">
                                        <div class="outer-search-box">
                                            <div class="seach-toggle"><i class="fa fa-search"></i></div>
                                            <ul class="search-box">
                                                <li>
                                                    <form method="post" action="search.php">
                                                        <div class="form-group">
                                                            <input type="search" name="search" placeholder="Search Here" required></input>
                                                            <button type="submit"  name="booksearch"><i class="fa fa-search"></i></button>
                                                        </div>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="cart-box">
										<?php
											$count=0;
											if(isset($_SESSION['cart']))
											{
												$count=count($_SESSION['cart']);
											}
										?>
                                            <a href="my-cart.php"><span class="icon-bag"><span class="number"><?php echo $count; ?></span></span></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<div class="header-upper-style1" style="padding: 18px 18px;"><div>
	   </header>
	    
