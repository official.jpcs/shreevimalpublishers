<?php
session_start();
 ?>
<!--Header-->
<?php
include 'header.php';
?>
        <!--Start Main project area-->
        <section class="main-project-area">
            <div class="container-fluid">
                <div class="row filter-layout masonary-layout">
                <div class="col-xl-12 col-md-12 col-sm-10 ml-3">
				<div class="row">
					<?php
     //connection
     include 'includes/dbcon.php';

     if (isset($_GET['bk'])) {
         $id = $_GET['bk'];

        /* $query = "SELECT * from tbl_books INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id)
						INNER JOIN tbl_languages ON
                        (tbl_books.book_language=tbl_languages.language_id)
						WHERE tbl_books.category_id=$id AND is_book_visible=2";*/
						
						
		$query="SELECT tbl_books.*,tbl_languages.*,tbl_book_categories.*,(SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tbl_books.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail_img from tbl_books INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id)
						INNER JOIN tbl_languages ON
                        (tbl_books.book_language=tbl_languages.language_id)
						WHERE tbl_books.category_id=$id AND is_book_visible=2";
						
         ($fire = mysqli_query($mysqli, $query)) or die("can not fetch the data." . mysqli_error($mysqli));
         if (mysqli_num_rows($fire) > 0) {
             while ($user = mysqli_fetch_assoc($fire)) { ?>
                    <!--Start single project item-->
					<form action="manage_cart.php" method="POST">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6  filter-item contem ret">
                        <div class="single-project-style4">
                            <div class="img-holder">
                                <div class="inner">
									<img src="admin/images/book_thumbnails/other_images/<?php echo $user['book_thumbnail_img']; ?>" alt="Awesome Image"/>
                                   
                                </div>
                                <div class="overlay-content">
                                    <div class="title">
                                        <div><h3><a href="Book_Category_Single.php?bk=<?php echo $user['book_id']; ?>"><?php echo $user['book_title']; ?></a></h3></div>
                                    </div>
                                </div>
                            </div>
                        <br>
						
								<div class="text-center"><span><b><?php echo $user['category_title']; ?>&nbsp;|&nbsp;<?php echo $user['title']; ?>&nbsp;|&nbsp;Rs.&nbsp;<?php echo $user['book_mrp']; ?></b></span></div>
								<?php 
									$book_id =  $user['book_id'];
                                                 $query1 = "SELECT * FROM `tbl_books_quantity_at_location` WHERE book_id = $book_id group by book_id";
                                                
                                                     ($fire1 = mysqli_query($mysqli, $query1)) or die("can not fetch the data from database." . mysqli_error($mysqli));
                                                
                                                     if (mysqli_num_rows($fire1) > 0) 
                                                	 {
                                                         while ($user1 = mysqli_fetch_assoc($fire1)) 
                                                		 {
                                                
                                                									$book_qty =  $user1['book_quantity'];

								if($book_qty >= 1){

									?>
										<button class="btn-sm btn-block" name="add_to_cart_category" style="background-color:#ff7b00; color:#fff">ADD TO CART</button><br>
										<?php
								}
								else{
										?>
										<a class="btn-sm btn-block disabled"   style="background-color:#6f7580; color:#fff; " readonly align="center">OUT OF STOCK</a><br>
										<?php
								}
		 }
	 }
										?>
										<input type="hidden" name="book_name" value="<?php echo $user['book_title']; ?>">
										<input type="hidden" name="₹" value="<?php echo $user['book_mrp']; ?>">
						</div>	
                    </div>
					</form>
                    <!--End single project item-->
					<?php }
         }
     }
     ?>
				</div>
				</div>
				</div>
            </div>
        </section>
        <!--End Main project area-->

<!--Footer-->
<?php include 'footer.php'; ?>
