<?php
include('includes/dbcon.php');
if((isset($_POST['newslatter'])))
	{
		$email = ($_POST['subscriber_email']);
			   $duplicate="SELECT subscriber_email FROM tbl_newsletter_subscriber WHERE 
							subscriber_email = '$email'";
				$result=mysqli_query($mysqli,$duplicate);
				$count=mysqli_num_rows($result);
				if($count > 0){
					$_SESSION['mailalready'] = 'You are already subscriber';
					$_SESSION['status'] = 'You are already subscriber';
					$_SESSION['status_code'] = 'error';

				}
				else
				{
				   $query = "INSERT INTO tbl_newsletter_subscriber(subscriber_email) VALUES('$email')";
				   $fire = mysqli_query($mysqli, $query) or die("can not insert into database." .mysqli_error($mysqli));
					if($fire)
					{
						$_SESSION['status'] = 'Your Subscription Is Added Successfully';
						$_SESSION['status_code'] = 'success';
					}
				}
	}
?> 

 <!--Start slogan area-->
        <section class="slogan-area">
			<div class="container">
			  <div class="d-flex justify-content-center">
				<div class="p-2 mt-1"><h3 style="color: #fff;">SIGN UP FOR NEWSLETTER</h3></div>
				<div class="p-2">
					<form method="POST" action="">
						<div class="input-group">
						  <input type="email" class="form-control" name="subscriber_email" placeholder="Your E-mail" required>
						  <div class="input-group-append">
							<button class="btn" name="newslatter" type="submit" style="background-color: #ff7b00; color: #fff;">Subscribe</button>
						  </div>
						</div>
					</form>
				</div>
			  </div>
			</div>
        </section>
        <!--End slogan area-->
		<!--Start footer area-->
		<div class="row">
		<div class="col-md-2" style="background: #fff;" align="center">
                    <!--Start single footer widget-->
                        <div class="single-footer-widget mt-4">
                            <div class="contact-info-box">
                                <div class="footer-logo ml-4">
                                    <a href="index.php">
                                        <img src="images/footer/footer-logo.png" alt="Awesome Logo">
                                    </a>
                                </div>
                            </div>
                        </div>
		</div>
		<div class="col-md-10">
        <footer class="footer-area" style="background-color:#fff; color:#000000">
           <div class="footer-shape-bg wow slideInRight"></div>
            <div class="container">
                <div class="row">
					<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
                        <div class="single-footer-widget marbtm50">
                            <div class="contact-info-box">
                                <ul>
                                    <li>
                                        <h6>Address</h6>
                                         <p>Registered office :<br>
											Shree Vimal Publishers Private Limited.<br>
											Shree Parnerkar Guruseva Mandal building,<br>
											Opposite Poornawad Bhavan,<br>
											Parner - 414302;<br> 
											District : Ahmednagar,<br>
											State : Maharashtra,<br>
											Bharat (India).</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
					<div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 ml-2">
                        <div class="single-footer-widget marbtm50">
                            <div class="contact-info-box">
                                <ul>
                                    <li>
                                        <h6>Contact No.</h6>
                                        <p>+91 - 9226885522<br>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
					<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 ml-2">
                        <div class="single-footer-widget marbtm50">
                            <div class="contact-info-box">
                                <ul>
                                    <li>
                                        <h6>Email Address</h6>
                                        <p>sales@shreevimalpublishers.com</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			</div>

        </footer>
			</div>
        <!--End footer area-->

        <!--Start footer bottom area-->
        <section class="footer-bottom-area style3" style="background-color:#000000;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <div class="copyright-text text-center">
							<p style="color:white;">Copyright&copy;|<b> Shree Vimal Publishers Private Limited 2021</b> | All Rights Reserved.</p>
                            <p><a href="#" target="_blank">JP CONSULTANCY SERVICES, SATARA</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End footer bottom area-->



    <div class="scroll-to-top-style2 scroll-to-target" data-target="html">
        <span class="fa fa-angle-up"></span>
    </div>



    <script src="js/jquery.js"></script>
    <script src="js/appear.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/isotope.js"></script>
    <script src="js/jquery.bootstrap-touchspin.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.enllax.min.js"></script>
    <script src="js/jquery.fancybox.js"></script>
    <script src="js/jquery.mixitup.min.js"></script>
    <script src="js/jquery.paroller.min.js"></script>
    <script src="js/owl.js"></script>
    <script src="js/validation.js"></script>
    <script src="js/wow.js"></script>

    <!---
<script src="js/gmaps.js"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyB2uu6KHbLc_y7fyAVA4dpqSVM4w9ZnnUw"></script>
<script src="js/mapapi.js"></script> 
--->
    <script src="js/map-helper.js"></script>

    <script src="assets/language-switcher/jquery.polyglot.language.switcher.js"></script>
    <script src="assets/timepicker/timePicker.js"></script>
    <script src="assets/html5lightbox/html5lightbox.js"></script>

    <!--Revolution Slider-->
    <script src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="js/main-slider-script.js"></script>

    <!-- thm custom script -->
    <script src="js/custom.js"></script>
      <style>

            .Swal-title {
              font-size: 10px;
              width: 400px;
            }

        </style>
    
   <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>		
			<?php
			if(isset($_SESSION['status']) && $_SESSION['status'] !='')
			{
				?>
				<script>
					Swal.fire({
						  title:  "<h6><?php echo  $_SESSION['status'];?></h6>",
						  // text: "You clicked the button!",
						  icon: "<?php  echo  $_SESSION['status_code'];?>",
						  button: "OK",
						  width: "400px",
						  allowOutsideClick: false,
						});
				</script>
				<?php
				unset($_SESSION['status']);
			}
		?>
</body>


<!-- index 06:41:43 GMT -->
</html>