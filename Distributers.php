<?php session_start(); ?>
<?php
	// Include Header Section
	include("header.php"); 
?>
<!--Start Our Distributors area-->
<section class="latest-blog-area">
    <div class="container inner-content">
        <div class="sec-title">
            <!--<p>News & Updates</p>-->
            <div class="title">our <span>Distributors</span></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xl-12">
            <div class="row">
                <?php 
										include('includes/dbcon.php'); 
											$query= "SELECT * FROM tbl_distributors
												left JOIN tbl_cities ON 
												(tbl_distributors.distributor_city_id=tbl_cities.id)";
	
										$fire = mysqli_query($mysqli,$query) or die("can not fetch the data from database." .mysqli_error($mysqli));
	
									if(mysqli_num_rows($fire)>0) { while($user = mysqli_fetch_assoc($fire)) { ?>

                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                    <div class="single-blog-post wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="text-holder">
                            <div class="post-date">
                                <h3 style="font-size:20px"><?php echo $user['name']?></h3>
                                <br />
                                <h6>
                                    <span><?php echo $user['distributor_name']?></span>
                                </h6>
                            </div>
                            <br />

                            <h6 class="blog-title"><a href="">ADDRESS</a></h6>
                            <div class="text">
                                <p><?php echo $user['distributor_address']?></p>
                            </div>
                            <h3 class="blog-title"><a href="">Contact No:</a></h3>
                            <div class="text">
                                <p><?php echo $user['distributor_contact_no']?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php 
										}
										}
					?>
            </div>
        </div>
    </div>
</section>
<!--End our distributors area-->

<?php
	//Include Footer Section
	include("footer.php");
?>