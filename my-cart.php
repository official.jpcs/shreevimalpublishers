<?php
session_start();
 ?>
<?php 
	//header
	include('header.php');
	
?>
<!--Start My Cart Area-->
        <section class="recently-project-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="sec-title float-left">
                            <div class="title">My Cart</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container mb-5">
			<div class="row">
			<div class="col-lg-9">
				<table class="table">
					<thead class="text-center">
						<tr>
							<th scope="col">Serial No.</th>
							<th scope="col">Book Name</th>
							<th scope="col">Book Price</th>
							<th scope="col">Quantity</th>
							<th scope="col">Total</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody class="text-center">
					<?php
					
						
						if(isset($_SESSION['cart']))
						{
							foreach($_SESSION['cart'] as $key => $value)
						{
							$sr=$key+1;
							echo"
								<tr>
									<td>$sr</td>
									<td>$value[book_name]</td>
									<td>$value[Price]<input type='hidden' class='iprice' value='$value[Price]'></td>
									<td><input type='number' class='text-center iquantity' onchange='subTotal()' value='$value[Quantity]' min='1' max='10'></td>
									<td class='itotal'>Total</td>
									<td>
										<form action='manage_cart.php' method='POST'>
											<button name='remove_book' class='btn btn-outline-danger btn-sm'>REMOVE</button>
											<input type='hidden' name='book_name' value='$value[book_name]'>
										</form>
									</td>
								</tr>
								";
						}
						}
					?>
					</tbody>
				</table>
			</div>
            
			<div class="col-lg-3">
				<div class="border bg-light rounded p-4" style="background-color: #C0C0C0;">
					<h4>Grand Total :</h4>
					<h5 class="text-right" id="gtotal"></h5>
					<br>
				</div>
			</div>
			</div><br>
			
			<?php
				if(isset($_SESSION['cart']) && count($_SESSION['cart'])>0)
				{
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="border bg-light rounded p-4">
						<h4>Customer Details :</h4><br>
							<form>
								<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Customer Name :</b></label>
										<input type="text" name="custmer_name" class="form-control" placeholder="Enter Your Name">
									</div>
									<div class="form-group">
										<label><b>Address Line 1 :</b></label>
										<input type="text" name="add1" class="form-control" placeholder="Enter Your Address 1">
									</div>
									<div class="form-group">
										<label><b>Address Line 2 :</b></label>
										<input type="text" name="add2" class="form-control" placeholder="Enter Your Address 2">
									</div>
									<div class="form-group">
										<label><b>Address Line 3(optional) :</b></label>
										<input type="text" name="add3" class="form-control" placeholder="Enter Your Address 3">
									</div>
									<div class="form-group">
										<label><b>Landmark(Optional) :</b></label>
										<input type="text" name="landmark" class="form-control" placeholder="Landmark(Optional)">
									</div>
									<div class="form-group">
										<label><b>Town :</b></label>
										<input type="text" name="add" class="form-control" placeholder="Town">
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label><b>Taluka :</b></label>
										<input type="text" name="Taluka" class="form-control" placeholder="Enter Your taluka Name">
									</div>
									<div class="form-group">
										<label><b>District:</b></label>
										<input type="text" name="District" class="form-control" placeholder="Enter Your District Name">
									</div>
									<div class="form-group">
										<label><b>PIN Code :</b></label>
										<input type="number" name="add" class="form-control" placeholder="Enter Your PIN Code">
									</div>
									<div class="form-group">
										<label><b>Email :</b></label>
										<input type="email" name="add" class="form-control" placeholder="Enter Your Email Id">
									</div>
									<div class="form-group">
										<label><b>Contact Number :</b></label>
										<input type="text" name="contact" class="form-control" placeholder="Enter Your Contact Number">
									</div>
									<div class="form-check">
										<input type="checkbox" class="form-check-input" id="exampleCheck1">
										<label class="form-check-label"><b>Are You Guruseva Mandal Member ?</b></label>
									</div>
								</div><br>
								<div class="col-md-12" align="center">
									<img src="images/icon/razer_button.png" href="" alt="razer pay"/>&nbsp;&nbsp;
									<button align="center" class="btn btn-info">RESET</button>
								</div>
								</div>
							</form>
					</div>
					
				</div>
			</div>
			<?php
				}
			?>
			</div>
			
			
        </section>
<!--End MY Cart Area-->
<script>
	var gt=0;
	var iprice=document.getElementsByClassName('iprice');
	var iquantity=document.getElementsByClassName('iquantity');
	var itotal=document.getElementsByClassName('itotal');
	var gtotal=document.getElementById('gtotal');
	
	function subTotal()
	{ 
		gt=0;
		for(i=0;i<iprice.length;i++)
		{
			itotal[i].innerText=(iprice[i].value)*(iquantity[i].value);
			gt=gt+(iprice[i].value)*(iquantity[i].value);
		}
		gtotal.innerText=gt;
	}
	
	subTotal();

</script>

<?php
	//footer
	include('footer.php');
?>