<?php
session_start();
 ?>
<!--Header-->
<?php include('header.php'); ?>

<!--css for slider-->

<style>
    * {
        box-sizing: border-box;
    }
    .mySlides1,
    .mySlides2 {
        display: none;
    }
    img {
        vertical-align: middle;
    }

    /* Slideshow container */
    .slideshow-container {
        max-width: 1000px;
        position: relative;
        margin: auto;
    }

    /* Next & previous buttons */
    .prev,
    .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        padding: 16px;
        margin-top: -22px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
    }

    /* Position the "next button" to the right */
    .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a grey background color */
    .prev:hover,
    .next:hover {
        background-color: #f1f1f1;
        color: black;
    }
</style>
<?php
						//connection
						include('includes/dbcon.php'); 
			
						if(isset($_GET['bk']))
					{
						$id=$_GET['bk'];
						$query = "SELECT * from tbl_books INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id)
                        INNER JOIN tbl_authors_publishers ON
                        (tbl_books.author_id=tbl_authors_publishers.id)
                        INNER JOIN tbl_languages ON
                        (tbl_books.book_language=tbl_languages.language_id) WHERE tbl_books.book_id=$id" ;
						$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
						if(mysqli_num_rows($fire)>0) { while($user = mysqli_fetch_assoc($fire)) { ?>

<!--Start Project Info Area-->
<section class="project-info-area">
    <form action="manage_cart.php" method="POST">
        <div class="row">
            <div class="col-xl-6">
                <div class="project-info-content">
                    <div>
                        <h5>Book Info</h5>
                    </div>
                    <div class="inner-content">
                        <ul>
                            <li>
                                <div class="title">
                                    <h6><?php echo $user['book_title']?></h6>
                                </div>
                            </li>
                            <li>
                                <!-- <div class="icon">
                                            <span class="icon-ruler"></span>
                                        </div> -->
                                <div class="title">
                                    <h6>Author Name</h6>
                                    <span><?php echo $user['name']?></span>
                                </div>
                            </li>
                            <li>
                                <!--<div class="icon">
                                            <span class="icon-calendar"></span>
                                        </div>-->
                                <div class="title">
                                    <h6>Book Category</h6>
                                    <span><?php echo $user['category_title']?></span>
                                </div>
                            </li>
                            <li>
                                <!--<div class="icon">
                                            <span class="icon-price"></span>
                                        </div>-->
                                <div class="title">
                                    <h6>Publisher Name</h6>
                                    <span><?php echo $user['name']?></span>
                                </div>
                            </li>
                            <li>
                                <!--<div class="icon">
                                            <span class="icon-group"></span>
                                        </div>-->
                                <div class="title">
                                    <h6>No Of Pages</h6>
                                    <span><?php echo $user['no_of_pages']?></span>
                                </div>
                            </li>
                            <li>
                                <!--<div class="icon">
                                            <span class="icon-group"></span>
                                        </div>-->
                                <div class="title">
                                    <h6>Book Language</h6>
                                    <span><?php echo $user['title']?></span>
                                </div>
                            </li>
                            <li>
                                <!--<div class="icon">
                                            <span class="icon-group"></span>
                                        </div>-->
                                <div class="title">
                                    <h6>₹ &nbsp;<?php echo $user['book_mrp']?></h6>
                                </div>
                            </li>
                            <li>
                                <!--<div class="icon">
                                            <span class="icon-group"></span>
                                        </div>-->
                                <!--<div class="title">
                                            <h3>Quantity</h3><br>
											<input type="number" class="form-control" name="quantity" value="1">
                                        </div>-->
                            </li>
                           	<?php 
									$book_id =  $user['book_id'];
                                     $query1 = "SELECT * FROM `tbl_books_quantity_at_location` WHERE book_id = $book_id group by book_id";
                                    
                                         ($fire1 = mysqli_query($mysqli, $query1)) or die("can not fetch the data from database." . mysqli_error($mysqli));
                                    
                                         if (mysqli_num_rows($fire1) > 0) 
                                    	 {
                                             while ($user1 = mysqli_fetch_assoc($fire1)) 
                                    		 {
                                    
                                    									$book_qty =  $user1['book_quantity'];

								if($book_qty >= 1){

									?>
										<button class="btn-sm btn-block" name="add_to_cart_category" style="background-color:#ff7b00; color:#fff">ADD TO CART</button><br>
										<?php
								}
								else{
										?>
										<a class="btn-sm btn-block disabled"   style="background-color:#6f7580; color:#fff; " readonly align="center">OUT OF STOCK</a><br>
										<?php
								}
		 }
	 }
										?>
                            <input type="hidden" name="book_name" value="<?php echo $user['book_title']?>" />
                            <input type="hidden" name="₹" value="<?php echo $user['book_mrp']?>" />
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="slideshow-container">
                    <?php
						//connection
					
						$query1 = "SELECT DISTINCT * FROM tbl_book_files WHERE tbl_book_files.book_id=$id  ORDER BY `tbl_book_files`.`position` ASC";
						$fire1 = mysqli_query($mysqli,$query1) or die("can not fetch the data." .mysqli_error($mysqli));
						
						if(mysqli_num_rows($fire1)>0) { while($user1 = mysqli_fetch_assoc($fire1)) { ?>
                    <div class="mySlides1" align="center">
                        <img src="admin/images/book_thumbnails/other_images/<?php echo $user1['input_file']?>" style="width: 70%;" />
                    </div>

                    <?php
					 
						 }							 
						}
						else
						{
							?>

                    <div class="mySlides1">
                        <img src="admin/images/book_thumbnails/default.png" style="width: 100%;" />
                    </div>

                    <?php
							
						}
						//connection
					
					?>
                    <a class="prev" onclick="plusSlides(-1, 0)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1, 0)">&#10095;</a>
                </div>
            </div>
        </div>
    </form>
<div class="container">
<div class="project-description-content">
    <div class="sec-title">
        <!-- <div class="title">similique sunt<br> qui officia deserunt</div> -->
    </div>
    <div class="inner-content">
        <p class="text-justify"><?php echo $user['book_description']?></p>
    </div>
</div>
</div>
<br />
<div class="project-description-content">
    <div class="sec-title">
        <h3 style="color: #00a2ff;">Related Books</h3>
        <!-- <div class="title">similique sunt<br> qui officia deserunt</div> -->
    </div>
    <div class="inner-content">
        <div class="container box">
            <?php
          ?>

            <div class="row">
                <div class="col-md-3">
                    <?php
						//connection
						include('includes/dbcon.php'); 
			
		  $realtedbook = $user['related_books'];
		  
			$colorsArray = explode(",", $realtedbook);
			$arr = $colorsArray[0];
						$query = "SELECT * FROM `tbl_books` INNER JOIN tbl_languages ON 
						(tbl_books.book_language=tbl_languages.language_id) INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id) WHERE tbl_books.book_id='$arr'";
						$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
						if(mysqli_num_rows($fire)>0) { while($user = mysqli_fetch_assoc($fire)) { ?>
                    <!--Start single project item-->
                    <form action="manage_cart.php" method="POST">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 filter-item contem ret">
                            <div class="single-project-style4">
                                <div class="img-holder">
                                    <div class="inner">
                                        <img src="admin/images/book_thumbnails/<?php echo $user['book_thumbnail']?>" alt="Awesome Image" />
                                    </div>
                                    <div class="overlay-content">
                                        <div class="title">
                                            <div>
                                                <h3>
                                                    <a href="Book_Category_Single.php?bk=<?php echo $user['book_id']?>"><?php echo $user['book_title']?></a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="text-center">
                                    <span>
                                        <b><?php echo $user['category_title']?>&nbsp;|&nbsp;<?php echo $user['title']?>&nbsp;|&nbsp;Rs.&nbsp;<?php echo $user['book_mrp']?></b>
                                    </span>
                                </div>
                                <button class="btn-sm btn-block" id="cat" name="add_to_cart_category" style="background-color: #ff7b00; color: #fff;">ADD TO CART</button>
                                <input type="hidden" name="book_name" value="<?php echo $user['book_title']?>" />
                                <input type="hidden" name="₹" value="<?php echo $user['book_mrp']?>" />
                            </div>
                        </div>
                    </form>
                    <!--End single project item-->
                    <?php
										}
										}
					
					
					?></div>
                <div class="col-md-3">
                    <?php
						//connection 
			
			$arr1 = $colorsArray[1];
						$query = "SELECT * FROM `tbl_books` INNER JOIN tbl_languages ON 
						(tbl_books.book_language=tbl_languages.language_id) INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id) WHERE tbl_books.book_id='$arr1'";
						$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
						if(mysqli_num_rows($fire)>0) { while($user = mysqli_fetch_assoc($fire)) { ?>
                    <!--Start single project item-->
                    <form action="manage_cart.php" method="POST">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 filter-item contem ret">
                            <div class="single-project-style4">
                                <div class="img-holder">
                                    <div class="inner">
                                        <img src="admin/images/book_thumbnails/<?php echo $user['book_thumbnail']?>" alt="Awesome Image" />
                                    </div>
                                    <div class="overlay-content">
                                        <div class="title">
                                            <div>
                                                <h3>
                                                    <a href="Book_Category_Single.php?bk=<?php echo $user['book_id']?>"><?php echo $user['book_title']?></a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="text-center">
                                    <span>
                                        <b><?php echo $user['category_title']?>&nbsp;|&nbsp;<?php echo $user['title']?>&nbsp;|&nbsp;Rs.&nbsp;<?php echo $user['book_mrp']?></b>
                                    </span>
                                </div>
                                <button class="btn-sm btn-block" id="cat" name="add_to_cart_category" style="background-color: #ff7b00; color: #fff;">ADD TO CART</button>
                                <input type="hidden" name="book_name" value="<?php echo $user['book_title']?>" />
                                <input type="hidden" name="₹" value="<?php echo $user['book_mrp']?>" />
                            </div>
                        </div>
                    </form>
                    <!--End single project item-->
                    <?php
										}
										}
					
					
					?></div>
                <div class="col-md-3">
                    <?php
						//connection 
			
			$arr2 = $colorsArray[2];
						$query = "SELECT * FROM `tbl_books` INNER JOIN tbl_languages ON 
						(tbl_books.book_language=tbl_languages.language_id) INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id) WHERE tbl_books.book_id='$arr2'";
						$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
						if(mysqli_num_rows($fire)>0) { while($user = mysqli_fetch_assoc($fire)) { ?>
                    <!--Start single project item-->
                    <form action="manage_cart.php" method="POST">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 filter-item contem ret">
                            <div class="single-project-style4">
                                <div class="img-holder">
                                    <div class="inner">
                                        <img src="admin/images/book_thumbnails/<?php echo $user['book_thumbnail']?>" alt="Awesome Image" />
                                    </div>
                                    <div class="overlay-content">
                                        <div class="title">
                                            <div>
                                                <h3>
                                                    <a href="Book_Category_Single.php?bk=<?php echo $user['book_id']?>"><?php echo $user['book_title']?></a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="text-center">
                                    <span>
                                        <b><?php echo $user['category_title']?>&nbsp;|&nbsp;<?php echo $user['title']?>&nbsp;|&nbsp;Rs.&nbsp;<?php echo $user['book_mrp']?></b>
                                    </span>
                                </div>
                                <button class="btn-sm btn-block" id="cat" name="add_to_cart_category" style="background-color: #ff7b00; color: #fff;">ADD TO CART</button>
                                <input type="hidden" name="book_name" value="<?php echo $user['book_title']?>" />
                                <input type="hidden" name="₹" value="<?php echo $user['book_mrp']?>" />
                            </div>
                        </div>
                    </form>
                    <!--End single project item-->
                    <?php
										}
										}
					
					
					?></div>
                <div class="col-md-3">
                    <?php
						//connection 
			
			$arr3 = $colorsArray[3];
						$query = "SELECT * FROM `tbl_books` INNER JOIN tbl_languages ON 
						(tbl_books.book_language=tbl_languages.language_id) INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id) WHERE tbl_books.book_id='$arr3'";
						$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
						if(mysqli_num_rows($fire)>0) { while($user = mysqli_fetch_assoc($fire)) { ?>
                    <!--Start single project item-->
                    <form action="manage_cart.php" method="POST">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 filter-item contem ret">
                            <div class="single-project-style4">
                                <div class="img-holder">
                                    <div class="inner">
                                        <img src="admin/images/book_thumbnails/<?php echo $user['book_thumbnail']?>" alt="Awesome Image" />
                                    </div>
                                    <div class="overlay-content">
                                        <div class="title">
                                            <div>
                                                <h3>
                                                    <a href="Book_Category_Single.php?bk=<?php echo $user['book_id']?>"><?php echo $user['book_title']?></a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="text-center">
                                    <span>
                                        <b><?php echo $user['category_title']?>&nbsp;|&nbsp;<?php echo $user['title']?>&nbsp;|&nbsp;Rs.&nbsp;<?php echo $user['book_mrp']?></b>
                                    </span>
                                </div>
                                <button class="btn-sm btn-block" id="cat" name="add_to_cart_category" style="background-color: #ff7b00; color: #fff;">ADD TO CART</button>
                                <input type="hidden" name="book_name" value="<?php echo $user['book_title']?>" />
                                <input type="hidden" name="₹" value="<?php echo $user['book_mrp']?>" />
                            </div>
                        </div>
                    </form>
                    <!--End single project item-->
                    <?php
										}
										}
					
					
					?></div>
                <div class="col-md-3">
                    <?php
						//connection 
			
			$arr4 = $colorsArray[4];
						$query = "SELECT * FROM `tbl_books` INNER JOIN tbl_languages ON 
						(tbl_books.book_language=tbl_languages.language_id) INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id) WHERE tbl_books.book_id='$arr4'";
						$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
						if(mysqli_num_rows($fire)>0) { while($user = mysqli_fetch_assoc($fire)) { ?>
                    <!--Start single project item-->
                    <form action="manage_cart.php" method="POST">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 filter-item contem ret">
                            <div class="single-project-style4">
                                <div class="img-holder">
                                    <div class="inner">
                                        <img src="admin/images/book_thumbnails/<?php echo $user['book_thumbnail']?>" alt="Awesome Image" />
                                    </div>
                                    <div class="overlay-content">
                                        <div class="title">
                                            <div>
                                                <h3>
                                                    <a href="Book_Category_Single.php?bk=<?php echo $user['book_id']?>"><?php echo $user['book_title']?></a>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />

                                <div class="text-center">
                                    <span>
                                        <b><?php echo $user['category_title']?>&nbsp;|&nbsp;<?php echo $user['title']?>&nbsp;|&nbsp;Rs.&nbsp;<?php echo $user['book_mrp']?></b>
                                    </span>
                                </div>
                                <button class="btn-sm btn-block" id="cat" name="add_to_cart_category" style="background-color: #ff7b00; color: #fff;">ADD TO CART</button>
                                <input type="hidden" name="book_name" value="<?php echo $user['book_title']?>" />
                                <input type="hidden" name="₹" value="<?php echo $user['book_mrp']?>" />
                            </div>
                        </div>
                    </form>
                    <!--End single project item-->
                    <?php
										}
										}
					
					
					?></div>
            </div>
            <br />
        </div>
    </div>
</div>
</section>
<!--End Project Info Area-->
<?php
										}
										}
					}
					
					?>
<script>
    var slideIndex = [1, 1];
    var slideId = ["mySlides1"];
    showSlides(1, 0);
    showSlides(1, 1);

    function plusSlides(n, no) {
        showSlides((slideIndex[no] += n), no);
    }

    function showSlides(n, no) {
        var i;
        var x = document.getElementsByClassName(slideId[no]);
        if (n > x.length) {
            slideIndex[no] = 1;
        }
        if (n < 1) {
            slideIndex[no] = x.length;
        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[slideIndex[no] - 1].style.display = "block";
    }
</script>

<!--footer-->
<?php include('footer.php'); ?>
