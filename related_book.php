	<div class="related-wapper">
	<div class="box product-related clearfix">	
	
	<div class="box-heading"><span>Related Book <?php echo " (".count($relatedBooksList)." )";   $total_grid=ceil(count($relatedBooksList)/4);?></span></div>
	
	<?php for($i=0;$i<$total_grid;$i++) { ?>
		<div data-interval="<?=$i?>" class="slide product-grid" id="related">
		<?php } ?>
		
		
		<div class="carousel-controls">
		<a data-slide="prev" href="#related" class="carousel-control left"><i class="fa fa-angle-left"></i></a>
		<a data-slide="next" href="#related" class="carousel-control right "><i class="fa fa-angle-right"></i></a>
		</div>
		
		<div class="products-block carousel-inner clearfix ">
		
		<?php $i=0; for($k=0;$k<$total_grid;$k++) { ?>
		
		
				<div class="item <?php if($k==0) echo "active";?>">
                      <div class="row products-row">
					  
					  <?php for($j=1;$i<count($relatedBooksList);$i++,$j++){  
					  if($j>3) break;
					  ?>
					  
                        <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12 product-col">
                          <div class="product-block">
                            <div class="image">
							<a class="img" title="Grouped product" href="book_detail.php?id=<?=$relatedBooksList[$i]['book_id'];?>">
							
							<!--------------- Book Image-------------->
							
							<img class="img-responsive" src="<?php if($relatedBooksList[$i]['book_thumbnail']==BOOK_THUMBNAIL_FOLDER_HTTP) echo BOOK_THUMBNAIL_FOLDER_HTTP."default.png"; else echo $relatedBooksList[$i]['book_thumbnail']; ?>" title="Grouped product" alt="Grouped product" />
                              <div class="effect-hover"></div>
                              </a>
                              
                            </div>
                            <div class="product-meta">
                              <div class="left">
							  
							   <!--------------- Book Title-------------->
							   
                                <h3 class="name">
								<a href="book_detail.php?id=<?=$relatedBooksList[$i]['book_id'];?>">

									<?php
									 if(strlen($relatedBooksList[$i]['book_title']) >28) 
									 {
										echo substr($relatedBooksList[$i]['book_title'],0,28)."..";
									}
									else
									{
										echo $relatedBooksList[$i]['book_title'];
									}	
									?>
									
								</a>
								</h3>
								
								
					            <div class="rating">
					               <?php 
					               
					               echo $result_booksMostSalable_list[$i]['category_title']; ?>
					            </div>
                               
                                  <!---- Book MRP Price----->
								  
                                <div class="price clearfix"> <span class="special-price">&#2352; <?= $relatedBooksList[$i]['book_mrp'].".00";?></span>
                                </div>
								
								 <!--------------- Book View Detail Button-------------->
								 
					            <div class="wrap-hover">
					            <?php

								$available_qty= $relatedBooksList[$i]['quantity'];
					           		
					           	if($available_qty > OUT_OF_STOCK_AFTER_QTY)
					            {
					            	?>
					               

					               <button  class="btn btn-shopping-cart btn-outline-default add-to-cart" cat_id="<?php echo $_REQUEST['catid'];?>" book_id="<?php echo $relatedBooksList[$i]['book_id'];?>">
					               <span>Add to Cart</span>
					               </button>
					            <?php
					            }
					            else
					            {
					             ?>	
				           			<span class="out_of_stock">Out Of Stock</span>

					            <?php 
					            }
					            ?>   
					            </div>
								
								
                              </div>
                            </div>
                          </div>
                        </div>
						
						<?php } ?>
						
					
                      </div>
                    </div>
		<?php } ?>
		
		</div>
		<?php for($i=0;$i<$total_grid;$i++) { ?>
		</div>
		<?php } ?>
		
	</div>	
	</div>	
