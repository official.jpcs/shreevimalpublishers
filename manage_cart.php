<?php

	session_start();
	if($_SERVER["REQUEST_METHOD"]=="POST")
	{
		if(isset($_POST['add_to_cart']))
		{	
			if(isset($_SESSION['cart']))
			{
				
				$mybooks=array_column($_SESSION['cart'],'book_name');
				if(in_array($_POST['book_name'],$mybooks))
				{
						$_SESSION['status'] = 'Book is already added. If you want to purchase same title then go to cart and increase the title quantity.';
						$_SESSION['status_code'] = 'warning';
						echo"<script>
						  window.history.back();
						</script>";

				}
				else
				{
				$count=count($_SESSION['cart']);
				$_SESSION['cart'][$count]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";
				}
			}
			else
			{
				$_SESSION['cart'][0]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";

			}
		}
		if(isset($_POST['add_to_cartup']))
		{	
			if(isset($_SESSION['cart']))
			{
				
				$mybooks=array_column($_SESSION['cart'],'book_name');
				if(in_array($_POST['book_name'],$mybooks))
				{
					$_SESSION['status'] = 'Book is already added. If you want to purchase same title then go to cart and increase the title quantity.';
						$_SESSION['status_code'] = 'warning';
						echo"<script>
						  window.history.back();
						</script>";

				}
				else
				{
				$count=count($_SESSION['cart']);
				$_SESSION['cart'][$count]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";
				}
			}
			else
			{
				$_SESSION['cart'][0]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";

			}
		}
		if(isset($_POST['add_to_cart_category']))
		{	
			if(isset($_SESSION['cart']))
			{
				
				$mybooks=array_column($_SESSION['cart'],'book_name');
				if(in_array($_POST['book_name'],$mybooks))
				{
					$_SESSION['status'] = 'Book is already added. If you want to purchase same title then go to cart and increase the title quantity.';
						$_SESSION['status_code'] = 'warning';
						echo"<script>
						  window.history.back();
						</script>";

				}
				else
				{
				$count=count($_SESSION['cart']);
				$_SESSION['cart'][$count]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";
				}
			}
			else
			{
				$_SESSION['cart'][0]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";

			}
		}
		if(isset($_POST['add_to_cart3']))
		{	
			if(isset($_SESSION['cart']))
			{
				
				$mybooks=array_column($_SESSION['cart'],'book_name');
				if(in_array($_POST['book_name'],$mybooks))
				{
					$_SESSION['status'] = 'Book is already added. If you want to purchase same title then go to cart and increase the title quantity.';
						$_SESSION['status_code'] = 'warning';
						echo"<script>
						  window.history.back();
						</script>";

				}
				else
				{
				$count=count($_SESSION['cart']);
				$_SESSION['cart'][$count]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";
				}
			}
			else
			{
				$_SESSION['cart'][0]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						echo"<script>
						  window.history.back();
						</script>";

			}
		}
		if(isset($_POST['add_to_cart_search']))
		{	
			if(isset($_SESSION['cart']))
			{
				
				$mybooks=array_column($_SESSION['cart'],'book_name');
				if(in_array($_POST['book_name'],$mybooks))
				{
					$_SESSION['status'] = 'Book is already added. If you want to purchase same title then go to cart and increase the title quantity.';
						$_SESSION['status_code'] = 'warning';
						header('Location: search.php');
				}
				else
				{
				$count=count($_SESSION['cart']);
				$_SESSION['cart'][$count]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						header('Location: search.php');
				}
			}
			else
			{
				$_SESSION['cart'][0]=array('book_name'=>$_POST['book_name'],'Price'=>$_POST['₹'],'Quantity'=>1);
				$_SESSION['status'] = 'Book is Added Successfully';
						$_SESSION['status_code'] = 'success';
						header('Location: search.php');
			}
		}
		if(isset($_POST['remove_book']))
		{ 
			foreach($_SESSION['cart'] as $key => $value)
			{	
				if($value['book_name']==$_POST['book_name'])
				{
				unset($_SESSION['cart'][$key]);
				$_SESSION['cart']=array_values($_SESSION['cart']);
				
					$_SESSION['status'] = 'Book Removed Successfully';
						$_SESSION['status_code'] = 'error';
						echo"<script>
						  window.history.back();
						</script>";
				}
			}
		}
	}
 ?>