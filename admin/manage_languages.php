<?php 
/***************************************************************
 *  File Name : Manage Book Languages
 *  Created Date: 19/05/2016
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Master Content";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_languages")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");



//Include Controller Section
include(CONTROLLER_PATH."LanguageController.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';


// Include Header Section
include(NAVIGATION_FILE . "header.php");


//Include View Section
include( VIEW_PATH."manage_languages_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
