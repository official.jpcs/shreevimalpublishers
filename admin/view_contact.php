<?php 
/***************************************************************
 *  File Name : Manage Book Categories
 *  Created Date: 27/02/2015
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Contact Us Massage";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("tbl_contact")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

//Include Controller Section
include(CONTROLLER_PATH."ContactController.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';


// Include Header Section
include(NAVIGATION_FILE . "header.php");

//print_r($result_contact_list);
//Include View Section
include( VIEW_PATH."view_contact_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
