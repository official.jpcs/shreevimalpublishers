<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($setting->setting_id!="") {echo "Manage Settings";}else{echo "Manage Settings";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Settings</a></li>
  </ol>
</section>




<!-- Main content -->
	<section class="content">
	
			<form class="form-vertical" id="frm_manage_setting" action="manage_setting.php" method="post">
						<input type="hidden" name="step" id="step" value="<?php if($setting->setting_id!="") {echo "update_setting";}else{echo "add_new_setting";}?>" />
						<input   name="setting_id" id="setting_id" type="hidden" value="<?=$setting->setting_id;?>">
						
						<!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  
						<div class="col-md-12">
							<div class="form-group"> 
								<label for="Admin_name">Do Not Display Books having count less than</label>
									<input required class="form-control"  name="books_in_stock_below_count" id="books_in_stock_below_count" type="number" min="1" max="100" value="<?=$setting->books_in_stock_below_count; ?>"> 
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group"> 
								<label for="Admin_name">Email Addrees For Communication</label>
									<input  required class="form-control" maxlength="50" name="email_id_sending_email" id="email_id_sending_email" type="email" value="<?=$setting->email_id_sending_email; ?>"> 
							</div>
						</div>
						
						
						  </div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer">
					<button class="btn" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button></div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
						          
	</section>
</div>
<!-- /.content-wrapper -->








