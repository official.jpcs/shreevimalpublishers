<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($step=="edit_info") {echo "Update Distributor";}else{echo "Add New Distributor";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Distributor</a></li>
  </ol>
</section>





<!-- Main content -->
	<section class="content">
		<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
		<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_distributor";}else{echo "add_distributor";}?>" />
		<input name="distributor_id" id="distributor_id" type="hidden" value="<?=$distributors->distributor_id;?>">
			  <!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
								  
								<div class="col-md-6">
									<div class="form-group div_book_name"> 
										<label for="Admin_first_name">Distributor Name</label> 
										<input placeholder="" class="form-control" maxlength="255" name="distributor_name" id="distributor_name" type="text" value="<?=$distributors->distributor_name;?>">              
										<span class="help-block error book_name" style="display: none">Please provide Distributor Name.</span>  
									</div>
									</div>
									
									<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group"> 
										<label for="Admin_first_name">Contact Number</label> 
										<input placeholder="" class="form-control" maxlength="15" name="distributor_contact_no" id="distributor_contact_no" type="text" value="<?=$distributors->distributor_contact_no;?>">              
									</div>
									</div>
									<div class="clear"></div>
		
		
		
								<div class="col-md-6">
									<div class="form-group"> 
										<label for="Admin_first_name"> Email ID</label> 
									    <input placeholder="" class="form-control" maxlength="150" name="distributor_emailid" id="distributor_emailid" type="text" value="<?=$distributors->distributor_emailid;?>">              
									</div>
									</div>
									<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group"> 
										<label for="Admin_first_name">Website</label> 
									    <input placeholder="" class="form-control" maxlength="150" name="distributor_website" id="distributor_website" type="text" value="<?=$distributors->distributor_website;?>">              
									</div>
									
								</div>
								
								
								<div class="clear"></div>
								
								<div class="col-md-6">
									<div class="form-group div_book_edition">
									<label for="Admin_first_name">State</label>
									<select class="form-control" name="distributor_state_id" id="state_list">
									<option value="0">--------- Select State ------</option>
									<?=$states_list_show;?>
									</select>      
										<span class="help-block error book_edition" style="display: none">Please provide State.</span> 
									</div>
									</div>
									<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group div_book_mrp"> 
										<label for="Admin_first_name">City </label> 
										 <div class="city_list_div">
										<select name="distributor_city_id" id="city" class="form-control">
										<option value="">--------- Select City ---------</option>
										
										</select></div>
										<span class="help-block error book_mrp" style="display: none">Please provide City.</span> 
									</div>
									</div>
									<div class="clear"></div>
		
								<div class="col-md-6">
									<div class="form-group"> 
										<label for="Admin_first_name">Address</label>
										<textarea class="form-control" cols="50" rows="5" name="distributor_address" id="distributor_address"><?=$distributors->distributor_address;?></textarea>
										 <span class="help-block error book_mrp" style="display: none">Please provide Address.</span>              
									</div>
									</div>
									
									
								<div class="clear"></div>
								<div class="col-md-6">
									<div class="form-group"> 
										<label for="Admin_first_name">Zip Code</label> 
									    <input placeholder="" class="form-control" maxlength="150" name="zip_code" id="zip_code" type="text" value="<?=$distributors->zip_code;?>">              
									</div>
									</div>
									
								<div class="clear"></div>							  

				  </div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer  text-right">
					<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
					<button class="btn" type="reset" name="yt1">Reset</button>
					<a href="manage_distributors.php" class="btn btn-primary">Cancel</a>
					</div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
	</section>
</div>
<!-- /.content-wrapper -->


