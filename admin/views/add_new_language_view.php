<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($step=="edit_info") {echo "Update Language";}else{echo "Add New Language";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Language</a></li>
  </ol>
</section>




<!-- Main content -->
	<section class="content">
	
			<form class="form-vertical" id="frm_new_category" action="add_edit_language.php" method="post">
						<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_language";}else{echo "add_language";}?>" />
						<input   name="language_id" id="language_id" type="hidden" value="<?=$language_id;?>">
						
						<!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  
						<div class="col-md-12">
						<div class="form-group"> 
							<label for="Admin_name">Language Title</label>
							<input class="form-control" maxlength="50" name="language_title" id="language_title" type="text" required value="<?=$laguages->title; ?>"> 
						</div>
					</div>
						
						
						  </div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer text-right">
					<button class="btn btn-primary" type="submit" name="yt0">Submit</button>     <button class="btn" type="reset" name="yt1">Reset</button>
					<a href="manage_languages.php" class="btn btn-primary">Cancel</a>
				</div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
						          
	</section>
</div>
<!-- /.content-wrapper -->


