<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  
	<div style="float:left" >
		<h4> Edit Book >> <?php echo $result_book_info[0]['book_title']; ?></h4>
	</div>
	 <?php $book_id=$_POST['update_id'];
		//echo $pages=$_POST['pages'];?>
	<div style="float:right" >
		<small><a class="btn btn-primary pull-right" href="manage_books.php?pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>">Manage Book List</a></small>
	</div>
  
 
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
<div id="exTab3" class="container col-xs-12">	
<?php
	$tab_edit ="general_info";
	include("includes/book_edit_tab.php");
?>

<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
<input type="hidden" name="step" id="step" value="update_generalinfo" />
<input name="book_id" id="book_id" type="hidden" value="<?=$book_id;?>">
<input name="pages" id="pages" type="hidden" value="<?php if(isset($_POST['pages'])) echo $_POST['pages']; else echo '1';?>">

		
		
<div class="tab-content clearfix">
	  <div class="tab-pane active" id="general_info">
          
		  <div class="box">
		 <div class="box-body">
		  
		  
					<!--Start Of Section1 : General Information-->
					 <div class="panel panel-default">
						  <div class="panel-heading">
								<div class="w40L">
									<h4 class="panel-title">General Information</h4>
								</div>
								
								
						  </div>
						  <div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
											  
								<div class="col-md-6 ">
									
									<div class="form-group div_book_name"> 
										<label for="Admin_first_name">Book Name</label> 
										<input placeholder="" class="form-control" maxlength="255" name="book_name" id="book_name" type="text" value="<?=$result_book_info[0]['book_title']; ?>">              
										<span class="help-block error book_name" style="display: none">Please provide Book Name.</span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group div_book_name"> 
										<label for="Admin_first_name">Book Name(in Marathi)</label> 
										<input placeholder="" class="form-control" maxlength="255" name="book_name_mr" id="book_name_mr" type="text" value="<?=$result_book_info[0]['book_title_mr']; ?>">              
										<span class="help-block error book_name_mr" style="display: none">Please provide Book Name.</span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_author"> 
										<label for="Admin_first_name">Author</label> 
										
										<select name="author" id="author" class="form-control"> 
										 <?php
										  $tbl="tbl_authors_publishers";
										  $val ="id";
										  $show="name";
										  $status="is_deleted";
										  $clause =" `is_deleted` ='0' AND `user_type`='2'";
										  $FirstNode="------------------ Select Author -----------------";
										  
										  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('id',$result_book_info[0]['author_id']), $print = 0, $status, $FirstNode,$prefix='');
										 ?>
										</select>    
										<span class="help-block error author" style="display: none">Please select Book Author.</span>        
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Compiled by</label> 
										<input placeholder="" class="form-control" maxlength="150" name="compile_by" id="compile_by" type="text" value="<?=$result_book_info[0]['compile_by']; ?>">              
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Translated by</label> 
									    <input placeholder="" class="form-control" maxlength="150" name="translate_by" id="translate_by" type="text" value="<?=$result_book_info[0]['translated_by']; ?>">              
									</div>
									
								</div>
								
								<div class="col-md-6 ">
								<div class="clear"></div>
		
									<div class="form-group div_book_language"> 
										<label for="Admin_first_name">Language <?= $result_book_info[0]['book_language']; ?></label>
										&nbsp;
										<a data-target="#saleModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
											<i class="fa fa-plus" aria-hidden="true"></i>
										</a>																						
										
										<select name="book_language" id="book_language" class="form-control"> 
										 <?php
										  $tbl="tbl_languages";
										  $val ="language_id";
										  $show="title";
										  $status="is_deleted";
										  $clause =" `is_deleted` ='0'";
										  $FirstNode="------------------ Select Language -----------------";
										  $order_by = "language_id ASC" ;
										  
										  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('id',$result_book_info[0]['sel_language_id']), $print = 0, $status, $FirstNode,$prefix='',$order_by);
										 ?>

										</select>  
										<span class="help-block error book_language" style="display: none">Please select Book Language.</span>         
									</div>
										
									<div class="clear"></div>
		
									<div class="form-group div_book_mrp"> 
										<label for="Admin_first_name">MRP </label> Rs. 
										<input placeholder="" class="form-control" maxlength="8" name="book_mrp" id="book_mrp" type="text" value="<?=$result_book_info[0]['book_mrp']; ?>">              
										<span class="help-block error book_mrp" style="display: none">Please provide Book MRP Price.</span> 
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_book_edition"> 
										<label for="Admin_first_name">Edition</label> 
										<input placeholder="" class="form-control" maxlength="150" name="book_edition" id="book_edition" type="text" value="<?=$result_book_info[0]['book_edition']; ?>">              
										<span class="help-block error book_edition" style="display: none">Please provide Book Edition.</span> 
									</div>
									<div class="clear"></div>

									<!-- <div class="form-group col-md-6"> 
										<label for="Admin_first_name">Upload Book Thumbnail</label> 
											
											<input placeholder="" class="" maxlength="150" name="book_thumbnail" id="book_thumbnail" type="file">               
											<span class="help-block error thumbnail" style="display: none">Please provide Book Thumbnail.</span> 
									</div> -->
									<?php 
									 /*if($result_book_info[0]['book_thumbnail']!='' || !empty($result_book_info[0]['book_thumbnail']))
									 {?>
										<img width="50" src="<?=BOOK_THUMBNAIL_FOLDER_HTTP.$result_book_info[0]['book_thumbnail']; ?>" /> 

									 <?php 		
									 }*/
									 ?>


									<div class="clear"></div>
									
									<div class="form-group" style="padding-top: 40px;"> 
									<input name="is_upcoming_book" id="is_upcoming_book" type="checkbox" value="1" <?php if($result_book_info[0]['is_upcoming_book']=='1') echo"checked"; ?>>
										<label for="Admin_first_name">Is Upcoming Book</label> 
									                  
									</div>
									<div class="clear"></div>

									<div class="form-group" style="padding-top: 30px;"> 
									<input name="is_book_visible" id="is_book_visible" type="checkbox" value="1" <?php if($result_book_info[0]['is_book_visible']=='1') echo"checked"; ?>>
										<label >Make Visible</label> 
									                  
									</div>
									<div class="clear"></div>
								



								</div>
								
								<div class="col-md-12 ">
								
									<div class="form-group"> 
										<label for="Admin_first_name">Book Description</label>
										<textarea class="form-control ckeditor" style="width:100%"  rows="5" name="book_description"><?=$result_book_info[0]['book_description']; ?></textarea>
										              
									</div>
								
								</div>
								
								<div class="clear"></div>							  

								
				
							</div>
							<div class="clear"></div>
						<div class="col-md-12 text-right" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>   
							<a href="manage_books.php" class="btn btn-primary">Cancel</a>								
								
							</div>
						</div>
						  </div>
						</div>
		  
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
      </div>
	
</div>
</form>  
  </div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  

  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
