<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Manage Book Categories
	<small><a class="btn btn-primary" href="add_edit_categories.php">Add New Book Categories</a></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Book Categories</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>




<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-striped table-hover">
		
				 <?php 
							 if($total_record >0)
							 {
							?>
							   <thead>
								<tr>
									<th id="sub_admin_grid_c0">Category No.</a></th>
									<th id="sub_admin_grid_c1">Category Title</a></th>
									<th id="sub_admin_grid_c1">Category Title(Marathi)</a></th>
									
									<th class="button-column" id="sub_admin_grid_c4">Action</th>
								</tr>
								</thead>
								<tbody>
	
									<?php 
									$sr=1;
									for($i_category=0;$i_category <=count($result_category_list_backend)-1;$i_category++)
									{
									
								 if($_REQUEST['pages']!="" &&$_REQUEST['pages']!=1) 
                            {
                                    $sr=($per_page * ($_REQUEST['pages']-1))+($i_category+1);
                            }
									 
									?>
									<tr class="odd">
										<td style="width:10%"><?=$sr; ?></td>
										<td style="width:30%"><?=$result_category_list_backend[$i_category]->category_title." (".$result_category_list_backend[$i_category]->category_count.") "; ?></td>
									
										<td style="width:30%"><?=$result_category_list_backend[$i_category]->category_title_mr; ?></td>
									
										<td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_category_list_backend[$i_category]->category_id; ?>" rel="add_edit_categories.php">Edit</a>&nbsp; |&nbsp; 
										<a title="Delete"  class="delete_info" id="<?=$result_category_list_backend[$i_category]->category_id; ?>" rel="manage_categories.php">Delete</a>  
											
										</td>
									</tr>
									<?php
									$sr=$sr+1;
									}
									?>									
								</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Book Category list not available to display. </td></tr>
							<?php 
							}
							?>
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

		 <!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 
					 $pages = ceil(($total_record/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['pages'])) 
                         {

                             if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_categories.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_categories.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_categories.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_categories.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->







<?php

/*



<!--main container-->
<div class="container-fluid">
  <div class="row">
  	<div class="col-md-16"> <!--dashboard container-->
	<div class="dashboard">
	 <h5 class="text-left">Manage Book Categories</h5>
	 <div class="clear"></div>
		<div class="tenant-dashboard contractor-issues clearfix"> 
			   <!--flash messages section-->
				 
			<a href="add_edit_categories.php" class="btn btn-default pull-right">
			   Add Book Category
			</a>
			<div class="clear">&nbsp;</div>
			
	
			<div class="clear"></div>
	
		   <!--------list--view---------->
				<div class="list-view">
					
					<div class="clear"></div>
					<div class="grid-view clearfix" id="sub_admin_grid">
						
							<table class="items table table-striped table-bordered">
							
							<?php 
							 if($total_record >0)
							 {
							?>
							   <thead>
								<tr>
									<th id="sub_admin_grid_c0">Category No.</a></th>
									<th id="sub_admin_grid_c1">Category Title</a></th>
									
									<th class="button-column" id="sub_admin_grid_c4">Action</th>
								</tr>
								</thead>
								<tbody>
	
									<?php 
									for($i_category=0;$i_category <=$total_record-1;$i_category++)
									{
									 $sr=$i_category+1;
									?>
									<tr class="odd">
										<td style="width:10%"><?=$sr; ?></td>
										<td style="width:50%"><?=$result_category_list_backend[$i_category]->category_title; ?></td>
									
										<td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_category_list_backend[$i_category]->category_id; ?>" rel="add_edit_categories.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_category_list_backend[$i_category]->category_id; ?>" rel="manage_categories.php">Delete</a>  
											
										</td>
									</tr>
									<?php
									}
									?>									
								</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Book Category list not available to display. </td></tr>
							<?php 
							}
							?>
							</table>
						<div class="keys" style="display:none" title="#"><span>6</span><span>5</span><span>4</span><span>3</span><span>1</span></div>
					</div>        
				</div>
		   <!--------list--view----------> 
	
		 </div>
	</div>

<!--end dashboard container--> 
 </div>
</div>
</div>
<!--end main container--> 
*/?>