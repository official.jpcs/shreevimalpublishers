<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($step=="edit_info") {echo "Update Location";}else{echo "Add New Location";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Location</a></li>
  </ol>
</section>




<!-- Main content -->
	<section class="content">
	
			<form class="form-vertical" id="frm_new_category" action="add_edit_location.php" method="post">
			<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_location";}else{echo "add_location";}?>" />
			<input name="location_id" id="location_id" type="hidden" value="<?=$location_id;?>">
				
						<!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  
					<div class="col-md-12">
						<div class="form-group"> 
							<label for="Admin_name">Storage Location</label>
							<input type="text" id="location_name" name="location_name" maxlength="50" class="form-control" required value="<?=$location->location_addr; ?>"> 
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group"> 
							<label for="Admin_name">Location Keyword</label>
							<input type="text" id="keyword" name="keyword" maxlength="50" class="form-control" required value="<?=$location->location_keyword; ?>"> 
						</div>
						
					</div>
						  </div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer text-right">
					<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
					<button class="btn" type="reset" name="yt1">Reset</button>
					<a href="manage_storage_locations.php" class="btn btn-primary">Cancel</a>
				</div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
						          
	</section>
</div>
<!-- /.content-wrapper -->
