<div class="container-fluid">
  <div class="row"><div class="col-md-16"> <!--dashboard container-->

<div class="dashboard">
<h5 class="text-left">Delivery Detail</h5>
<div class="clear"></div><div class="clear"></div>
 <a class="btn btn-default pull-right" href="manage_delivery_challan.php"><i class="glyphicon glyphicon-chevron-left"></i></a>
 
	<div class="tenant-dashboard clearfix">
		 <div class="clear"></div>
		 
		  
		 <div class="my-profile form-top clearfix">
					<!--Start Of Section2 : Purchase Entry-->
					<div class="panel panel-default">
		
					  <div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
										  
							<div class="clear"></div>		

							<div class="grid-view clearfix" id="view_sale_customer_info">
							
								<table class="items table table-striped table-bordered">
								
									<thead>
										<tr>
											<th id="sub_admin_grid_c0">#Invoice ID</a></th>
											<th id="sub_admin_grid_c1">Date</th>
											<th id="sub_admin_grid_c3">Party Detail</th>
										</tr>
									</thead>
									<tbody>
										<tr class="odd">
											<td style="width:20%"><?=$delivery_master_entry_detail[0]["delivery_maste_id"]; ?></td>
											<td style="width:20%;"><?=date("d-M-Y", strtotime($delivery_master_entry_detail[0]["delivery_date"])); ?></td> 
											<td style="width:40%">
											Saraswat , Pune
												
											</td>
											
											
										</tr>
										
									</tbody>
								 </table>	
							</div>							
							
							
							<div class="grid-view clearfix" id="new_delivery_grid">

								<table class="items table table-striped table-bordered">
								
									<thead>
										<tr>
											<th id="sub_admin_grid_c1">Book Title</th>
											<th id="sub_admin_grid_c2">Price</th>
											<th id="sub_admin_grid_c3">Discount</th>
											<th id="sub_admin_grid_c3">Quantity</th>
											<th id="sub_admin_grid_c3">Amount</th>
											
										</tr>
									</thead>
									
									<tbody id="delivery_entry_table">
									<?php
										
									  if(count($delivery_book_entry_list) >0)
									  {
									  	for($i_delivery_entry = 0;$i_delivery_entry <=count($delivery_book_entry_list)-1;$i_delivery_entry++)
										{	
									  
									?>	
										<tr>
											<td style="width:30%"><?=$delivery_book_entry_list[$i_delivery_entry]["book_title"]; ?></td>
											<td style="width:15%"><?=$delivery_book_entry_list[$i_delivery_entry]["book_mrp"]; ?></td>
											<td style="width:15%"><?=$delivery_book_entry_list[$i_delivery_entry]["book_discount"]; ?></td>
											<td style="width:15%"><?=$delivery_book_entry_list[$i_delivery_entry]["book_quantity"]; ?></td>
											<td style="width:20%"><?=$delivery_book_entry_list[$i_delivery_entry]["book_delivery_amount"]; ?></td>
										</tr>
									<?php
										}
									 }
									?>	
										<tr>
											<td colspan="3">&nbsp;</td>
											<td class="bold_lbl">Total Amount</td>
											<td>
												<label class="final_amount" id="final_amount_lbl"><span class="rupyaINR">Rs</span> 
													<?=$delivery_master_entry_detail[0]["final_amount"]; ?>
												</label>
											</td>
										</tr>

									</tbody>
									
								 </table>	
							</div>	 							
							
						
						</div>
					  </div>
					</div>
					<!--End of Section1--> 	
					
					
						
		 </div>
	</div>
</div>
<!--end dashboard container-->  
</div>
</div>
</div>


