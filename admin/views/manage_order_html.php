
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	<?php echo $title;?>
	
  </h1>
  <ol class="breadcrumb">
	<li><a href="#" class="show"><i class="fa fa-search"></i> Search</a></li>
  </ol>
 
</section>


	
<!-- Main content -->
<section class="content">


<?php 

			$search_file="manage_orders.php";
			$from_date='order_date_from';
			$to_date='order_date_to';
			$search_array= array("From_To_date_orders");

			//Include Search Section
			include(NAVIGATION_FILE."search.php");
/*
<div class="row filter-panel collapse in" id="filter-panel" style="height: auto;display:none">
	<div class="col-sm-12">
	  <div class="box">
		<div class="box-body ">
		
		<!------------------ Filter OR Search start----------------------->
		
	<div  class="filter-panel collapse in">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form" method="POST" action="manage_orders.php">
					<label>From</label>

					<div class="form-group">
					<input id="order_date_from" class="form-control datepicker" type="text" placeholder="Select Date" name="order_date_from"   value="<?php if(isset($_REQUEST['order_date_from']) && $_REQUEST['order_date_from']!="") echo $_REQUEST['order_date_from'];?>" style="width: 72%;float: left;border-radius: 0px;">
					<button id="order_datebtn" style="padding:5px;"class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
					</div><!-- form group [search] -->
					
					<label>To</label>
					
					<div class="form-group">
					<input id="order_date_to" class="form-control datepicker" type="text" placeholder="Select Date" name="order_date_to"   value="<?php if(isset($_REQUEST['order_date_to']) && $_REQUEST['order_date_to']!="") echo $_REQUEST['order_date_to'];?>" style="width: 72%;float: left;border-radius: 0px;">
					<button id="order_datebtn" style="padding:5px;"class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
				    </div><!-- form group [search] -->
						
                        <div class="form-group">
							<button type="submit" id="search" class="btn btn-primary">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							</button>
							<button type="reset"  class="btn btn-primary"  rel="manage_orders.php"><span class="fa fa-refresh" aria-hidden="true"></span>
							</button>						
       					</div>
                    </form>
                </div>
            </div>
        </div>
		<!------------------ Filter OR Search start----------------------->
	  
		</div>
	  </div>
	</div>
</div>

*/?>

  <div class="row">
	<div class="col-sm-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
		<div class="box-body ">
		
		<ul class="nav nav-tabs">
			
			<li>
				<a  href="manage_incomplete_orders.php" aria-expanded="false">Incomplete Orders</a>
			</li>

			<li class="active"><a  href="manage_orders.php" aria-expanded="true">Completed Orders (<?php echo $total_CompleteOrders; ?>)</a>
			</li>			

		</ul>

		<div class="tab-content">
		
		
		<div id="cmp_order" class="tab-pane  fade active in">
  

		  <table id="example2" class=" table-responsive table table-bordered table-hover">
			<?php 
			if(count($completed_order_list) >0)
			{
			?>
			<thead>
			  <tr>
				<th width="5%">Sr No</th>
				<!--<th width="5%">#Order ID</th>-->
				<th width="15%">Customer Name</th>
				<!--<th width="10%">Total Amount</th>-->
				<th width="5%">Discount Amount</th>
				<th width="5%">Amount Paid</th>
				<th width="5%">Delivery Charges</th>
				<th width="8%">Order Date</th>
				<th width="10%">Order Status </th>
				<th width="10%">Payment Status</th>
				<th width="3%">Action</th>
			  </tr>
			</thead>
			<tbody>
			<?php 
			 $cnt=1;
			for($list_i=0;$list_i<count($completed_order_list);$list_i++)
			{
				
			 
	            if($_REQUEST['page']!="" &&$_REQUEST['page']!=1) 
	            {
	                $cnt=($per_page * ($_REQUEST['page']-1))+($list_i+1);
	            }
							
			?>
			  <tr>
				<td><?=$cnt;?></td>
				<!--<td>#<?=$completed_order_list[$list_i]['order_id'];?></td>-->
				<td><?=$completed_order_list[$list_i]['customer_name'] . "<br/> ". $completed_order_list[$list_i]['taluka']." ,".$completed_order_list[$list_i]['district'];?></td>
				<!--<td><?//=$completed_order_list[$list_i]['order_total_amount'];?></td>-->
				<td><?php echo $completed_order_list[$list_i]['discount_amount'];?></td>
				
				<td style='color:green;font-weight:bold'>
				<sup style="font-size:13px;"><?=sprintf("%01.2f", $completed_order_list[$list_i]['final_amount_paid']); ?></sup>
				</td>
				<td style='color:green;font-weight:bold'><?= "Rs. ".number_format( $completed_order_list[$list_i]['delivery_charges'],2) ." /-";?></td>
				<td><?= date("jS M Y", strtotime($completed_order_list[$list_i]['order_date'])); ?></td>
				<td style='color:green;font-weight:bold'><?php if($completed_order_list[$list_i]['order_status']==0)echo "<span style='color:red'>Incompleted</span>"; else echo "<span style='color:green'>Completed</span>";?></td>
				<td style='color:green;font-weight:bold'><?php if($completed_order_list[$list_i]['payment_status']==0)echo "<span style='color:red'>Incompleted</span>"; else echo "<span style='color:green'>Completed</span>";?></td>
				
				<td>
					<a href="order_detail.php?id=<?=$completed_order_list[$list_i]['order_id']; ?>&step=view_info"><i class="fa fa-eye" aria-hidden="true"></i></a> &nbsp; 
					 &nbsp;
				</td>
			  </tr>
			  <?php 
			  $cnt=$cnt+1;
			  }  
}
			else
			{
			?>
			 <tr><td>Order list not available to display. </td></tr>
			<?php 
			}
			?>
			
			</tbody>
		  </table>
		
		
		
		<?php
		// Total Records
				$total_records=$total_CompleteOrders;

				//Include Pagination Section
				include(NAVIGATION_FILE."pagination.php");
		/*
		<!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 $order_date="";
					 if(isset($_REQUEST['order_date']) && $_REQUEST['order_date']!="")
						$order_date="&order_date=".$_REQUEST['order_date'];
					 
					 $pages = ceil(($total_CompleteOrders/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['cmp_ord_pages'])) 
                         {

                             if($_REQUEST['cmp_ord_pages']==$i || $_REQUEST['cmp_ord_pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_orders.php?cmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_orders.php?cmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_orders.php?cmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_orders.php?cmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>
			 
			*/ ?>
		</div><!-- /#cmp_order -->


		</div><!-- /.tab-content-->
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  
	  
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
