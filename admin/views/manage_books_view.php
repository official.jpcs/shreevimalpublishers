<style>
img.lazy {
        width: 60%; 
        
        display: block;
		
/* optional way, set loading as background */
        background-image: url('images/loader.svg');
        background-repeat: no-repeat;
        background-position: 50% 50%;		
    }
        
</style>



<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> Manage Books 
  <a class="btn btn-primary" href="add_edit_book.php">Add New Book</a>
  </h1>
  
  <ol class="breadcrumb">
  <li><a href="#" class="show"><i class="fa fa-search"></i> Search</a></li>
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Books</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>



<!-- Main content -->
<section class="content">



<div class="row filter-panel collapse in" id="filter-panel" style="height: auto;display:none">
	<div class="col-sm-12">
	  <div class="box">
		<div class="box-body ">
		
		<!------------------ Filter OR Search start----------------------->
		
	<div  class="filter-panel ">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form" method="POST">
						<div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="name">Book Title:</label>
                            <input type="text" class="form-control input-sm" name="book_title" id="book_title" value="<?php if(isset($_REQUEST['book_title']) && $_REQUEST['book_title']!="") echo $_REQUEST['book_title'];?>">
                        </div><!-- form group [search] -->
						<div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">ISBN:</label>
                            <input type="text" class="form-control input-sm" name="isbn_no" id="isbn_no" value="<?php if(isset($_REQUEST['isbn_no']) && $_REQUEST['isbn_no']!="") echo $_REQUEST['isbn_no'];?>">
                        </div><!-- form group [search] -->
						<div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="name">Author:</label>
							
                            <input type="text" class="form-control input-sm" name="author_name" id="first_last_name" value="<?php if(isset($_REQUEST['author_name']) && $_REQUEST['author_name']!="") echo $_REQUEST['author_name'];?>">
                        </div><!-- form group [search] -->
                        <div class="form-group">
							<button type="submit" id="search" class="btn btn-primary">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							</button>
							<a href="manage_books.php" type="reset"  class="btn btn-primary"><span class="fa fa-refresh" aria-hidden="true"></span>
							</a>						
       					</div>
                    </form>
                </div>
            </div>
        </div>
		<!------------------ Filter OR Search start----------------------->
	  
		</div>
	  </div>
	</div>
</div>



		 <!--- Pagination Code --->
<div id="pagination" class="pull-left" style="margin-top:15px;"><b>Total Record : <?=$total_record;?> </b></div>
<div id="pagination" class="pull-right">
	 <ul class="pagination" style="margin:5px !important;">
		 <?php
		 
		 $pages = ceil(($total_record/$per_page));
		 //Pagination Numbers
		 for($i=1; $i<=$pages; $i++)
		 {

			 if(isset($_REQUEST['pages'])) 
			 {

				 if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
					 echo '<li id="'.$i.'" class="active"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
					 else
					 echo '<li id="'.$i.'"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			 }
			 else
			 {
				if($i==1)
echo '<li id="'.$i.'" class="active"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
else
echo '<li id="'.$i.'"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			 }
		 }
		 ?>
	 </ul>
 </div>
 
 <!--- Pagination Code --->

  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-hover">
			<?php 
							 if($total_books >0)
							 {
							?>
                                                    
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr.</th>
								<th id="sub_admin_grid_c0">Book Image</th>
								
								<th id="sub_admin_grid_c1">Book Name</th>
								<th id="sub_admin_grid_c2">Author</th>
								<th id="sub_admin_grid_c3">Price</th>
								<th class="button-column" id="sub_admin_grid_c4">Quantity</th>
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
                                                            
								<?php    
								$sr=1;
								for($i_book=0;$i_book <=$total_books-1;$i_book++)
								{

									if($result_book_list[$i_book]['book_thumbnail']==BOOK_THUMBNAIL_FOLDER_HTTP) 
									{	
										$image_path = BOOK_THUMBNAIL_FOLDER_HTTP."default.png"; 
									}
									else 
									{	
										$image_path = $result_book_list[$i_book]['book_thumbnail'];							
									}
									
									
									
								
									if($_REQUEST['pages']!="" &&$_REQUEST['pages']!=1) 
									{
									$sr=($per_page * ($_REQUEST['pages']-1))+($i_book+1);
									}
								 
								?>
								<tr class="odd">
									<td style="width:5%">
										<?php 
										echo $sr ."&nbsp;";
										if($result_book_list[$i_book]['is_book_visible']=='1')
										{

											echo'<a title="'.$result_book_list[$i_book]['book_id'].'" rel="'.$result_book_list[$i_book]['is_book_visible'].'" class="updateVisibleStatus visibleStatus_'.$result_book_list[$i_book]['book_id'].'"><i class="fa fa-eye " style="color:blue;cursor:pointer"></i></a>';

										}
										else
										{

											echo'<a title="'.$result_book_list[$i_book]['book_id'].'" rel="'.$result_book_list[$i_book]['is_book_visible'].'" class="updateVisibleStatus visibleStatus_'.$result_book_list[$i_book]['book_id'].'"><i class="fa fa-eye-slash" style="color:red;cursor:pointer"></i></a>';

										}
										//echo $result_book_list[$i_book]['is_book_visible']; 
										?>

										
											
									</td>
									<td style="width:20%">
									
									
									
										<img class="lazy" data-src ="<?=$image_path;?>"  width="60%"  />									
										
										
										
										<br/>
										<b>ISBN No : <?=$result_book_list[$i_book]['isbn_no']; ?></b>
									</td>
									
									
									<td style="width:25%"><?php
									if(strlen($result_book_list[$i_book]['book_title'])>80)
									echo substr($result_book_list[$i_book]['book_title'],0,80)."...";
									else
									echo substr($result_book_list[$i_book]['book_title'],0,80);
									?></td>
									<td style="width:15%"><?=$result_book_list[$i_book]['author_name']; ?></td>
									<td style="width:10%">Rs.<?=$result_book_list[$i_book]['book_mrp']; ?></td>
									<td style="width:10%"><?=$result_book_list[$i_book]['quantity']; ?></td>
									<td style="width:12%">
									
                                         <a  title="<?=$result_book_list[$i_book]['book_title']; ?>"  style="cursor: pointer" class="edit_info" id="<?=$result_book_list[$i_book]['book_id']; ?>"  rel="view_books_detail.php">View</a> |
										<a title="Edit"  id="<?=$result_book_list[$i_book]['book_id']; ?>" href="edit_book_general_info.php?book_id=<?=$result_book_list[$i_book]['book_id']; ?>&step=general_info&pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" pages="<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" >Edit</a> |
										<a title="Delete"  class="delete_info" id="<?=$result_book_list[$i_book]['book_id']; ?>" rel="manage_books.php">Delete</a>																							
									</td>
								</tr>
								<?php
								
								$sr=$sr+1;
								}
								?>									
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Book list not available to display. </td></tr>
							<?php 
							}
							?>
		  </table>
		</div><!-- /.box-body -->
		
		 <!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 
					 $pages = ceil(($total_record/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['pages'])) 
                         {

                             if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_books.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>

	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
