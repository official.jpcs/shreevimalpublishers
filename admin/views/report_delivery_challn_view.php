<!--main container-->
<?php
 $searchFields=array('delivery_from_date','sel_location','deliveryToDate','search_book_name','sel_party');
 $searchFieldArray=implode(",",$searchFields);
 
// var_dump( $_REQUEST); die;
?>

<div class="container-fluid">
  <div class="row">
<div class="col-md-16"> <!--dashboard container-->
<div class="dashboard">
 <h5 class="text-left">Delivery Challan Report</h5>
 <div class="clear"></div>
    <div class="tenant-dashboard contractor-issues clearfix"> 
           <!--flash messages section-->
		<div class="clear"></div>   
		
        <div class="panel-group form-top" id="accordion">
          <div class="panel panel-default">
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="">
                <div class="form-wrap "> 
                   
					<form class="form-vertical" id="frmDeliveryReport" method="POST" autocomplete="off"> 
					<!--Start Of Section4 : Storage Location-->				
					<div class="panel panel-default">
					  
					  <div id="collapseFour" class="panel-collapse collapse in ">
		
							<div class="panel-body">
								<div class="col-md-6 col-md-offset-1">
									<div class="form-group"> 
										<label for="Admin_first_name">From Date</label> 
											<div class="clear"></div>	
											<input id="delivery_from_date" class="form-control" type="text" placeholder="Select From Date" name="delivery_from_date"  style="width: 90%;float: left" value="<?php echo $_REQUEST['delivery_from_date']; ?>">
											<button id="saleFromDatebtn" class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
										
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Location</label> 

										<select name="sel_location[]" id="sel_location" class="form-control">
											 <?php
											  $tbl="tbl_storage_locations";
											  $val ="location_id";
											  $show="location_addr";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="---- Select Storage Location ----";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($_REQUEST['sel_location']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
	
										</select>
										
									</div>
									<div class="clear"></div>		

									<div class="form-group"> 
										<label for="Admin_first_name">Party</label> 

										<select name="sel_party[]" id="sel_party" class="form-control">
											 <?php
											  $tbl="tbl_partys";
											  $val ="party_id";
											  $show="name";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="---- Select Party ----";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($_REQUEST['sel_party']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
	
										</select>
										
									</div>
									<div class="clear"></div>

																
											
								</div>
								
								<div class="col-md-6 col-md-offset-1">
								
		
									<div class="form-group"> 
										<label for="Admin_first_name">To Date</label> 
										
											<div class="clear"></div>	
											<input id="deliveryToDate" class="form-control" type="text" placeholder="Select To Date" name="sale_to_date" value="<?php if(!empty($_REQUEST['deliveryToDate'])){echo $_REQUEST['deliveryToDate'];} ?>"  style="width: 90%;float: left">
											<button id="saleToDatebtn" class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
										
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Book Title</label> 

											<input id="search_book_name" name="search_book_name" class="form-control txt-auto" onkeyup="suggest(this,'tbl_bloggers','first_name','suggestions8','0','search');"  value="<?=$_REQUEST['search_book_name']; ?>"/>
		
											<!-- smart search div -->
											<div id="suggestions8" class="smart_search_div_class"> 
											  <div id="suggestions8List"> &nbsp; 
											  </div>
											 </div>
											<!-- smart search div ends here -->
											
											<select name="search_book" id="search_book" class="form-control" style="display: none" >
												  <option value="0">---- Select Book Title ----</option>
												  <?php 
												  for($i_book=0;$i_book <=$total_books-1;$i_book++)
												  {
												  ?>
														  <option value="<?=$result_book_list[$i_book]['book_id']."~".$result_book_list[$i_book]['book_title']."~".$result_book_list[$i_book]['book_mrp'];  ?>" >
																  <?=$result_book_list[$i_book]['book_title']; ?>
														  </option>
												  <?php
												  }
												  ?>
											</select>	 
										  <span class="help-block error" id="sel_book_msg" style="display: none">Please Select Book.</span>
										
									</div>
									<div class="clear"></div>
									
		
								</div>
								
								<div class="clear"></div>
								<div class="col-md-6 col-md-offset-6" style="margin-top:10px;">
									<div class="btn-block">
									<input type="hidden" id="step_search" name="step_search" value="search">
										<button class="btn" type="button" name="yt0" onclick='funDoSearchActionByField("search_delivery","challan_report.php","frmDeliveryReport",<?php echo json_encode($searchFieldArray) ?>)'>Submit</button>              
										<button class="btn" type="reset" name="yt1" onclick="funResetRedirect('challan_report.php')">Reset</button>            
									</div>
								</div>
				
							</div>
						
					  </div>
		
					  
					</div>
					<!--End Of Sectio4-->		
						
					</form>  


						
               </div>
              </div>
            </div>
          </div>
        </div>

        <div class="clear"></div>

       <!--------list--view---------->
			<div class="list-view">
				
				<div class="clear"></div>
				<div class="grid-view clearfix" id="sub_admin_grid">
					<div class="page-dropdown pull-left" style="font-weight:bold">Total Record : <?=$report_count ? $report_count :0 ; ?></div>
						<div class="page-dropdown pull-right">
						<?php
						 if($report_count >0)
						 {
						 ?>	
							<a title="Export To Excel" style="cursor:pointer" onclick='funDoSearchActionByField("export_delivery","challan_report.php","frmDeliveryReport",<?php echo json_encode($searchFieldArray) ?>)'><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>						
							<?php
							 if($filename!="")
							 {
							 ?>
								&nbsp; | &nbsp; <a title="Download File" href="report_files/export/<?=$filename; ?>" download><i class="fa fa-download" aria-hidden="true"></i></a>
							<?php
							 }
							 ?>
						 <?php
						 }
						 ?>	
						</div>
						<table class="items table table-striped table-bordered">
							<?php 
							 if($report_count >0)
							 {
							?>
                                                    
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</th>
								<th id="sub_admin_grid_c1">Date</th>
								<th id="sub_admin_grid_c1">Party Name</th>
								<th id="sub_admin_grid_c2">Location</th>
								<th id="sub_admin_grid_c3">Book Title</th>
								<th id="sub_admin_grid_c3">Price</th>
								<th id="sub_admin_grid_c3">Discount</th>																
								<th class="button-column" id="sub_admin_grid_c4">Quantity</th>
								<th class="button-column" id="sub_admin_grid_c5">Total Amount</th>
							</tr>
							</thead>
							<tbody>
                                                            
								<?php 
								for($i_report=0;$i_report <=$report_count-1;$i_report++)
								{
								 $sr=$i_report+1;
								?>
								<tr class="odd">
									<td style="width:7%"><?=$sr ; ?></td>
									<td style="width:10%"><?= date("d-M-Y", strtotime($deliverysReportList[$i_report]['delivery_date'])); ?></td>
									<td style="width:15%"><?= $deliverysReportList[$i_report]['party_name']; ?></td>
									<td style="width:15%"><?=$deliverysReportList[$i_report]['location_addr']; ?></td>
									<td style="width:18%"><?=$deliverysReportList[$i_report]['book_title']; ?></td>
									<td style="width:8%"><span class='rupyaINR'>Rs</span> <?=$deliverysReportList[$i_report]['book_mrp']; ?></td>
									<td style="width:8%"><?=$deliverysReportList[$i_report]['book_discount']; ?>%</td>																		
									<td style="width:8%"><?=$deliverysReportList[$i_report]['book_quantity']; ?></td>
									<td style="width:12%"><span class='rupyaINR'>Rs</span> <?=$deliverysReportList[$i_report]['book_delivery_amount']; ?></td>
									
								</tr>
								<?php
								}
								?>									
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Report not available to  display.</td></tr>
							<?php 
							}
							?>
						</table>
					<div class="keys" style="display:none" title="#"><span>6</span><span>5</span><span>4</span><span>3</span><span>1</span></div>
				</div>        
			</div>
       <!--------list--view----------> 

     </div>
</div>

<!--end dashboard container--> 
 </div>
</div>
</div>
<!--end main container--> 


