<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Manage  My Shop
	<!--<small><a class="btn btn-primary" href="new_delivery_entry.php">Add New  My Shop</a></small>-->
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage  My Shop</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>






<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-striped table-hover">
		
						<?php 
							 if($total_books >0)
							 {
							?>
                                                    
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">#ISBN</th>
								<th id="sub_admin_grid_c1">Book Name</th>
								<th id="sub_admin_grid_c2">Author</th>
								<th id="sub_admin_grid_c3">Price</th>
								<th class="button-column" id="sub_admin_grid_c4">Available Quantity</th>
								
							</tr>
							</thead>
							<tbody>
                                                            
								<?php 
								for($i_book=0;$i_book <=$total_books-1;$i_book++)
								{
								 $sr=$i_book+1;
								?>
								<tr class="odd">
									<td style="width:10%"><?=$result_book_list[$i_book]['isbn_no']; ?></td>
									<td style="width:30%"><?=$result_book_list[$i_book]['book_title']; ?></td>
									<td style="width:15%"><?=$result_book_list[$i_book]['author_name']; ?></td>
									<td style="width:15%">Rs.<?=$result_book_list[$i_book]['book_mrp']; ?></td>
									<td style="width:12%"><?=$result_book_list[$i_book]['book_quantity']; ?></td>
									
								</tr>
								<?php
								}
								?>									
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Book list not available to display. </td></tr>
							<?php 
							}
							?>
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->




<?php
/*

<!--main container-->
<div class="container-fluid">
  <div class="row">
<div class="col-md-16"> <!--dashboard container-->
<div class="dashboard">
 <h5 class="text-left">My Shop</h5>
 <div class="clear"></div>
    <div class="tenant-dashboard contractor-issues clearfix"> 
           <!--flash messages section-->
		<div class="clear"></div>   
        
		     
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-default pull-right">
           <i class="glyphicon glyphicon-search"></i>
        </a>
        <div class="clear"></div>
        <div class="panel-group form-top" id="accordion">
          <div class="panel panel-default">
            <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="form-wrap clearfix"> 
                <!--------Search--view---------->
                    <form class="form-vertical" id="search_subadmin" action="#" method="get">
                        <div class="col-md-4">
                                <div class="form-group"> 
                                        <label for="Admin_name">Book Name</label><input placeholder="" class="form-control" maxlength="50" name="Admin[name]" id="Admin_name" type="text"> 
                                </div>
                        </div>
                        <div class="col-md-4">
                                <div class="form-group"> 
                                        <label for="Admin_name">ISBN</label><input placeholder="" class="form-control" maxlength="50" name="Admin[name]" id="Admin_name" type="text"> 
                                </div>

                        </div>
                        <div class="col-md-8">
                                <div class="form-group"> 
                                        <label for="text">&nbsp;</label>        
                                        <div class="clear"></div>
                                        <button class="btn" type="submit" name="yt0">Search</button>                    
                                        <button class="reset-form btn" type="reset" name="yt1">Reset</button>    
                                </div>
                        </div>
                    </form>           
            
                 <!--------Search--view---------->
               </div>
              </div>
            </div>
          </div>
        </div>

        <div class="clear"></div>

       <!--------list--view---------->
			<div class="list-view">
				
				<div class="clear"></div>
				<div class="grid-view clearfix" id="sub_admin_grid">
					<div class="page-dropdown pull-right">Displaying 1-5 of 5 results.</div>
						<table class="items table table-striped table-bordered">
							<?php 
							 if($total_books >0)
							 {
							?>
                                                    
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">#ISBN</th>
								<th id="sub_admin_grid_c1">Book Name</th>
								<th id="sub_admin_grid_c2">Author</th>
								<th id="sub_admin_grid_c3">Price</th>
								<th class="button-column" id="sub_admin_grid_c4">Available Quantity</th>
								
							</tr>
							</thead>
							<tbody>
                                                            
								<?php 
								for($i_book=0;$i_book <=$total_books-1;$i_book++)
								{
								 $sr=$i_book+1;
								?>
								<tr class="odd">
									<td style="width:10%"><?=$result_book_list[$i_book]['isbn_no']; ?></td>
									<td style="width:30%"><?=$result_book_list[$i_book]['book_title']; ?></td>
									<td style="width:15%"><?=$result_book_list[$i_book]['author_name']; ?></td>
									<td style="width:15%">Rs.<?=$result_book_list[$i_book]['book_mrp']; ?></td>
									<td style="width:12%"><?=$result_book_list[$i_book]['book_quantity']; ?></td>
									
								</tr>
								<?php
								}
								?>									
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Book list not available to display. </td></tr>
							<?php 
							}
							?>
						</table>
					<div class="keys" style="display:none" title="#"><span>6</span><span>5</span><span>4</span><span>3</span><span>1</span></div>
				</div>        
			</div>
       <!--------list--view----------> 

     </div>
</div>

<!--end dashboard container--> 
 </div>
</div>
</div>
<!--end main container--> 


<!---- Book Detail PopUp --->

<div class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="book_detail">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <input type="hidden" name="info_for_book" id="info_for_book"  />
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="book_title"></h4>
        </div>
          
        <div class="modal-body">
          <div class="container-fluid">

            <div role="tabpanel">

              <!-- Book Tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li id="tab_gen" role="presentation" class="active book_tab"><a href="#general_info" aria-controls="general_info" role="tab" data-toggle="tab">General Info</a></li>
                  <li role="presentation" class="book_tab"><a href="#pub_info" aria-controls="pub_info" role="tab" data-toggle="tab">Publisher's Info</a></li>
                  <li role="presentation" class="book_tab"><a href="#book_info" aria-controls="book_info" role="tab" data-toggle="tab">Book Details</a></li>
                  <li role="presentation" class="book_tab"><a  class="storage_detail" href="#storage_detail" aria-controls="storage_detail" role="tab" data-toggle="tab">Storage Details</a></li>
                </ul>


                <div class="tab-content">
                      <!-- Book General Info -->  
                      <div role="tabpanel" class="tab-pane fade in active" id="general_info" style="margin-top: 10px;">

                            
                          <div class="col-md-4 book_thumbnail" >
                              
                          </div>
                          
                            <div class="col-md-6">
                                <div class="form-group">	
                                    <div class="col-md-6 book_info_lbl">Book name :</div>
                                    <div class="col-md-8 book_name"></div>
                                </div>
                                <div class="clear"></div>
                                <div class="form-group">	
                                    <div class="col-md-6 book_info_lbl">Author :</div>
                                    <div class="col-md-8 book_author"></div>
                                </div>    
                                <div class="clear"></div>
                                <div class="form-group">	
                                    <div class="col-md-6 book_info_lbl">Compiled by :</div>
                                    <div class="col-md-8 compile_by"></div>
                                </div>   
                                <div class="clear"></div>
                                <div class="form-group">	
                                    <div class="col-md-6 book_info_lbl">Translated by :</div>
                                    <div class="col-md-8 translate-by">12345</div>
                                </div>                    

                            </div>

                          <div class="col-md-6 ">
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Language :</div>
                                  <div class="col-md-8 book_lng">12345</div>
                              </div>      
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">MRP :</div>
                                  <div class="col-md-8 book_mrp">12345</div>
                              </div>                  
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Edition :</div>
                                  <div class="col-md-8 book_edn">12345</div>
                              </div>                    
                          </div>
                      </div>

                      <!-- Publisher Info --->  
                      <div role="tabpanel" class="tab-pane fade" id="pub_info" style="margin-top: 10px;">
                          <div class="col-md-8">
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Name :</div>
                                  <div class="col-md-8 pub_name"></div>
                              </div>
                              <div class="clear"></div>
                              <div class="form-group ">	
                                  <div class="col-md-6 book_info_lbl">Address :</div>
                                  <div class="col-md-8 pub_addr"></div>
                              </div>    
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Contact No :</div>
                                  <div class="col-md-8 pub_contact"></div>
                              </div>   
                              <div class="clear"></div>
                          </div>

                          <div class="col-md-8 ">
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Email :</div>
                                  <div class="col-md-8 pub_email"></div>
                              </div>      
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Website :</div>
                                  <div class="col-md-8 pub_web"></div>
                              </div>                  

                          </div>
                      </div>

                      <!-- Book Detail -->
                      <div role="tabpanel" class="tab-pane fade" id="book_info" style="margin-top: 10px;">

                          <div class="col-md-8">
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">ISBN No. :</div>
                                  <div class="col-md-8 book_isbn"></div>
                              </div>
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Book Category :</div>
                                  <div class="col-md-8 book_cat"></div>
                              </div>    
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Dimension Width :</div>
                                  <div class="col-md-8 dim_width"></div>
                              </div>   
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Dimension Height :</div>
                                  <div class="col-md-8 dim_heigh"></div>
                              </div>                    

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Dimension Depth :</div>
                                  <div class="col-md-8 dim_depth"></div>
                              </div>                    

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">No. of Pages :</div>
                                  <div class="col-md-8 no_of_pages"></div>
                              </div>                    

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Binding Type :</div>
                                  <div class="col-md-8 binding_type"></div>
                              </div>                    

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Cover Type :</div>
                                  <div class="col-md-8 cover_type"></div>
                              </div>                    
                              
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Weight :</div>
                                  <div class="col-md-8 weight"></div>
                              </div>          
                              
                              <div class="clear"></div>
                             
                          </div>

                          <div class="col-md-8 ">

                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Production Cost :</div>
                                  <div class="col-md-8 prod_cost"></div>
                              </div>      
                              
                              <div class="clear"></div>
                               
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Translated? :</div>
                                  <div class="col-md-8 translated"></div>
                              </div>                  
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Translated From :</div>
                                  <div class="col-md-8 trn_frm_book"></div>
                              </div>   

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Original Language :</div>
                                  <div class="col-md-8 trn_frm_lng"></div>
                              </div>   

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Publication Date :</div>
                                  <div class="col-md-8 pub_date"></div>
                              </div>   

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Co-Author :</div>
                                  <div class="col-md-8 co_auth"></div>
                              </div>   
                              
                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Contributor :</div>
                                  <div class="col-md-8 contributor"></div>
                              </div>   

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Editor :</div>
                                  <div class="col-md-8 editor"></div>
                              </div>   

                              <div class="clear"></div>
                              <div class="form-group">	
                                  <div class="col-md-6 book_info_lbl">Copyright :</div>
                                  <div class="col-md-8 copyright"></div>
                              </div>   
                              

                              
                          </div>
                          
                      
                      </div>

                      <!-- Storagewise Quantity -->
                      <div role="tabpanel" class="tab-pane fade" id="storage_detail" style="margin-top: 10px;">
                           <div class="col-md-8" id="storage_list" >
                           </div>  
                      </div>
                </div>

            </div>
          
          </div>
        </div>
        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  */
  ?>