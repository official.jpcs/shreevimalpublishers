<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Manage Publishers
	<small><a class="btn btn-primary" href="add_new_publisher.php">Add New Publishers</a></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Publishers</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>






<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-striped table-hover">
		
					<?php 
						 if($total_record >0)
						 {
						?>
						
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No</a></th>
								<th id="sub_admin_grid_c0">Publisher Name</a></th>
								<th id="sub_admin_grid_c1">Address<span class="caret"></span></a></th>
								<th id="sub_admin_grid_c0">Publisher Name(Marathi)</a></th>
								<th id="sub_admin_grid_c1">Address(Marathi)<span class="caret"></span></a></th>
								<th id="sub_admin_grid_c2">Contact No.</th>
								<th id="sub_admin_grid_c3">Email Id<span class="caret"></span></a></th>
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
								<?php 
								$sr=1;
								for($i_publisher=0;$i_publisher <=count($result_authpub_list)-1;$i_publisher++)
								{

								 if($_REQUEST['pages']!="" &&$_REQUEST['pages']!=1) 
                            {
                                    $sr=($per_page * ($_REQUEST['pages']-1))+($i_publisher+1);
                            }								?>
							
								<tr class="odd">
									<td style="width:5%"><?=$sr; ?></td>
									<td style="width:20%"><?=$result_authpub_list[$i_publisher]->name; ?></td>
									<td style="width:25%"><?=$result_authpub_list[$i_publisher]->address; ?></td>
									<td style="width:20%"><?=$result_authpub_list[$i_publisher]->name_mr; ?></td>
									<td style="width:25%"><?=$result_authpub_list[$i_publisher]->address_mr; ?></td>
									<td style="width:10%"><?=$result_authpub_list[$i_publisher]->contact_no; ?></td>
									<td style="width:10%"><?=$result_authpub_list[$i_publisher]->email_id; ?></td>
									<td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_authpub_list[$i_publisher]->id; ?>" rel="add_new_publisher.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_authpub_list[$i_publisher]->id; ?>" rel="manage_publishers.php">Delete</a>																							
									</td>
								</tr>
								<?php
								$sr=$sr+1;

								}
								?>
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Publishers list not available to display. </td></tr>
							<?php 
							}
							?>
							
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

		 <!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 
					 $pages = ceil(($total_record/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['pages'])) 
                         {

                             if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_publishers.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_publishers.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_publishers.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_publishers.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->


<?php /*


<!--main container-->
<div class="container-fluid">
  <div class="row"><div class="col-md-16"> <!--dashboard container-->
<div class="dashboard">
 <h5 class="text-left">Manage Publishers</h5>
 <div class="clear"></div>
    <div class="tenant-dashboard contractor-issues clearfix"> 
           <!--flash messages section-->
             
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="btn btn-default pull-right">
           <i class="glyphicon glyphicon-search"></i>
        </a>
        <div class="clear"></div>
        <div class="panel-group form-top" id="accordion">
          <div class="panel panel-default">
            <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="form-wrap clearfix"> 
                <!--------Search--view---------->
					<form class="form-vertical" id="search_subadmin" action="#" method="get">
					<div class="col-md-4">
						<div class="form-group"> 
							<label for="Admin_name">Publisher Name</label><input placeholder="" class="form-control" maxlength="50" name="Admin[name]" id="Admin_name" type="text"> 
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group"> 
							<label for="Admin_name">Email ID</label><input placeholder="" class="form-control" maxlength="50" name="Admin[name]" id="Admin_name" type="text"> 
						</div>
						
					</div>
					<div class="col-md-8">
						<div class="form-group"> 
							<label for="text">&nbsp;</label>        
							<div class="clear"></div>
							<button class="btn" type="submit" name="yt0">Search</button>                    
							<button class="reset-form btn" type="reset" name="yt1">Reset</button>    
						</div>
					</div>
					</form>           
            
                 <!--------Search--view---------->
               </div>
              </div>
            </div>
          </div>
        </div>

        <div class="clear"></div>

       <!--------list--view---------->
			<div class="list-view">
				<div class="col-md-4">
					<div class="form-group"> <label for="text">&nbsp;</label>              
						<div class="btn-block"> 
							<a class="btn btn-default" href="add_new_publisher.php">Add New Publisher</a> 
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="grid-view clearfix" id="sub_admin_grid">
					<div class="page-dropdown pull-right">Displaying 1-5 of 5 results.</div>
						<table class="items table table-striped table-bordered">
						<?php 
						 if($total_record >0)
						 {
						?>
						
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Publisher Name</a></th>
								<th id="sub_admin_grid_c1">Address<span class="caret"></span></a></th>
								<th id="sub_admin_grid_c2">Contact No.</th>
								<th id="sub_admin_grid_c3">Email Id<span class="caret"></span></a></th>
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
								<?php 
								for($i_publisher=0;$i_publisher <=$total_record-1;$i_publisher++)
								{
								 $sr=$i_publisher+1;
								?>
							
								<tr class="odd">
									<td style="width:20%"><?=$result_authpub_list[$i_publisher]->name; ?></td>
									<td style="width:30%"><?=$result_authpub_list[$i_publisher]->address; ?></td>
									<td style="width:15%"><?=$result_authpub_list[$i_publisher]->contact_no; ?></td>
									<td style="width:10%"><?=$result_authpub_list[$i_publisher]->email_id; ?></td>
									<td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_authpub_list[$i_publisher]->id; ?>" rel="add_new_publisher.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_authpub_list[$i_publisher]->id; ?>" rel="manage_publishers.php">Delete</a>																							
									</td>
								</tr>
								<?php
								}
								?>
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Publishers list not available to display. </td></tr>
							<?php 
							}
							?>
						</table>
					<div class="keys" style="display:none" title="#"><span>6</span><span>5</span><span>4</span><span>3</span><span>1</span></div>
				</div>        
			</div>
       <!--------list--view----------> 

     </div>
</div>

<!--end dashboard container--> 
 </div>
</div>
</div>
<!--end main container--> 

*/
?>