<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
 
	<div style="float:left" >
		<h4> Edit Book >> <?php echo $result_book_info[0]['book_title']; ?></h4>
	</div>
	 <?php $book_id=$_POST['update_id'];
		//echo $pages=$_POST['pages'];?>
	<div style="float:right" >
		<small><a class="btn btn-primary pull-right" href="manage_books.php?pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>">Manage Book List</a></small>
	</div>
  
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
<div id="exTab3" class="container col-xs-12">	
<?php
	$tab_edit ="storage_info";
	include("includes/book_edit_tab.php");
?>

<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
	<input type="hidden" name="step" id="step" value="update_storage_detail" />
	<input name="book_id" id="book_id" type="hidden" value="<?=$book_id;?>">
	<input name="pages" id="pages" type="hidden" value="<?php if(isset($_POST['pages'])) echo $_POST['pages']; else echo '1';?>">
				
		
		
<div class="tab-content clearfix">
	
	
    <div class="tab-pane active" id="storage_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section4 : Storage Location-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							<h4 class="panel-title">Storage Details</h4>
						</div>
						
					  </div>
					  <div id="collapseFour" class="panel-collapse">
		
						<div class="panel-body">
							<?php
							if(count($location_info) >0)
							{
								for($i_loc=0;$i_loc <= count($location_info)-1;$i_loc++)
								{
								  
								  $location_id = $location_info[$i_loc]['location_id'];
								  $quantity = $location_info[$i_loc]['quantity'];
								  
								  if($i_loc >0)
								  {
								   $add_btn ="style='display:none;'";;
								   $remove_btn ="style='display:block;'";
								  }
								  else
								  {
								   $add_btn ="style='display:block;'";
								   $remove_btn ="style='display:none;'";
								  }
							?>
							
								<div id="clone"  class="col-md-10 ">
									<div style="float: left;margin-right:20px; " class="form-group">
										<select name="sel_location[]" class="form-control">
											 <?php
											  $tbl="tbl_storage_locations";
											  $val ="location_id";
											  $show="location_addr";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="---- Select Storage Location ----";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($location_id), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
	
										</select>
									</div>
									<div style="float: left;margin-right:20px;" class="form-group ">
										<input placeholder="Quantity" class="form-control" maxlength="10" name="quantity[]"  type="text" value="<?=$quantity; ?>">
									</div>
								
								   <div class="form-group" style="float: left">
										<button <?=$add_btn; ?> class="btn_icons mr-lft-5 mr-botom-5" type="button" name="add_storage_location" id="add_storage_location" title="Add Storage Location"><i class=" glyphicon glyphicon-plus"></i></button>
										<button <?=$remove_btn; ?>  class="btn_icons remove_client_contact" type="button" id="remove_station" name="remove_station[]" title="Remove Storage Location" lang="1"><i class="glyphicon glyphicon-remove"></i></button>
									</div>
								
							</div>	
						 <?php
						 	}
						  }
						  else
						  {
						  ?>
						  
								<div id="clone"  class="col-md-10 ">
									<div style="float: left;margin-right:20px; " class="form-group">
										<select name="sel_location[]" class="form-control">
											 <?php
											  $tbl="tbl_storage_locations";
											  $val ="location_id";
											  $show="location_addr";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="---- Select Storage Location ----";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array(0), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
	
										</select>
									</div>
									<div style="float: left;margin-right:20px;" class="form-group ">
										<input placeholder="Quantity" class="form-control" maxlength="10" name="quantity[]"  type="text">
									</div>
								
								   <div class="form-group" style="float: left">
										<button class="btn_icons mr-lft-5 mr-botom-5" type="button" name="add_storage_location" id="add_storage_location" title="Add Storage Location"><i class=" glyphicon glyphicon-plus"></i></button>
										<button  style="display:none;" class="btn_icons remove_client_contact" type="button" id="remove_station" name="remove_station[]" title="Remove Storage Location" lang="1"><i class="glyphicon glyphicon-remove"></i></button>
									</div>
								
							</div>	


						  <?php
						  }
						 ?>		
							<!-- Storage Location --->
								<div id="insert_flag" class="clear"></div>
						</div>	
												
						<div class="clear"></div>
						<div class="col-md-12 text-right" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>     
								<a href="manage_books.php" class="btn btn-primary">Cancel</a>	
							</div>
						</div>
						
					  </div>
					</div>
					<!--End Of Sectio4-->	
					
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
	
</div>
</form>  
  </div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  

  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
