<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($step=="edit_info") {echo "Update Author";}else{echo "Add New Author";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Author</a></li>
  </ol>
</section>




<!-- Main content -->
	<section class="content">
	
			<form class="form-vertical" id="frm_new_publisher" action="add_new_author.php" method="post">
				<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_author_publisher";}else{echo "add_author_publisher";}?>" />
			<input name="pub_auth_id" id="pub_auth_id" type="hidden" value="<?=$pub_auth_id;?>">

						<!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  
					<div class="col-md-6">
					<div class="col-md-12">
							<div class="form-group"> 
								<label for="Admin_first_name">Author Name</label> 
								<input class="form-control" maxlength="250" name="publisher_name" id="publisher_name" type="text" value="<?=$publisher_list->name; ?>">              
								
							</div>
							<div class="clear"></div>

							<div class="form-group"> 
								<label for="Admin_first_name">Address</label> 
								<input type="text" size="50" name="address" id="address"  class="txt_field_font form-control" value="<?=$publisher_list->address; ?>"/>  
							</div>
							<div class="clear"></div>
							
							
									
						</div>
						
						<div class="col-md-12 ">
							<div class="form-group"> 
								<label for="Admin_first_name">Contact No.</label> 
								<input required class="form-control" maxlength="11" name="contact_no" id="contact_no" type="text" value="<?=$publisher_list->contact_no; ?>">              
								
							</div>
							<div class="clear"></div>
						

							<div class="form-group"> 
								<label for="Admin_first_name">Email ID</label> 
								
								<input type="text" size="100" name="pub_email_id" id="pub_email_id"  class="txt_field_font form-control" value="<?=$publisher_list->email_id; ?>"/>              
								
							</div>
							<div class="clear"></div>

							

						</div>
						</div>
						
						<div class="col-md-6">
					<div class="col-md-12">
							<div class="form-group"> 
								<label for="Admin_first_name">Author Name (Marathi)</label> 
								<input class="form-control" maxlength="250" name="publisher_name_mr" id="publisher_name_mr" type="text" value="<?=$publisher_list->name_mr; ?>">              
								
							</div>
							<div class="clear"></div>

							<div class="form-group"> 
								<label for="Admin_first_name">Address (Marathi)</label> 
								<input type="text" size="50" name="address_mr" id="address_mr"  class="txt_field_font form-control" value="<?=$publisher_list->address_mr; ?>"/>  
							</div>
							<div class="clear"></div>
							
							
									
						</div>
						
						
						</div>
						
						
						  </div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer text-right">
					<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
					<button class="btn" type="reset" name="yt1">Reset</button>
					<a href="manage_authors.php" class="btn btn-primary">Cancel</a>
								
					</div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
						          
	</section>
</div>
<!-- /.content-wrapper -->


