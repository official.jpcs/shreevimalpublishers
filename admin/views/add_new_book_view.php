<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Add New Book <?php echo $book_id=$_POST['update_id'];
		echo $pages=$_POST['pages'];?>
	<small><a class="btn btn-primary" href="manage_books.php">Manage Book List</a></small>
  </h1>
 
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
<div id="exTab3" class="container col-xs-12">	
<ul  class="nav nav-pills" style="margin-top: 10px;">
			<li class="active"><a  href="#general_info" data-toggle="tab">GENERAL INFORMATION</a></li>
			<li><?php if($book_id!=NULL){?>
			<a href="#publisher_info" data-toggle="tab">PUBLISHER'S INFORMATION</a> 
			<?php } else{echo"<a>PUBLISHER'S INFORMATION</a>";}?></li>
			<li><?php if($book_id!=NULL){?><a href="#book_info" data-toggle="tab">BOOK DETAILS</a><?php } else{echo"<a>BOOK DETAILS</a>";}?></li>
			<li><?php if($book_id!=NULL){?><a href="#storage_info" data-toggle="tab">STORAGE DETAILS</a><?php } else{echo"<a>STORAGE DETAILS</a>";}?></li>

			<li><?php if($book_id!=NULL){?><a href="#related_books" data-toggle="tab">RELATED BOOKS</a><?php } else{echo"<a>RELATED BOOKS</a>";}?></li>
			
			<li><?php if($book_id!=NULL){?><a href="#book_images" data-toggle="tab">BOOK IMAGES</a><?php } else{echo"<a>BOOK IMAGES</a>";}?></li>
			
		</ul>
<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
						<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_book";}else{echo "add_book";}?>" />
						<input name="book_id" id="book_id" type="hidden" value="<?=$book_id;?>">
						<input name="pages" id="pages" type="hidden" value="<?php if(isset($_POST['pages'])) echo $_POST['pages']; else echo '1';?>">
				
		
		
<div class="tab-content clearfix">
	  <div class="tab-pane active" id="general_info">
          
		  <div class="box">
		 <div class="box-body">
		  
		  
					<!--Start Of Section1 : General Information-->
					 <div class="panel panel-default">
						  <div class="panel-heading">
								<div class="w40L">
									<h4 class="panel-title">General Information</h4>
								</div>
								
								
						  </div>
						  <div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
											  
								<div class="col-md-6 ">
									
									<div class="form-group div_book_name"> 
										<label for="Admin_first_name">Book Name</label> 
										<input placeholder="" class="form-control" maxlength="255" name="book_name" id="book_name" type="text" value="<?=$result_book_info[0]['book_title']; ?>">              
										<span class="help-block error book_name" style="display: none">Please provide Book Name.</span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group div_book_name"> 
										<label for="Admin_first_name">Book Name(in Marathi)</label> 
										<input placeholder="" class="form-control" maxlength="255" name="book_name_mr" id="book_name_mr" type="text" value="<?=$result_book_info[0]['book_title_mr']; ?>">              
										<span class="help-block error book_name_mr" style="display: none">Please provide Book Name.</span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_author"> 
										<label for="Admin_first_name">Author</label> 
										
										<select name="author" id="author" class="form-control"> 
										 <?php
										  $tbl="tbl_authors_publishers";
										  $val ="id";
										  $show="name";
										  $status="is_deleted";
										  $clause =" `is_deleted` ='0' AND `user_type`='2'";
										  $FirstNode="------------------ Select Author -----------------";
										  
										  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('id',$result_book_info[0]['author_id']), $print = 0, $status, $FirstNode,$prefix='');
										 ?>
										</select>    
										<span class="help-block error author" style="display: none">Please select Book Author.</span>        
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Compiled by</label> 
										<input placeholder="" class="form-control" maxlength="150" name="compile_by" id="compile_by" type="text" value="<?=$result_book_info[0]['compile_by']; ?>">              
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Translated by</label> 
									    <input placeholder="" class="form-control" maxlength="150" name="translate_by" id="translate_by" type="text" value="<?=$result_book_info[0]['translated_by']; ?>">              
									</div>
									
								</div>
								
								<div class="col-md-6 ">
								<div class="clear"></div>
		
									<div class="form-group div_book_language"> 
										<label for="Admin_first_name">Language <?= $result_book_info[0]['book_language']; ?></label>
										&nbsp;
										<a data-target="#saleModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
											<i class="fa fa-plus" aria-hidden="true"></i>
										</a>																						
										
										<select name="book_language" id="book_language" class="form-control"> 
										 <?php
										  $tbl="tbl_languages";
										  $val ="language_id";
										  $show="title";
										  $status="is_deleted";
										  $clause =" `is_deleted` ='0'";
										  $FirstNode="------------------ Select Language -----------------";
										  $order_by = "language_id ASC" ;
										  
										  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('id',$result_book_info[0]['sel_language_id']), $print = 0, $status, $FirstNode,$prefix='',$order_by);
										 ?>

										</select>  
										<span class="help-block error book_language" style="display: none">Please select Book Language.</span>         
									</div>
										
									<div class="clear"></div>
		
									<div class="form-group div_book_mrp"> 
										<label for="Admin_first_name">MRP </label> Rs. 
										<input placeholder="" class="form-control" maxlength="8" name="book_mrp" id="book_mrp" type="text" value="<?=$result_book_info[0]['book_mrp']; ?>">              
										<span class="help-block error book_mrp" style="display: none">Please provide Book MRP Price.</span> 
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_book_edition"> 
										<label for="Admin_first_name">Edition</label> 
										<input placeholder="" class="form-control" maxlength="150" name="book_edition" id="book_edition" type="text" value="<?=$result_book_info[0]['book_edition']; ?>">              
										<span class="help-block error book_edition" style="display: none">Please provide Book Edition.</span> 
									</div>
									<div class="clear"></div>

								<!-- 	<div class="form-group col-md-6"> 
										<label for="Admin_first_name">Upload Book Thumbnail</label> 
											
											<input placeholder="" class="" maxlength="150" name="book_thumbnail" id="book_thumbnail" type="file">               
											<span class="help-block error thumbnail" style="display: none">Please provide Book Thumbnail.</span> 
									</div> -->
									<?php 
									 /*if($result_book_info[0]['book_thumbnail']!='' || !empty($result_book_info[0]['book_thumbnail']))
									 {
									 	?>
										<!-- <img width="50" src="<?=BOOK_THUMBNAIL_FOLDER_HTTP.$result_book_info[0]['book_thumbnail']; ?>" />  -->

									 <?php 		
									 }*/
									 ?>


									<div class="clear"></div>
									
									<div class="form-group" style="padding-top: 30px;"> 
									<input name="is_upcoming_book" id="is_upcoming_book" type="checkbox" value="1" <?php if($result_book_info[0]['is_upcoming_book']=='1') echo"checked"; ?>>
										<label for="Admin_first_name">Is Upcoming Book</label> 
									                  
									</div>
									<div class="clear"></div>

									<div class="form-group" style="padding-top: 30px;"> 
									<input name="is_book_visible" id="is_book_visible" type="checkbox" value="1" <?php if($result_book_info[0]['is_book_visible']=='1') echo"checked"; ?>>
										<label >Make Visible</label> 
									                  
									</div>
									<div class="clear"></div>

								
								</div>
								
								<div class="col-md-12 ">
								
									<div class="form-group"> 
										<label for="Admin_first_name">Book Description</label>
										<textarea class="form-control ckeditor" style="width:100%"  rows="5" name="book_description"><?=$result_book_info[0]['translated_by']; ?></textarea>
										              
									</div>
								
								</div>
								
								<div class="clear"></div>							  

								
				
							</div>
							<div class="clear"></div>
						<div class="col-md-6 col-md-offset-6" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>            
							</div>
						</div>
						  </div>
						</div>
		  
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
      </div>
	

	<div class="tab-pane" id="publisher_info">
          
		  <div class="box">
		 <div class="box-body">
		 <!--Start Of Section2 : Publisher's Information-->				
					 <div class="panel panel-default">
						  <div class="panel-heading">
								<div class="w40L">
									<h4 class="panel-title">Publisher's Information</h4>
								</div>
								
		
						  </div>
						  <div id="collapseTwo" class="panel-collapse ">
			
								<div class="panel-body">
												  
									<div class="col-md-6 ">
										
										<div class="form-group div_book_publisher"> 
											<label for="Admin_first_name">Publisher's Name</label> 
		
												<select id="book_publisher" name="book_publisher" class="form-control"> 
												 <?php
												  $tbl="tbl_authors_publishers";
												  $val ="id";
												  $show="name";
												  $status="is_deleted";
												  $clause =" `is_deleted` ='0' AND `user_type`='1'";
												  $FirstNode="------------------ Select Publisher -----------------";
												  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('id',$result_book_info[0]['publisher_id']), $print = 0, $status, $FirstNode,$prefix='');
												 ?>
												</select>          
											<span class="help-block error book_publisher" style="display: none">Please select Book Publisher.</span> 
										</div>
										<div class="clear"></div>
			
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Address</label> 
											<input placeholder="" class="form-control"  name="pub_addr" id="pub_addr" type="text" readonly="" value="<?=$result_book_info[0]['pub_addr']; ?>">              
											
										</div>
										<div class="clear"></div>
										
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Contact No.</label> 
											<input placeholder="" class="form-control" maxlength="50" name="pub_contact_no" id="pub_contact_no" type="text" readonly="" value="<?=$result_book_info[0]['pub_contact']; ?>">              
											
										</div>
										<div class="clear"></div>
			
												
									</div>
									
									<div class="col-md-6 ">
									
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Email</label> 
											<input placeholder="" class="form-control"  name="pub_email" id="pub_email" type="text" readonly="" value="<?=$result_book_info[0]['pub_email']; ?>">              
											<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
										</div>
										<div class="clear"></div>
			
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Website</label> 
											<input placeholder="" class="form-control"  name="pub_website" id="pub_website" type="text" readonly="" value="<?=$result_book_info[0]['pub_website']; ?>">              
											<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
										</div>
										<div class="clear"></div>

									</div>
									
									<div class="clear"></div>							  
										

								</div>
							
							<div class="clear"></div>
						<div class="col-md-6 col-md-offset-6" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>            
							</div>
						</div>
						  </div>
						</div>
					<!--End Of Section2-->		
		
		 
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
    </div>
	
	
    <div class="tab-pane" id="book_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section3 : Book Details-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							<h4 class="panel-title">Book Details</h4>
						</div>
						
					  </div>
					  <div id="collapseThree" class="panel-collapse ">
		
							<div class="panel-body">
								<div class="col-md-6">
									<div class="form-group  div_isbn_no"> 
										<label for="Admin_first_name">ISBN No.#</label> 
										<input placeholder="" class="form-control" maxlength="50" name="isbn_no" id="isbn_no" type="text" value="<?=$result_book_info[0]['isbn_no']; ?>">              
										<span class="help-block error isbn_no" style="display: none">Provide ISBN Number.</span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_book_category"> 
										<label for="Admin_first_name">Book Category</label> 
		
											<select id="book_category" name="book_category" class="form-control"> 
											 <?php
											  $tbl="tbl_book_categories";
											  $val ="category_id";
											  $show="category_title";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="------------------ Select Book Category -----------------";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('category_id',$result_book_info[0]['category_id']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
											</select>          
										<span class="help-block error book_category" id="Admin_first_name_em_" style="display: none">Please Select Book Category.</span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Dimension Width</label> (in mm) 
										<input placeholder="" class="form-control" maxlength="10" name="dim_width" id="dim_width" type="text" value="<?=$result_book_info[0]['dim_width']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Dimension Height </label> (in mm) 
										<input placeholder="" class="form-control" maxlength="10" name="dim_height" id="dim_height" type="text" value="<?=$result_book_info[0]['dim_height']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
		
									<div class="form-group"> 
										<label for="Admin_first_name">Dimension Depth</label> 
										<input placeholder="" class="form-control" maxlength="10" name="dim_depth" id="dim_depth" type="text" value="<?=$result_book_info[0]['dim_depth']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_no_of_pages"> 
										<label for="Admin_first_name">Number of Pages</label> 
										<input placeholder="" class="form-control" maxlength="6" name="no_of_pages" id="no_of_pages" type="text" value="<?=$result_book_info[0]['no_of_pages']; ?>">              
										<span class="help-block error no_of_pages" id="Admin_first_name_em_" style="display: none">Provide number of pages. </span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_binding_type"> 
										<label for="Admin_first_name">Binding Type</label> 
											<select id="binding_type" name="binding_type" class="form-control"> 
											 <?php
											  $tbl="tbl_binding_type";
											  $val ="binding_id";
											  $show="title";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0' AND `type`='1'";
											  $FirstNode="------------------ Select Binding Type -----------------";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('binding_id',$result_book_info[0]['binding_id']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
											</select>          
		
										<span class="help-block error binding_type" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_cover_type"> 
										<label for="Admin_first_name">Cover Type</label> 
											<select id="cover_type" name="cover_type" class="form-control"> 
											 <?php
											  $tbl="tbl_binding_type";
											  $val ="binding_id";
											  $show="title";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0' AND `type`='2'";
											  $FirstNode="------------------ Select Cover Type -----------------";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('binding_id',$result_book_info[0]['cover_id']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
											</select>          
										<span class="help-block error cover_type" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
									<div class="form-group"> 
										<label for="Admin_first_name">Weight </label> (in gm) 
										<input placeholder="" class="form-control" maxlength="10" name="weight" id="weight" type="text" value="<?=$result_book_info[0]['book_weight']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
											
								</div>
								
								<div class="col-md-6 ">
								
		
									<div class="form-group"> 
										<label for="Admin_first_name">Production Cost</label> 
										Rs.<input placeholder="" class="form-control" maxlength="6" name="prod_cost" id="prod_cost" type="text" value="<?=$result_book_info[0]['production_cost']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Translated? &nbsp;&nbsp;</label> <br/>
										<input  name="translated" type="radio" value="1"> &nbsp;Yes            
										<input  name="translated" type="radio" checked="checked" value="0">&nbsp;No   
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>             
									</div>
									<div class="clear"></div>
		
									<div class="form-group" id="trans_frm_book"> 
										<label for="Admin_first_name">Translated From (Book Name)</label> 
										<input placeholder="" class="form-control" maxlength="150" name="trans_frm_book" id="trans_frm_book" type="text" value="<?=$result_book_info[0]['translated_from_book']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group" id="trans_frm_lng"> 
										<label for="Admin_first_name">Translated From Language(Orignal Book Language)</label> 
										<select name="transl_frm_lng" id="transl_frm_lng" class="form-control">
										 <option value="">--- Select Language ---</option>
										 <?php
										  $language_list=unserialize(LANGUAGE);
										  for($i_ln=1;$i_ln<=count($language_list);$i_ln++)
										  {
										   ?>
											<option value="<?=$i_ln; ?>" <?php if($result_book_info[0]['translated_frm_lang']==$i_ln){ echo "selected";} ?>><?=$language_list[$i_ln];?></option>
										   <?php 
										  }
										 ?>
										</select>
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Publication Date ( 'DD-MM-YYYY')</label> 
										<input placeholder="" class="form-control" maxlength="50" name="pub_date" id="pub_date" type="text" value="<?=$result_book_info[0]['publication_date']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Co-Author</label> 
										<input placeholder="" class="form-control" maxlength="150" name="co_author" id="co_author" type="text" value="<?=$result_book_info[0]['co_author']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Contributor</label> 
										<input placeholder="" class="form-control" maxlength="200" name="contributor" id="contributor" type="text" value="<?=$result_book_info[0]['contributor']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Editor</label> 
										<input placeholder="" class="form-control" maxlength="150" name="editor" id="editor" type="text" value="<?=$result_book_info[0]['editor']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_copyright"> 
										<label for="Admin_first_name">Copyright</label> 
										<input placeholder="" class="form-control" maxlength="150" name="copyright" id="copyright" type="text" value="<?=$result_book_info[0]['copyright']; ?>">              
										<span class="help-block error copyright" id="Admin_first_name_em_" style="display: none">Provide Copyright name.</span>  
									</div>
									<div class="clear"></div>
									
		
								</div>
								
								<div class="clear"></div>									 
								</div>
								
								<div class="clear"></div>
						<div class="col-md-6 col-md-offset-6" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>            
							</div>
						</div>
						
					  </div>
					</div>
					<!--End Of Sectio3-->		
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
    <div class="tab-pane" id="storage_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section4 : Storage Location-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							<h4 class="panel-title">Storage Details</h4>
						</div>
						
					  </div>
					  <div id="collapseFour" class="panel-collapse">
		
						<div class="panel-body">
							<?php
							if(count($location_info) >0)
							{
								for($i_loc=0;$i_loc <= count($location_info)-1;$i_loc++)
								{
								  
								  $location_id = $location_info[$i_loc]['location_id'];
								  $quantity = $location_info[$i_loc]['quantity'];
								  
								  if($i_loc >0)
								  {
								   $add_btn ="style='display:none;'";;
								   $remove_btn ="style='display:block;'";
								  }
								  else
								  {
								   $add_btn ="style='display:block;'";
								   $remove_btn ="style='display:none;'";
								  }
							?>
							
								<div id="clone"  class="col-md-10 ">
									<div style="float: left;margin-right:20px; " class="form-group">
										<select name="sel_location[]" class="form-control">
											 <?php
											  $tbl="tbl_storage_locations";
											  $val ="location_id";
											  $show="location_addr";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="---- Select Storage Location ----";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($location_id), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
	
										</select>
									</div>
									<div style="float: left;margin-right:20px;" class="form-group ">
										<input placeholder="Quantity" class="form-control" maxlength="10" name="quantity[]"  type="text" value="<?=$quantity; ?>">
									</div>
								
								   <div class="form-group" style="float: left">
										<button <?=$add_btn; ?> class="btn_icons mr-lft-5 mr-botom-5" type="button" name="add_storage_location" id="add_storage_location" title="Add Storage Location"><i class=" glyphicon glyphicon-plus"></i></button>
										<button <?=$remove_btn; ?>  class="btn_icons remove_client_contact" type="button" id="remove_station" name="remove_station[]" title="Remove Storage Location" lang="1"><i class="glyphicon glyphicon-remove"></i></button>
									</div>
								
							</div>	
						 <?php
						 	}
						  }
						  else
						  {
						  ?>
						  
								<div id="clone"  class="col-md-10 ">
									<div style="float: left;margin-right:20px; " class="form-group">
										<select name="sel_location[]" class="form-control">
											 <?php
											  $tbl="tbl_storage_locations";
											  $val ="location_id";
											  $show="location_addr";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="---- Select Storage Location ----";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array(0), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
	
										</select>
									</div>
									<div style="float: left;margin-right:20px;" class="form-group ">
										<input placeholder="Quantity" class="form-control" maxlength="10" name="quantity[]"  type="text">
									</div>
								
								   <div class="form-group" style="float: left">
										<button class="btn_icons mr-lft-5 mr-botom-5" type="button" name="add_storage_location" id="add_storage_location" title="Add Storage Location"><i class=" glyphicon glyphicon-plus"></i></button>
										<button  style="display:none;" class="btn_icons remove_client_contact" type="button" id="remove_station" name="remove_station[]" title="Remove Storage Location" lang="1"><i class="glyphicon glyphicon-remove"></i></button>
									</div>
								
							</div>	


						  <?php
						  }
						 ?>		
							<!-- Storage Location --->
								<div id="insert_flag" class="clear"></div>
						</div>	
												
						<div class="clear"></div>
						<div class="col-md-6 col-md-offset-6" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>            
							</div>
						</div>
						
					  </div>
					</div>
					<!--End Of Sectio4-->	
					
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
	
</div>
</form>  
  </div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  

  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
