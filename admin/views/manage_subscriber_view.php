<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> Manage Subscriber </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Subscriber</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>






<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-hover">
			<thead>
				 <?php if(count($result_subscriber_list)>0){?>                       
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</th>
								<th id="sub_admin_grid_c0">Subscriber Email</th>
								<th id="sub_admin_grid_c1">Subscription Date</th>
								<th id="sub_admin_grid_c3">Action</th>
								
							</tr>
							</thead>
							<tbody>
                                                         
							<?php 
							
							$sr=1;
							for($i=0;$i<count($result_subscriber_list);$i++){
							
							if($_REQUEST['pages']!="" &&$_REQUEST['pages']!=1) 
                            {
                                    $sr=($per_page * ($_REQUEST['pages']-1))+($i+1);
                            }
							?>	
								<tr class="odd">
									<td style="width:5%"><?=$sr;?></td>
									<td style="width:10%"><?=$result_subscriber_list[$i]->subscriber_email;?></td>
									<td style="width:10%"><?=$result_subscriber_list[$i]->subscription_date;?></td>
									
									<td style="width:12%">
									
										<a title="Delete"  class="delete_info" id="<?=$result_subscriber_list[$i]->subscriber_id;?>" rel="manage_subscriber.php">Delete</a>																							
									</td>
								</tr>
								<?php 
								
								$sr=$sr+1;
								
								}?>								
							</tbody>
							<?php } else{?>
							<tr><td>No Data</td></tr>
							<?php } ?>
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
<!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 
					 $pages = ceil(($total_record/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['pages'])) 
                         {

                             if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_subscriber.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_subscriber.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_subscriber.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_subscriber.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->



