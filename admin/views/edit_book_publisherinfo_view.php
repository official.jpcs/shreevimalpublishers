<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
 
 
  
	<div style="float:left" >
		<h4> Edit Book >> <?php echo $result_book_info[0]['book_title']; ?></h4>
	</div>
	 <?php $book_id=$_POST['update_id'];
		//echo $pages=$_POST['pages'];?>
	<div style="float:right" >
		<small><a class="btn btn-primary pull-right" href="manage_books.php?pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>">Manage Book List</a></small>
	</div>
  
 
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
<div id="exTab3" class="container col-xs-12">	
<?php
	$tab_edit ="publisherinfo_info";
	include("includes/book_edit_tab.php");
?>

<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
	<input type="hidden" name="step" id="step" value="update_publisher_info" />
	<input name="book_id" id="book_id" type="hidden" value="<?=$book_id;?>">
	<input name="pages" id="pages" type="hidden" value="<?php if(isset($_POST['pages'])) echo $_POST['pages']; else echo '1';?>">
				
		
		
<div class="tab-content clearfix">
	

	<div class="tab-pane active" id="publisher_info">
          
		  <div class="box">
		 <div class="box-body">
		 <!--Start Of Section2 : Publisher's Information-->				
					 <div class="panel panel-default">
						  <div class="panel-heading">
								<div class="w40L">
									<h4 class="panel-title">Publisher's Information</h4>
								</div>
								
		
						  </div>
						  <div id="collapseTwo" class="panel-collapse ">
			
								<div class="panel-body">
												  
									<div class="col-md-6 ">
										
										<div class="form-group div_book_publisher"> 
											<label for="Admin_first_name">Publisher's Name</label> 
		
												<select id="book_publisher" name="book_publisher" class="form-control"> 
												 <?php
												  $tbl="tbl_authors_publishers";
												  $val ="id";
												  $show="name";
												  $status="is_deleted";
												  $clause =" `is_deleted` ='0' AND `user_type`='1'";
												  $FirstNode="------------------ Select Publisher -----------------";
												  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('id',$result_book_info[0]['publisher_id']), $print = 0, $status, $FirstNode,$prefix='');
												 ?>
												</select>          
											<span class="help-block error book_publisher" style="display: none">Please select Book Publisher.</span> 
										</div>
										<div class="clear"></div>
			
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Address</label> 
											<input placeholder="" class="form-control"  name="pub_addr" id="pub_addr" type="text" readonly="" value="<?=$result_book_info[0]['pub_addr']; ?>">              
											
										</div>
										<div class="clear"></div>
										
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Contact No.</label> 
											<input placeholder="" class="form-control" maxlength="50" name="pub_contact_no" id="pub_contact_no" type="text" readonly="" value="<?=$result_book_info[0]['pub_contact']; ?>">              
											
										</div>
										<div class="clear"></div>
			
												
									</div>
									
									<div class="col-md-6 ">
									
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Email</label> 
											<input placeholder="" class="form-control"  name="pub_email" id="pub_email" type="text" readonly="" value="<?=$result_book_info[0]['pub_email']; ?>">              
											<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
										</div>
										<div class="clear"></div>
			
										<div class="form-group"> 
											<label for="Admin_first_name">Publisher's Website</label> 
											<input placeholder="" class="form-control"  name="pub_website" id="pub_website" type="text" readonly="" value="<?=$result_book_info[0]['pub_website']; ?>">              
											<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
										</div>
										<div class="clear"></div>

									</div>
									
									<div class="clear"></div>							  
										

								</div>
							
							<div class="clear"></div>
						<div class="col-md-12 text-right" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>      
								<a href="manage_books.php" class="btn btn-primary">Cancel</a>			
							</div>
						</div>
						  </div>
						</div>
					<!--End Of Section2-->		
		
		 
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
    </div>
	

</div>
</form>  
  </div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  

  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
