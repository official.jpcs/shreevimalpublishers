<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Manage Languages
	<small><a class="btn btn-primary" href="add_edit_language.php">Add New Language</a></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Book Languages</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>





<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-striped table-hover">
		
				<?php 
						 if($total_record >0)
						 {
						?>
						  <thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</a></th>
								<th id="sub_admin_grid_c1">Language </a></th>
								
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
								<?php 
								for($i_language=0;$i_language <=$total_record-1;$i_language++)
								{
								 $sr=$i_language+1;
								?>
									<tr class="odd">
										<td style="width:10%"><?=$sr; ?></td>
										<td style="width:50%"><?=$result_laguages_list[$i_language]->title; ?></td>
	
									   <td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_laguages_list[$i_language]->language_id; ?>" rel="add_edit_language.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_laguages_list[$i_language]->language_id; ?>" rel="manage_languages.php">Delete</a>													                                       </td>
									</tr>
								 <?php 
								 }
								 ?>	
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Language list not available to display. </td></tr>
							<?php 
							}
							?>
							
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->






<?php

/*



<!--main container-->
<div class="container-fluid">
  <div class="row"><div class="col-md-16"> <!--dashboard container-->
<div class="dashboard">
 <h5 class="text-left">Manage Book Languages</h5>
 <div class="clear"></div>
    <div class="tenant-dashboard contractor-issues clearfix"> 
           <!--flash messages section-->
             
        <a href="add_edit_language.php" class="btn btn-default pull-right">
           Add New Language
        </a>
        <div class="clear"></div>
        <div class="panel-group form-top" id="accordion">
          <div class="panel panel-default">
            <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="form-wrap clearfix"> 
                <!--------Search--view---------->
					<form class="form-vertical" id="search_subadmin" action="#" method="get">
					<div class="col-md-4">
						<div class="form-group"> 
							<label for="Admin_name">Language Title</label>
							<input placeholder="" class="form-control" maxlength="50" name="Admin[name]" id="Admin_name" type="text"> 
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group"> 
							<label for="text">&nbsp;</label>        
							<div class="clear"></div>
							<button class="btn" type="submit" name="yt0">Submit</button>                    
							<button class="reset-form btn" type="reset" name="yt1">Reset</button>    
						</div>
					</div>
					</form>           
            
                 <!--------Search--view---------->
               </div>
              </div>
            </div>
          </div>
        </div>

        <div class="clear"></div>

       <!--------list--view---------->
			<div class="list-view">
				
				<div class="clear"></div>
				<div class="grid-view clearfix" id="language_grid">
					
						<table class="items table table-striped table-bordered">
						<?php 
						 if($total_record >0)
						 {
						?>
						  <thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</a></th>
								<th id="sub_admin_grid_c1">Language </a></th>
								
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
								<?php 
								for($i_language=0;$i_language <=$total_record-1;$i_language++)
								{
								 $sr=$i_language+1;
								?>
									<tr class="odd">
										<td style="width:10%"><?=$sr; ?></td>
										<td style="width:50%"><?=$result_laguages_list[$i_language]->title; ?></td>
	
									   <td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_laguages_list[$i_language]->language_id; ?>" rel="add_edit_language.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_laguages_list[$i_language]->language_id; ?>" rel="manage_languages.php">Delete</a>													                                       </td>
									</tr>
								 <?php 
								 }
								 ?>	
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Language list not available to display. </td></tr>
							<?php 
							}
							?>
							
						</table>
					<div class="keys" style="display:none" title="#"><span>6</span><span>5</span><span>4</span><span>3</span><span>1</span></div>
				</div>        
			</div>
       <!--------list--view----------> 

     </div>
</div>

<!--end dashboard container--> 
 </div>
</div>
</div>
<!--end main container--> 
*/?>