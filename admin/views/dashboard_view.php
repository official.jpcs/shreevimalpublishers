 
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  <h1>Dashboard</h1>
	</section>

<section class="content">
     <div class="row">
	 
            <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><i class="fa fa-inr" aria-hidden="true"></i> <?=$result["todays_sale_amount"] ? number_format($result["todays_sale_amount"] , 2, '.', '') : 0 ; ?></h3>
                  <p>Todays Sale</p>
                </div>
                <div class="icon">
                  <i class="fa fa-shopping-bag" aria-hidden="true"></i>

                </div>
               <a href="#" class="small-box-footer">&nbsp;<!--More info <i class="fa fa-arrow-circle-right"></i>--></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><i class="fa fa-inr" aria-hidden="true"></i> <?=$result["total_sale_amount"] ? number_format($result["total_sale_amount"] , 2, '.', '') : 0 ; ?></h3>
                  <p>Total Sale</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">&nbsp;<!--More info <i class="fa fa-arrow-circle-right"></i>--></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><i class="fa fa-book" aria-hidden="true"></i> <?=$result["todays_book_sold_out"] ? $result["todays_book_sold_out"] : 0; ?></h3>
                  <p>Todays Book Sold Out</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">&nbsp;<!--More info <i class="fa fa-arrow-circle-right"></i>--></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-12">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><i class="fa fa-book" aria-hidden="true"></i> <?=$result["total_book_sold_out"] ? $result["total_book_sold_out"] : 0; ?></h3>
                  <p>Total Book Sold Out</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">&nbsp;<!--More info <i class="fa fa-arrow-circle-right"></i>--></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->

		  
		  <div class="row">
		 <!-- -------------- Latest Sales Log ---------->
		  <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Latest Sales Log</h3>
            </div> <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-responsive">
			
              	<?php if(count($result_sales_list) >0){ ?>
				
				<thead>
					<tr>
					<th>Sale Date</th>
					<th>Sale Type</th>
					<th>Total Amt</th>
					<th>Customer Name</th>
					<th>Customer Contact</th>
					</tr>
				</thead>
				
				<?php	for($i_book=0;$i_book <=count($result_sales_list)-1;$i_book++){
							if($i_book>4) break;
							$tax_type=$result_sales_list[$i_book]['tax_type']==1? "Cash":"Credit";?>
						
				<tbody>
						<tr>
						<td style="width:20%"><?=$result_sales_list[$i_book]['sale_date']; ?></td>
						<td style="width:10%"><?=$tax_type; ?></td>
						<td style="width:20%">Rs.<?=$result_sales_list[$i_book]['total_amount'];?></td>
						<td style="width:30%"><?=$result_sales_list[$i_book]['customer_name']; ?></td>
						<td style="width:10%"><?=$result_sales_list[$i_book]['cust_contact_no'];?></td>
						</tr>
							
				<?php
					}// For Loop End
				} //IF end
				else { ?>
					<tr><td>Sales list not available to display. </td></tr><?php }?>
			</tbody>
            </table>
            </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col-md-6 -->
		
		
		
		<!-- -------------- Latest Upcoming Books ---------->
		  <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Latest Upcoming Books</h3>
            </div> <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-responsive">
			
              	<?php if($total_booksUpcomingBook >0) { ?>
				
				<thead>
					<tr>
					<th>Book Image</th>
					<th>#ISBN</th>
					<th>Book Name</th>
					<th>Author</th>
					<th>Price</th>
					</tr>
				</thead>
				
				<?php	for($i_book=0;$i_book <=$total_booksUpcomingBook-1;$i_book++){ ?>
						
				<tbody>
						<tr>
						<td style="width:20%"><img src="<?php if($result_booksUpcomingBook_list[$i_book]['book_thumbnail']==BOOK_THUMBNAIL_FOLDER_HTTP) echo BOOK_THUMBNAIL_FOLDER_HTTP."default.png"; else echo $result_booksUpcomingBook_list[$i_book]['book_thumbnail']; ?>" width="40px" height="40px"/></td>
						<td style="width:10%"><?=$result_booksUpcomingBook_list[$i_book]['isbn_no']; ?>	</td>
						<td style="width:30%"><?=$result_booksUpcomingBook_list[$i_book]['book_title']; ?></td>
						<td style="width:15%"><?=$result_booksUpcomingBook_list[$i_book]['author_name']; ?></td>
						<td style="width:10%">Rs.<?=$result_booksUpcomingBook_list[$i_book]['book_mrp']; ?></td>
						</tr>
							
				<?php
					}// For Loop End
				} //IF end
				else { ?>
					<tr><td>Upcoming Book list not available to display. </td></tr><?php }?>
			</tbody>
            </table>
            </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col-md-6 -->
		
		
		
				
		<!-- -------------- >Latest Contact Unread Messages ---------->
		  <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Contact Unread Messages</h3>
            </div> <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-responsive">
			
              	<?php if($result_contact_list >0) { ?>
				
				<thead>
					<tr>
					<th >Contact Name</a></th>
					<th >Contact No</a></th>
					<th >Contact Email</a></th>
					<th >Message</a></th>
					</tr>
				</thead>
				<tbody>
				<?php	for($i_contact=0;$i_contact <=count($result_contact_list);$i_contact++){
							if($i_contact>4)break; ?>
						
				
				
						<tr>
							<td><?=$result_contact_list[$i_contact]->contact_name; ?></td>
							<td><?=$result_contact_list[$i_contact]->contact_no; ?></td>
							<td><?=$result_contact_list[$i_contact]->contact_email; ?></td>
							<td><a title="Edit" class="edit_info" id="<?=$result_contact_list[$i_contact]->contact_id; ?>" rel="view_contact.php"><?=$result_contact_list[$i_contact]->contact_message; ?></a></td>
							
						</tr>
							
				<?php
					}// For Loop End
				} //IF end
				else { ?>
					<tr><td>Contact list not available to display. </td></tr><?php }?>
			</tbody>
            </table>
            </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col-md-6 -->
		
		
					
		<!-- -------------- Book Out Of Stock ---------->
		  <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border ">
              <h3 class="box-title">Book Out Of Stock</h3>
            </div> <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-bordered table-responsive">
			
              	<?php if($total_books >0){ ?>
				
				<thead>
					<tr>
					<th >ISBN No</a></th>
					<th >Book Title</a></th>
					<th >Book Qty</a></th>
					</tr>
				</thead>
				
				<?php	for($i_book=0;$i_book <=$total_books-1;$i_book++){ ?>
						
				<tbody>
						<tr>
							<td width="5%">#<?=$result_book_list[$i_book]['isbn_no']; ?></td>
							<td width="30%"><?=$result_book_list[$i_book]['book_title']; ?></td>
							<td width="5%"><?=$result_book_list[$i_book]['book_quantity']; ?></td></tr>
							
				<?php
					}// For Loop End
				} //IF end
				else { ?>
					<tr><td>Book Out Of Stock list not available to display. </td></tr><?php }?>
			</tbody>
            </table>
            </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col-md-6 -->
		
		
		
		</div><!-- /.row -->
		
</section>  
	
</div>	
	
	
	
	

  <!-- /.content-wrapper -->
