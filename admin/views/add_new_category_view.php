<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($step=="edit_info") {echo "Update Category";}else{echo "Add New Category";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Category</a></li>
  </ol>
</section>




<!-- Main content -->
	<section class="content">
	
			<form class="form-vertical" id="frm_new_category" action="add_edit_categories.php" method="post">
						<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_category";}else{echo "add_category";}?>" />
						<input   name="category_id" id="category_id" type="hidden" value="<?=$category_id;?>">
						
						<!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  
						<div class="col-md-6">
							<div class="form-group"> 
								<label for="Admin_name">Category Title</label>
									<input required class="form-control" maxlength="50" name="category_title" id="category_title" type="text" value="<?=$category->category_title; ?>"> 
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group"> 
								<label for="Admin_name">Category Title(Marathi)</label>
									<input  class="form-control" maxlength="50" name="category_title_mr" id="category_title_mr" type="text" value="<?=$category->category_title_mr; ?>"> 
							</div>
						</div>
						
						
						  </div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer text-right">
					<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>
						<a href="manage_categories.php" class="btn btn-primary">Cancel</a>		
					</div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
						          
	</section>
</div>
<!-- /.content-wrapper -->








