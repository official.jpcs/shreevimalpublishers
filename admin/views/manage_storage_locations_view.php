<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Manage Storage Locations
	<small><a class="btn btn-primary" href="add_edit_location.php">Add New Storage Locations</a></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Storage Locations</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>





<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-striped table-hover">
		
					
						<?php 
						 if($total_record >0)
						 {
						?>
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</a></th>
								<th id="sub_admin_grid_c1">Storage Location</a></th>
								<th id="sub_admin_grid_c1">Location Keyword</a></th>
								
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
							
								<?php 
								for($i_location=0;$i_location <=$total_record-1;$i_location++)
								{
								 $sr=$i_location+1;
								?>
								<tr class="odd">
									<td style="width:10%"><?=$sr; ?></td>
									<td style="width:50%"><?=$result_location_list[$i_location]->location_addr; ?></td>
									<td style="width:20%"><?=$result_location_list[$i_location]->location_keyword; ?></td>

									<td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_location_list[$i_location]->location_id; ?>" rel="add_edit_location.php">Edit</a> &nbsp;|&nbsp; 
										<a title="Delete"  class="delete_info" id="<?=$result_location_list[$i_location]->location_id; ?>" rel="manage_storage_locations.php">	Delete </a>									
									</td>
								</tr>
								<?php
								}
								?>									
							</tbody>
						<?php 
						}
						else
						{
						?>
						 <tr><td>Storage Location list not available to display. </td></tr>
						<?php 
						}
						?>
							
							
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->




<?php /*


<!--main container-->
<div class="container-fluid">
  <div class="row"><div class="col-md-16"> <!--dashboard container-->
<div class="dashboard">
 <h5 class="text-left">Manage Storage Locations</h5>
 <div class="clear"></div>
    <div class="tenant-dashboard contractor-issues clearfix"> 
           <!--flash messages section-->
             
        <a href="add_edit_location.php" class="btn btn-default pull-right">
           Add Storage Location
        </a>
        <div class="clear"></div>
        <div class="panel-group form-top" id="accordion">
          &nbsp;
        </div>

        <div class="clear"></div>

       <!--------list--view---------->
			<div class="list-view">
				
				<div class="clear"></div>
				<div class="grid-view clearfix" id="sub_admin_grid">
					
						<table class="items table table-striped table-bordered">
						<?php 
						 if($total_record >0)
						 {
						?>
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</a></th>
								<th id="sub_admin_grid_c1">Storage Location</a></th>
								<th id="sub_admin_grid_c1">Location Keyword</a></th>
								
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
							
								<?php 
								for($i_location=0;$i_location <=$total_record-1;$i_location++)
								{
								 $sr=$i_location+1;
								?>
								<tr class="odd">
									<td style="width:10%"><?=$sr; ?></td>
									<td style="width:50%"><?=$result_location_list[$i_location]->location_addr; ?></td>
									<td style="width:20%"><?=$result_location_list[$i_location]->location_keyword; ?></td>

									<td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_location_list[$i_location]->location_id; ?>" rel="add_edit_location.php">Edit</a> &nbsp;|&nbsp; 
										<a title="Delete"  class="delete_info" id="<?=$result_location_list[$i_location]->location_id; ?>" rel="manage_storage_locations.php">	Delete </a>									
									</td>
								</tr>
								<?php
								}
								?>									
							</tbody>
						<?php 
						}
						else
						{
						?>
						 <tr><td>Storage Location list not available to display. </td></tr>
						<?php 
						}
						?>
							
						</table>
					<div class="keys" style="display:none" title="#"><span>6</span><span>5</span><span>4</span><span>3</span><span>1</span></div>
				</div>        
			</div>
       <!--------list--view----------> 

     </div>
</div>

<!--end dashboard container--> 
 </div>
</div>
</div>
<!--end main container--> 
*/
?>