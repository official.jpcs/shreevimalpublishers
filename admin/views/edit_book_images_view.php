<style type="text/css">
.card-body {
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
}

.card {
    position: relative;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
    float: left;
    margin-right: 15px;
}
.bd-example {
    position: relative;
    padding: 1rem;
    margin: 1rem -15px 0;
    border: solid #f7f7f9;
    border-width: .2rem 0 0;
}

.card-img-top {
    width: 100%;
    border-top-left-radius: calc(.25rem - 1px);
    border-top-right-radius: calc(.25rem - 1px);
}

img {
    vertical-align: middle;
    border-style: none;
}	

img.lazy {
        width: 60%; 
        
        display: block;
		
/* optional way, set loading as background */
        background-image: url('images/loader.svg');
        background-repeat: no-repeat;
        background-position: 50% 50%;		
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
 
	<div style="float:left" >
		<h4> Edit Book >> <?php echo $result_book_info[0]['book_title']; ?></h4>
	</div>
	 <?php $book_id=$_POST['update_id'];
		  $page_no=$_POST['pages'];?>
	<div style="float:right" >
		<small><a class="btn btn-primary pull-right" href="manage_books.php?pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>">Manage Book List</a></small>
	</div>
  
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		
		
		
<div id="exTab3" class="container col-xs-12">	
<?php
	$tab_edit ="book_images";
	include("includes/book_edit_tab.php");
?>
				
		
		
<div class="tab-content clearfix">
	
	
    <div class="tab-pane active" id="storage_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section4 : Storage Location-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							
							<h4 class="panel-title">Upload Book Images</h4>
						</div>
						
					  </div>


					  <div id="collapseFour" class="panel-collapse">
		
						<div class="panel-body">
						  <div class="col-md-12 text-right ">

							<ul class="m-0" style="list-style-type: none;">
		                        
								<li class="">
									<div class="col-md-4 text-left" style="font-weight:bold;margin-left:-50px">Total Images : <?php echo count($book_images); ?> </div>
									<div class="col-md-8 mb-3" style="margin-bottom:10px;">
                                    <button  type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#exampleModal"><i class="feather-plus"></i> Upload Files </button>
                                    </div>                       
                                </li>		                        
		                    </ul>
		                   </div>

<?php
$srno =0;
if(count($book_images) >0)
{
	for($i_book=0;$i_book <=count($book_images)-1;$i_book++)
	{

	 $image_path = BOOK_OTHER_IMAGES_FOLDER_HTTP.$book_images[$i_book]['input_file'];	
	 $book_id= $book_images[$i_book]['book_id'];	
	 $file_id= $book_images[$i_book]['file_id'];						
	 $position= $book_images[$i_book]['position'];						

?>

	<div class="card " style="width: 30rem;margin-top:15px">
		
	<img class="lazy card-img-top" data-src ="<?=$image_path;?>"  style="height: 220px; width: 100%; display: block;" data-holder-rendered="true" />


		<!-- <img class="card-img-top" data-src="holder.js/100px180/" alt="100%x180" style="height: 220px; width: 100%; display: block;" src="<?php echo $image_path; ?>" data-holder-rendered="true"> -->

	  <div class="card-body">
	    <div class="col-md-6 pull-left">
	    	<select class="form-control book_position" book_id="<?php echo $book_id; ?>" file_id="<?php echo $file_id; ?>" >
	    		<option value="">-- Position --</option>
	    		<?php
	    		 for($i=1;$i<=20;$i++)
	    		 {
	    		     if($i==$position)
	    		     {
	    		         $sel= "selected='selected'";
	    		     }
	    		     else
	    		     {
	    		         $sel="";
	    		     }
	    		     
	    		 	echo "<option value='".$i."' $sel >".$i."</option>";
	    		 }
	    		?>
	    	</select>
	    </div>
	    <div class="col-md-6 pull-right text-right">
	    	<a style="cursor: pointer;" onclick="deleteImage('<?php echo $book_id; ?>','<?php echo $file_id; ?>')" class="deleteImage" >
	    		<i class="fa fa-trash-o" style="font-size:20px;color:red"></i>
	    	</a>
	    </div>
	  </div>
	</div>
    <?php
    }
}
?>        



		                    
		                   <div class="col-md-8" style="display:none">
		                   		<!-- Add images to <div class="fotorama"></div> -->
		                   	 <div  data-allowfullscreen="true" class="fotorama" data-nav="thumbs">	
	                         <?php 
	                        
	                          $srno =0;
	                            if(count($book_images) >0)
	                            {
	                                for($i_book=0;$i_book <=count($book_images)-1;$i_book++)
	                                {
								
										 $image_path = BOOK_OTHER_IMAGES_FOLDER_HTTP.$book_images[$i_book]['input_file'];							
	                                ?>

									
									  <a href="<?=$image_path;?>">
									  	<img src="<?=$image_path;?>" width="144" height="96">
									  </a>
									  
									
									<?php    
									}
								}
								else
								{
								echo 'No Record Available';
								}
								
							 ?>
							 </div>

		                   </div> 

						</div>
					  </div>		

 <?php 

//$srno =0;
  //   if(count($book_images) >0)
  //   {
  //       for($i_book=0;$i_book <=count($book_images)-1;$i_book++)
  //       {
?>
<!--<div class="fotorama">
  <img src="https://s.fotorama.io/1.jpg">
  <img src="https://s.fotorama.io/2.jpg">
</div>-->
<?php
// }
// }
?>
		                <!-- <div class="page-title-right">
		                    <ul class="m-0" style="list-style-type: none;">
		                        <li class="">
		                            <button  type="button" class="btn btn-primary mb-3 " data-toggle="modal" data-target="#exampleModal"><i class="feather-plus"></i> Upload File </button>
		                        </li>
		                    </ul>

		                

		                </div> -->


					 
					</div>
					<!--End Of Sectio4-->	
					
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
	
</div>

</div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  


  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- add model -->
    <style type="text/css">
        .font-color{
            font-size: 18px;
            padding: 0px 5px;
            box-sizing: border-box;
            cursor: pointer;
        }
    </style>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel">Upload File</h5>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
           
                 
                  <input type="hidden" value="<?php echo $book_id;?>" id="book_id"> 
                  <input type="hidden" value="<?php echo $page_no;?>" id="page_no"> 

                  
                  <div class="custom-file">
                    <input type="file" multiple  class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                  </div>


            
         </div>
         <div class="modal-footer">
            <button class="btn btn-primary " id="upload_campaign_file">Submit</button>
            <button type="button" class="btn btn-light waves-effect waves-light" data-dismiss="modal">Close</button>

        </div>
         
    </div>
</div>
</div>
