<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	View Book Detail
	<small><a class="btn btn-primary" href="manage_books.php">Manage Book List</a></small>
  </h1>
 
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
<div id="exTab3" class="container col-xs-12">	
<ul  class="nav nav-pills" style="margin-top: 10px;">
			<li class="active"><a  href="#general_info" data-toggle="tab">GENERAL INFORMATION</a></li>
			<li><?php if($book_id!=NULL){?>
			<a href="#publisher_info" data-toggle="tab">PUBLISHER'S INFORMATION</a> 
			<?php } else{echo"<a>PUBLISHER'S INFORMATION</a>";}?></li>
			<li><?php if($book_id!=NULL){?><a href="#book_info" data-toggle="tab">BOOK DETAILS</a><?php } else{echo"<a>BOOK DETAILS</a>";}?></li>
			<li><?php if($book_id!=NULL){?><a href="#storage_info" data-toggle="tab">STORAGE DETAILS</a><?php } else{echo"<a>STORAGE DETAILS</a>";}?></li>
			
		</ul>
<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
						<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_book";}else{echo "add_book";}?>" />
						<input name="book_id" id="book_id" type="hidden" value="<?=$book_id;?>">
				
		
		
<div class="tab-content clearfix">
	  
	  <div class="tab-pane active" id="general_info">
			<div class="box">
			<div class="box-body">
		  
					<!--Start Of Section1 : General Information-->
					 <div class="panel panel-default">
							<div class="panel-heading">
								<div class="w40L">
									<h4 class="panel-title">General Information</h4>
								</div>
							</div>
							<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								<div class="col-md-6">
									
									<div class="form-group col-md-12"> 
									<label class="col-md-12">Book Image</label> 
									<span class="col-md-12"><img src="<?php if($result_book_list[0]['book_thumbnail']==BOOK_THUMBNAIL_FOLDER_HTTP) echo BOOK_THUMBNAIL_FOLDER_HTTP."default.png"; else echo $result_book_list[0]['book_thumbnail']; ?>" width="200px" height="200px"/></span>
									</div>
									
									
									<div class="form-group col-md-12"> 
									<label class="col-md-12">Compiled by</label> 
									<span class="col-md-12"><?=$result_book_info[0]['compile_by']; ?></span>
									</div>  
									
									<div class="form-group col-md-12"> 
									<label class="col-md-12">Translated by</label> 
									<span class="col-md-12"><?=$result_book_info[0]['translated_by']; ?></span>
									</div>
									
								<div class="form-group col-md-12"> 
								<label class="col-md-12">Is Upcoming Book</label> 
									<span class="col-md-12">
										<?php 
											echo $result_book_info[0]['is_upcoming_book'] =='1' ? 'Yes' : 'No' ; 
										?>
									</span>
								</div> 

									
								</div><!---</.col-md-6>---------->
								
								<div class="col-md-6 ">
								
									<div class="form-group col-md-12"> 
									<label class="col-md-12">Book Name</label> 
									<span class="col-md-12"><?=$result_book_info[0]['book_title']; ?></span>
									</div>
									
									<div class="form-group col-md-12"> 
									<label class="col-md-12">Book Name(in Marathi)</label> 
									<span class="col-md-12"><?=$result_book_info[0]['book_title_mr']; ?></span>
									</div>
									
									<div class="form-group col-md-12"> 
									<label class="col-md-12">Author</label> 
									<span class="col-md-12"><?=$result_book_info[0]['author_name']; ?></span>
									</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-12">Language</label> 
								<span class="col-md-12"><?=$result_book_info[0]['book_language']; ?></span>
								</div> 
								
								<div class="form-group col-md-12"> 
								<label class="col-md-12">MRP Rs</label> 
								<span class="col-md-12"><?=$result_book_info[0]['book_mrp']; ?></span>
								</div> 
								
								<div class="form-group col-md-12"> 
								<label class="col-md-12">Edition</label> 
								<span class="col-md-12"><?=$result_book_info[0]['book_edition']; ?></span>
								</div> 
								
								
								
								</div><!---</.col-md-6>---------->
								<div class="col-md-12">
									<div class="form-group col-md-12"> 
									<label class="col-md-12">Book Description</label> 
									<span class="col-md-12" style="font-size:15px"><?= html_entity_decode($result_book_info[0]['book_description']); ?></span>
									</div> 
								</div>
								
							</div> <!---</.panel-body>---------->
							</div> <!---</.panel-collapse collapse in>---------->
							</div> <!---</.panel panel-default>---------->
							
		</div><!-- /.box-body -->
		</div><!-- /.box -->
		</div><!-- /#general_info -->
     
	

	<div class="tab-pane" id="publisher_info">
          
		<div class="box">
		<div class="box-body">
		
		 <!--Start Of Section2 : Publisher's Information-->				
			<div class="panel panel-default">
				<div class="panel-heading">
				<div class="w40L">
					<h4 class="panel-title">Publisher's Information</h4>
				</div>
				</div>
				
					<div id="collapseTwo" class="panel-collapse ">
						<div class="panel-body">
							<div class="col-md-6">
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Publisher's Name</label> 
								<span class="col-md-6"><?=$result_book_info[0]['pub_name']; ?></span>
								</div>
									
			
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Publisher's Address</label> 
								<span class="col-md-6"><?=$result_book_info[0]['pub_addr']; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Publisher's Contact No.</label> 
								<span class="col-md-6"><?=$result_book_info[0]['pub_contact']; ?></span>
								</div>
											
									</div>
									
									<div class="col-md-6 ">
									
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Publisher's Email</label> 
								<span class="col-md-6"><?=$result_book_info[0]['pub_email']; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Publisher's Website</label> 
								<span class="col-md-6"><?=$result_book_info[0]['pub_website']; ?></span>
								</div>
											
										
										
									</div>
									
									
								</div>
							
							
						  </div>
						</div>
					<!--End Of Section2-->		
		
		 
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
    </div>
	
	
    <div class="tab-pane" id="book_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section3 : Book Details-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							<h4 class="panel-title">Book Details</h4>
						</div>
						
					  </div>
					  		
								
					  <div id="collapseThree" class="panel-collapse ">
		
							<div class="panel-body">
								<div class="col-md-6">
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">ISBN No.#</label> 
								<span class="col-md-6"><?=$result_book_info[0]['isbn_no']; ?></span>
								</div>
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Book Category</label> 
								<span class="col-md-6"><?=$result_book_info[0]['category_title']; ?></span>
								</div>
								<!--	
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Dimension Width <small>(in mm)</small></label> 
								<span class="col-md-6"><?=$result_book_info[0]['dim_width']; ?></span>
								</div>
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Dimension Height<small>(in mm)</small></label> 
								<span class="col-md-6"><?=$result_book_info[0]['dim_height']; ?></span>
								</div>--->
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Dimension Depth</label> 
								<span class="col-md-6"><?=$result_book_info[0]['dim_depth']; ?></span>
								</div>
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Number of Pages</label> 
								<span class="col-md-6"><?=$result_book_info[0]['no_of_pages']; ?></span>
								</div>
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Binding Type</label> 
								<span class="col-md-6"><?=$result_book_info[0]['binding_type']; ?></span>
								</div>
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Cover Type</label> 
								<span class="col-md-6"><?=$result_book_info[0]['cover_type']; ?></span>
								</div>
										
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Weight <small>(in gm)</small></label> 
								<span class="col-md-6"><?=$result_book_info[0]['book_weight']; ?></span>
								</div>
									
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Production Cost Rs.</label> 
								<span class="col-md-6"><?=$result_book_info[0]['production_cost']; ?></span>
								</div>
				
								</div>
								
								<div class="col-md-6 ">
								
									
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Translated? &nbsp;&nbsp;</label> 
								<span class="col-md-6"><?php if($result_book_info[0]['is_translated']==1) echo "Yes"; else echo"No"; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Translated From (Book Name)</label> 
								<span class="col-md-6"><?=$result_book_info[0]['translated_from_book']; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Translated From Language(Orignal Book Language)</label> 
								<span class="col-md-6"><?php $language_list=unserialize(LANGUAGE);
								echo $language_list[$result_book_info[0]['translated_frm_lang']];?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Publication Date ( 'DD-MM-YYYY')</label> 
								<span class="col-md-6"><?=$result_book_info[0]['publication_date']; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Co-Author</label> 
								<span class="col-md-6"><?=$result_book_info[0]['co_author']; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Contributor</label> 
								<span class="col-md-6"><?=$result_book_info[0]['contributor']; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Editor</label> 
								<span class="col-md-6"><?=$result_book_info[0]['Editor']; ?></span>
								</div>
								
								<div class="form-group col-md-12"> 
								<label class="col-md-6">Copyright</label> 
								<span class="col-md-6"><?=$result_book_info[0]['copyright']; ?></span>
								</div>
								
								
									
									
								</div>
								
								<div class="clear"></div>									 
								</div>
								
								
						
					  </div>
					</div>
					<!--End Of Sectio3-->		
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
    <div class="tab-pane" id="storage_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section4 : Storage Location-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							<h4 class="panel-title">Storage Details</h4>
						</div>
						
					  </div>
					  <div id="collapseFour" class="panel-collapse">
		
						
						<div class="panel-body">
							<div class="form-group col-md-6"> 
								<label>Storage Location</label> 
							</div>
							<div class="form-group col-md-6"> 
								<label>Qauntity</label> 
							</div>
							<?php
							if(count($location_info) >0)
							{
								for($i_loc=0;$i_loc <= count($location_info)-1;$i_loc++)
								{
							?>
							
							<div class="form-group col-md-6"> 
								
								<span class="col-md-6"><?=$location_info[$i_loc]['location']; ?></span>
								</div>
								
								<div class="form-group col-md-6"> 
								
								<span class="col-md-6"><?=$location_info[$i_loc]['quantity']; ?></span>
								</div>	
						 <?php
						 	}
						  }
						  
						  ?>
						  
							
							<!-- Storage Location --->
							
						</div>	
												
						
						
					  </div>
					</div>
					<!--End Of Sectio4-->	
					
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
	
</div>
</form>  
  </div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  

  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
