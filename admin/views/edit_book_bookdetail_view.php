<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
 	<div style="float:left" >
		<h4> Edit Book >> <?php echo $result_book_info[0]['book_title']; ?></h4>
	</div>
	 <?php $book_id=$_POST['update_id'];
		//echo $pages=$_POST['pages'];?>
	<div style="float:right" >
		<small><a class="btn btn-primary pull-right" href="manage_books.php?pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>">Manage Book List</a></small>
	</div>
 
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
<div id="exTab3" class="container col-xs-12">	
<?php
	$tab_edit ="book_detail";
	include("includes/book_edit_tab.php");
?>

<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
	<input type="hidden" name="step" id="step" value="update_book_detail" />
	<input name="book_id" id="book_id" type="hidden" value="<?=$book_id;?>">
	<input name="pages" id="pages" type="hidden" value="<?php if(isset($_POST['pages'])) echo $_POST['pages']; else echo '1';?>">
				
		
		
<div class="tab-content clearfix">
	
	
    <div class="tab-pane active" id="book_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section3 : Book Details-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							<h4 class="panel-title">Book Details</h4>
						</div>
						
					  </div>
					  <div id="collapseThree" class="panel-collapse ">
		
							<div class="panel-body">
								<div class="col-md-6">
									<div class="form-group  div_isbn_no"> 
										<label for="Admin_first_name">ISBN No.#</label> 
										<input placeholder="" class="form-control" maxlength="50" name="isbn_no" id="isbn_no" type="text" value="<?=$result_book_info[0]['isbn_no']; ?>">              
										<span class="help-block error isbn_no" style="display: none">Provide ISBN Number.</span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_book_category"> 
										<label for="Admin_first_name">Book Category</label> 
		
											<select id="book_category" name="book_category" class="form-control"> 
											 <?php
											  $tbl="tbl_book_categories";
											  $val ="category_id";
											  $show="category_title";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="------------------ Select Book Category -----------------";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('category_id',$result_book_info[0]['category_id']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
											</select>          
										<span class="help-block error book_category" id="Admin_first_name_em_" style="display: none">Please Select Book Category.</span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Dimension Width</label> (in mm) 
										<input placeholder="" class="form-control" maxlength="10" name="dim_width" id="dim_width" type="text" value="<?=$result_book_info[0]['dim_width']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Dimension Height </label> (in mm) 
										<input placeholder="" class="form-control" maxlength="10" name="dim_height" id="dim_height" type="text" value="<?=$result_book_info[0]['dim_height']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
		
									<div class="form-group"> 
										<label for="Admin_first_name">Dimension Depth</label> 
										<input placeholder="" class="form-control" maxlength="10" name="dim_depth" id="dim_depth" type="text" value="<?=$result_book_info[0]['dim_depth']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_no_of_pages"> 
										<label for="Admin_first_name">Number of Pages</label> 
										<input placeholder="" class="form-control" maxlength="6" name="no_of_pages" id="no_of_pages" type="text" value="<?=$result_book_info[0]['no_of_pages']; ?>">              
										<span class="help-block error no_of_pages" id="Admin_first_name_em_" style="display: none">Provide number of pages. </span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_binding_type"> 
										<label for="Admin_first_name">Binding Type</label> 
											<select id="binding_type" name="binding_type" class="form-control"> 
											 <?php
											  $tbl="tbl_binding_type";
											  $val ="binding_id";
											  $show="title";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0' AND `type`='1'";
											  $FirstNode="------------------ Select Binding Type -----------------";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('binding_id',$result_book_info[0]['binding_id']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
											</select>          
		
										<span class="help-block error binding_type" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_cover_type"> 
										<label for="Admin_first_name">Cover Type</label> 
											<select id="cover_type" name="cover_type" class="form-control"> 
											 <?php
											  $tbl="tbl_binding_type";
											  $val ="binding_id";
											  $show="title";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0' AND `type`='2'";
											  $FirstNode="------------------ Select Cover Type -----------------";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array('binding_id',$result_book_info[0]['cover_id']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
											</select>          
										<span class="help-block error cover_type" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
									<div class="form-group"> 
										<label for="Admin_first_name">Weight </label> (in gm) 
										<input placeholder="" class="form-control" maxlength="10" name="weight" id="weight" type="text" value="<?=$result_book_info[0]['book_weight']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
											
								</div>
								
								<div class="col-md-6 ">
								
		
									<div class="form-group"> 
										<label for="Admin_first_name">Production Cost</label> 
										Rs.<input placeholder="" class="form-control" maxlength="6" name="prod_cost" id="prod_cost" type="text" value="<?=$result_book_info[0]['production_cost']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Translated? &nbsp;&nbsp;</label> <br/>
										<input  name="translated" type="radio" value="1"> &nbsp;Yes            
										<input  name="translated" type="radio" checked="checked" value="0">&nbsp;No   
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>             
									</div>
									<div class="clear"></div>
		
									<div class="form-group" id="trans_frm_book"> 
										<label for="Admin_first_name">Translated From (Book Name)</label> 
										<input placeholder="" class="form-control" maxlength="150" name="trans_frm_book" id="trans_frm_book" type="text" value="<?=$result_book_info[0]['translated_from_book']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group" id="trans_frm_lng"> 
										<label for="Admin_first_name">Translated From Language(Orignal Book Language)</label> 
										<select name="transl_frm_lng" id="transl_frm_lng" class="form-control">
										 <option value="">--- Select Language ---</option>
										 <?php
										  $language_list=unserialize(LANGUAGE);
										  for($i_ln=1;$i_ln<=count($language_list);$i_ln++)
										  {
										   ?>
											<option value="<?=$i_ln; ?>" <?php if($result_book_info[0]['translated_frm_lang']==$i_ln){ echo "selected";} ?>><?=$language_list[$i_ln];?></option>
										   <?php 
										  }
										 ?>
										</select>
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Publication Date ( 'DD-MM-YYYY')</label> 
										<input placeholder="" class="form-control" maxlength="50" name="pub_date" id="pub_date" type="text" value="<?=$result_book_info[0]['publication_date']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Co-Author</label> 
										<input placeholder="" class="form-control" maxlength="150" name="co_author" id="co_author" type="text" value="<?=$result_book_info[0]['co_author']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Contributor</label> 
										<input placeholder="" class="form-control" maxlength="200" name="contributor" id="contributor" type="text" value="<?=$result_book_info[0]['contributor']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
									
									<div class="form-group"> 
										<label for="Admin_first_name">Editor</label> 
										<input placeholder="" class="form-control" maxlength="150" name="editor" id="editor" type="text" value="<?=$result_book_info[0]['editor']; ?>">              
										<span class="help-block error" id="Admin_first_name_em_" style="display: none"></span>  
									</div>
									<div class="clear"></div>
		
									<div class="form-group div_copyright"> 
										<label for="Admin_first_name">Copyright</label> 
										<input placeholder="" class="form-control" maxlength="150" name="copyright" id="copyright" type="text" value="<?=$result_book_info[0]['copyright']; ?>">              
										<span class="help-block error copyright" id="Admin_first_name_em_" style="display: none">Provide Copyright name.</span>  
									</div>
									<div class="clear"></div>
									
		
								</div>
								
								<div class="clear"></div>									 
								</div>
								
								<div class="clear"></div>
						<div class="col-md-12 text-right" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>    
								<a href="manage_books.php" class="btn btn-primary">Cancel</a>								
							</div>
						</div>
						
					  </div>
					</div>
					<!--End Of Sectio3-->		
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
 	
	
	
</div>
</form>  
  </div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  

  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
