<div class="container-fluid">
  <div class="row"><div class="col-md-16"> <!--dashboard container-->

<div class="dashboard">
<h5 class="text-left">Add New Party</h5>
<div class="clear"></div>

	<div class="tenant-dashboard clearfix">
		 <div class="clear"></div>
		  <a class="btn btn-default pull-right" href="manage_partys.php"><i class="glyphicon glyphicon-chevron-left"></i></a>    
		 <div class="clear"></div>
		 <div class="my-profile form-top clearfix">
	 

			<form class="form-vertical" id="frm_new_party" action="add_new_party.php" method="post"> 
			<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_party";}else{echo "add_party";}?>" />
			<input name="party_id" id="pub_auth_id" type="hidden" value="<?=$party_id;?>">

			<!--Start Of Section4 : Storage Location-->				
			<div class="panel panel-default">
			  
			  <div id="collapseFour" class="panel-collapse collapse in ">

					<div class="panel-body">
						<div class="col-md-6 col-md-offset-1">
							<div class="form-group"> 
								<label for="Admin_first_name">Party Name</label> 
								<input class="form-control" maxlength="250" name="party_name" id="party_name" type="text" value="<?=$party_list->name; ?>">              
								
							</div>
							<div class="clear"></div>

							<div class="form-group"> 
								<label for="Admin_first_name">Address</label> 
								<input type="text" size="50" name="address" id="address"  class="txt_field_font form-control" value="<?=$party_list->address; ?>"/>  
							</div>
							<div class="clear"></div>
							
							
									
						</div>
						
						<div class="col-md-6 col-md-offset-1">
							<div class="form-group"> 
								<label for="Admin_first_name">Contact No.</label> 
								<input required class="form-control" maxlength="11" name="contact_no" id="contact_no" type="text" value="<?=$party_list->contact_no; ?>">              
								
							</div>
							<div class="clear"></div>
						

							<div class="form-group"> 
								<label for="Admin_first_name">Email ID</label> 
								
								<input type="text" size="100" name="party_email_id" id="party_email_id"  class="txt_field_font form-control" value="<?=$party_list->email_id; ?>"/>              
								
							</div>
							<div class="clear"></div>

							

						</div>
						
						<div class="clear"></div>
						<div class="col-md-6 col-md-offset-6" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>            
							</div>
						</div>
		
					</div>
				
			  </div>

			  
			</div>
			<!--End Of Sectio4-->		
				
			</form>  
		</div>
	</div>
</div>
<!--end dashboard container-->  </div>
</div>
</div>