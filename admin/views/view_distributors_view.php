<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Distributor Detail</h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Distributor</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		<div class="box-body">
		  <div class="col-md-12 pull-right text-right" style="margin-bottom:10px;">
			<a href="manage_distributors.php" class="btn btn-primary"><< Back</a>
		  </div> 
		
		  <table id="example2" class="table table-bordered table-striped table-hover">
			<thead>
				  <?php if(count($distributors_view)>0){?>                       
							<thead>
							<tr>
								<th>Distributor Name</th>
								<td><?=$distributors_view[0]['distributor_name']?></td>
							</tr>
							<tr>
								<th>Contact Number</th>
								<td><?=$distributors_view[0]['distributor_contact_no']?></td>
							</tr>
							<tr>
								<th>Email ID</th>
								<td><?=$distributors_view[0]['distributor_emailid']?></td>
							</tr>
							<tr>
								<th>Website</th>
								<td><?=$distributors_view[0]['distributor_website']?></td>
							</tr>
							<tr>
								<th>Address</th>
								<td><?=$distributors_view[0]['distributor_address']?></td>
							</tr>
							<tr>
								<th>City </th>
								<td><?=$distributors_view[0]['distributor_city_id']?></td>
							</tr>
							<tr>
								<th>State</th>
								<td><?=$distributors_view[0]['distributor_state_id']?></td>
							</tr>
							<tr>
								<th>Zip Code</th>
								<td><?=$distributors_view[0]['zip_code']?></td>
							</tr>
							</thead>
						<?php }?>	
							
		  </table>
		  <div class="col-md-12 pull-right text-right"><br/>
			<a href="manage_distributors.php" class="btn btn-primary"><< Back</a>
		  </div> 
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
