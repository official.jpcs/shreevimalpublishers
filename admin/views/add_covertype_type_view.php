<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($step=="edit_info") {echo "Update Cover Type";}else{echo "Add New Cover Type";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Cover Type</a></li>
  </ol>
</section>




<!-- Main content -->
	<section class="content">
	
			<form class="form-vertical" id="frm_new_category" action="add_edit_cover_type.php" method="post">
						<input type="hidden" name="step" id="step" value="<?php if($step=="edit_info") {echo "update_binding_type";}else{echo "add_binding_type";}?>" />
						<input   name="binding_id" id="binding_id" type="hidden" value="<?=$binding_id;?>">
					
						<!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  
						
					<div class="col-md-12">
						<div class="form-group"> 
							<label for="Admin_name">Cover Type</label>
							<input class="form-control" maxlength="50" name="binding_type" id="binding_type" type="text" required value="<?=$binding_type->title; ?>"> 
						</div>
					</div>
						
						
						  </div><!-- /.row -->
				</div><!-- /.box-body -->
				<div class="box-footer text-right">
					<button class="btn btn-primary" type="submit" name="yt0">Submit</button>        
					<button class="btn" type="reset" name="yt1">Reset</button>
					<a href="manage_coverage_types.php" class="btn btn-primary">Cancel</a>
				</div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
						          
	</section>
</div>
<!-- /.content-wrapper -->





