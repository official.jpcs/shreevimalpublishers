<!-- Content Wrapper. Contains page content -->
<style>
	.dropdown-toggle{
		height: 45px !important;
	}
</style>
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
 
	<div style="float:left" >
		<h4> Edit Book >> <?php echo $result_book_info[0]['book_title']; ?></h4>
	</div>
	 <?php $book_id=$_POST['update_id'];
		//echo $pages=$_POST['pages'];?>
	<div style="float:right" >
		<small><a class="btn btn-primary pull-right" href="manage_books.php?pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>">Manage Book List</a></small>
	</div>
  
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
<div id="exTab3" class="container col-xs-12">	
<?php
	$tab_edit ="related_books";
	include("includes/book_edit_tab.php");
?>

<form class="form-vertical" id="frm_new_book" name="frm_new_book" method="post" enctype="multipart/form-data"> 
	<input type="hidden" name="step" id="step" value="update_related_books" />
	<input name="book_id" id="book_id" type="hidden" value="<?=$book_id;?>">
	<input name="pages" id="pages" type="hidden" value="<?php if(isset($_POST['pages'])) echo $_POST['pages']; else echo '1';?>">
				
		
		
<div class="tab-content clearfix">
	
	
    <div class="tab-pane active" id="storage_info">
         
		  <div class="box">
		 <div class="box-body">
		  
		  <!--Start Of Section4 : Related Books-->				
					 <div class="panel panel-default">
					  <div class="panel-heading">
						<div class="w40L">
							<h4 class="panel-title">Related Books</h4>
						</div>
						
					  </div>
					  <?php 
						$related_book = explode(",",$result_book_info[0]["related_books"]);

						// var_dump($related_book);
						// echo "A:". $book_id;
						// echo "B:".in_array($book_id, $related_book);

						// die;
					  ?>
					  <div id="collapseFour" class="panel-collapse">
		
						<div class="panel-body">

							<div class="col-md-12" style="margin-top: 15px;margin-bottom: 15px;">
								<select name="related_books[]" style="height:25px !important" class="selectpicker col-md-12" multiple data-live-search="true">
									<?php    
									$sr=1;
									for($i_book=0;$i_book <=count($result_book_list)-1;$i_book++)
									{
										
											$book_title = $result_book_list[$i_book]['book_title'];
											$book_id = $result_book_list[$i_book]['book_id'];

											if(in_array($book_id, $related_book))
											{
												$sel ="selected";
											}
											else{
												$sel ="";
											}

										 	echo "<option ".$sel." value='".$book_id."'>".$book_title."</option>";

										
									}

									?>
								</select>
							</div>
							
						</div>	
												
						<div class="clear"></div>
						<div class="col-md-12 text-right" style="margin-top:10px;">
							<div class="btn-block">
								<button class="btn btn-primary" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button>     
								<a href="manage_books.php" class="btn btn-primary">Cancel</a>	
							</div>
						</div>
						
					  </div>
					</div>
					<!--End Of Sectio4-->	
					
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div>
	
	
	
</div>
</form>  
  </div>

		
		 
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
  
  
  

  
  
  </section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
