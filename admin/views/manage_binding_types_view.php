<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Manage Binding Types
	<small><a class="btn btn-primary" href="add_edit_binding_type.php">Manage Binding Types</a></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Binding Types</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>






<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-striped table-hover">
		
					<?php 
						 if($total_record >0)
						 {
						?>
						  <thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</a></th>
								<th id="sub_admin_grid_c1">Binding Type </a></th>
								
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
								<?php 
								$sr=$sr+1;
								for($i_binding=0;$i_binding <=$total_record-1;$i_binding++)
								{
								
								
								 if($_REQUEST['pages']!="" &&$_REQUEST['pages']!=1) 
                            {
                                    $sr=($per_page * ($_REQUEST['pages']-1))+($i_binding+1);
                            }
								?>
									<tr class="odd">
										<td style="width:10%"><?=$sr; ?></td>
										<td style="width:50%"><?=$result_binding_list[$i_binding]->title; ?></td>
	
									   <td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_binding_list[$i_binding]->binding_id; ?>" rel="add_edit_binding_type.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_binding_list[$i_binding]->binding_id; ?>" rel="manage_binding_types.php">Delete</a>													                                       </td>
									</tr>
								 <?php 
								 $sr=$sr+1;
								 }
								 ?>	
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Binding Type list not available to display. </td></tr>
							<?php 
							}
							?>
							
							
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  
		 <!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 
					 $pages = ceil(($total_record/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['pages'])) 
                         {

                             if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_binding_types.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_binding_types.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_binding_types.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_binding_types.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->




<?php /*

<!--main container-->
<div class="container-fluid">
  <div class="row"><div class="col-md-16"> <!--dashboard container-->
<div class="dashboard">
 <h5 class="text-left">Manage Binding Types</h5>
 <div class="clear"></div>
    <div class="tenant-dashboard contractor-issues clearfix"> 
           <!--flash messages section-->
             
        <a href="add_edit_binding_type.php" class="btn btn-default pull-right">
           Add Binding Type
        </a>
        <div class="clear"></div>
        <div class="panel-group form-top" id="accordion">
          <div class="panel panel-default">
            <div id="collapseOne" class="panel-collapse collapse">
              <div class="panel-body">
                <div class="form-wrap clearfix"> 
                <!--------Search--view---------->
					<form class="form-vertical" id="search_subadmin" action="#" method="get">
					<div class="col-md-4">
						<div class="form-group"> 
							<label for="Admin_name">Binding Type Title</label><input placeholder="" class="form-control" maxlength="50" name="Admin[name]" id="Admin_name" type="text"> 
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group"> 
							<label for="text">&nbsp;</label>        
							<div class="clear"></div>
							<button class="btn" type="submit" name="yt0">Submit</button>                    
							<button class="reset-form btn" type="reset" name="yt1">Reset</button>    
						</div>
					</div>
					</form>           
            
                 <!--------Search--view---------->
               </div>
              </div>
            </div>
          </div>
        </div>

        <div class="clear"></div>

       <!--------list--view---------->
			<div class="list-view">
				
				<div class="clear"></div>
				<div class="grid-view clearfix" id="sub_admin_grid">
					
						<table class="items table table-striped table-bordered">
						<?php 
						 if($total_record >0)
						 {
						?>
						  <thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</a></th>
								<th id="sub_admin_grid_c1">Binding Type </a></th>
								
								<th class="button-column" id="sub_admin_grid_c4">Action</th>
							</tr>
							</thead>
							<tbody>
								<?php 
								for($i_binding=0;$i_binding <=$total_record-1;$i_binding++)
								{
								 $sr=$i_binding+1;
								?>
									<tr class="odd">
										<td style="width:10%"><?=$sr; ?></td>
										<td style="width:50%"><?=$result_binding_list[$i_binding]->title; ?></td>
	
									   <td style="width:10%">
										<a title="Edit" class="edit_info" id="<?=$result_binding_list[$i_binding]->binding_id; ?>" rel="add_edit_binding_type.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_binding_list[$i_binding]->binding_id; ?>" rel="manage_binding_types.php">Delete</a>													                                       </td>
									</tr>
								 <?php 
								 }
								 ?>	
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Binding Type list not available to display. </td></tr>
							<?php 
							}
							?>
							
						</table>
					<div class="keys" style="display:none" title="#"><span>6</span><span>5</span><span>4</span><span>3</span><span>1</span></div>
				</div>        
			</div>
       <!--------list--view----------> 

     </div>
</div>

<!--end dashboard container--> 
 </div>
</div>
</div>
<!--end main container--> 

*/?>