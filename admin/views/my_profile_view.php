<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	 My Profile
	<!--<small><a class="btn btn-primary" href="new_delivery_entry.php">Add New  My Shop</a></small>-->
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">My Profiles</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>




<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	
		
				<div class="box">
				<div class="box-body">
					 <div class="clear"></div>
					  
					 <div class="my-profile form-top clearfix">
						<form class="form-vertical" id="frm_add_new_user" action="#" method="post"> 
							<input type="hidden" name="step" id="step" value="my_profile" />
							<input   name="user_id" id="user_id" type="hidden" value="<?=$user_id;?>">
							
								<!-- SELECT2 EXAMPLE -->
			  
				  <div class="row">
				  
									<div class="col-md-6 ">
										<div class="form-group"> 
											<label for="Admin_first_name">Full Name</label>
											<input placeholder="Enter Full Name " class="form-control" maxlength="50" name="name" id="name" type="text" value="<?=$name;?>">
											<span class="help-block error" id="name_msg" style="display: none">Field cannot be blank.</span>
										</div>
										<div class="clear"></div>
			
										<div class="form-group">
											<label for="Admin_first_name">Address</label>
											<input placeholder="Enter the Address" class="form-control" maxlength="50" name="address" id="address" type="text" value="<?=$address;?>">
											<span class="help-block error" id="address_msg" style="display: none">Field cannot be blank.</span>
										</div>
										<div class="clear"></div>
										
										<div class="form-group">
											<label for="Admin_first_name">Contact No.</label> 
											<input placeholder="Enter the Contact No." class="form-control" maxlength="15" name="contact_no" id="contact_no" type="text" value="<?=$contact_no;?>" >
											<span class="help-block error" id="contact_no_msg" style="display: none">Field cannot be blank.</span>
										</div>
										<div class="clear"></div>
										
										<div class="form-group">
											<label for="Admin_first_name">Email ID</label> 
											<input placeholder="Enter the Email Id" class="form-control" maxlength="50" name="email_id" id="email_id" type="text" value="<?=$email;;?>">
											<span class="help-block error" id="email_id_msg" style="display: none">Field cannot be blank.</span>
										</div>
										<div class="clear"></div>
												
									</div>
									
									<div class="col-md-6 ">
									

										<div class="form-group">
											<label for="Admin_first_name">Username</label> 
											<input placeholder="Username" class="form-control" maxlength="15" name="user_name" id="user_name" type="text" value="<?=$username;?>">
											<span class="help-block error" id="user_name_msg" style="display: none">Field cannot be blank.</span>
										</div>
										<div class="clear"></div>
										
										<div class="form-group" >
											<label for="Admin_password_name">Password</label>
											<input placeholder="Enter the Password" class="form-control" maxlength="10" name="password" id="password" type="password">
											<span class="help-block error" id="password_msg" style="display: none">Field cannot be blank.</span>
										</div>
										
										<div class="form-group" >
											<label for="Admin_password_name">User Type</label>
											
											<select name="user_type" id="user_type" class="form-control">
												<option value="">-- Select User Type --</option>
												<?php 
												 for($i_usrtp=1;$i_usrtp<=count($user_type_list);$i_usrtp++)
												 {
												?>
												<option value="<?=$i_usrtp; ?>" <?php if($user_type==$i_usrtp) { echo "selected"; } ?>>
													<?=$user_type_list[$i_usrtp]; ?>
												</option>
												<?php
												}
												?>
											</select>
											<span class="help-block error" id="password_msg" style="display: none">Field cannot be blank.</span>
										</div>
										
                                                                                <div class="clear"></div>

										<div class="form-group" >
											<label for="Admin_password_name">Alloted Shop</label>
											
                                                                                      
                                                                                    <select name="alloted_shop" id="alloted_shop" class="form-control">

                                                                                        <?php
                                                                                         $tbl="tbl_storage_locations";
                                                                                         $val ="location_id";
                                                                                         $show="location_addr";
                                                                                         $status="is_deleted";
                                                                                         $clause =" `is_deleted` ='0'";
                                                                                         $FirstNode="---- Select Storage Location ----";
                                                                                         echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($location_id), $print = 0, $status, $FirstNode,$prefix='');
                                                                                        ?>

                                                                                    </select>	 
											<span class="help-block error" id="password_msg" style="display: none">Field cannot be blank.</span>
										</div>
                                                                                
									</div>
									
									</div><!-- /.row -->
				
				<div class="box-footer">
					<button class="btn" type="submit" name="yt0">Submit</button>              
								<button class="btn" type="reset" name="yt1">Reset</button></div>
			  </div><!-- /.box -->
			  <!-- /.row -->
		</form>
		
		
							

</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->