
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Contact Us Massage </h1>
  <ol class="breadcrumb">
	<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="manage_contact.php">Contact Us Massage</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-hover">
			
							<?php 
							
							 for($i_contact=0;$i_contact <=count($contact)-1;$i_contact++)
									{
							?>
							   <thead>
								<tr>
									<th id="sub_admin_grid_c0">Contact Name</a></th>
									<td><?=$contact->contact_name; ?></td>
								</tr>	
								<tr>	
									<th id="sub_admin_grid_c0">Contact No</a></th>
									<td><?=$contact->contact_no; ?></td>
								</tr>	
								<tr>
									<th id="sub_admin_grid_c0">Contact Email</a></th>
									<td><?=$contact->contact_email; ?></td>
								</tr>	
								<tr>
									<th id="sub_admin_grid_c0">Contact Address</a></th>
									<td><?=$contact->contact_address; ?></td>
								</tr>	
								<tr>
									<th class="button-column" id="sub_admin_grid_c4">Message</th>
									<td><?=$contact->contact_message; ?></td>
								</tr>
								<tr>
									<th id="sub_admin_grid_c0">Created Date</a></th>
									<td><?=$contact->created_date; ?></td>
								</tr>	
								
								</thead>
								
								<?php 
							}
							?>
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
