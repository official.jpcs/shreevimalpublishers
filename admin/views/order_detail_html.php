
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	<?php echo $title;?>
		<small><a class="btn btn-primary" href="manage_orders.php">Orders List</a></small>

  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="manage_orderss.php"><?php echo $title;?></a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>




<!-- Modal confirm -->
	<div class="modal" id="confirmModal" style="display: none; z-index: 1050;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body" id="confirmMessage">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="confirmOk">Ok</button>
		        	<button type="button" class="btn btn-default" id="confirmCancel">Cancel</button>
		        </div>
			</div>
		</div>
	</div>

	
	
<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-sm-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
		<!--- Invoice Div --->
		<div class="box-body " style="display: none">
		
		  <table id="example2" class=" table-responsive table table-bordered table-hover">
				<?php 
				//print_r($order_detail);
			
			?>
			<tr>
				<th colspan="2" >
					<span style="width: 50%;text-align: right;float:left">Tax Invoice</span>
					<span style="width: 45%;text-align: right;float:left">Invoice No : <?php echo $order_detail[0]['invoice_no'];?></span>
				</th>
			</tr>
			
				<tr>
				 
				   <td style="width: 50%">
				   	 <?php
				   	 
				   	  echo "<b>".$order_detail[0]['customer_name']."</b><br/>".$order_detail[0]['address_line1']."<br/>".$order_detail[0]['address_line2']."<br/>".$order_detail[0]['address_line3']."<br/>".$order_detail[0]['landmark']."<br/>".$order_detail[0]['town']."-".$order_detail[0]['pin_code']."<br/>".$order_detail[0]['taluka'].", ".$order_detail[0]['district']."<br/>".$order_detail[0]['mobile_no']."<br/>".$order_detail[0]['email_address'];
				   	  ?>	
				   </td>	
				
					<td>
						<b>SHREE VIMAL PUBLISHERS PRIVATE LIMITED </b><br/>
						Opposite to Poornawad Bhavan, <br/>
						Parner-414302, Dist.- Ahmednagar, Maharashtra<br/>
						sales@shreevimalpublishers.com<br/>
						www.shreevimalpublishers.com<br/>Contact No. 9226885522<br/>GSTIN: 27AARCS6147G2ZU<br/>
						PAN No. AARCS6147G <br/>CIN: U22219PN2012PTC143528


					</td>
				</tr>


			
				
			
			

		  </table>
		  			<hr>

		   <table id="example2" class=" table-responsive table table-bordered table-hover">
				
			<thead>
			  <tr>
				<th  colspan="9" style="background-color: rgba(47, 79, 79, 0.21);">Order Detail</th>
			  </tr>
			<tr>
				<th width="10%">Sr.No</th>
				<th width="10%">Product Image</th>
				<th width="10%">Product Title</th>
				
				<th width="20%">Quantity Order</th>
				<th width="20%">Product Rate</th>
				<th width="20%">Order Cost</th>
				
				
					</tr>
                        </thead>              
			<?php 
			
            for($i=0;$i<count($order_product_detail);$i++)
            {
            ?>
			<tr>
				<td width="10%"><?=$i+1?></td>
				<td width="10%">

					<img style="height: 51px; width: 50px;" class="img-responsive" src="<?php if($order_product_detail[$i]['product_image']==BOOK_THUMBNAIL_FOLDER_HTTP) echo BOOK_THUMBNAIL_FOLDER_HTTP."default.png"; else echo BOOK_THUMBNAIL_FOLDER_HTTP.$order_product_detail[$i]['product_image']; ?>" title="<?php echo $order_product_detail[$i]['book_title']; ?>" alt="<?php echo $order_product_detail[$i]['book_title']; ?>"  />
				    
				</td>
				<td width="10%"><?=$order_product_detail[$i]['product_title']?></td>
				
				<td width="10%"><?=$order_product_detail[$i]['product_quantity']?></td>
				<td width="10%"><?=sprintf("%01.2f", $order_product_detail[$i]['product_rate'])?></td>
				<td width="10%">
				<b style="color:#FF0000">	Rs. <?=sprintf("%01.2f", $order_product_detail[$i]['product_quantity'] * $order_product_detail[$i]['product_rate'] + $order_product_detail[$list_i]['delivery_charges']);?> /- </b>
				</td>
			</tr>
						
			<?php 
            }
        	?>    
                        
		  </table>
                <table style="width: 100%">
                    <tr>
                        <td colspan="3" style="text-align: left;">Order Date : <b><?= date("d-m-Y", strtotime($order_detail[0]['order_date'])); ?></b></td>
                        <td colspan="4"  style="text-align: center;">
                            <?php 
                             if($order_detail[0]['payment_status']==1)
                             {
                             ?>
                            <span class="text-green text-bold">Your Payment in Completed </span>
                             <?php 
                             }
                             else
                             {
                             ?>
                               <span class="text-red text-bold">Your Payment in Pending </span> 
                             <?php 
                             }
                            ?>
                            
                        </td>
                        <td colspan="3"  style="text-align: right;font-weight: bold;"><br>Sub Total 	:  Rs. <?= sprintf("%01.2f", $order_detail[0]['order_total_amount'])." /-"; ?></td>

                    </tr>


                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    	<td colspan="3">&nbsp;</td>      
						<td colspan="4" style="text-align: right;color:red"><br>Discount Amount 	: - Rs. <?= sprintf("%01.2f", $order_detail[0]['discount_amount'])." /-"; ?></td>
                    </tr>

                    
                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    	<td colspan="3">&nbsp;</td>      
						<td colspan="4" style="text-align: right;"><br>Postal Charges 	:  Rs. <?= sprintf("%01.2f", $order_detail[0]['postage_charges'])." /-"; ?></td>
                    </tr>

                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    	<td colspan="3">&nbsp;</td>      
						<td colspan="4" style="text-align: right;color: blue;font-size: 16px;"><br><b>Final Amount Paid 	:  Rs. <?= sprintf("%01.2f", $order_detail[0]['final_amount_paid'])." /-"; ?></b></td>

                    </tr>

                </table>

                                        
		  <br>
			<?php
			if($order_detail[0]['order_status']==0)
			{
			    ?>

			<a href="#" id="<?php echo $order_detail[0]['order_id']; ?>" class="btn btn-primary update_dispach_modal">
			Update Dispatch Detail
			</a>
			<?php
			} 
			else
			{
			?>
			 <div class="text-green text-bold"  style="text-align: center;">Order Delivered Successfully !</div>      
			<?php 
			}
			?>
		</div><!-- /.box-body -->

		<!------- Invoice DIV -->

<?php

$invoicepdf= "";
$message ="";

$total_qty =0;
$total_rate=0;
$total_discount = 0; 
$total_taxable_amount = 0; 
$hsn_code ="49011010";




$to_customer=$order_detail[0]['email_address'];  
$customer_name = $order_detail[0]['customer_name'];

$discount_amount= $order_detail[0]['discount_amount'];

?>

<div style="width:100%;"> 

<!-- Main content -->
<section Style="min-height: 250px; padding: 5px;padding-left: 5px;padding-right: 5px;">

<!-- row -->
<div class="margin-right: 5px;margin-left: 5px;width:100%">

<!--col-sm-12 -->
<div style="width: 100%;">

<div style="width: 100%;text-align:center">
	<span style="color:blue;font-size:25px;text-align:center">
		Tax Invoice
	</span>
</div>

<!--box -->
<div style="position: relative;border-radius: 3px;background: #ffffff;margin-bottom: 5px;width: 100%;">

<!--box-body -->
<div style="border-top-left-radius: 0;border-top-right-radius: 0; border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;padding: 15px;">


<!-- row -->
<div class="margin-right: -15px;margin-left: -15px;">

<!--col-sm-12 -->
<div style="width: 100%;">
<!--col-sm-9 -->
<div style="position:absolute">
	<img src="http://shreevimalpublishers.com/admin/images/logo.png" width="120px" style="text-align:left">
</div>
<div style="width: 100%;float: right;text-align:center;line-height: 1.5;">

<b>SHREE VIMAL PUBLISHERS PRIVATE LIMITED </b><br/>
 Opposite to Poornawad Bhavan, 
Parner-414302, Dist.- Ahmednagar, Maharashtra<br/>
Mob No.: 9226885522 , sales@shreevimalpublishers.com , www.shreevimalpublishers.com<br/>
GSTIN: 27AARCS6147G2ZU , 
PAN No. AARCS6147G , CIN: U22219PN2012PTC143528
<br>

</div>

<div style="float:none;clear:both;margin-top:10px;"><br/>

<hr style="margin-top: 5px;margin-bottom: 5px;border: 0; border-top: 1px solid gray;" />

<table id="example2" style=" width: 100%;">



</tr>


<tr>
	<td style="line-height: 1.5;"><b>Bill To,</b> <br/><b><?php echo $order_detail[0]['customer_name']; ?></b><br/><?php echo $order_detail[0]['address_line1']. ",".$order_detail[0]['address_line2']; ?><br/><?php echo $order_detail[0]['town'].",".$order_detail[0]['taluka']."-".$order_detail[0]['pin_code'].", ".$order_detail[0]['district']; ?><br/><?php echo $order_detail[0]['mobile_no']; ?></td>
	<td style="line-height: 1.5;text-align:right">Place of supply: 27-Maharashtra <br/>INVOICE NO.: <?php echo $order_detail[0]['invoice_no']; ?><br/>DATE : <b><?php echo date("d-m-Y", strtotime($order_detail[0]['order_date'])); ?></b><br/></td>
</tr>

</table>

<br>
<div style="width: 100%; display: inline-block;float:left;"></div>

</div>


</div><!-- /.row -->


<div style="float:none;clear:both;">


<!-------------------- Row 2 start --------------->
<hr style="margin-top: 10px;margin-bottom: 10px;border: 0; border-top: 1px solid gray">

<!-- row -->
<div class="margin-right: -15px;margin-left: -15px;">

<!--col-sm-12 -->
<div style="width: 100%;">



<table id="example2"  class=" table-responsive table table-bordered table-hover" border="1" style="border:1px solid;"  >
				
			<thead>
			  <tr>
				<th colspan="12" style="background-color: rgba(47, 79, 79, 0.21);text-align:left;padding:5px;">Order Detail</th>
			  </tr>
			<tr>
				<th style="text-align:left;width:20px;">#</th>
				<th style="text-align:left;width:220px;">Item Name</th>
				<th style="text-align:left;width:80px;" >HSN/SAC</th>				
				<th style="text-align:center;width:50px;">Quantity</th>
				<th style="text-align:center;width:100px;">Price/ Unit</th>
				<th style="text-align:center;width:100px;">Discount</th>
				<th style="text-align:center;width:100px;">Taxable Amount</th>
				<th style="text-align:center;width:100px;">CGST</th>
				<th style="text-align:center;width:100px;">SGST</th>
				<th style="text-align:center;width:100px;">Amount</th>
				
				
			</tr>
             </thead>              
			 <tbody>

			<?php 
            for($i=0;$i<count($order_product_detail);$i++)
            {
            	$sr =$i+1;


            	if($discount_amount==null || discount_amount=="")
            	{
	            	$disc_per = 0;  //0% Discount
            	}
            	else
            	{
	            	$disc_per = 15;  //15% Discount
            	}

            	
            	$book_quantity = $order_product_detail[$i]['product_quantity'];
            	$book_rate = $order_product_detail[$i]['product_rate'];

            	$book_n_qty_amount =$book_rate*$book_quantity;

            	$book_discount = ($book_n_qty_amount * $disc_per)/100;

            	$book_taxable_amt = $book_n_qty_amount - $book_discount;

            	
            	## Total

            	$total_qty = $total_qty+$book_quantity; 

            	$total_discount = $total_discount+$book_discount; 
            	$total_taxable_amount = $total_taxable_amount+$book_taxable_amt; 

            	
            ?>	

            	
			<tr>
				<td><?php echo $sr; ?></td>

				<td ><?php echo $order_product_detail[$i]['product_title']; ?></td>
				<td><?php echo $hsn_code; ?></td>
				
				<td style="text-align:center"><?php echo $book_quantity; ?></td>
				<td style="text-align:center"> &#8377; <?php echo sprintf("%01.2f",$book_rate); ?></td>
				<td style="text-align:center"> &#8377; <?php echo sprintf("%01.2f",$book_discount); ?> (<?php echo $disc_per; ?>%)</td>
				<td style="text-align:center"> &#8377; <?php echo sprintf("%01.2f",$book_taxable_amt); ?></td>
				<td style="text-align:center"> &#8377; 0.00 (0%)</td>
				<td style="text-align:center"> &#8377; 0.00 (0%)</td>
				<td style="text-align:center"> &#8377; <?php echo sprintf("%01.2f",$book_taxable_amt); ?></td>
			</tr>
			<?php
			
            }
            ?>

			<tr>
				<th style="text-align:center;width:20px;font-weight:bold">#</th>
				<th style="text-align:center;width:220px;font-weight:bold">Total</th>
				<th style="text-align:center;width:80px;font-weight:bold" >&nbsp;</th>				
				<th style="text-align:center;width:100px;font-weight:bold"><?php echo $total_qty; ?></th>
				<th style="text-align:center;width:100px;font-weight:bold">&nbsp;</th>
				<th style="text-align:center;width:100px;font-weight:bold">  &#8377; <?php echo sprintf("%01.2f",$total_discount); ?></th>
				<th style="text-align:center;width:100px;font-weight:bold"> &#8377; <?php echo sprintf("%01.2f",$total_taxable_amount); ?></th>
				<th style="text-align:center;width:100px;font-weight:bold"> &#8377; 0.00</th>
				<th style="text-align:center;width:100px;font-weight:bold"> &#8377; 0.00</th>
				<th style="text-align:center;width:100px;font-weight:bold">  &#8377; <?php echo sprintf("%01.2f",$total_taxable_amount); ?></th>
				
			</tr>
 						
			
		 	</tbody>
		  </table>

			<table style="width: 100%">
                    <tr>
                        
                        <td colspan="4"  style="text-align: left;">
                            <?php
                             if($order_detail[0]['payment_status']==1)
                             {
                             ?>
                             <span style="color:green;font-weight:bold">Your Payment in Completed </span>
                             <?php
                             }
                             else
                             {
                             ?>
                            <span class="text-red text-bold">Your Payment in Pending </span>
                            <?php
                             }
                           ?>
                        </td>
                        <td colspan="3"  style="text-align: right;"><br>Sub Total 	: &#8377;  <?php echo  sprintf("%01.2f",$total_taxable_amount); ?> /- </td>

                    </tr>
                  
                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    	<td colspan="3">&nbsp;</td>      
						<td colspan="4" style="text-align: right;"><br>Postal Charges 	:  &#8377; <?php echo sprintf("%01.2f", $order_detail[0]['postage_charges']); ?> /- </td>

                    </tr>

                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    	<td colspan="3">&nbsp;</td>      
						<td colspan="4" style="text-align: right;color:blue;font-size:16px;"><br><b>Final Amount Paid 	:  &#8377;  <?php echo sprintf("%01.2f", $order_detail[0]['final_amount_paid']); ?> /-</b></td>

                    </tr>

                </table><hr/>	  

				<table id="example2" border="1" class=" table-responsive table table-bordered table-hover" width="100%" style="width:100%;text-align: center;" >
				<tr>
					<td>
						
						<?php
						if($order_detail[0]['order_status']==0)
						{
						    ?>

						<a href="#" id="<?php echo $order_detail[0]['order_id']; ?>" class="btn btn-primary update_dispach_modal">
						Update Dispatch Detail
						</a>
						<?php
						} 
						else
						{
						?>
						 <div class="text-green text-bold"  style="text-align: center;">Order Delivered Successfully !</div>      
						<?php 
						}
						?>

					</td>
				</tr>	
				
			  </table>


			</div><!-- /.row -->
			</section><!-- /.Main content -->
			</div><!-- /.content-wrapper -->




	  </div><!-- /.box -->

	  
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div class="modal fade" id="updateDispatch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h5 class="modal-title " id="exampleModalLabel">
                	<b>Dispatch Detail</b>
                
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </h5>
            </div>
            <div class="modal-body">
           			 <input type="hidden" id="order_id_data" name="order_id_data">
	                 <div class="row">
	                 	<div class="col-md-12">Courier Company<span style="color:red">*</span></div>
	                 	<div class="col-md-12">
	                 		<input id="courier_company" type="text" name="courier_company" class="form-control">
	                 	</div>	
	                 </div>
                	  <div class="clear">&nbsp;</div>
	                 <div class="row">
	                 	<div class="col-md-12">Dispatch Date<span style="color:red">*</span></div>
	                 	<div class="col-md-12">
	                 		<input type="text" name="dispatch_date" id="dispatch_date" class="form-control">
	                 	</div>	
	                 </div>
	                 <div class="clear">&nbsp;</div>
	                 <div class="row">
	                 	<div class="col-md-12">Tracking Id <span style="color:red">*</span></div>
	                 	<div class="col-md-12">
	                 		<input type="text" name="tracking_id" id="tracking_id" class="form-control">
	                 	</div>	
	                 </div>

            
	        </div>
	        <div class="modal-footer">
	            <button class="btn btn-primary " id="update_dispatch_detail">Submit</button>
	            <button type="button" class="btn btn-light waves-effect waves-light" data-dismiss="modal">Close</button>

	        </div>
         
    </div>
</div>
</div>
