

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1> Manage Books 
  <a class="btn btn-primary" href="add_edit_book.php">Add New Book</a>
  </h1>
  
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Books</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>



<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		
		  <table id="example2" class="table table-bordered table-hover">
			                   
							   
							<thead>
							<tr>
								<th colspan="2"style="background-color: brown;color:white;" >Book Detail</th>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Book Image</th>
								<td style="width:70%"><img src="<?php if($result_book_list[0]['book_thumbnail']==BOOK_THUMBNAIL_FOLDER_HTTP) echo BOOK_THUMBNAIL_FOLDER_HTTP."default.png"; else echo $result_book_list[0]['book_thumbnail']; ?>" width="100px" height="100px"/></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Book Title</th>
								<td style="width:70%"><?=$result_book_list[0]['book_title']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0">Book Title Marathi</th>
								<td style="width:70%"><?=$result_book_list[0]['book_title_mr']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0">Author Name</th>
								<td style="width:70%"><?=$result_book_list[0]['author_name']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0">Compiled by</th>
								<td style="width:70%"><?=$result_book_list[0]['compile_by']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0">Translated by</th>
								<td style="width:70%"><?=$result_book_list[0]['translated_by']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0">Language</th>
								<td style="width:70%"><?=$result_book_list[0]['book_language']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0">MRP  Rs.</th>
								<td style="width:70%"><?=$result_book_list[0]['book_mrp']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0">Edition</th>
								<td style="width:70%"><?=$result_book_list[0]['book_edition']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0"> Is Upcoming Book</th>
								<td style="width:70%"><?=$result_book_list[0]['is_upcoming_book']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0"> Book Description</th>
								<td style="width:70%"><?=$result_book_list[0]['book_description']; ?></td>
							</tr>							
							<tr>
								<th id="sub_admin_grid_c0"> ISBN No</th>
								<td style="width:70%"><?=$result_book_list[0]['isbn_no']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0"> Production Cost Rs.</th>
								<td style="width:70%"><?=$result_book_list[0]['production_cost']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Book Category</th>
								<td style="width:70%"><?=$result_book_list[0]['category_title']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Dimension Width (in mm)</th>
								<td style="width:70%"><?=$result_book_list[0]['dim_width']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Dimension Height  (in mm)</th>
								<td style="width:70%"><?=$result_book_list[0]['dim_height']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Number of Pages</th>
								<td style="width:70%"><?=$result_book_list[0]['no_of_pages']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Binding Type</th>
								<td style="width:70%"><?=$result_book_list[0]['binding_type']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Cover Type</th>
								<td style="width:70%"><?=$result_book_list[0]['cover_type']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Weight  (in gm)</th>
								<td style="width:70%"><?=$result_book_list[0]['book_weight']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0"> Translated ?    </th>
								<td style="width:70%"><?=$result_book_list[0]['is_translated']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0"> Translated From (Book Name)</th>
								<td style="width:70%"><?=$result_book_list[0]['translated_from_book']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Translated From Language(Orignal Book Language)</th>
								<td style="width:70%"><?=$result_book_list[0]['translated_frm_lang']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0"> Publication Date ( 'DD-MM-YYYY')</th>
								<td style="width:70%"><?=$result_book_list[0]['created_date']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0"> Co-Author</th>
								<td style="width:70%"><?=$result_book_list[0]['co_author']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Contributor</th>
								<td style="width:70%"><?=$result_book_list[0]['contributor']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Editor</th>
								<td style="width:70%"><?=$result_book_list[0]['editor']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Copyright</th>
								<td style="width:70%"><?=$result_book_list[0]['copyright']; ?></td>
							</tr>
							
							
							
							
							<tr>
								<th colspan="2"style="background-color: brown;color:white;" >Publisher's Information</th>
								
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Publisher's Name</th>
								<td style="width:70%"><?=$result_book_list[0]['pub_name']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Publisher's Email</th>
								<td style="width:70%"><?=$result_book_list[0]['pub_email']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Publisher's Address</th>
								<td style="width:70%"><?=$result_book_list[0]['pub_addr']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Publisher's Website</th>
								<td style="width:70%"><?=$result_book_list[0]['pub_website']; ?></td>
							</tr>
							<tr>
								<th id="sub_admin_grid_c0">Publisher's Contact No</th>
								<td style="width:70%"><?=$result_book_list[0]['pub_contact']; ?></td>
							</tr>
							
							
							
							</thead>
							
		  </table>
		</div><!-- /.box-body -->
		

	  </div><!-- /.box -->

	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
