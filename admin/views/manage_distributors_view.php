<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	Manage Distributors
	<small><a class="btn btn-primary" href="add_edit_distributor.php">Add New Distributor</a></small>
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Manage Distributors</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-striped table-hover">
			<thead>
				  <?php if(count($result_distributors_list)>0){?>                       
							<thead>
							<tr>
								<th id="sub_admin_grid_c0">Sr No.</th>
								<th id="sub_admin_grid_c0">Distributor Name</th>
								<th id="sub_admin_grid_c1">Contact Number</th>
								<th id="sub_admin_grid_c2">Email ID</th>
								<th id="sub_admin_grid_c3">Website</th>
								<th id="sub_admin_grid_c3">City </th>
								<th id="sub_admin_grid_c3">State</th>
								<th id="sub_admin_grid_c3">Zip Code</th>
								<th id="sub_admin_grid_c3">Action</th>
								
							</tr>
							</thead>
							<tbody>
                                                         
							<?php 
							$sr=1;
							for($i=0;$i<count($result_distributors_list);$i++){
							
							if($_REQUEST['pages']!="" &&$_REQUEST['pages']!=1) 
                            {
                                    $sr=($per_page * ($_REQUEST['pages']-1))+($i+1);
                            }
							
							
							?>	
								<tr class="odd">
									<td style="width:5%"><?=$sr;?></td>
									<td style="width:10%"><?=$result_distributors_list[$i]['distributor_name'];?></td>
									<td style="width:10%"><?=$result_distributors_list[$i]['distributor_contact_no'];?></td>
									<td style="width:10%"><?=$result_distributors_list[$i]['distributor_emailid'];?></td>
									<td style="width:10%"><?=$result_distributors_list[$i]['distributor_website'];?></td>
									<td style="width:5%"><?=$result_distributors_list[$i]['distributor_city_id'];?></td>
									<td style="width:5%"><?=$result_distributors_list[$i]['distributor_state_id'];?></td>
									<td style="width:5%"><?=$result_distributors_list[$i]['zip_code'];?></td>
									<td style="width:8%">
									
									<a rel="view_distributors.php" class="edit_info" id="<?=$result_distributors_list[$i]['distributor_id'];?>"><i class="fa fa-eye view_distributor_detail" aria-hidden="true"></i></a> &nbsp; 
									
							<a title="Delete"  class="delete_info" id="<?=$result_distributors_list[$i]['distributor_id'];?>" rel="manage_distributors.php"><i class="fa fa-trash" aria-hidden="true"></i></a> &nbsp;
							<a title="Edit" class="edit_info" id="<?=$result_distributors_list[$i]['distributor_id'];?>" rel="add_edit_distributor.php"><i class="fa fa-pencil" aria-hidden="true"></i></a>
									<!--
                                         <a  title=""  style="cursor: pointer" class="view_distributor_detail" lang="">View</a> 
										<a title="Edit" class="edit_info" id="<?=$result_distributors_list[$i]['distributor_id'];?>" rel="add_edit_distributor.php">Edit</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_distributors_list[$i]['distributor_id'];?>" rel="manage_distributors.php">Delete</a>	-->																						
									</td>
								</tr>
								
								<?php  $sr=$sr+1; }?>								
							</tbody>
							<?php } else{?>
							<tr><td>No Data</td></tr>
							<?php } ?>
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

		 <!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 
					 $pages = ceil(($total_record/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['pages'])) 
                         {

                             if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_distributors.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_distributors.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_distributors.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_distributors.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
