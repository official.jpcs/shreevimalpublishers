
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
	<?php echo $title;?>
	
  </h1>
  <ol class="breadcrumb">
	<li><a href="#" class="show"><i class="fa fa-search"></i> Search</a></li>
  </ol>
 
</section>



<!-- Main content -->
<section class="content">


		
<?php 


			
			$search_file="manage_incomplete_orders.php";
			$from_date='order_date_from';
			$to_date='order_date_to';
			$search_array= array("From_To_date_orders");

			//Include Search Section
			include(NAVIGATION_FILE."search.php");
/*
/*
<div class="row filter-panel collapse in" id="filter-panel" style="height: auto;display:none">
	<div class="col-sm-12">
	  <div class="box">
		<div class="box-body ">


		<!------------------ Filter OR Search start----------------------->
		
	<div  class="filter-panel collapse in">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form" method="POST" action="manage_incomplete_orders.php">
					<div class="form-group">
					<input id="order_date" class="form-control datepicker" type="text" placeholder="Select Date" name="order_date"   value="<?php if(isset($_REQUEST['order_date']) && $_REQUEST['order_date']!="") echo $_REQUEST['order_date'];?>" style="width: 72%;float: left;border-radius: 0px;">
					<button id="order_datebtn" style="padding:5px;"class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
												 </div><!-- form group [search] -->
						
						
                        <div class="form-group">
							<button type="submit" id="search" class="btn btn-primary">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							</button>
							<button type="reset"  class="btn btn-primary" rel="manage_incomplete_orders.php"><span class="fa fa-refresh" aria-hidden="true"></span>
							</button>						
       					</div>
                    </form>
                </div>
            </div>
        </div>
		<!------------------ Filter OR Search start----------------------->
	  
		</div>
	  </div>
	</div>
</div>
*/
?>

  <div class="row">
	<div class="col-sm-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		
		
		<div class="box-body ">
		
		<ul class="nav nav-tabs">
			<li class="active">
				<a  href="manage_incomplete_orders.php" aria-expanded="true">Incomplete Orders (<?php echo $total_IncompleteOrders?>)
				</a>
			</li>
			<li><a  href="manage_orders.php" aria-expanded="false">Completed Orders</a>
			</li>
			  		

		</ul>

		<div class="tab-content">
		
		
		<div id="incmp_order" class="tab-pane fade active in">

		<form name="frm_incompleted" action="manage_orders.php" method="POST">
		  <table id="example2" class=" table-responsive table table-bordered table-hover">
				<?php 
			if(count($incompleted_order_list) >0)
			{
			?>
			<thead>
			  <tr>
				<th width="5%">Sr No</th>
				<!--<th width="5%">#Order ID</th>-->
				<th width="10%">Customer Name</th>
				<!--<th width="10%">Total Amt</th>-->
				<th width="5%">Discount Amount</th>
				<th width="8%"> Amount Paid</th>
				<th width="8%">Order Date</th>
				<!--<th width="10%">Order Status Incompleted</th>-->
				<th width="10%">Payment Status</th>

				<th width="5%">Action</th>
			  </tr>
			</thead>
			<tbody>
			<?php 
			 $cnt=1;
			for($list_i=0;$list_i<count($incompleted_order_list);$list_i++)
			{
			 
                if($_REQUEST['page']!="" &&$_REQUEST['page']!=1) 
                {
                        $cnt=($per_page * ($_REQUEST['page']-1))+($list_i+1);
                }
							
			?>
			  <tr>
				<td><?=$cnt;?></td>
				<!--<td>#<?=$incompleted_order_list[$list_i]['order_id'];?></td>-->
				<td><?=$incompleted_order_list[$list_i]['customer_name'] . "<br/> ". $incompleted_order_list[$list_i]['taluka']." ,".$incompleted_order_list[$list_i]['district'];?></td>
				<!--<td><?//=$incompleted_order_list[$list_i]['order_total_amount'];?></td>-->
				<td><?php echo $incompleted_order_list[$list_i]['discount_amount'];?></td>
				<td style='color:green'><b><?="Rs. ".number_format($incompleted_order_list[$list_i]['final_amount_paid'],2) ." /-";?></b></td>
				<td><?= date("jS M Y", strtotime($incompleted_order_list[$list_i]['order_date'])); ?></td>
				<!--<td><?php //if($incompleted_order_list[$list_i]['order_status']==0)echo "<span style='color:red'>Incompleted</span>"; else echo "<span style='color:green'>Completed</span>";?></td>-->
				<td><?php if($incompleted_order_list[$list_i]['payment_status']==0)echo "<span style='color:red'>Incompleted</span>"; else echo "<span style='color:green;font-weight:bold'>Completed</span>";?></td>
				
				
				<td>
					<a href="order_detail.php?id=<?=$incompleted_order_list[$list_i]['order_id']; ?>&step=view_info"><i class="fa fa-eye" aria-hidden="true"></i></a> &nbsp; 
					 &nbsp;
				</td>
			  </tr>
			  <?php 
			  $cnt=$cnt+1;
			  }  
}
			else
			{
			?>
			 <tr><td>Order list not available to display. </td></tr>
			<?php 
			}
			?>
			
			</tbody>
		  </table>
		<input type="hidden" name="step" value="make_complete_order">
		<button class="btn btn-primary pull-right" id="make_complete_order"  style="display:none;">Mark Order As Delivered</button><br/><br/>
		</form>
		
		<?php
		
		// Total Records
				$total_records=$total_IncompleteOrders;

				//Include Pagination Section
				include(NAVIGATION_FILE."pagination.php");
		/*
		<!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					  $order_date="";
					 if(isset($_REQUEST['order_date']) && $_REQUEST['order_date']!="")
						$order_date="&order_date=".$_REQUEST['order_date'];
					 
					 $pages = ceil(($total_IncompleteOrders/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['incmp_ord_pages'])) 
                         {

                             if($_REQUEST['incmp_ord_pages']==$i || $_REQUEST['incmp_ord_pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_incomplete_orders.php?incmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_incomplete_orders.php?incmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_incomplete_orders.php?incmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_incomplete_orders.php?incmp_ord_pages=' .$i.$order_date. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>
			 
			*/ ?>
		</div><!-- /#incmp_order -->
		

		</div><!-- /.tab-content-->
		</div><!-- /.box-body -->
	  </div><!-- /.box -->

	  
	  
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
  <div class="modal fade bs-example-modal-lg" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span style="color:red;">Send By Email Or SMS Or Both<span></h4>
        </div>
        <div class="modal-body modal-lg">
          <div id="send_sms_email_info"></div>
          <div id="sms_content_div" class="hide"><br>
		  <label>SMS Content <span class='red'>*</span></label>
         <textarea name="sms_content" id="sms_content" class="form-control ckeditor"> </textarea><br>
         </div>
		 <div style="border-top:1px dotted red" class="col-md-12">&nbsp;</div>
		 <div id="email_content_div" class="hide">
		 <br>
                  <div class="form-group">
                    <label>Email Subject<span class="red">*</span></label>
					<input type="text" name="email_subject" id="email_subject"  class="form-control" placeholder="Enter email subject" value="">
										
                  </div><!-- /.form-group -->
		<label>Email Content <span class='red'>*</span></label>		
		 <textarea name="email_content" id="email_content" class="form-control ckeditor"> </textarea>
        </div>
       
        </div>
        <div class="modal-footer">
          <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button"  class="btn btn-default sendemail" name="send" data-dismiss="modal">Send</button>
        </div>
      </div>
      
    </div>
  </div>
  