<div class="container-fluid">
  <div class="row"><div class="col-md-16"> <!--dashboard container-->

<div class="dashboard">
<h5 class="text-left">Make Delivery Challan Entry</h5>
<div class="clear"></div>

	<div class="tenant-dashboard clearfix">
		 <div class="clear"></div>
		  
		 <div class="my-profile form-top clearfix">
				 <form class="form-vertical" id="frm_new_delivery" name="frm_new_delivery" action="#" method="post" enctype="multipart/form-data"> 
					
					<input type="hidden" value="add_delivery_entry" name="step"/>
					<!--Start Of Section2 : Purchase Entry-->
					<div class="panel panel-default">
					  <div class="panel-heading">
							<div class="w50L">
								<h4 class="panel-title">Book List</h4>
							</div>
					  </div>
					  <div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
										  
							<div class="clear"></div>		
							
							<div class="grid-view clearfix" id="new_sale_grid">

								<table class="items table table-striped table-bordered">
								
									<thead>
										<tr>
											<th id="sub_admin_grid_c1">Book Title</th>
											<th id="sub_admin_grid_c2">Price</th>
											<th id="sub_admin_grid_c2">Discount</th>
											<th id="sub_admin_grid_c3">Quantity</th>
											<th id="sub_admin_grid_c3">Amount</th>											
											<th class="button-column" id="sub_admin_grid_c4">Action</th>
										</tr>
									</thead>
									
									<tbody id="sale_entry_table"></tbody>
									
								 </table>	
							</div>	 							
							
							<a class="small_btn" type="submit" name="yt0" style="float:right;margin-top:10px;cursor:pointer" data-whatever="@mdo" data-target="#saleModal" data-toggle="modal" data-backdrop="static" data-keyboard="false">
								<i class="fa fa-plus" aria-hidden="true"></i>
							</a>																						

						</div>
					  </div>
					</div>
					<!--End of Section1--> 	


					<!--Start Of Section2 : Purchase Entry-->
						<div class="panel panel-default">
						  <div class="panel-heading">
								<div class="w50L">
									<h4 class="panel-title">Billing Information</h4>
								</div>
						  </div>
						  <div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
											  
								<div class="clear"></div>		
								
								
								
								<div class="grid-view clearfix" id="new_sale_grid_billing">
						
									<table class="items table table-striped table-bordered">
									
										<thead>
											<tr>
												<th id="sub_admin_grid_c0">Date</a></th>
												<th id="sub_admin_grid_c1">Party</th>
												<th id="sub_admin_grid_c3">Final Amount</th>
											</tr>
										</thead>
										<tbody>
											<tr class="odd">
											
												<td style="width:20%">
																									
													<input id="saleDate" class="form-control" type="text" placeholder="Select Date" name="delivery_date"  required value="<?=date("m/d/Y")?>" style="width: 72%;float: left">
													<button id="saleDatebtn" class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
												</td>
												
												<td style="width:20%">
												  <select name="party_id" id="party_id"  class="form-control" >
													
														 <?php
														  $tbl="tbl_storage_locations";
														  $val ="location_id";
														  $show="location_addr";
														  $status="is_deleted";
														  $clause =" `is_deleted` ='0'";
														  $FirstNode="---- Select Storage Location ----";
														  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($_SESSION['user']['location_id']), $print = 0, $status, $FirstNode,$prefix='');
														 ?>
												
												  </select>	 
												</td>
												<td style="width:18%;">
													<input type="hidden" name="total_amount" id="total_amount" value="0"/>
													<input type="hidden" name="final_amount" id="final_amount" value="0"/>
													<label class="final_amount" id="final_amount_lbl"><span class="rupyaINR">Rs</span> 0</label>
												</td> 
												
												
											</tr>
											
										</tbody>
									 </table>	
								</div>	 							
							</div>
						  </div>
						</div>					
					<!--End of Section1--> 	
					
					<!--Start Of Section1 : Customer Information-->
					<div class="panel panel-default">
					  <div class="panel-heading">
							<div class="w50L">
								<h4 class="panel-title">Note</h4>
							</div>
					  </div>
					  <div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
										  
							<div class="col-md-13 ">
							
								<div class="form-group div_book_language"> 
									<textarea name="cust_note" class="form-control" id="cust_note" rows="2" style="width:100%"></textarea>
								</div>
							</div>

						</div>
					  </div>
					</div>
					<!--End of Section1--> 	
					<div class="clear"></div>
					<div class="col-md-6 col-md-offset-6" style="margin-top:10px;">
						<div class="btn-block">
							<button class="btn" type="button" id="submit_delivery_entry" name="yt0">Submit</button>              
							<button class="btn" type="reset" name="yt1">Reset</button>            
						</div>
					</div>
					
				</form>  
		 </div>
	</div>
</div>
<!--end dashboard container-->  
</div>
</div>
</div>


<!-- Add BOOk  -->  
<div class="modal fade" id="saleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Add Book</h4>
      </div>
      <div class="modal-body">
        <form name="frm_connect_train" id="frm_connect_train" method="post" action="party_train_connection.php?party_id=<?=$party_id; ?>">
		 <input type="hidden" name="sel_book_id" id="sel_book_id"  />
                    <div class="form-group">
                      <label for="recipient-name" class="control-label">Search Book Title</label>
                                  <!--	<input type="text" class="form-control" name="book_title" id="search_book"/>	 -->	
                                    
                                    <input id="search_book_name" class="form-control txt-auto" onkeyup="suggest(this,'tbl_bloggers','first_name','suggestions8','0');" />

                                    <!-- smart search div -->
                                    <div id="suggestions8" class="smart_search_div_class"> 
                                      <div id="suggestions8List"> &nbsp; 
                                      </div>
                                     </div>
                                    <!-- smart search div ends here -->
                                    
                                    <select name="search_book" id="search_book" class="form-control" style="display: none">
                                          <option value="0">---- Select Book Title ----</option>
                                          <?php 
                                          for($i_book=0;$i_book <=$total_books-1;$i_book++)
                                          {
                                          ?>
                                                  <option value="<?=$result_book_list[$i_book]['book_id']."~".$result_book_list[$i_book]['book_title']."~".$result_book_list[$i_book]['book_mrp'];  ?>" >
                                                          <?=$result_book_list[$i_book]['book_title']; ?>
                                                  </option>
                                          <?php
                                          }
                                          ?>
                                    </select>	 
                                  <span class="help-block error" id="sel_book_msg" style="display: none">Please Select Book.</span>
                    </div>
                 
                   

                    <div class="form-group"> 		
                             <div class="w40L" style="text-align:left"> 		
                                      <div class="form-group">
                                            <label for="recipient-name" class="control-label">Price</label>
                                                    <input type="text" class="form-control" name="sel_book_price" id="sel_book_price" readonly="" />
                                             <span class="help-block error" id="sel_book_price_msg" style="display: none">Only Numeric.</span>
                                      </div>
                             </div>
                            <!-- oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');-->
                             <div class="w40R" style="text-align:left"> 		
                                      <div class="form-group">
                                            <label for="recipient-name" class="control-label">Discount(%)</label>
                                                    <input type="number" class="form-control calculate_amount" name="discount_txtbox" id="sel_book_discount"  />
                                             <span class="help-block error" id="train_msg" style="display: none">Field cannot be blank.</span>
                                      </div>
                             </div>

                    </div>	 
                      <div class="clear"></div>


                    <div class="form-group"> 		

                             <div class="w40L" style="text-align:left"> 		
                                      <div class="form-group">
                                            <label for="recipient-name" class="control-label">Quantity</label>
                                                    <input type="number" class="form-control calculate_amount" name="qualtity_txtbox" id="qualtity_txtbox" min="1"   step="1" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"/>
                                             <span class="help-block error" id="qualtity_txtbox_msg" style="display: none">Provide Book Quantity.</span>
                                      </div>
                             </div>		  


                             <div class="w40R" style="text-align:left"> 		
                                      <div class="form-group">
                                            <label for="recipient-name" class="control-label">Amount </label>
                                            <input type="number" class="form-control" name="sel_book_amount" id="sel_book_amount" min="1"  />
                                             <span class="help-block error" id="sel_book_amount_msg" style="display: none">Provide Amount.</span>
                                      </div>
                             </div>		  
                    </div>	 

                    <div class="clear"></div>
                     
					<div class="form-group">
					<label for="recipient-name" class="control-label">Storage Location</label>
					  <!--	<input type="text" class="form-control" name="book_title" id="search_book"/>	 -->		
					  <select name="search_book" id="storage_loc" class="form-control" disabled="disabled">
						
							 <?php
							  $tbl="tbl_storage_locations";
							  $val ="location_id";
							  $show="location_addr";
							  $status="is_deleted";
							  $clause =" `is_deleted` ='0'";
							  $FirstNode="---- Select Storage Location ----";
							  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($_SESSION['user']['location_id']), $print = 0, $status, $FirstNode,$prefix='');
							 ?>
					
					  </select>	 
					  <span class="help-block error" id="storage_loc_msg" style="display: none">Select Storage Location</span>
					</div>
                    

                      
		  <div class="clear"></div><div class="clear"></div>
        </form>
		
      </div>
      <div class="modal-footer">
        <button class="btn" type="button" name="yt0" id="frm_save_book_delivery_challan"> Submit</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Popup Ends -->