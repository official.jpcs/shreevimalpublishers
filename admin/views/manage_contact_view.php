
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Contact Us Massage </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Contact Us Massage</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
	<div class="col-xs-12">
	  <div class="box">
		<!--<div class="box-header">
		  <h3 class="box-title">Hover Data Table</h3>
		</div>--><!-- /.box-header -->
		<div class="box-body">
		  <table id="example2" class="table table-bordered table-hover">
			
							<?php 
							 if($total_record >0)
							 {
							?>
							   <thead>
								<tr>
									<th id="sub_admin_grid_c0">Sr. No.</a></th>
									<th id="sub_admin_grid_c0">Contact Name</a></th>
									<th id="sub_admin_grid_c0">Contact No</a></th>
									<th id="sub_admin_grid_c0">Contact Email</a></th>
									<th id="sub_admin_grid_c0">Contact Address</a></th>
									<th id="sub_admin_grid_c0">Created Date</a></th>
									<th class="button-column" id="sub_admin_grid_c4">Action</th>
								</tr>
								</thead>
								<tbody>
	
									<?php  $sr=1;
									for($i_contact=0;$i_contact <=count($result_contact_list)-1;$i_contact++)
									{
									
									if($_REQUEST['pages']!="" &&$_REQUEST['pages']!=1) 
									{
                                    $sr=($per_page * ($_REQUEST['pages']-1))+($i_contact+1);
									}
									
									?>
									<tr class="odd <?php if($result_contact_list[$i_contact]->is_view=='1')
									echo "success"; else echo "danger";?>">
										<td><?=$sr; ?></td>
										<td><?=$result_contact_list[$i_contact]->contact_name; ?></td>
									<td><?=$result_contact_list[$i_contact]->contact_no; ?></td>
									<td><?=$result_contact_list[$i_contact]->contact_email; ?></td>
									<td><?=$result_contact_list[$i_contact]->contact_address; ?></td>
									<td><?=$result_contact_list[$i_contact]->created_date; ?></td>
									
										<td>
										<a title="Edit" class="edit_info" id="<?=$result_contact_list[$i_contact]->contact_id; ?>" rel="view_contact.php">View</a> 
										<a title="Delete"  class="delete_info" id="<?=$result_contact_list[$i_contact]->contact_id; ?>" rel="manage_contact.php">Delete</a>  
											
										</td>
									</tr>
									<?php
									 $sr=$sr+1;
									}
									?>									
								</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Contact list not available to display. </td></tr>
							<?php 
							}
							?>
		  </table>
		</div><!-- /.box-body -->
	  </div><!-- /.box -->
<!--- Pagination Code --->
          <div id="pagination" class="pull-right">
                 <ul class="pagination">
                     <?php
					 
					 $pages = ceil(($total_record/$per_page));
                     //Pagination Numbers
                     for($i=1; $i<=$pages; $i++)
                     {

                         if(isset($_REQUEST['pages'])) 
                         {

                             if($_REQUEST['pages']==$i || $_REQUEST['pages']=='')
                                 echo '<li id="'.$i.'" class="active"><a href="manage_contact.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                                 else
                                 echo '<li id="'.$i.'"><a href="manage_contact.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                         else
                         {
                            if($i==1)
			echo '<li id="'.$i.'" class="active"><a href="manage_contact.php?pages=' .$i. '">  ' .$i. '  </a></li>';
			else
			echo '<li id="'.$i.'"><a href="manage_contact.php?pages=' .$i. '">  ' .$i. '  </a></li>';
                         }
                     }
                     ?>
                 </ul>
             </div>
	  <!-- /.box -->
	</div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
