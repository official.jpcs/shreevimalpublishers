
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  <?php if($step=="edit_info") {echo "Update Sales Entry";}else{echo "Add New Sales Entry";}?> 
  </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Edit Sales Entry</a></li>
  </ol>
</section>



<!-- Main content -->
	<section class="content">

<div class="box box-default">
				<div class="box-body">
				  <div class="row col-md-12">






<h5 class="text-left">Sales Entry Detail</h5>
 
	<div class="tenant-dashboard clearfix">
		 <div class="clear"></div>
		 
		  
		 <div class="my-profile form-top clearfix">
					<!--Start Of Section2 : Purchase Entry-->
										  
							<div class="clear"></div>		

							<div class="grid-view clearfix" id="view_sale_customer_info">
							
								<table class="items table table-striped table-bordered">
								
									<thead>
										<tr>
											<th id="sub_admin_grid_c0">#Invoice ID</a></th>
											<th id="sub_admin_grid_c1">Date</th>
											<th id="sub_admin_grid_c3">Sales Type</th>
										</tr>
									</thead>
									<tbody>
										<tr class="odd">
											<td style="width:20%"><?=$sale_master_entry_detail[0]["sales_maste_id"]; ?></td>
											<td style="width:20%;"><?=date("d-M-Y", strtotime($sale_master_entry_detail[0]["sale_date"])); ?></td> 
											<td style="width:40%">
											
												<?php 
												  $sales_type = $sale_master_entry_detail[0]["sales_type"]; 
												  
												  switch($sales_type)
												  {
													   case '1':
													    echo "Cash";
													   break;
													   
													   case '2':
													    echo "Credit";
													   break;
												  }
												?>
											
											</td>
											
											
										</tr>
										
									</tbody>
								 </table>	
							</div>							
							
							<?php
							if($sale_master_entry_detail[0]["customer_name"]!="" )
							{
							?>
							<div class="grid-view clearfix" id="view_sale_customer_info">
							
								<table class="items table table-striped table-bordered">
								
									<thead>
										<tr>
											<th id="sub_admin_grid_c0">Customer Name</a></th>
											<th id="sub_admin_grid_c1">Contact No</th>
											<th id="sub_admin_grid_c3">Email Id</th>
											<th id="sub_admin_grid_c2">Address</th>
										</tr>
									</thead>
									<tbody>
										<tr class="odd">
											<td style="width:20%"><?=$sale_master_entry_detail[0]["customer_name"]; ?></td>
											<td style="width:20%"><?=$sale_master_entry_detail[0]["cust_contact_no"]; ?>	</td>
											<td style="width:18%;"><?=$sale_master_entry_detail[0]["cust_email_id"]; ?>	</td> 
											<td style="width:22%;"><?=$sale_master_entry_detail[0]["cust_address"]; ?>	</td>
										</tr>
										<tr class="odd">
											<td style="width:100%" colspan="4"><?=$sale_master_entry_detail[0]["cust_note"]; ?></td>
										</tr>
										
									</tbody>
								 </table>	
							</div>	
							<?php
							}
							?>						
							
							<div class="grid-view clearfix" id="new_sale_grid">

								<table class="items table table-striped table-bordered">
								
									<thead>
										<tr>
											<th id="sub_admin_grid_c1">Book Title</th>
											<th id="sub_admin_grid_c2">Price</th>
											<th id="sub_admin_grid_c3">Discount</th>
											<th id="sub_admin_grid_c3">Quantity</th>
											<th id="sub_admin_grid_c3">Amount</th>
											
										</tr>
									</thead>
									
									<tbody id="sale_entry_table">
									<?php
										
									  if(count($sales_book_entry_list) >0)
									  {
									  	for($i_sale_entry = 0;$i_sale_entry <=count($sales_book_entry_list)-1;$i_sale_entry++)
										{	
									  
									?>	
										<tr>
											<td style="width:30%"><?=$sales_book_entry_list[$i_sale_entry]["book_title"]; ?></td>
											<td style="width:15%"><?=$sales_book_entry_list[$i_sale_entry]["book_mrp"]; ?></td>
											<td style="width:15%"><?=$sales_book_entry_list[$i_sale_entry]["book_discount"]; ?></td>
											<td style="width:15%"><?=$sales_book_entry_list[$i_sale_entry]["book_quantity"]; ?></td>
											<td style="width:20%"><?=$sales_book_entry_list[$i_sale_entry]["book_sale_amount"]; ?></td>
										</tr>
									<?php
										}
									 }
									?>	
										<tr>
											<td colspan="3">&nbsp;</td>
											<td class="bold_lbl">Total Amount</td>
											<td>
												<label class="total_amount" id="total_amount_lbl">
													<span class="rupyaINR">Rs</span> <?=$sale_master_entry_detail[0]["total_amount"]; ?>
												</label>
											</td>
										</tr>
										
										<tr>
											<td colspan="3">&nbsp;</td>
											<td class="bold_lbl">Service Tax / VAT</td>
											<td>
												<span class="rupyaINR"></span>  <?=$sale_master_entry_detail[0]["tax_amount"]. "%"; ?>
											</td>
										</tr>

										<tr>
											<td colspan="3">&nbsp;</td>
											<td class="bold_lbl">Final Amount</td>
											<td>
												<label class="final_amount" id="final_amount_lbl"><span class="rupyaINR">Rs</span> 
													<?=$sale_master_entry_detail[0]["final_amount"]; ?>
												</label>
											</td>
										</tr>

									</tbody>
									
								 </table>	
							</div>	 							
							
						
					
					
						
		 </div>
	</div>
</div>



</div>
</div>


</section>

</div>


