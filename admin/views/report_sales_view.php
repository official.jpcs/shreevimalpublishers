<!--main container-->
<?php
 $searchFields=array('sale_from_date','sel_location','saleToDate','search_book_name');
 $searchFieldArray=implode(",",$searchFields);
 
// var_dump( $_REQUEST); die;
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Sales Report </h1>
  <ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#">Sales Report</a></li>
	<!--<li class="active">Data tables</li>-->
  </ol>
</section>



<!-- Main content -->
<section class="content">
 
					<form class="form-vertical" id="frmSalesReport" method="POST" autocomplete="off"> 
					
					<!-- SELECT2 EXAMPLE -->
			  <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  
					<div class="col-md-6 ">
									<div class="form-group"> 
										<label for="Admin_first_name">From Date</label> 
											<div class="clear"></div>
											
											<input id="sale_from_date" class="form-control" type="text" placeholder="Select From Date" name="sale_from_date"  style="width: 90%;float: left" value="<?php echo $_REQUEST['sale_from_date']; ?>">
											<button id="saleFromDatebtn" class="datepick_btn" style="padding: 5px;" type="button"><i class="fa fa-calendar" aria-hidden="true" ></i></button>
										
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Location</label> 

										<select name="sel_location[]" id="sel_location" class="form-control">
											 <?php
											  $tbl="tbl_storage_locations";
											  $val ="location_id";
											  $show="location_addr";
											  $status="is_deleted";
											  $clause =" `is_deleted` ='0'";
											  $FirstNode="---- Select Storage Location ----";
											  echo $re->CommonDropDown($tbl, $clause , $val, $show, $select = array($_REQUEST['sel_location']), $print = 0, $status, $FirstNode,$prefix='');
											 ?>
	
										</select>
										
									</div>
									<div class="clear"></div>									
											
								</div>
								
								<div class="col-md-6 ">
								
		
									<div class="form-group"> 
										<label for="Admin_first_name">To Date</label> 
										
											<div class="clear"></div>	
											<input id="saleToDate" class="form-control" type="text" placeholder="Select To Date" name="sale_to_date" value="<?php if(!empty($_REQUEST['saleToDate'])){echo $_REQUEST['saleToDate'];} ?>"  style="width: 90%;float: left">
											<button id="saleToDatebtn" class="datepick_btn" type="button"  style="padding: 5px;"><i class="fa fa-calendar" aria-hidden="true"></i></button>
										
									</div>
									<div class="clear"></div>
		
									<div class="form-group"> 
										<label for="Admin_first_name">Book Title</label> 

											<input id="search_book_name" name="search_book_name" class="form-control txt-auto" onkeyup="suggest(this,'tbl_bloggers','first_name','suggestions8','0','search');"  value="<?=$_REQUEST['search_book_name']; ?>"/>
		
											<!-- smart search div -->
											<div id="suggestions8" class="smart_search_div_class"> 
											  <div id="suggestions8List"> &nbsp; 
											  </div>
											 </div>
											<!-- smart search div ends here -->
											
											<select name="search_book" id="search_book" class="form-control" style="display: none" >
												  <option value="0">---- Select Book Title ----</option>
												  <?php 
												  for($i_book=0;$i_book <=$total_books-1;$i_book++)
												  {
												  ?>
														  <option value="<?=$result_book_list[$i_book]['book_id']."~".$result_book_list[$i_book]['book_title']."~".$result_book_list[$i_book]['book_mrp'];  ?>" >
																  <?=$result_book_list[$i_book]['book_title']; ?>
														  </option>
												  <?php
												  }
												  ?>
											</select>	 
										  <span class="help-block error" id="sel_book_msg" style="display: none">Please Select Book.</span>
										
									</div>
									<div class="clear"></div>
									
		
								</div>
								
								  </div><!-- /.row -->
				</div><!-- /.box-body -->
				
				<div class="box-footer">
					<input type="hidden" id="step_search" name="step_search" value="search">
					<button class="btn" type="button" name="yt0" onclick='funDoSearchActionByField("search","sales_report.php","frmSalesReport",<?php echo json_encode($searchFieldArray) ?>)'>Submit</button>              
					<button class="btn" type="reset" name="yt1" onclick="funResetRedirect('sales_report.php')">Reset</button>  			
				</div>
								
				</div><!-- /.box -->
							
			</form>  



       
       <!--------list--view---------->
	   <div class="box box-default">
				<div class="box-body">
				  <div class="row">
				  <div class="col-md-12">
			<div class="list-view">
				
				<div class="clear"></div>
				<div class="grid-view clearfix" id="sub_admin_grid">
					<div class="page-dropdown pull-left" style="font-weight:bold">Total Record : <?=$report_count ? $report_count :0 ; ?></div>
						<div class="page-dropdown pull-right">
						<?php
						 if($report_count >0)
						 {
						 ?>	
							<a title="Export To Excel" style="cursor:pointer" onclick='funDoSearchActionByField("export","sales_report.php","frmSalesReport",<?php echo json_encode($searchFieldArray) ?>)'><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>						
							<?php
							 if($filename!="")
							 {
							 ?>
								&nbsp; | &nbsp; <a title="Download File" href="report_files/export/<?=$filename; ?>" download><i class="fa fa-download" aria-hidden="true"></i></a>
							<?php
							 }
							 ?>
						 <?php
						 }
						 ?>	
						</div>
						<table class="items table table-striped table-bordered">
							<?php 
							 if($report_count >0)
							 {
							?>
                                                    
							<thead>
							<tr>
								<th>Sr No.</th>
								<th>Date</th>
								<th>Location</th>
								<th>Book Title</th>
								<th>Price</th>
								<th>Discount</th>																
								<th class="button-column">Quantity</th>
								<th class="button-column">Total Amount</th>
							</tr>
							</thead>
							<tbody>
                                                            
								<?php 
								for($i_report=0;$i_report <=$report_count-1;$i_report++)
								{
								 $sr=$i_report+1;
								?>
								<tr class="odd">
									<td style="width:5%"><?=$sr ; ?></td>
									<td style="width:10%"><?= date("d-M-Y", strtotime($salesReportList[$i_report]['sale_date'])); ?></td>
									<td style="width:10%"><?=$salesReportList[$i_report]['location_addr']; ?></td>
									<td style="width:25%"><?=$salesReportList[$i_report]['book_title']; ?></td>
									<td style="width:8%"><span class='rupyaINR'>Rs</span> <?=$salesReportList[$i_report]['book_mrp']; ?></td>
									<td style="width:10%"><?=$salesReportList[$i_report]['book_discount']; ?>%</td>																		
									<td style="width:8%"><?=$salesReportList[$i_report]['book_quantity']; ?></td>
									<td style="width:10%"><span class='rupyaINR'>Rs</span> <?=$salesReportList[$i_report]['book_sale_amount']; ?></td>
									
								</tr>
								<?php
								}
								?>									
							</tbody>
							<?php 
							}
							else
							{
							?>
							 <tr><td>Report not available to  display.</td></tr>
							<?php 
							}
							?>
						</table>
					
				</div>        
			</div>
			</div>
			
			 </div><!-- /.row -->
				</div><!-- /.box-body -->
				</div><!-- /.box-->
       <!--------list--view----------> 

   

</section><!-- /.content -->
</div>
<!-- /.content-wrapper -->
