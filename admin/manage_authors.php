<?php 
/***************************************************************
 *  File Name : Manage Publisher
 *  Created Date: 21/03/2015
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Master Content";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_authors_publishers")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");


// Include Header Section
include(NAVIGATION_FILE . "header.php");

//User Type
$user_type=2;

//Include Controller Section
include(CONTROLLER_PATH."AuthorPublisherController.php");

//Include View Section
include( VIEW_PATH."manage_authors_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
