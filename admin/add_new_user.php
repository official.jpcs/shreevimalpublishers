<?php
$tab="Manage Users";
/* Including Globally Declared Variables */
include("config/config.php");


$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");


/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';

$page="add_new_user";

// Include Header Section
include(NAVIGATION_FILE . "header.php");

// Include Controller 
include("controller/UserController.php");



// Get Counts
//$counts=$re->fun_get_counts();

// Include View
include(VIEW_PATH."add_new_user_view.php");


//Include Footer
include(NAVIGATION_FILE ."footer.php");
?>