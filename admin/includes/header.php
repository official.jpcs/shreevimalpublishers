
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vimal Publisher - Admin Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/mobile_wrapper.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
	<link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
	
<!-- Fotorama from CDNJS, 19 KB -->
<link  href="css/fotorama.css" rel="stylesheet">
  
	<?php	
		/* Including the common_includes_css.php to include all css files in Add_css array */
		//include_once(NAVIGATION_PATH . "common_includes_css.php");
	?>
  </head>
  <body class="hold-transition skin-red sidebar-mini ">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Backend</b> Storage</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
			  <li class="dropdown user user-menu">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<?=$_SESSION['user']["name"];?>&nbsp;&nbsp;
				  <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
				  <span class="hidden-xs"><i class="fa fa-power-off "></i></span>-->
				</a>
			  </li>
              <li>
                <a href="log_out.php" data-toggle="index.php"><i class="fa fa-power-off"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->

	<!--end header--> 
	 <?php 
		
		if(count($Messages) >0) 
		{       
				echo implode("<br/>",$Messages); 
				// $_SESSION['Message']=0 ;
				//die;
		} 
		
	 ?>
	  
	  
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
		  
          
           <?php 
            include_once("left_menu.php");
           ?>

     
        </section>
        <!-- /.sidebar -->
      </aside>
