<?php 
	/**************************************************************
	*  messages.php
	*  Description: File is used to display Messages used in  Appzeal
	*  Created On : 04/03/2015
	*  Created By: Prasad
	*  NOTE : use 'alert-success' class to display Success Message & use 'alert-error' class to display ' Error Message'	 
	***************************************************************/

	$Message = (isset($_SESSION['Message_ID']) && $_SESSION['Message_ID']!= "") ? $_SESSION['Message_ID'] : '0';
	
	
	switch($Message)
	{
                
	 	case '1'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Invalid login credentials</div>'; break;
		case '2'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New vendor created successfully. </div>'; break;
		case '3'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Vendor info updated successfully. </div>'; break;
		case '4'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Vendor deleted successfully. </div>'; break;
	 	case '30'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Daily Thougth successfully created. </div>'; break;
	 	case '31'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Daily Thougth successfully updated. </div>'; break;
	 	case '32'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Daily Thougth successfully deleted. </div>'; break;
	 	case '40'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Pooja Prakar successfully created. </div>'; break;
	 	case '41'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Pooja Prakar successfully updated. </div>'; break;
	 	case '42'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Pooja Prakar successfully deleted. </div>'; break;
	 	
		case '100'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Book added succesfully.</div>'; break;
		case '101'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error while adding book.</div>'; break;

	 	case '102'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Book Information updated succesfully.</div>'; break;
		case '103'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error while updating booke.</div>'; break;
		
		case '104'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Language Added successfully.</div>'; break;
		case '105'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error while adding new language.</div>'; break;
		case '106'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Language updated successfully.</div>'; break;
		case '107'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Language entry deleted successfully.</div>'; break;


		/******* Sales Entry *******/ 
	 	case '200'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Sales Entry Added Succesfully.</div>'; break;
		case '201'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error While Adding Sales Entry.</div>'; break;
		case '202'   :	$rec_msg = '<div id="message_div"  class="alert alert-success" align="center">Sales Entry Deleted Successfully.</div>'; break;
		case '203'   :	$rec_msg = '<div id="message_div"  class="alert alert-success" align="center">Sales Entry Updated Successfully.</div>'; break;

        /********* User Module  ******/
        case '301'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Invalid Login details provided.</div>'; break;
		case '302'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Email address provided, does not exist.</div>'; break;
		case '303'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Username has been sent to the Email address provided.</div>'; break;
		case '304'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Password sent to Email address provided.</div>'; break;
		case '305'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Email address specified already exists, please use a different Email address.</div>'; break;
		case '306'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Username specified already exists, please use a different Username.</div>'; break;
		case '307'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">You have been successfully logged out.</div>'; break;
        case '308'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New User Created Successfully.</div>'; break;

	}
	unset($_SESSION['Message_ID']) ;	
?>