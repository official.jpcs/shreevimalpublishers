<?php 
	/**************************************************************
	*  messages.php
	*  Description: File is used to display Messages used in  Appzeal
	*  Created On : 04/03/2015
	*  Created By: Prasad
	*  NOTE : use 'alert-success' class to display Success Message & use 'alert-error' class to display ' Error Message'	 
	***************************************************************/

	$Message = (isset($_SESSION['Message']) && $_SESSION['Message']!= "") ? $_SESSION['Message'] : '0';
	
	
	switch($Message)
	{
                /******* Book Module *******/ 
	 	case '1'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Book category added succesfully.</div>'; break;
		case '2'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error while adding category.</div>'; break;
	 	case '100'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Book added succesfully.</div>'; break;
		case '101'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error while adding book.</div>'; break;

	 	case '102'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Book Information updated succesfully.</div>'; break;
		case '103'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error while updating book.</div>'; break;
		
		case '104'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Language Added successfully.</div>'; break;
		case '105'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error while adding new language.</div>'; break;
		case '106'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Language updated successfully.</div>'; break;
		case '107'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Language entry deleted successfully.</div>'; break;

	 	case '108'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Book Publisher Information updated succesfully.</div>'; break;

	 	case '109'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Book Details updated succesfully.</div>'; break;
		
	 	case '110'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Book Storag Details updated succesfully.</div>'; break;


	 	case '111'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Related Book Info updated succesfully.</div>'; break;

	 	case '112'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Please Select Related Books.</div>'; break;
		
		/******* Sales Entry *******/ 
	 	case '200'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Sales Entry Added Succesfully.</div>'; break;
		case '201'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Error While Adding Sales Entry.</div>'; break;
		case '202'   :	$rec_msg = '<div id="message_div"  class="alert alert-success" align="center">Sales Entry Deleted Successfully.</div>'; break;
		case '203'   :	$rec_msg = '<div id="message_div"  class="alert alert-success" align="center">Sales Entry Updated Successfully.</div>'; break;

        /********* User Module  ******/
        case '301'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Invalid Login details provided.</div>'; break;
		case '302'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Email address provided, does not exist.</div>'; break;
		case '303'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">Username has been sent to the Email address provided.</div>'; break;
		case '304'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New Password sent to Email address provided.</div>'; break;
		case '305'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Email address specified already exists, please use a different Email address.</div>'; break;
		case '306'   :	$rec_msg = '<div id="message_div" class="alert alert-error" align="center">Username specified already exists, please use a different Username.</div>'; break;
		case '307'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">You have been successfully logged out.</div>'; break;
        case '308'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">New User Created Successfully.</div>'; break;
		
		case '309'   :	$rec_msg = '<div id="message_div" class="alert alert-success" align="center">My Profile Successfully Updated.</div>'; break;

	}
	unset($_SESSION['Message']) ;	
?>