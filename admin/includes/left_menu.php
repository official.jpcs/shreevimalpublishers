<ul class="sidebar-menu">
    <li class="treeview <?php if($tab=="Dashboard") { echo "active"; } ?>">
        <a href="dashboard.php">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span> <!--<i class="fa fa-angle-left pull-right"></i>-->
        </a>
    </li>
	<!---
	<li class="treeview <?php if($tab=="Manage Users") { echo 'active'; } ?>">
        <a href="manage_users.php">
            <i class="fa fa-th"></i>
            <span>Manage Users</span>
        </a>
    </li>--->
							<li <?php if($tab=="Manage Distributors") { echo'class="active"'; } ?> >
								<a tabindex="-1" href="manage_distributors.php"><i class="glyphicon glyphicon-list"></i><span> &nbsp; Manage Distributors</span> </a>
							</li>
							
							<li <?php if($tab=="Contact Us Massage") { echo'class="active"'; } ?> >
								<a tabindex="-1" href="manage_contact.php"><i class="glyphicon glyphicon-phone"></i><span> &nbsp; Contact Us Massage</span> </a>
							</li>
							
							<li <?php if($tab=="Manage Subscriber") { echo'class="active"'; } ?> >
								<a tabindex="-1" href="manage_subscriber.php"><i class="glyphicon glyphicon-list"></i><span> &nbsp; Manage Subscriber</span> </a>
							</li>
							<li <?php if($tab=="Manage Books") { echo "class='active'"; } ?>>
								<a tabindex="-1" href="manage_books.php"><span><i class="glyphicon glyphicon-book"></i> &nbsp; Manage Books</span> </a>
							</li>


							<li <?php if($tab=="Manage Orders") { echo "class='active'"; } ?>>
								<a tabindex="-1" href="manage_incomplete_orders.php"><span> <i class="fa fa-rupee"></i> &nbsp; Manage Orders</span> </a>
							</li>

							
							
							<li class="treeview <?php if($tab =="Sales") { echo "class='active'"; } ?>">
								<a href="#">
								<i class="fa fa-th"></i>
									<span>Sales</span>
									<span class="label label-primary pull-right"> + </span>
								</a>
								<ul class="treeview-menu">
								<li role="presentation">
								<a  tabindex="-1" href="new_sales_entry.php">Sale Entry</a>
								</li>
								<li role="presentation"><a  tabindex="-1" href="manage_sales.php">Manage Sales</a></li>
								</ul>
							</li>
							
						


						<li <?php if($tab=="Invoices") { echo "class='active'"; } ?>>
								<a tabindex="-1" href="admin/contractors"><span><i class="glyphicon glyphicon-user"></i> &nbsp; Invoices</span> </a>
							</li>

						<li <?php if($tab=="Tax Invoice") { echo "class='active'"; } ?>>
								<a tabindex="-1" href="admin/contractors"><span><i class="glyphicon glyphicon-user"></i> &nbsp; Tax Invoice</span> </a>
							</li>

						<li <?php if($tab=="Report") { echo "class='active'"; } ?>>
								<a tabindex="-1" href="sales_report.php"><span><i class="glyphicon glyphicon-user"></i> &nbsp; Report</span> </a>
							</li>
							
							
							
							
							<li class="treeview <?php if($tab=="Master Content") { echo'class="active"'; } ?>">
							<a href="#">
							<i class="fa fa-th"></i>
								<span>&nbsp; Master Content</span>
								<span class="label label-primary pull-right"> + </span>
							</a>
						<ul class="treeview-menu">
									<li role="presentation"><a  tabindex="-1" href="manage_categories.php">Manage Categories</a></li>
									<li role="presentation"><a  tabindex="-1" href="manage_binding_types.php">Binding Type</a></li>
									<li role="presentation"><a  tabindex="-1" href="manage_coverage_types.php">Cover Type</a></li>
									<li role="presentation"><a  tabindex="-1" href="manage_languages.php">Manage Languages</a></li>
									<li role="presentation"><a  tabindex="-1" href="manage_publishers.php">Manage Publishers</a></li>
									<li role="presentation"><a  tabindex="-1" href="manage_authors.php">Manage Authors</a></li>
									<li role="presentation"><a  tabindex="-1" href="manage_storage_locations.php">Manage Storage Location</a></li>
									<li role="presentation"><a  tabindex="-1" href="manage_users.php">Manage User</a></li>
								  </ul>	
					</li>
						<!---
						<li class="treeview <?php if($tab=="Sales") { echo'class="active"'; } ?>">

						<a href="#">
							<i class="fa fa-th"></i>
								<span>&nbsp; Manage Sales</span>
								<span class="label label-primary pull-right"> + </span>
							</a>
								<ul class="treeview-menu">
														<li role="presentation"><a  tabindex="-1" href="new_sales_entry.php">Sale Entry</a></li>
														<li role="presentation"><a  tabindex="-1" href="manage_sales.php">Manage Sales</a></li>
												  </ul>				
										</li>	--->					
							
							
							<li  class="treeview <?php if($tab=="My Account") { echo'class="active"'; } ?>"> 
							<a href="#">
							<i class="fa fa-th"></i>
								<span>&nbsp; My Account</span>
								<span class="label label-primary pull-right"> + </span>
							</a>
								
									<ul class="treeview-menu">
											<li role="presentation"><a  tabindex="-1" href="my_shop.php">My Shop</a></li>
											<li role="presentation"><a  tabindex="-1" href="my_profile.php">Profile</a></li>
									  </ul>				
							</li>
							
							<li <?php if($tab=="Manage Setting") { echo'class="active"'; } ?> >
								<a tabindex="-1" href="manage_setting.php"><i class="glyphicon glyphicon-list"></i><span> &nbsp; Manage Setting</span> </a>
							</li>
							

							
</ul>