<?php
/* * ************************************************************
*  File Name : common_includes_css.php
*  File Description: includes all css files which are sent in AddCSS array of Motherwhood Project.
*  Author: Benchmark, 
*  Created Date: 24/10/2013
*  Created By: Raju Tikale
* ************************************************************* */
    if (isset($AddCSS) && count($AddCSS) > 0) 
    {
        $files_count = count($AddCSS);
        for ($CSS = 0; $CSS < $files_count; $CSS++) 
        {
            ?>
                <link href="<?php echo CSS_PATH_HTTP . $AddCSS[$CSS] ?>.css" rel="stylesheet" type="text/css"/>
            <?php
        }
    }
?>
		