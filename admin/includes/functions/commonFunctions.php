<?php
/**************************************************************
*  File Name : CommonFunctions.php
*  File Description: Common Reuse Functions For SVPPL
*  Created Date: 25/02/2015
*  Created By: Prasad Bhale
***************************************************************/

class reuseFunction 
{
    function reuseFunction() 
    {
        $this->errorarray = array();
    }
// Function to make a custom drop down
/**
* $tbl = table name
* $clause = condtion (id=1)
* $val = value in option
* $show = Drop down value to be listed
* $select = array of id to shown as selected
* $print = set to 1 to print the query
*/
    function CommonDropDown($tbl, $clause = 1, $val, $show, $select = array(0), $print = 0, $status, $FirstNode = '',$prefix='' ,$order_by ='') 
    {
        include_once(MODEL_DIR_PATH."class.database.php");
		
		$this->database = new Database();

		if($order_by !="")
		{
		 $order_by = $order_by;
		}
		else
		{
		 $order_by = $show;		
		}
			
        $sql = "SELECT {$val},{$show},{$status} FROM {$tbl} WHERE {$clause} ORDER BY {$order_by}"; 

        if ($print > 0)
        {
            echo $sql;
        }
        //$inactive = ($status=='status') ? 'I' :0;
        if ($FirstNode == '')
        {
            $DropDown = "<option value='0'>---Select---</option>";
        }
        else
        {
            $DropDown = "<option value=''>{$FirstNode}</option>";
        }

        try 
        {
           
			$result =  $this->database->query($sql);
			$result = $this->database->result;
		   
            if (!$result || mysqli_num_rows($result)  < 1) 
            {
                return $DropDown;
            } 
            else 
            {
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				// var_dump($row);die;
                    $sel = "";
                    if (count($select) > 0) 
                    {
                        $sel = (in_array($prefix.$row->$val, $select)) ? "selected='selected'" : "";
                    }
                    ##Reduced Cobo Size 
                    $original_len = $row->$show;
                    if (strlen($row->$show) >= '40') 
                    {
                        $row->$show = substr($row->$show, 0, 40);
                    }
                    //$class = ('I' == $row->$status) ? "style='cursor:text' class='inactive' title='Deactive record'" : "";
                    $DropDown.= "<option {$class} value='" . $prefix.$row->$val . "' {$sel} title='" . $original_len . "' >" . $row->$show . "</option>";
                }
                return $DropDown;
            }
        } 
        catch (Exception $e) 
        {
            return $DropDown;
        }
    }
	
//==========Function To Redirect==========\\
    function redirectPage($URL=null,$module=null) 
    { 
        if (trim($URL) == "")
        {
            return false;
        }
        if (!headers_sent())
        {
        $uri=$_SERVER['REQUEST_URI'];
        

         header('Location: ' . $URL);
           // http_response_code()
        }
        else
        {
            print "<script language='javascript'>location.href='" . $URL . "';</script>";
        }
        exit;
    }

    function deleteDir($dir) 
    {
        if (substr($dir, strlen($dir) - 1, 1) != '/')
        {
            $dir .= '/';
        }
        //echo $dir; 
        if ($handle = opendir($dir)) 
        {
            while ($obj = readdir($handle)) 
            {
                if ($obj != '.' && $obj != '..') 
                {
                    if (is_dir($dir . $obj)) 
                    {
                        if (!$this->deleteDir($dir . $obj))
                        {
                            return false;
                        }
                    }
                    elseif (is_file($dir . $obj)) 
                    {
                        if (!unlink($dir . $obj))
                        {
                                return false;
                        }
                    }
                }
            }
        closedir($handle);
        if (!@rmdir($dir))
        {
            return false;
        }
        return true;
        }
        return false;
    }

//==========pipe separated  values by Maheshk ==========\\
    function pipeSeparated($Val) 
    {
        if (is_array($Val)) 
        {
            $array_unique_Val = array_unique($Val);
        } 
        else 
        {
            $array_unique_Val = $Val;
        }
        $countVal = count($array_unique_Val);
        //echo $countVal;exit;
        for ($i = 0; $i < $countVal; $i++) 
        {
            $all_id_Val .= "|" . $Val[$i] . "|,";
        }
        $removeLastVal = substr_replace($all_id_Val, "", -1);
        //echo $removeLastVal;exit;
        return $removeLastVal;
    }

//==========get selected values by Maheshk ==========\\
	function getSelectedVal($databaseVal, $allVal) 
	{
		for ($i = 0; $i < count($databaseVal); $i++) 
		{
			if ($databaseVal[$i] == $allVal)
			{
				$selected = "selected='selected'";
				break;
			} 
			else 
			{
				$selected = "";
			}
		}
		return $selected;
	}

//==========get checked values by Maheshk ==========\\
    function getCheckedVal($databaseVal, $allVal) 
    {
        for ($i = 0; $i < count($databaseVal); $i++) 
        {
            if ($databaseVal[$i] == $allVal) 
            {
                //echo "if";exit;
                $selected = "checked='checked'";
                break;
            } else 
            {
                //echo "--";exit;
                $selected = "";
            }
        }
        return $selected;
    }

//------------------Display Error MSg---------------------
    function errorbox($message, $type = "") 
    {
        if (is_array($message)) 
        {
            $type = "Error : ";
            for ($i = 0; $i < count($message); $i++) 
            {
                if ($i != 0)
                {
                    $type = "";
                }
                $str = '<b>' . $type . '</b>' . $message[$i];
            }
        }
        else 
        {
            if ($type == "" || $type == "Error") 
            {
                $type = "Error : ";
            } 
            else 
            {
                $type = "Message : ";
            }
            $str = '<b>' . $type . '</b>' . $message[$i];
        }
        return $str;
    }

//--------------end function---------------------------------
//============= Used to Change Date-time format ============
    function changeDateTimeFormat($old_date) 
    {
        if ($old_date != '0000-00-00 00:00:00' && $old_date != '') 
        {
            $arr = explode('-', $old_date);
            $new_date = date('Y-m-d', mktime(0, 0, 0, $arr[1], $arr[2], $arr[0]));
            $arrtime = explode(' ', $old_date);
            //$new_date = $new_date." ".$arrtime[1];
            $new_date = $new_date;
        }
        else
        {
            $new_date = '--';
        }
    return $new_date;
    }

//end function changeDateFormat

function changeDBDateFormat($old_date)
{
	if($old_date != '')
	{
		$arr = explode('/',$old_date);
		$new_date=$arr[2]."-".$arr[0]."-".$arr[1];
		$new_date = $new_date;
	}
	else
		$new_date = '--';
	return $new_date;
}
	function isDateLess($fdate,$ldate)
	{
		$date1 		= strtotime($this->changeDBDateFormat($fdate));
		$date2		= strtotime($this->changeDBDateFormat($ldate));
		if($date2 < $date1)
			return true;
		else
			return false;
	}
    function changeToHumanReadableDate($old_date) 
    {
        if ($old_date != '0000-00-00 00:00:00' && $old_date != '') 
        {
            $arr = explode('-', $old_date);
            $new_date = date('d M Y', mktime(0, 0, 0, $arr[1], $arr[2], $arr[0]));
        }
        else
        {
            $new_date = '--';
        }
        return $new_date;
    }


    function createDropdown($arr, $frm, $val, $sel) 
    {
        $i = 0;
        //print_r($val);
        //echo $sel;
        echo '<SELECT id="' . $frm . '" name="' . $frm . '" >
        <OPTION value="" >Select</OPTION>';
        foreach ($arr as $key => $value) 
        {
            if ($val[$i] == $sel) 
            {
                echo '<OPTION value="' . $val[$i] . '"  selected>' . $value . '</OPTION>';
            } 
            else 
            {
                echo '<OPTION value="' . $val[$i] . '"  >' . $value . '</OPTION>';
            }
            $i++;
        } 
        echo '</SELECT>';
    }

    function imageResize($width, $height, $target) 
    {
        if ($height > $target || $width > $target) 
        {
            if (($width > $target) && ($width > $height)) 
            {
                $ratio = $target / $width;
                $newheight = $height * $ratio;
                $newwidth = $target;
            } 
            else 
            {
                $ratio = $target / $height;
                $newwidth = $width * $ratio;
                $newheight = $target;
            }
        } 
        else 
        {
            $newheight = $height;
            $newwidth = $width;
        }
        $this->width = $newwidth;
        $this->height = $newheight;
    }

    function check_alphanumeric_password($new_password)
    {
        if ((preg_match("/[0-9]/", $new_password)) && (preg_match("/[A-Z]/", $new_password)) && (preg_match("/[a-z]/", $new_password)))
        {
            return(1);
        } 
        else 
        {
            return(0);
        }
    }

    function thumb($filename, $type, $d_filename, $th_width, $th_height, $forcefill = 1)
    {
        $image_property = getImageSize($filename);
        $width = $image_property[0];
        $height = $image_property[1];
        $type = $image_property[2];
        if ($type == 1)
        {
            $source = @imagecreatefromgif($filename);
        }
        if ($type == 2)
        {
            $source = @imagecreatefromjpeg($filename);
        }
        if ($type == 3)
        {
            $source = @imagecreatefrompng($filename);
        }
        if ($th_height == "") 
        {
            if ($type == 1)
            {
                @imagegif($thumb, $d_filename);
            }
            if ($type == 2)
            {
                @imagejpeg($source, $d_filename);
            }
            if ($type == 3)
            {
                @imagepng($thumb, $d_filename);
            }
            ImageDestroy($source);
            return true;
        }
        if ($width > $th_width || $height > $th_height) 
        {
            $a = $th_width / $th_height;
            $b = $width / $height;
            if (($a > $b) ^ $forcefill) 
            {
                $src_rect_width = $a * $height;
                $src_rect_height = $height;
                if (!$forcefill) 
                {
                    $src_rect_width = $width;
                    $th_width = $th_height / $height * $width;
                }
            } 
            else 
            {
                $src_rect_height = $width / $a;
                $src_rect_width = $width;
                if (!$forcefill) 
                {
                    $src_rect_height = $height;
                    $th_height = $th_width / $width * $height;
                }
            }

            $src_rect_xoffset = ($width - $src_rect_width) / 2 * intval($forcefill);
            $src_rect_yoffset = ($height - $src_rect_height) / 2 * intval($forcefill);
            $thumb = @imagecreatetruecolor($th_width, $th_height);
            // Enable Options For Quality
            @ImageAlphaBlending($thumb, true);
            @ImageSaveAlpha($thumb, true);
            // Create Backgrounds
            $white_bg = @ImageColorAllocate($thumb, 255, 255, 255);
            //$alpha_bg = ImageColorTransparent($source,$black_bg);
            @ImageFill($thumb, 0, 0, $white_bg);
            @imagecopyresampled($thumb, $source, 0, 0, $src_rect_xoffset, $src_rect_yoffset, $th_width, $th_height, $src_rect_width, $src_rect_height);
            if ($type == 1)
            {
                @imagegif($thumb, $d_filename);
            }
            if ($type == 2)
            {
                @imagejpeg($thumb, $d_filename);
            }
            if ($type == 3)
            {
                @imagepng($thumb, $d_filename);
            }
            return true;
        }
        else 
        {
            if ($type == 1)
            {
                @imagegif($source, $d_filename);
            }
            if ($type == 2)
            {
                @imagejpeg($source, $d_filename);
            }
            if ($type == 3)
            {
                @imagepng($source, $d_filename);
            }
            return true;
        }
    }

    function dbInput($string) 
    {
        if (function_exists('mysqli_real_escape_string')) 
        {
            return mysqli_real_escape_string($string);
        } 
        elseif (function_exists('mysql_escape_string')) 
        {
            return mysqli_escape_string($string);
        }
        return addslashes($string);
    }

    function sanitizeString($string) 
    {
        $string = preg_replace('{ +}', ' ', trim($string));
        return preg_replace("/[<>]/", '_', $string);
    }


    function HtmlSpecialCharsArray($Array) 
    {
        if (is_array($Array))
        {
            foreach ($Array as $key => $value)
            {
                if (is_array($value))
                {
                    $Array[$key] = $this->HtmlSpecialCharsArray($value);
                }
                else
                {
                    $Array[$key] = htmlentities($value, ENT_QUOTES);
                }
            }
        }
        else
        {
            $Array = htmlentities($Array, ENT_QUOTES);
        }
        return $Array;
    }

    function UploadFile($FolderPath, $File, $AllowedFileTypes, $ThumbPath = '', $ThumbX = 100, $ThumbY = 100) 
    {
        global $global_max_file_size;
        $max_file_size = ($global_max_file_size != '') ? $global_max_file_size : 2097152;
        if (!is_dir($FolderPath))
        {
            if (!mkdir($FolderPath, 0777))
            {
                throw new Exception("Unable to create directory", 408);
            }
        }
        if (!is_array($File))
        {
            throw new Exception("Error with File upload", 409);
        }
        if ($File['error'] != 0)
        {
            throw new Exception("Error with File upload", 409);
        }
        if ($File['size'] > $max_file_size)
        {
            throw new Exception("Upload file size exceeded the limit", 407);
        }
        $arr_file_name = explode(".", $File['name']);
        if (!in_array(strtolower($arr_file_name[count($arr_file_name) - 1]), $AllowedFileTypes))
        {
            throw new Exception("Invalid file type", 406);
        }
        $FileName = time() . rand(10, 100) . "." . $arr_file_name[count($arr_file_name) - 1];
        if (!move_uploaded_file($File['tmp_name'], $FolderPath . $FileName))
        {
            throw new Exception("Can not Upload File", 1);
        }
        if (strpos($File['type'], 'image/') !== false and $ThumbPath != '')
        {
            reuseFunction :: thumb($FolderPath . $FileName, $File['type'], $ThumbPath . $FileName, $ThumbX, $ThumbY, 1);
        }
        $ReturnArray = array('Original File Name' => $File['name'], 'Disk File Name' => $FileName);
        return $ReturnArray;
    }


    function setPaginationMaxRows($endIndex)
    {
        return isset($endIndex) && $endIndex != '' ? $endIndex : INCREMENT_BY;
    }

    function setURL($pagename, $querystring, $skip_variable) 
    {
        $querystring = unserialize(base64_decode($querystring));
        if (sizeof($querystring)) 
        {
            $new_qs = '';
            foreach ($querystring as $variable => $var_val) 
            {
                if (!in_array($variable, $skip_variable)) 
                {
                    if ($variable != 'page') 
                    {
                        $new_qs.=$variable . '=' . $var_val . '&';
                    }
                }
            }
        }
        $url.='&' . $new_qs;
        return $url;
    }

    function make_url($sufix = '', $prefix = '') 
    {
        $new_url = "";
        $sufix = strtolower(htmlentities($sufix));
        $sufix = str_replace(" ", "-", $sufix);
        if (!empty($prefix))
        $new_url = $prefix . "_";
        $new_url .= $sufix . ".html";
        //$new_url = "view_brand.php?brand_name=".$sufix;
        return $new_url;
    }

    function make_file_name($file_name = '') 
    {
        $file_name = strtolower(htmlentities($file_name));
        $file_name = str_replace(" ", "-", $file_name);
        return $file_name;
    }



    function SetImageIcon($FileType) 
    {
        $FileType = strtolower($FileType);
        $IconFileName = "file-icon.png";
        switch ($FileType) 
        {
            case "url": $IconFileName = "url-icon.png";
            break;
            case "jpg": $IconFileName = "image-icon.png";
            break;
            case "jpeg": $IconFileName = "image-icon.png";
            break;
            case "png": $IconFileName = "image-icon.png";
            break;
            case "gif": $IconFileName = "image-icon.png";
            break;
            case "pdf": $IconFileName = "pdf-icon.png";
            break;
            case "doc": $IconFileName = "word-icon.png";
            break;
            case "docx": $IconFileName = "word-icon.png";
            break;
            case "xls": $IconFileName = "xls-icon.png";
            break;
            case "xlsx": $IconFileName = "xls-icon.png";
            break;
            case "ppt": $IconFileName = "ppt-icon.png";
            break;
            case "pptx": $IconFileName = "ppt-icon.png";
            break;
            case "txt": $IconFileName = "txt-icon.png";
            break;
            default: $IconFileName = "file-icon.png";
            break;
        }
        if (!file_exists(reuseFunction::getUserPath('USER_PUBLICATIONS_PATH', $_SESSION['user_id']) . "/" . $IconFileName))
        {
            copy(FOLDER_PATH . "images/icons/" . $IconFileName, reuseFunction::getUserPath('USER_PUBLICATIONS_PATH', $_SESSION['user_id']) . "/" . $IconFileName);
            copy(FOLDER_PATH . "images/icons/" . $IconFileName, reuseFunction::getUserPath('USER_PUBLICATIONS_THUMB_PATH', $_SESSION['user_id']) . "/" . $IconFileName);
        }
        return $IconFileName;
    }



    function parseValue($item, $seperater) 
    { ////print_r($item); exit;
        $a = explode($seperater, $item);
        if (is_array($a)) 
        {
            foreach ($a as $k => $v)
            {
                $b = explode('-', $v);
                $c[$b[0]] = $b[1];
            }
        }
        return($c);
    }


    function getURLWithSubStr($url) 
    {
        $uri = '';
        if (!empty($url)) 
        {
            $parse = parse_url($url);
            print_r($parse);
            $uri .= isset($parse['scheme']) ? trim($parse['scheme'] . '://www.') : 'http://www.';
            $host = str_ireplace('www.', '', str_ireplace('wwww.', '', str_ireplace('http://', '', str_ireplace('https://', '', $url))));
            $uri .= $host;
        }
        return $uri;
    }


    function check_empty($str) 
    {
        $val = (isset($_POST[$str]) && $_POST[$str] != "") ? $_POST[$str] : "";
        return (($val == '') ? true : false);
    }

    function check_expression_email($str) 
    {
        $val = (isset($_POST[$str]) && $_POST[$str] != "") ? $_POST[$str] : "";
        return (!preg_match("/[._a-z0-9-]+(\.[._a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $val) ? true : false);
    }



#-------------------------
# This Function is used to validate and clean the input data	
# Input: $input_data
# Output: $cleaned_data
# Created By: Prasad Bhale 04/12/13
# Last Modify By & Date
#-------------------------
    function funGetDbValue($method,$atribute,$data)
    { 
        try 
        {
            if($method=='GET'||$method=='POST' || $method=='REQUEST')
            {
                if (count($data) > 0) 
                {
                    
                    $cleaned_data = !empty($data[$atribute]) ? trim($data[$atribute]) : '';
                    return $cleaned_data;
                } 
                else 
                {
                    return 0;
                }
            }
            else
            {
                $cleaned_data = !empty($data) ? trim($data) : '';
                return $cleaned_data; 
            }
        } 
        catch (Exception $e) 
        {
            return 0;
        }
    return true;
    }


#-------------------------
# This Function is used to send email to destined user along with sender message.	
# Input: $user_email,$user_message
# Output: $mail_status
# Created By: Prasad Bhale 04/12/13
# Last Modify By & Date
#-------------------------
    
function funSendEmail($user_email,$user_message)
{
    /* php mailer functionality */
    /* mail send to */
    $to  = $user_email; 
    // subject
    $subject = 'New Password ';
    // message
    $message = '
    <html>
    <head>
    <title>New Password Of Appzeal Account</title>
    </head>
    <body>
    <p>Bellow Is Your New Password as requested By you!</p>
    <p>'.$user_message.'</p>
    </body>
    </html>
    ';
    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    // Additional headers
    $headers .= 'To:'.$user_email. "\r\n";
    $headers .= 'From: www.motherhood.com' . "\r\n";
    // Mail it
    $mail_status = mail($to, $subject, $message, $headers);
    return $mail_status;
    /* php mailer functionality ends here */
}


#----------------------------------------
# This is Generic Function to Archive / Delete / Block user,company etc. 
# InPut : Table Name , Column Name , Value to modify 
# Createde By : Prasad Bhale  / 06-12-2013
#----------------------------------------

function funDoGenericAction($parameter)
{
	
    $table_name=$parameter[0];
	$column_name=$parameter[1];
	$where_column=$parameter[2];
	$where_cond_val=$parameter[3];
	$value_update=$parameter[4]; // add to archive 	
	
	include_once(MODEL_DIR_PATH."class.database.php");
	//$re = new tbl_book_categories();
	$this->database = new Database();
	
	$sql="UPDATE ". $table_name." SET  ". $column_name."='".$value_update."' WHERE ".$where_column ."='".$where_cond_val."'" ;
	
	$result = $this->database->query($sql);

}


#----------------------------------------
# This is Generic Function to Archive / Delete / Block user,company etc. 
# InPut : Table Name , Column Name , Value to modify 
# Createde By : Prasad Bhale  / 06-12-2013
#----------------------------------------

function funDoGenericActionDelete($parameter)
{
	
    $table_name=$parameter[0];
	$where_column=$parameter[1];
	$where_cond_val=$parameter[2];
	
	include_once(MODEL_DIR_PATH."class.database.php");
	//$re = new tbl_book_categories();
	$this->database = new Database();
	
	$sql="DELETE FROM ".$table_name." WHERE ".$where_column ."='".$where_cond_val."'";
//	$sql="UPDATE ". $table_name." SET  ". $column_name."='".$value_update."' WHERE ".$where_column ."='".$where_cond_val."'" ;
	
	$result = $this->database->query($sql);

}



#----------------------------------------
# This is Generic Function to check Unique Field. 
# InPut : Table Name , Column Name , Value to Check 
# Createde By : Prasad Bhale  / 10-12-2013
#----------------------------------------

function funChkUniqueField($parameter)
{
	$conn 	= new ConnectionPool();

	$table_name=$parameter[0];
	$column_name=$parameter[1];
	$condition=$parameter[2];
	
	$AddClass = array($table_name);
	include(CONFIG_CLASS_PATH . "class.php");

	$sql="SELECT COUNT(".$column_name.") AS count  FROM  ". $table_name." WHERE ".$condition;

    $res = $conn->db_query($sql);

	$row	= $conn->db_fetch_object($res);

	return $row->count;
}


#----------------------------------------
# This is Generic Function to check Valid PAssword. 
# InPut : Table Name , Column Name , Value to Check 
# Createde By : Prasad Bhale  / 15-12-2013
#----------------------------------------

function funVerifyPassword($old_password,$client_id,$table_name,$column_name)
{
	$conn 	= new ConnectionPool();

	$AddClass = array($table_name);
	include(CONFIG_CLASS_PATH . "class.php");

	if($table_name=="tbl_bloggers")
	{
	 $condition=" AND blogger_id=".$client_id;
	}
	else if($table_name=="tbl_clientsncontacts")
	{
	 $condition=" AND client_id=".$client_id;
	}
	else if($table_name=="tbl_admin")
	{
		$condition=" AND admin_id=".$client_id;
	}
	
	$sql="SELECT COUNT(".$column_name.") AS count  FROM  ". $table_name." WHERE  ".$column_name." ='".$old_password."'".$condition;
    $res = $conn->db_query($sql);
	$row	= $conn->db_fetch_object($res);
	return $row->count;
}


#----------------------------------------
# This is Generic Function to Update the table info. 
# InPut : UpdateColumnName Array , UpdateValue Array , tbl_name & where condition 
# Createde By : Prasad Bhale  / 10-12-2013
#----------------------------------------

function funGenericUpdateAction($updateColumn,$updateValue,$table_name,$condition)
{
	$conn 	= new ConnectionPool();
	
	$AddClass = array("reuse",$table_name);
	include(CONFIG_CLASS_PATH . "class.php");
	$updateColCount=count($updateColumn);
	
	$sql="UPDATE ". $table_name. " SET  ";
	for($i_col=0;$i_col<=$updateColCount-1;$i_col++)
	{
		
			//$column_value = htmlspecialchars($updateValue[$i_col], ENT_QUOTES);
			$column_value = $updateValue[$i_col];
			$sql.= "`".$updateColumn[$i_col]."`='".$this->re_escape($column_value)."'";
				if($i_col!=$updateColCount-1)
		{
			$sql.=","; 
		}
	}
   $sql.=" WHERE ".$condition[0];
	
    return $res = $conn->db_query($sql);
}


function re_escape($string) 
{ 
	include_once(CLASS_DIR_PATH."util/ConnectionPool.php");
	$conn	= new ConnectionPool();
	return $conn->escape($string);
}


#----------------------------------------
# This is Generic Function For Selection. 
# InPut : Table Name , Condition 
# Createde By : Prasad Bhale  / 24-12-2013
#----------------------------------------

function funGenericSelectAction($table_name,$condition="")
{
	$conn 	= new ConnectionPool();

	$AddClass = array($table_name);
	include(CONFIG_CLASS_PATH . "class.php");
	$sql="SELECT COUNT(".$column_name.") AS count  FROM  ". $table_name." WHERE  ".$column_name." ='".$old_password."'";
    $res = $conn->db_query($sql);
	$row	= $conn->db_fetch_object($res);
	return $row->count;
}


#----------------------------------------
# This is alternative Function to mysqli_real_escape_string to clean string . 
# InPut : $string
# Createde By : Prasad Bhale  / 12-12-2013
#----------------------------------------

function real_escape_string($string) 
  {	 
  
$search = array("\x00",	"\n", "\r", "\\", "'", "\"", "\x1a"); 
$replace = array("\\x00", "\\n", "\\r", "\\\\" ,"\'", "\\\"", "\\\x1a"); 
return str_replace($search, $replace, $string);
  }
  


function funGenericInsertAction($user_id,$message,$update_date,$table_name)
{
	$conn 	= new ConnectionPool();
	$AddClass = array($table_name);
	include(CONFIG_CLASS_PATH . "class.php");

    $sql="INSERT INTO ".$table_name."(`client_id` ,`admin_id`,`message`,`send_date`) VALUES ('".$user_id."','0','".$message."','".$update_date."')";
    return $res = $conn->db_query($sql);
}



function funGetMessages($user_id)
{
	$conn 	= new ConnectionPool();
	$AddClass = array("tbl_client_messags");
	include(CONFIG_CLASS_PATH . "class.php");
	$whereClause=" client_id= $user_id AND read_status ='0' ORDER BY send_date ASC LIMIT 0,1";	
	$count=$tbl_client_messags_manager->getAllTblClientMessagsCount($whereClause);
	if($count >0)
	{
	  $message=$tbl_client_messags_manager->getAllTblClientMessags($whereClause);
	  
	  $msg_id=$message[0]->message_id;
	  $msg=$message[0]->message;		  
	  
	  #####Update Message ID ####
	  
	$updateColumn=array('read_status');
	$updateValue=array('1');
	$condition=array("message_id='".$msg_id."'");
	
	$this->funGenericUpdateAction($updateColumn,$updateValue,'tbl_client_messags',$condition);
    return $msg;
	   	   
	}
}

#----------------------------------------
# This function is used to get folder path . 
# InPut : $path_name,$match,$replace,$createfolder
# Createde By : Prasad  / 23-12-2013
#----------------------------------------


function get_folder_path($path_name,$match,$replace,$createfolder=0)
{
	$new_path = str_replace($match, $replace, $path_name);
	
	if(!is_dir($new_path) && $createfolder == 1 )
	{
		//$this->createAppFolder($new_path);
	}
	return $new_path;
}




#------------------
# Create Static Path
#------------------
function staticPath($parm,$isCreateFolder=0)
{
	$folders = str_split((string)$parm);
	$path = implode( '/', $folders).'/'.$parm;
	$path = trim($path);
	if($isCreateFolder==1)
		$this->creatFolder($path);
	return $path;
}


function creatFolder($target)
 {
	$wrapper = null;

	// strip the protocol
	if( $this->motherhood_is_stream( $target ) ) {
		list( $wrapper, $target ) = explode( '://', $target, 2 );
	}

	// from php.net/mkdir user contributed notes
	$target = str_replace( '//', '/', $target );

	// put the wrapper back on the target
	if( $wrapper !== null ) {
		$target = $wrapper . '://' . $target;
	}

	// safe mode fails with a trailing slash under certain PHP versions.
	$target = rtrim($target, '/'); // Use rtrim() instead of untrailingslashit to avoid formatting.php dependency.
	if ( empty($target) )
		$target = '/';

	if ( file_exists( $target ) )
		return @is_dir( $target );

	// Attempting to create the directory may clutter up our display.
	if ( @mkdir( $target ) ) {
		$stat = @stat( dirname( $target ) );
		$dir_perms = $stat['mode'] & 0007777;  // Get the permission bits.
		@chmod( $target, $dir_perms );
		return true;
	} elseif ( is_dir( dirname( $target ) ) ) {
			return false;
	}

	// If the above failed, attempt to create the parent node, then try again.
	if ( ( $target != '/' ) && ( $this->creatFolder( dirname( $target ) ) ) )
		return $this->creatFolder( $target );

	return false;
}






	//function added by Prasad
	function check_filetype( $filename, $mimes = null ) 
	{
			if ( empty($mimes) )
				$mimes = $this -> get_allowed_mime_types();
			$type = false;
			$ext = false;
		
			foreach ( $mimes as $ext_preg => $mime_match )
			 {
				$ext_preg = '!\.(' . $ext_preg . ')$!i';
				if ( preg_match( $ext_preg, $filename, $ext_matches ) ) 
				{
					$type = $mime_match;
					$ext = $ext_matches[1];
					break;
				}
			}
			
			return compact( 'ext', 'type' );
	}
	
	function get_allowed_mime_types()
	{
		static $mimes = false;
	
		if ( !$mimes ) 
		{
			// Accepted MIME types are set here.
			$mimes = array(
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif' => 'image/gif',
			'png' => 'image/png'
			);
		}
	
		return $mimes;
	}

/* alternative function for $conn->escape() for non sql section */
	function sql_escape_function($string) 
			{
				//return $conn->escape( $string );
				$string = $this ->real_escape_string((htmlentities(utf8_decode($string))));
				if(!get_magic_quotes_gpc())
				{
					return $string;
				}
				else
				{
					return addslashes($string);
				}
			}
			
/* function to handle special charecters in non sql section of code */
	function funHandleSpecialChars($var) 
			{
				
				$cleaned_var = (htmlspecialchars($var, ENT_QUOTES));
			
			    return $cleaned_var;
				
			}
	
	function dropdown_display($name,$listArray,$is_required=1,$selected='')
	{
		$seletHtml = "";
		$seletHtml = '<select class="span12" name="'.$name.'" id="'.$name.'" '.($is_required==1?'required':($is_required==2?'disabled':'')).'>';
		if(is_array($listArray) && count($listArray) > 0)
		{
			foreach($listArray as $key => $val)
			{
				$seletHtml .= '<option value="'.$key.'"';
				if($key === '')
					$seletHtml .= 'selected disabled';
				else if($key == $selected)
					$seletHtml .= ' selected="selected"';
				$seletHtml .= '>'.$val.'</option>';
			}
		}
		$seletHtml .= '</select>';
		return $seletHtml;
	}

   function funCheckUniqueFieldExist($field_name = null,$field_value = null,$tbl_name = null)
	{
		$conn 	  = new ConnectionPool();
		$AddClass = array($tbl_name);
		include(CONFIG_CLASS_PATH . "class.php");
	    $sql	  = "SELECT count(*) AS count
					 FROM $tbl_name
					 WHERE $field_name = '$field_value'
					";    
	    $result	  = $conn->db_query($sql);
		$row	  = $conn->db_fetch_object($result);
        return $row->count;
    }


  //Generic Function to set Message Code
  function funGenericSetMessageCode($Message_code)
  {
	
	include(MODULE_PATH."activity_log_messages.php");
	return $rec_msgs ;
	
  }


// Get Year From Date 
  function GetYearFromdate($date)
  {
	$year_date = explode('-',$date);
	//$year_date = date('Y',$date);
	return $year_date[0] ;
	
  }
  
//class ends

/*
 * Developed By: Prasad Bhale
 * Date: 13 Feb 2014
 * Function To Validate US Phone Number
 */

	function validatePhoneNumberFormat($phone)
	{
	
		if(preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $phone)) 
		{
		  return 1;
		}
		else
		{
		 return 0;
		}	
	}
	

	/* Read CSV file Line By Line 
	*  Developed By : Prasad Bhale 
	*  Created Date : 05-05-2014
	*/

	function readCSV($csvFile)
	{
		$file_handle = fopen($csvFile, 'r');
		while (!feof($file_handle) ) 
		{
			$line_of_text[] = fgetcsv($file_handle, 1024);
		}
		fclose($file_handle);
		return $line_of_text;
	}
	

	


	function funSmartSearch($book_title)
	{
	
		$conn 	= new ConnectionPool();
		$AddClass = array("tbl_books");
		include(CONFIG_CLASS_PATH . "class.php");
		return $whereClause=" book_title LIKE '%".$book_title."%'";
	
		$count=$tbl_client_messags_manager->getAllTblClientMessagsCount($whereClause);

	}
	

	#----------------------------------------
	# This is Generic Function to minus the book quantity from location
	# InPut : Table Name , Column Name , Value to Check 
	# Createde By : Prasad Bhale  / 10-12-2013
	#----------------------------------------
	
	function funMinusTheBookQuantityFromLocation($parameter)
	{
	
		$table_name=$parameter[0];
		$column_name=$parameter[1];
		$condition=$parameter[2];
		$value_update = $parameter[3];
		
        include_once(MODEL_DIR_PATH."class.database.php");
		
		$this->database = new Database();
				
		$sql="UPDATE  `". $table_name."`   SET  `". $column_name."`=".$value_update." WHERE ". $condition ; 
		return $result =  $this->database->query($sql);
		//$result = $this->database->result;
	}


	#----------------------------------------
	# This is Generic Function to minus the book quantity from location
	# InPut : Table Name , Column Name , Value to Check 
	# Createde By : Prasad Bhale  / 10-12-2013
	#----------------------------------------
	
	function fungetDashboardCount($user_id)
	{
	
		$table_name=$parameter[0];
		$column_name=$parameter[1];
		$condition=$parameter[2];
		$value_update = $parameter[3];
		
		
        include_once(MODEL_DIR_PATH."class.database.php");
		
		$this->database = new Database();
		
		
		//	 Todays Sale Amoutn
		 $sql_todays_sale_amount="SELECT SUM(ts.final_amount)  as todays_sale_amount FROM `tbl_sales_master_entry` ts WHERE ts.`user_id`=".$user_id." AND ts.`sale_date`= '".date('Y-m-d')."' AND ts.is_deleted='0'"; 
		 $result_sql_todays_sale_amount =  $this->database->query($sql_todays_sale_amount);
		 $result_sql_todays_sale_amount = $this->database->result;
		 $row_todays_sale_amount = mysqli_fetch_object($result_sql_todays_sale_amount);
		 $result_set['todays_sale_amount'] =  $row_todays_sale_amount->todays_sale_amount;

		//	 Total Sale Amoutn
		 $sql_total_sale_amount=" SELECT SUM(tst.final_amount)  as total_sale_amount FROM `tbl_sales_master_entry` tst WHERE tst.`user_id`=".$user_id. " AND tst.is_deleted='0'";
		 $result_sql_total_sale_amount =  $this->database->query($sql_total_sale_amount);
		 $result_sql_total_sale_amount = $this->database->result;
		 $row_total_sale_amount = mysqli_fetch_object($result_sql_total_sale_amount);
		 $result_set['total_sale_amount'] =  $row_total_sale_amount->total_sale_amount;


		//	Total Book Sale Sold
		 $sql_total_book_sold_out=" SELECT SUM(tsb.book_quantity)  as total_book_sold_out FROM `tbl_sales_book_entry` tsb INNER JOIN tbl_sales_master_entry tsm ON tsb.sales_maste_id = tsm.sales_maste_id WHERE tsm.`user_id`=".$user_id." AND tsm.is_deleted='0'";
		 
		 $result_sql_total_sale_amount =  $this->database->query($sql_total_book_sold_out);
		 $result_sql_total_book_sold_out = $this->database->result;
		 $row_total_book_sold_out = mysqli_fetch_object($result_sql_total_book_sold_out);
		 $result_set['total_book_sold_out'] =  $row_total_book_sold_out->total_book_sold_out;

		//	Total Book Sale Sold
		 $sql_todays_book_sold_out=" SELECT SUM(tsb.book_quantity)  as todays_book_sold_out FROM `tbl_sales_book_entry` tsb INNER JOIN tbl_sales_master_entry tsm ON tsb.sales_maste_id = tsm.sales_maste_id WHERE tsm.`user_id`=".$user_id ." AND tsm.`sale_date`= '".date('Y-m-d')."' AND tsm.is_deleted='0'";
		 
		 $result_total_book_sale_amount =  $this->database->query($sql_todays_book_sold_out);
		 $result_sql_todays_book_sold_out = $this->database->result;
		 $row_todays_book_sold_out = mysqli_fetch_object($result_sql_todays_book_sold_out);
		 $result_set['todays_book_sold_out'] =  $row_todays_book_sold_out->todays_book_sold_out;

		return $result_set;
	
	}
	
	

 #------------------------------------------------------------------------------------
 # Function for set cell values into excel sheet
 # Developerd By: Sandip Yeole
 # Date: 28.04.2014
 #------------------------------------------------------------------------------------
	function setCell( &$worksheet, $row/*1-based*/, $col/*0-based*/, $val, $style=NULL ) 
	{
		//return $row."~".$col;
		if ($style) 
		{
			$worksheet->getStyleByColumnAndRow($col,$row)->applyFromArray( $style );
			$cell = $worksheet->getCellByColumnAndRow( $col, $row );
			$style = $cell->getStyle();
			
				if ($style->getNumberFormat()->getFormatCode() == '@') {
					$cell->setValueExplicit( $val );
				} 
				else 
				{
					$cell->setValue( $val );
				}
		} 
		else
		{
		$cell = $worksheet->getCellByColumnAndRow( $col, $row );
		$cell->setValue( $val );
		}
	}
	

	#----------------------------------------
	# This is Generic Function to minus the book quantity from location
	# InPut : Table Name , Column Name , Value to Check 
	# Createde By : Prasad Bhale  / 10-12-2013
	#----------------------------------------
	
	function calSalesFinalAmout($total_amount,$tax_amount)
	{
	
		$tax_amount_val = ($total_amount * $tax_amount)/ 100 ;
		$final_amount = $total_amount+$tax_amount_val;
		
		return  number_format($final_amount,2);
		
	}



 }
 


?>