<?php

//error_reporting(E_ALL); 
 //ini_set('display_errors', 1);
 require_once 'phpmailer/src/PHPMailer.php';
 require_once 'phpmailer/src/Exception.php';
 require_once 'phpmailer/src/SMTP.php';
 require_once 'phpmailer/src/POP3.php';
 require_once 'phpmailer/src/OAuth.php';

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
 use PHPMailer\PHPMailer\PHPMailer;
 use PHPMailer\PHPMailer\Exception;
 
//Load composer's autoloader

$mail = new PHPMailer(false);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'mail.shreevimalpublishers.com';  // Specify main and backup SMTP servers
   //$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'sales@shreevimalpublishers.com';                 // SMTP username
   // $mail->Username = 'billing.desifarmsindia@gmail.com';                 // SMTP username
   // $mail->Password = 'billingmatters@789';    
   $mail->Password = 'Sales123!@#';                           // SMTP password
   
    
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	//$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
   // $mail->Port = 465;
    $mail->Port = 25;                                    // TCP port to connect to
	$mail->isHTML(true);                                  // Set email format to HTML
    $mail->AltBody = '';

    
    if($send_frm!="")
    {
       $send_frm = $send_frm;
    }
    else
    {
       $send_frm = 'sales@shreevimalpublishers.com';
    }
    
    //Recipients
    $mail->setFrom($send_frm,'Shree Vimal Publishers');
         // Add a recipient
    //$mail->addAddress('ellen@example.com');               // Name is optional
    // $mail->addReplyTo('info@example.com', 'Information');
    if($cc_to!="")
    {
        $mail->addCC($cc_to,$cc_name);
    }
    // $mail->addBCC('bcc@example.com');
	

	$mail->addAddress($to_customer,  $customer_name);
    //Attachments
    $mail->addAttachment($invoice_pdf);         // Add attachments
   // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->Subject =  $subject;
    $mail->Body    =  $bodymessage;

    $res = $mail->send();
    
	if($res >0)
	{
    echo 'success';

    //Something to write to txt log
    $log  = "\n Invoice Email Sent to ".$customer_name ."-". $to_customer  ." on ". date("F j, Y, g:i a")."\n";
    //Save string to log, use FILE_APPEND to append.
    file_put_contents('../invoice_log/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
  }		
	
} 
catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
	
		//Something to write to txt log
	//$log  = "\n ERROR : Email Not Sent to ".$customer_name ."-". $to_customer  ." on ". date("F j, Y, g:i a")."\n";
	//Save string to log, use FILE_APPEND to append.
	//file_put_contents('../invoice_log/log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
	
  
}