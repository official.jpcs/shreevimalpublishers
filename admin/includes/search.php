
<div class="row filter-panel collapse in" id="filter-panel" style="height: auto;display:none">
	<div class="col-sm-12">
	  <div class="box">
		<div class="box-body ">
		
		<!------------------ Filter OR Search start----------------------->
		
	<div  class="filter-panel collapse in">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form method="POST" action="<?=$search_file;?>">
					
						<div class="row">
					
					<?php

					if (in_array("customer_name", $search_array))
					{ ?>
						<div class="col-md-3">
							<div class="col-md-12">
								<label style="margin-right:0;">Customer Name</label>
								<input type="text" class="form-control input-sm wd100" name="first_last_name" value="<?php if(isset($_REQUEST['first_last_name']) && $_REQUEST['first_last_name']!="") echo $_REQUEST['first_last_name'];?>">
							</div>	
                        </div>
  
					<?php }	?>
						
						
						<?php

					if (in_array("user_name", $search_array))
					{ ?>
						<div class="col-md-3">
							<div class="col-md-12">
								<label style="margin-right:0;">User Name</label>
								<input type="text" class="form-control input-sm wd100" name="user_name" value="<?php if(isset($_REQUEST['user_name']) && $_REQUEST['user_name']!="") echo $_REQUEST['user_name'];?>">
							</div>	
                        </div>
  
					<?php }	?>
						
						
						
						<?php

					if (in_array("customer_contact_no", $search_array))
					{ ?>
						
						<div class="col-md-3">
							<div class="col-md-12">
								<label style="margin-right:0;">Contact No</label>
								<input type="text" class="form-control input-sm wd100" name="mobile_no" value="<?php if(isset($_REQUEST['mobile_no']) && $_REQUEST['mobile_no']!="") echo $_REQUEST['mobile_no'];?>">
							</div>	
                        </div>
  
					<?php }	?>
						
						
						
					
						<?php

					if (in_array("date", $search_array))
					{ ?>
						<div class="col-md-3">
				<div class="col-md-12">					
                    <label style="margin-right:0;"> Date </label>

					<div>
                            <input id="todays_order_date" class="form-control datepicker input-sm" type="text" placeholder="Select Date" name="todays_order_date"   value="<?php if(isset($todays_order_date) && $todays_order_date!="") echo $todays_order_date; else echo date('d-m-Y');?>" style="width: 72%;float: left;border-radius: 0px;">
							<button id="order_datebtn" class="datepick_btn input-sm" type="button" style="padding: 4px;"><i class="fa fa-calendar" aria-hidden="true"></i></button>
						</div>
                </div>	
                </div>
						
  
					<?php }	?>
					
							<?php

					if (in_array("product_title", $search_array))
					{ ?>
						<div class="col-md-3">
				<div class="col-md-12">					
                    <label style="margin-right:0;"> Product Title </label>

                            <input type="text" class="form-control input-sm" name="product_title" id="product_title" value="<?php if(isset($_REQUEST['product_title']) && $_REQUEST['product_title']!="") echo $_REQUEST['product_title'];?>">
                       
                </div>	
                </div>
						
  
					<?php }	?>
							<?php

					if (in_array("product_category", $search_array))
					{ ?>
						<div class="col-md-3">
				<div class="col-md-12">					
                    <label>Product Category</label>
					<select name="product_category" id="product_category" class="form-control">					
					<option value=""> Select Category</option>
					<?php for($i=0;$i<count($category);$i++){?>
					<option value="<?=$category[$i]->category_id;?>" <?php if(isset($_REQUEST['product_category']) && $_REQUEST['product_category']==$category[$i]->category_id) echo "selected";?>>
					<?=$category[$i]->category_title;?></option>
					<?php }?>
					</select>
                </div>	
                </div>
						
  
					<?php }	?>
						
				<?php

					if (in_array("From_To_date_orders", $search_array))
					{ ?>
				<div class="col-md-3">
				<div class="col-md-12">					
                    <label>From</label>
					<div>
					<input id="<?=$from_date?>" class="form-control datepicker" type="text" placeholder="Select Date" name="<?=$from_date?>"   value="<?php if(isset($_REQUEST[$from_date]) && $_REQUEST[$from_date]!="") echo $_REQUEST[$from_date];?>" style="width: 72%;float: left;border-radius: 0px;">
					<button id="order_datebtn" style="padding:5px;"class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
					</div>
                </div>	
                </div>
				<div class="col-md-3">
				<div class="col-md-12">	
					
                    <label>To</label>
					<div>
					<input id="<?=$to_date?>" class="form-control datepicker" type="text" placeholder="Select Date" name="<?=$to_date?>"   value="<?php if(isset($_REQUEST[$to_date]) && $_REQUEST[$to_date]!="") echo $_REQUEST[$to_date];?>" style="width: 72%;float: left;border-radius: 0px;">
					<button id="order_datebtn" style="padding:5px;"class="datepick_btn" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
					</div>
                </div>	
                </div>
						
  
					<?php }	?>
						<?php

					if (in_array("date_month_year", $search_array))
					{ 
					
						$MONTH_ARRAY=unserialize(MONTH_ARRAY); 
						if(isset($_REQUEST[$month_name]))
						$cur_month=$_REQUEST[$month_name];	
						else	
						$cur_month=date("m",strtotime(" +0 month"));
						
					
					?>
					<div class="col-md-3">
				
				<div class="col-md-12">					
                    <label>Month</label>
					<select name="<?=$month_name;?>" class="form-control input-sm" id="<?=$month_name;?>">
						<option value="">select month</option>
						<?php	for($i=1;$i<=count($MONTH_ARRAY);$i++){
							$j = sprintf("%02d", $i);							?>
						<option value="<?php echo $j;?>" <?php if($cur_month==$j) { echo "selected"; }?>><?php echo $MONTH_ARRAY[$i];?></option>
						<?php } ?>
						</select>
						
						
                </div>	
                </div>
				<div class="col-md-3">
				<div class="col-md-12">	
					
                    <label>Year</label>
					<select name="<?=$year_name;?>" class="form-control input-sm" id="<?=$year_name;?>">
						<option value="">select year</option>
						<?php 
						$current_year=date('Y');
						for($i=2016;$i<2035;$i++){ ?>
					<option value="<?=$i?>" <?php if($_REQUEST[$year_name]==$i) echo "selected"; if(($_REQUEST[$year_name]=="" && $i==$current_year)) echo "selected";?>><?=$i?> </option>
					<?php  } ?>
					</select>
                </div>	
                </div>
						
  
					<?php }	?>
						
						
						
					
						<?php

					if (in_array("date_month_year_for_payment", $search_array))
					{ 
					
						$MONTH_ARRAY=unserialize(MONTH_ARRAY); 
						
					?>
				<div class="col-md-6">
				<div class="col-md-6">					
                <label class="pull-left">By For Month:</label>

					
					<select name="month_for" id="month_for" class="form-control">
							<option value="">select month</option>
							<?php 
							for($i=1;$i<=12;$i++){
							$j = sprintf("%02d", $i);							?>
						<option value="<?php echo $MONTH_ARRAY[$i];?>" <?php if($_REQUEST['month_for']==$MONTH_ARRAY[$i]) { echo "selected"; }?>><?php echo $MONTH_ARRAY[$i];?></option>
						<?php } ?>
							
							
							
							</select>	
                </div>	
               
				<div class="col-md-6">	
					<label class="pull-left">&nbsp;</label>
					<select name="year_for" class="form-control input-sm" id="year_for">
						<option value="">select year</option>
						<?php 
						$current_year=date('Y');
						for($i=2016;$i<2035;$i++){ ?>
					<option value="<?=$i?>" <?php if($_REQUEST['year_for']==$i) echo "selected";?>><?=$i?> </option>
					<?php  } ?>
					</select>
                </div>	
                </div>



				<div class="col-md-6">
				<div class="col-md-6">					
                <label class="pull-left">By Payment Date:</label>

					<select name="payment_month" class="form-control input-sm" id="payment_month">
						<option value="">select month</option>
						<?php	for($i=1;$i<=count($MONTH_ARRAY);$i++){
							$j = sprintf("%02d", $i);							?>
						<option value="<?php echo $j;?>" <?php if($_REQUEST['payment_month']==$j) { echo "selected"; }?>><?php echo $MONTH_ARRAY[$i];?></option>
						<?php } ?>
						</select>
						
						
                </div>	
               
				<div class="col-md-6">	
					<label class="pull-left">&nbsp;</label>
					<select name="payment_year" class="form-control input-sm" id="payment_year">
						<option value="">select year</option>
						<?php 
						$current_year=date('Y');
						for($i=2016;$i<2035;$i++){ ?>
					<option value="<?=$i?>" <?php if($_REQUEST['payment_year']==$i) echo "selected";?>><?=$i?> </option>
					<?php  } ?>
					</select>
                </div>	
                </div>
				
  
					<?php }	?>
						
						
												<?php

					if (in_array("invoice_status", $search_array))
					{ ?>
						<div class="col-md-3">
				<div class="col-md-12">					
                    <label>Invoice Status</label>
					<select name="invoice_status" id="invoice_status" class="form-control">					
					<option value="">Select Invoice Status</option>
                            <option value="0" <?php if($_REQUEST['invoice_status']=='0') echo "selected";?>>Pending</option>
                            <option value="1"  <?php if($_REQUEST['invoice_status']=='1') echo "selected";?>>Completed</option>
							</select>
                </div>	
                </div>
						
  
					<?php }	?>
						<?php

					if (in_array("invoice_no", $search_array))
					{ ?>
						<div class="col-md-3">
							<div class="col-md-12">
								<label style="margin-right:0;">Invoice No:</label>
								<input type="text" class="form-control input-sm" name="invoice_no" id="invoice_no" value="<?php if(isset($_REQUEST['invoice_no']) && $_REQUEST['invoice_no']!="") echo $_REQUEST['invoice_no'];?>">
							</div>	
                        </div>
  
					<?php }	?>
						
						<?php

					if (in_array("location", $search_array))
					{ ?>
						<div class="col-md-3">
							<div class="col-md-12">
								 <label style="margin-right:0;">Location</label>				
								 <select name="location" class="form-control input-sm wd100">
									<option value="0" <?=($_REQUEST['location']==0? "selected":null)?>>-- Select Location --</option>
									<?php 
									for($i=0;$i< count($location_list);$i++) { 
									
									$location_id=$location_list[$i]->location_id;
									$location_title=$location_list[$i]->location_title;
									
									$location_list_option.="<option value=$location_id ".($_REQUEST['location']==$location_id? "selected":null).">$location_title</option>";
									
									} 
									echo $location_list_option;
									?>
											
								</select> 
							</div>
						</div>	
						
  
					<?php }	?>
						
							
						
					<?php

					if (in_array("route", $search_array))
					{ ?>
				<div class="col-md-3">
				<div class="col-md-12">					
                    <label style="margin-right:0;"> Route </label>

					<select name="route" class="form-control input-sm wd100">
						<option value="0" <?php if($_REQUEST['route']==0) echo"selected";?> >-- Select Route --</option>
						<?php for($i=0;$i< count($route_list);$i++) { ?>
						<option value="<?php echo $route_list[$i]->route_id; ?>" <?php if($_REQUEST['route']==$route_list[$i]->route_id) echo"selected";?>><?php echo $route_list[$i]->route_name." ( ".$route_list[$i]->route_no.") ";?></option>
						<?php } ?>
					</select>
                </div>	
                </div>
						
  
					<?php }	?>

					
					
						<?php

					if (in_array("date_month_year_for_invoice", $search_array))
					{ 
					
						$MONTH_ARRAY=unserialize(MONTH_ARRAY); 
						
					?>
				<div class="col-md-6">
				<div class="col-md-6">					
                <label class="pull-left">Invoice For Month:</label>

					
					<select name="month_for" id="month_for" class="form-control">
							<option value="">select month</option>
							<?php 
							for($i=1;$i<=12;$i++){
							$j = sprintf("%02d", $i);							?>
						<option value="<?php echo $MONTH_ARRAY[$i];?>" <?php if($_REQUEST['month_for']==$MONTH_ARRAY[$i]) { echo "selected"; }?>><?php echo $MONTH_ARRAY[$i];?></option>
						<?php } ?>
							
							
							
							</select>	
                </div>	
               
				<div class="col-md-6">	
					<label class="pull-left">&nbsp;</label>
					<select name="year_for" class="form-control input-sm" id="year_for">
						<option value="">select year</option>
						<?php 
						$current_year=date('Y');
						for($i=2016;$i<2035;$i++){ ?>
					<option value="<?=$i?>" <?php if($_REQUEST['year_for']==$i) echo "selected";?>><?=$i?> </option>
					<?php  } ?>
					</select>
                </div>	
                </div>

  
					<?php }	?>
						
						
						<?php

					if (in_array("transaction_type", $search_array))
					{ ?>
						<div class="col-md-3">
				<div class="col-md-12">					
                    <label>Transaction Type</label>
					<select name="transaction_type" id="transaction_type" class="form-control">
							<option value="">select Transaction Type</option>
							<option value="1" <?php if($_REQUEST['transaction_type']==1) echo"selected";?>>Credit</option>
							<option value="2" <?php if($_REQUEST['transaction_type']==2) echo"selected";?>>Debit</option>

							</select>
                </div>	
                </div>
						
  
					<?php }	?>
                      <?php

					if (in_array("payment_mode", $search_array))
					{ ?>
						<div class="col-md-3">
				<div class="col-md-12">					
                    <label>Payment Mode</label>
					<select name="payment_mode" class="form-control" id="payment_mode">
							<option value=""> Select Payment Mode</option>
							<option value="1"  <?php if($_REQUEST['payment_mode']==1) echo"selected";?>> Cash</option>
							<option value="2"  <?php if($_REQUEST['payment_mode']==2) echo"selected";?>> cheque</option>
							<option value="3"  <?php if($_REQUEST['payment_mode']==3) echo"selected";?>> paytm</option>
							<option value="4"  <?php if($_REQUEST['payment_mode']==4) echo"selected";?>> Neft</option>
							</select>
                </div>	
                </div>
						
  
					<?php }	?>
                        <div class="col-md-2">
						   <div class="col-md-12 mtop20">
							<button type="submit" id="search" class="btn btn-primary">
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
							</button>
							<button type="reset"  class="btn btn-primary" rel="<?=$search_file;?>"><span class="fa fa-refresh" aria-hidden="true"></span>
							</button>				
						  </div>		
       					</div>
                     
					  </div>		
					</form>
                </div>
            </div>
        </div>
		<!------------------ Filter OR Search start----------------------->
	  
		</div>
	  </div>
	</div>
</div>


