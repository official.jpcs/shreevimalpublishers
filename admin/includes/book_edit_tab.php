<?php
$book_id =$_REQUEST['book_id'];
?>

<ul  class="nav nav-pills" style="margin-top: 10px;">
	<li class="<?php if($tab_edit=='general_info') { echo 'active'; }else{ echo ''; }?>">
		<a href="edit_book_general_info.php?book_id=<?=$book_id ?>&step=general_info&pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" >GENERAL INFORMATION</a>
	</li>
	
	<li class="<?php if($tab_edit=='publisherinfo_info') { echo 'active'; }else{ echo ''; }?>">
		<a href="edit_book_publisher_info.php?book_id=<?=$book_id ?>&step=publisher_info&pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" >
			PUBLISHER'S INFORMATION
		</a> 
	</li>
	
	<li class="<?php if($tab_edit=='book_detail') { echo 'active'; }else{ echo ''; }?>">
		<a href="edit_book_detail.php?book_id=<?=$book_id ?>&step=book_detail&pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" >BOOK DETAILS</a>
	</li>
	
	<li class="<?php if($tab_edit=='storage_info') { echo 'active'; }else{ echo ''; }?>">
		<a href="edit_book_storage_detail.php?book_id=<?=$book_id ?>&step=storage_detail&pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" >STORAGE DETAILS</a>
	</li>

	<li class="<?php if($tab_edit=='related_books') { echo 'active'; }else{ echo ''; }?>">
		<a href="edit_related_books.php?book_id=<?=$book_id ?>&step=related_books&pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" >RELATED BOOKS</a>
	</li>

	<li class="<?php if($tab_edit=='book_images') { echo 'active'; }else{ echo ''; }?>">
		<a href="edit_book_images.php?book_id=<?=$book_id ?>&step=book_images&pages=<?php if(isset($_GET['pages'])) echo $_GET['pages']; else echo 1;?>" >BOOK IMAGES</a>
	</li>
			
</ul>
<!--
<ul  class="nav nav-pills" style="margin-top: 10px;">
	<li class="active"><a href="edit_book_general_info.php#general_info" data-toggle="tab">GENERAL INFORMATION</a></li>
	<li><?php //if($book_id!=NULL){?>
	<a href="edit_book_publisher_info.php" data-toggle="tab">PUBLISHER'S INFORMATION</a> 
	<?php //} else{echo"<a>PUBLISHER'S INFORMATION</a>";}?></li>
	<li><?php //if($book_id!=NULL){?><a href="edit_book_detail.php#book_info" data-toggle="tab">BOOK DETAILS</a><?php //} else{echo"<a>BOOK DETAILS</a>";}?></li>
	<li><?php //if($book_id!=NULL){?><a href="edit_book_storage_detail.php#storage_info" data-toggle="tab">STORAGE DETAILS</a><?php //} else{echo"<a>STORAGE DETAILS</a>";}?></li>
			
</ul>
-->