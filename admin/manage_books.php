<?php 
/***************************************************************
 *  File Name : Manage Books
 *  Created Date: 25/03/2015
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Manage Books";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_authors_publishers","tbl_books","tbl_book_quantity_at_location")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';

// Include Header Section
include(NAVIGATION_FILE . "header.php");


//Include Controller Section
include(CONTROLLER_PATH."BooksController.php");

//Include View Section
include( VIEW_PATH."manage_books_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
<script type="text/javascript">
$(function() {
        $('.lazy').lazy({
            placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7..."
        });
    })
</script>