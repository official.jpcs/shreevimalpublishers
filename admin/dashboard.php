<?php 
/***************************************************************
 *  File Name : Dasboard
 *  Created Date: 05/03/2015
 *  Created By: Prasad Bhale
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Dashboard";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("tbl_book_categories","reuse","tbl_books","tbl_sales_master_entry","tbl_contact")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

//Include Controller Section
include(CONTROLLER_PATH."DashboardController.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';


// Include Header Section
include(NAVIGATION_FILE . "header.php");

         

//Include View Section
include( VIEW_PATH."dashboard_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
