<?php 
/***************************************************************
 *  File Name : Add/Edit Books
 *  Created Date: 25/03/2015
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Manage Distributors";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_authors_publishers","tbl_books","tbl_book_quantity_at_location","tbl_distributors","tbl_states","tbl_cities")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';

// Include Header Section
include(NAVIGATION_FILE . "header.php");



//Include Controller Section
include(CONTROLLER_PATH."DistributorController.php");

//print_r($distributors);


$state_list  = new tbl_states();
			$country_id=101; 
			$state_list = $state_list->select_state_list($country_id);
			$state_id=0;
			$states_list_show="";
			   for($i_state=0;$i_state <=count($state_list)-1;$i_state++) {
			 
					if($state_list[$i_state]->id==$distributors->distributor_state_id){
					   $states_list_show.="<option value='".$state_list[$i_state]->id."' selected>".$state_list[$i_state]->name ."</option>";
					   $state_id=$state_list[$i_state]->id;
					   }
					   else
					   $states_list_show.="<option value='".$state_list[$i_state]->id."'>".$state_list[$i_state]->name ."</option>";
			   }

			   
/********************************* Display Cities **************************************/			/*
			$cities_list_show="";
			$city_list  = new tbl_cities();
			$result_city_list = $city_list->select_city_list($state_id);
			
			for($i_city=0;$i_city <=count($result_city_list)-1;$i_city++){
			
					if($result_city_list[$i_city]->id==$distributors->distributor_city_id)
						$cities_list_show.= "<option value='".$result_city_list[$i_city]->id."'  selected>".$result_city_list[$i_city]->name."</option>";
						
					else
						$cities_list_show.= "<option value='".$result_city_list[$i_city]->id."'>".$result_city_list[$i_city]->name."</option>";
			}
*/


//Include View Section
include( VIEW_PATH."add_new_distributor_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
