<?php
	/**
	 * class.php
	 *
	 * Description	: A Class Config File
	 *
	 * Developed By : Prasad Bhale
	 *
	 * Developed On : 28 Feb 2015
	 *
	 * Liscence 	: GPL
	 *
	 * Created On 	: 24/10/2013
	 *
	 * Created By 	: Prasad Bhale
	 *
	 */

	/**
	 * DB Connectivity
	 */

	foreach ($include_files["model"] as $ClassName)
	{
		switch($ClassName)
		{
			case 'reuse':
				include_once(FUNCTION_DIR_PATH."commonFunctions.php");
				$re    = new reuseFunction();
			break;

			case 'tbl_book_categories':
				require_once(MODEL_DIR_PATH."class.tbl_book_categories.php");
			break;

			case 'tbl_binding_type':
                            require_once(MODEL_DIR_PATH."class.tbl_binding_type.php");
			break;
			case 'tbl_authors_publishers':
                            require_once(MODEL_DIR_PATH."class.tbl_authors_publishers.php");
			break;
			case 'tbl_storage_locations':
                            require_once(MODEL_DIR_PATH."class.tbl_storage_locations.php");
			break;
			case 'tbl_books':
                            require_once(MODEL_DIR_PATH."class.tbl_books.php");
			break;

			case 'tbl_book_quantity_at_location':
                            require_once(MODEL_DIR_PATH."class.tbl_book_quantity_at_location.php");
			break;


			case 'tbl_sales_master_entry':
                            require_once(MODEL_DIR_PATH."class.tbl_sales_master_entry.php");
			break;

			case 'tbl_sales_book_entry':
                            require_once(MODEL_DIR_PATH."class.tbl_sales_book_entry.php");
			break;

			case 'tbl_users':
                            require_once(MODEL_DIR_PATH."class.tbl_users.php");
			break;

			case 'tbl_languages':
                            require_once(MODEL_DIR_PATH."class.tbl_languages.php");
			break;

			case 'tbl_delivery_master_entry':
                            require_once(MODEL_DIR_PATH."class.tbl_delivery_master_entry.php");
			break;

			case 'tbl_delivery_book_entry':
                            require_once(MODEL_DIR_PATH."class.tbl_delivery_book_entry.php");
			break;

			case 'tbl_distributors':
                            require_once(MODEL_DIR_PATH."class.tbl_distributors.php");
			break;
			
			case 'tbl_states':
                            require_once(MODEL_DIR_PATH."class.tbl_states.php");
			break;
			
			case 'tbl_cities':
                            require_once(MODEL_DIR_PATH."class.tbl_cities.php");
			break;

			case 'tbl_newsletter_subscriber':
                            require_once(MODEL_DIR_PATH."class.tbl_newsletter_subscriber.php");
			break;

			case 'tbl_contact':
                            require_once(MODEL_DIR_PATH."class.tbl_contact.php");
			break;

			case 'tbl_settings':
			
                            require_once(MODEL_DIR_PATH."class.tbl_settings.php");
			break;
			
			case 'tbl_order':
                require_once(MODEL_DIR_PATH."class.tbl_order.php");
			break;
			
			case 'tbl_order_detail':
                require_once(MODEL_DIR_PATH."class.tbl_order_detail.php");
			break;
			
			

		}
	}
	
?>
