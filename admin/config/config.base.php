<?php
@session_start();

// Set PHP Configuration
@ini_set('arg_separator.output', '&amp;');
@ini_set('magic_quotes_runtime', 0);
@ini_set('magic_quotes_sybase', 0);
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Set Current Protocol
define('PROTOCOL', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http');

// Set Current Host Name
define('HOST_NAME', $_SERVER['HTTP_HOST']);

// Set Current Domain Name
preg_match("/[^\.\/]*\.[^\.\/]+$/", HOST_NAME, $domains);
$domain_name = count($domains) > 0 ? array_pop($domains) : HOST_NAME;
define('DOMAIN_NAME', $domain_name);


// Set Base Path
define('BASE_PATH', str_replace("\\", "/", realpath(dirname(__FILE__) . "/../") . DIRECTORY_SEPARATOR));



// Set Absolute Path
define('ABSOLUTE_PATH', strtolower(str_replace($_SERVER['DOCUMENT_ROOT'], "", BASE_PATH)));



// Set Base URL
define('BASE_URL', PROTOCOL . '://' . HOST_NAME . ABSOLUTE_PATH);

// Set Absolute URL
define('ABSOLUTE_URL', ABSOLUTE_PATH);

// Set Server Path/URL
define('SERVER_PATH', BASE_URL);


define('SERVER_URL', SERVER_PATH);
?>