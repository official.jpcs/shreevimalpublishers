<?php
session_start();
error_reporting(0);

if($page_loc =="Front_end")
{
	
}
else
{
	if(!isset($_SESSION['user']) && $tab!="User Login")
	{
	 echo "<script>window.location='index.php';</script>";
	}
}

require_once(dirname(__FILE__) . "/config.base.php");
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 0);

define("ITEMS_PER_PAGE",20);
define("ITEMS_PER_PAGE_ACTIVITY",2);



define("FOLDER_PATH", BASE_PATH);
define("FOLDER_PATH_HTTP", BASE_URL);


//======== CONTROLLER_PATH PATH =============//
define("CONTROLLER_PATH", FOLDER_PATH . "controller/");
define("CONTROLLER_PATH_HTTP", FOLDER_PATH_HTTP . "controller/");


//======== VENDOR CONTROLLER_PATH PATH =============//
define("VENDOR_CONTROLLER_PATH", FOLDER_PATH . "controller/vendor/");
define("VENDOR_CONTROLLER_PATH_HTTP", FOLDER_PATH_HTTP . "controller/vendor/");


//======== CONTROLLER_PATH PATH AJAX=============//
define("AJAX_PATH", FOLDER_PATH . "controller/ajax/");
define("AJAX_PATH_HTTP", FOLDER_PATH_HTTP . "controller/ajax/");

//======== CSS PATH =============//
define("CSS_PATH_HTTP", FOLDER_PATH_HTTP . "css/");
//======== IMAGE PATH =============//
define("IMAGE_PATH_HTTP", FOLDER_PATH_HTTP . "images/");

//======== VIEW PATH =============//
define("VIEW_PATH", FOLDER_PATH . "views/");
define("VENDOR_VIEW_PATH", FOLDER_PATH . "views/vendor/");



//======== class config Path =============//
define("CONFIG_CLASS_PATH", BASE_PATH . "config/");

//======== NAVIGATION PATH =============//
define("NAVIGATION_FILE", FOLDER_PATH . "includes/");
define("JS_PATH", FOLDER_PATH_HTTP . "js/");
define("MODULE_PATH", FOLDER_PATH . "includes/modules/");
define("MODEL_DIR_PATH", FOLDER_PATH . "model/");
define("MODULE_PATH_HTTP", FOLDER_PATH_HTTP . "includes/modules/");
define("INCLUDE_DIR_PATH", FOLDER_PATH . "includes/");
define("MODULES_DIR_PATH", INCLUDE_DIR_PATH . "modules/");
define("FUNCTION_DIR_PATH", INCLUDE_DIR_PATH . "functions/");
define("ADMIN_DIR_PATH", FOLDER_PATH . "admin/");
define("CLIENT_DIR_PATH", FOLDER_PATH . "client/");
define("ADMIN_COMMON_FILE_PATH", INCLUDE_DIR_PATH . "navigation/admin/");
define("CLIENT_COMMON_FILE_PATH", INCLUDE_DIR_PATH . "navigation/client/");

define("CONTROLLER_PATH", FOLDER_PATH . "controller/admin/");
define("ADMIN_CONTROLLER_PATH_HTTP", FOLDER_PATH_HTTP . "controller/admin/");

//Pagination Admin
define("INCREMENT_BY", 10);

//======== SENDEMAIL PATH =============//
define("EMAIL_DIR_PATH", MODULES_DIR_PATH . "email/");

define('CURR_DATE', date('Y-m-d H:i:s'));

/* HTML5 validations patterns */
/* validation pattern for text field */
define("ONLY_TEXT_PATTERN","^[a-zA-Z0-9]+(\s?[a-zA-Z0-9().\-_&\s]+)*$");
define("ONLY_TEXT_MESSAGE","Only Alhanumeric,(),- ,_,& allowed. ");

//define("TEXT_PATTERN","^[a-zA-Z0-9]+(\s?[a-zA-Z0-9().\-_'&\s]+)*$");
define("TEXT_PATTERN1","^[a-zA-Z0-9]+(\s?[a-zA-Z0-9().\-_&']+)*$");
define("TEXT_PATTERN","^[-@./#&+\w\s]*$");

define("TEXT_MESSAGE","Provide alphanumeric value");

define("DESCRIPTION_PATTERN","[a-zA-Z0-9()-']");
define("DESCRIPTION_MESSAGE","Only Alhanumeric,(),- ,' allowed. ");


//define("EMAIL_PATTERN","[a-zA-Z0-9.@-_]");
define("EMAIL_PATTERN","[^@]+@[^@]+\.[a-zA-Z]{2,6}");
 
define("EMAIL_MESSAGE","Provide proper Email Address");

define("NUMBER_PATTERN_3_DIG",".{3,}");
define("NUMBER_PATTERN_3_MSG","Minimum 3 digit require");

define("NUMBER_PATTERN_4_DIG",".{4,}");
define("NUMBER_PATTERN_4_MSG","Minimum 4 digit require");


define("FLOAT_PATTERN","[0-9.,]");
define("FLOAT_PATTERN_MSG","Only Integer / Float Value allowed");

define("NUMBER_PATTERN","[0-9]*");
define("NUMBER_PATTERN_MSG","Only Integer allowed");

define("FLOAT_PATTERN_2","[0-9]+([\.|,][0-9]+)?");



define("NUMBER_PATTERN_GRETER","^[1-9][0-9]*$");
define("NUMBER_PATTERN_GRETER_MSG","number should be greater than zero");

/* validation pattern for url */
define("URL_PATTERN","(http|https|ftp)://www\.([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])|([a-zA-Z0-9-]+\.)+(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(/($|[a-zA-Z0-9.,?'\\+&%$#=\~_-]+))*");
define("URL_PATTERN_MSG","Provide proper url");

//========== Logo image ============//

define('YEAR_RANGE', date('Y')); 

define("MONTH_ARRAY", serialize(array(1 =>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December')));

define("GENDER", serialize(array('0' =>'---Select Gender---','1' =>'Male','2'=>'Female')));

// LAnguage Array
define("LANGUAGE", serialize(array('1' =>'Marathi','2'=>'Hindi','3'=>'English','4'=>'Gujarati')));

define("TXTMAXLENGTH", 30);


//======= BOOK_THUMBNAIL Folder Path =========// 
define("BOOK_THUMBNAIL_FOLDER_HTTP", FOLDER_PATH_HTTP."/images/book_thumbnails/other_images/");
define("BOOK_THUMBNAIL_FOLDER_PATH", FOLDER_PATH."images/book_thumbnails/other_images/");

//======= BOOK_OTHER_IMAGES Folder Path =========// 
define("BOOK_OTHER_IMAGES_FOLDER_HTTP", FOLDER_PATH_HTTP."/images/book_thumbnails/other_images/");
define("BOOK_OTHER_IMAGES_FOLDER_PATH", FOLDER_PATH."images/book_thumbnails/other_images/");



// Export CSV
define("EXPORT_DIR_PATH",FOLDER_PATH."report_files/export/");
define("EXPORT_FILE_PATH",FOLDER_PATH_HTTP."report_files/export/");

// Import Blogger CSV
define("IMPORT_CSV_PATH", BASE_PATH . "user_files/imported_csv");

define("EXT",".php");

define("USER_TYPE", serialize(array(1=>'Admin',2=>'General User')));

define("OUT_OF_STOCK_AFTER_QTY",2);

// Razer Pay
//$keyId = 'rzp_test_6zQUILB1Ff89dS';
//$keySecret = 'TiKvssyVPaPwyTtlK9805Z58';
//$displayCurrency = 'INR';


//Razerpay Detail
define("JOBSEEKER_REG_FEE", "100" );  //250 * 100
define("RAZER_API_KEY", "rzp_test_6zQUILB1Ff89dS" );  // Live Mode Key 
define("RAZER_KEY_SECRET", "TiKvssyVPaPwyTtlK9805Z58" );  // Live Mode Secret 



// Database Connection
$connectImg=mysqli_connect("localhost","shreeptb_shreeptb","P@ssion!@#123","shreeptb_svppl") or die("Could not connect database");
//$connectImg = mysqli_connect("localhost","root","root") or die("Could not connect database");
mysqli_select_db($connectImg,"shreeptb_svppl") or die("Could not select database");
?>
