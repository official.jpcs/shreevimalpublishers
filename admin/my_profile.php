<?php
$tab="My Account";
/* Including Globally Declared Variables */
include("config/config.php");


$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_users")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");


// Include Controller 
include("controller/UserController.php");


		$user = new tbl_users();
		$user = $user->selectSingleUser($_SESSION['user']['user_id'],'','');
		 $user_id =$user[0]['user_id'];
		 $name =$user[0]['name'];
		 $address =$user[0]['address'];
		 $contact_no =$user[0]['contact_no'];
		 $email=$user[0]['email_id'];
		 $username=$user[0]['username'];
		 $user_type=$user[0]['user_type'];
         $location_id =$user[0]['location_id'];
		 
/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';


// Include Header Section
include(NAVIGATION_FILE . "header.php");


$user_type_list=unserialize(USER_TYPE);


// Include View
include(VIEW_PATH."my_profile_view.php");


//Include Footer
include(NAVIGATION_FILE ."footer.php");
?>