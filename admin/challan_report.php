<?php 
/***************************************************************
 *  File Name : Challan Report
 *  Created Date: 20/05/2016
 *  Created By: Prasad
 ************************************************************** */
/* Including Globally Declared Variables */
include("config/config.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';
$filename ="";
// Include Header Section
include(NAVIGATION_FILE . "header.php");


$tab="Reports";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_sales_master_entry","tbl_sales_book_entry","tbl_delivery_master_entry","tbl_delivery_book_entry")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

 /** Include PHPExcel */
require_once MODULE_PATH.'PHPExcel/PHPExcel.php';


//Include Controller Section
include(CONTROLLER_PATH."ReportSalesController.php");

//Include View Section
include( VIEW_PATH."report_delivery_challn_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
