<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery_002.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<title>SVPPL</title>
<!-- Bootstrap Do not make any changes in this css -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<!-- Customize Css -->
<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body class="login-bg">

<!--main container-->
<div class="container-fluid">
  <div class="row">
    <div class="col-md-16"> <div class="col-md-16"> 
<div class="col-md-8 col-md-offset-4 col-sm-16">
   <div class="clear">&nbsp;</div>
    <div class="login-wrap"> 
	  <img src="images/SVPPL_Logo.png" alt="SVPPL Logo" class="img-responsive center-block">
        <!--<h2>Login</h2>-->
        <div class="clear">&nbsp;</div>
        <!--flash messages section-->
        <form class="form-vertical" id="login-form" action="#" method="post">        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></div>
                <input placeholder="Enter Your Email" class="form-control" name="LoginForm[email]" id="LoginForm_email" type="text"> 
                    <span class="help-block error" id="LoginForm_email_em_" style="display: none"></span> </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon reveal"><i class="glyphicon glyphicon glyphicon-lock"></i></div>
                <input placeholder="Enter Your Password" class="form-control pwd" maxlength="12" name="LoginForm[password]" id="LoginForm_password" type="password"> <span class="help-block error" id="LoginForm_password_em_" style="display: none"></span> </div>
        </div>
        <div class="btn-block">
           <a class="btn btn-default" href="dashboard.php">Login</a>        
		   <input type="reset" class="btn btn-default" value="Reset"/>  
		  
		</div>
        
       
</form>     <!--End login form section-->
     <!--footer-->
<div class="text-center">Copyright <?=date("Y");?> © All rights reserved SVPPL</div>
   <div class="clear">&nbsp;</div>
<!--end footer--> 
    </div>
</div>
 </div>
 
      
      <!--end main container--> 
    </div>
  </div>
</div>

</body></html>