<?php 
/***************************************************************
 *  File Name : View Delivery Detail 
 *  Created Date: 17-04-2016
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Delivery";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_delivery_master_entry","tbl_delivery_book_entry")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';

// Include Header Section
include(NAVIGATION_FILE . "header.php");


//Include Controller Section
include(CONTROLLER_PATH."DeliveryChallanController.php");



//Include View Section
include( VIEW_PATH."view_delivery_entry_detail_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
