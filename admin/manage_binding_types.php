<?php 
/***************************************************************
 *  File Name : Manage Bindin Types
 *  Created Date: 27/02/2015
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Master Content";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("reuse","tbl_binding_type")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

$cover_type=1;

//Include Controller Section
include(CONTROLLER_PATH."BindingController.php");

/* Include message.php file */
include_once(MODULE_PATH."messages.php");

$Messages[] = $rec_msg;	
$rec_msg='';


// Include Header Section
include(NAVIGATION_FILE . "header.php");


//Include View Section
include( VIEW_PATH."manage_binding_types_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
