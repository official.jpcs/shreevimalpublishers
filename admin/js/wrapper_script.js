$(document).ready(function() {

var add_station_index = 0;
     // Add New User Registration section validation
     $('#frm_login_btn').click(function (){
         
		 var password=$("#password").val();
		 var user_name=$("#user_name").val();
		 var step=$("#step").val();

		if(user_name=='' || user_name==null)
		 {
             $("#user_name_msg").css("display","block");
	         $("#user_name").focus();
			 return false; 
		 }

		 if(password=='' || password==null)
		 {
             $("#password_msg").css("display","block");
	         $("#password").focus();
			 return false; 
		 }

	 
         $("#frm_login").submit();

      });


     // Add New User Registration section validation
     $('#frm_add_vendor').click(function (){
         

		var vendor_name = $("#vendor_name").val();
		var vendor_username = $("#vendor_username").val();
		/*var lead_mobile = document.getElementById("lead_mobile");
		var lead_appDate = document.getElementById("lead_appDate");
		var lead_allocation = document.getElementById("lead_allocation");
		var lead_email = document.getElementById("lead_email");
		*/
		if (vendor_name=="" || vendor_name==null ) {
			return false;
		}
		
		if (vendor_username=="" || vendor_username==null ) {
			return false;
		}
		 
         $("#frm_vendor").submit();

      });
	  

	  // Active Deactive User Info 
	  $('.active_info').click(function (){
	  
			var id=$(this).attr("id");
			var current_status=$(this).attr("title");
			var res=confirm(" Do you want to change "+current_status+" status");
			
			if(res==true){
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("active_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			}
      });
	  
	// Edit User Info 
	  $('.edit_info').click(function (){
	  
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("edit_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });

	// View Info 
	  $('.view_info').click(function (){
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("view_detail");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });

	// Delete Info 
	  $('.delete_info').click(function (){
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("delete_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });
	  
	  
	  /********************************************************************************/
	  /****************************** Assosciate start*************************************/
	  

	  // Active Deactive User Info 
	  	$("#results").on( "click", ".active_info", function (e){
	  
			var id=$(this).attr("id");
			var current_status=$(this).attr("title");
			var res=confirm(" Do you want to change "+current_status+" status");
			
			if(res==true){
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("active_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			}
      });
	  
	// Edit User Info 
	   	$("#results").on( "click", ".edit_info", function (e){

			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("edit_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });

	// View Info 
	  
	  $("#results").on( "click", ".view_info", function (e){

			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("view_detail");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });

	// Delete Info 
		  $("#results").on( "click", ".delete_info", function (e){

			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("delete_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });
	  
	  
	  /****************************** Assosciate End*************************************/
	  /********************************************************************************/


   
	// Hide Alert message on Key Press for all Text Box 
	$( "input[type='text'],[type='password']" ).keypress(function() {
	   
	   var field_id =$(this).attr("id");
	   $("#"+field_id+"_msg").css("display","none");
	   
	});



	
//*************************** Add Thought************************************//	
$('#add_thougth').click(function (e){
            e.preventDefault();
			var thought_en = $.trim($("#thought_en").val());
			var thought_mr = $.trim($("#thought_mr").val());
			if(thought_mr==""){
			   $("#thought_mr_error").html("Enter Description");
			   return false;
			   }else
			   $("#thought_mr_error").html('');
			if(thought_en==""){
			   $("#thought_en_error").html('Enter Description');
				return false;
				}else
				$("#thought_en_error").html('');				
			$("#frm_daily_thougth").submit(); 
			
			
}); //add thougth
//*************************** Add Quick Tips************************************//	
$('#add_quick_tips').click(function (e){
            e.preventDefault();
			var question_mr = $.trim($("#question_mr").val());
			var answer_mr = $.trim($("#answer_mr").val());
			var question_en = $.trim($("#question_en").val());
			var answer_en = $.trim($("#answer_en").val());
			if(question_mr==""){
			   $("#question_mr_error").html("Enter Question");
			   $("#question_mr").focus();
			   return false;
			   }else
			   $("#question_mr_error").html('');
			if(answer_mr==""){
			   $("#answer_mr_error").html('Enter Answer');
			   $("#answer_mr").focus();
				return false;
				}else
				$("#answer_mr_error").html('');	

			if(question_en==""){
			   $("#question_en_error").html("Enter Question");
			   $("#question_en").focus();
			   return false;
			   }else
			   $("#question_en_error").html('');
			if(answer_en==""){
			   $("#answer_en_error").html('Enter Answer');
			   $("#answer_en").focus();
				return false;
				}else
				$("#answer_en_error").html('');					
			$("#frm_daily_thougth").submit(); 
			
			
}); //add Quick Tips

//*************************** Add Upcoming Muhurt start************************************//	
$('#add_upcoming_muhurt').click(function (e){
            e.preventDefault();
			
			var date_mr = $.trim($("#date_mr").val());
			var title_mr = $.trim($("#title_mr").val());
			var days_list_mr = $.trim($("#days_list_mr").val());
			
			var date_en = $.trim($("#date_en").val());
			var title_en = $.trim($("#title_en").val());
			var days_list_en = $.trim($("#days_list_en").val());
			
			if(date_mr==""){
			   $("#date_mr_error").html("Select Date");
			   $("#date_mr").focus();
			   return false;
			   }else
			   $("#date_mr_error").html('');
			   
			if(title_mr==""){
			   $("#title_mr_error").html('Enter Muhurt Title');
			   $("#title_mr").focus();
				return false;
				}else
				$("#title_mr_error").html('');	

			if(days_list_mr==""){
			   $("#days_list_mr_error").html("Enter day list");
			   $("#days_list_mr").focus();
			   return false;
			   }else
			   $("#days_list_mr_error").html('');
			   
			 if(date_en==""){
			   $("#date_en_error").html("Select Date");
			   $("#date_en").focus();
			   return false;
			   }else
			   $("#date_en_error").html('');
			   
			if(title_en==""){
			   $("#title_en_error").html('Enter Muhurt Title');
			   $("#title_en").focus();
				return false;
				}else
				$("#title_en_error").html('');	

			if(days_list_en==""){
			   $("#days_list_en_error").html("Enter day list");
			   $("#days_list_en").focus();
			   return false;
			   }else
			   $("#days_list_en_error").html('');
			
			$("#frm_upcoming_muhurt").submit(); 
			
			
}); 
//*************************** Add Upcoming Muhurt End************************************//	


//*************************** Add Upcoming Event start************************************//	
$('#add_upcoming_event').click(function (e){
            e.preventDefault();
			
			var event_date = $.trim($("#event_date").val());
			var event_title_mr = $.trim($("#event_title_mr").val());
			var event_title_en = $.trim($("#event_title_en").val());
			
			if(event_date==""){
			   $("#event_date_error").html("Select Date");
			   $("#event_date").focus();
			   return false;
			   }else
			   $("#event_date_error").html('');
			   
			if(event_title_mr==""){
			   $("#event_title_mr_error").html('Enter Marathi Event Title');
			   $("#event_title_mr").focus();
				return false;
				}else
				$("#event_title_mr_error").html('');	

			if(event_title_en==""){
			   $("#event_title_en_error").html("Enter English Event Title");
			   $("#event_title_en").focus();
			   return false;
			   }else
			   $("#event_title_en_error").html('');
			 
			$("#frm_upcoming_event").submit(); 
			
			
}); 
//*************************** Add Upcoming Event End************************************//	

	  

//*************************** Add Testimonial start************************************//	
$('#add_testimonial').click(function (e){
            e.preventDefault();
			
			var photo = $.trim($("#photo").val());
			var customer_name = $.trim($("#customer_name").val());
			var content = $.trim($("#content").val());
			
			if(photo==""){
			   $("#photo_error").html("Select Profile Image");
			   $("#photo").focus();
			   return false;
			   }else
			   $("#photo_error").html('');
			   
			if(customer_name==""){
			   $("#customer_name_error").html('Enter Name');
			   $("#customer_name").focus();
				return false;
				}else
				$("#customer_name_error").html('');	

			if(content==""){
			   $("#content_error").html("Enter Content");
			   $("#content").focus();
			   return false;
			   }else
			   $("#content_error").html('');
			 
			$("#frm_testimonial").submit(); 
			
			
}); 
//*************************** Add Testimonial End************************************//	



//***************************  strat Create User ************************************//	
$('#create_user').click(function (e){
            e.preventDefault();
//alert('AAA');
			
			var full_name = $.trim($("#full_name").val());
			var user_email = $.trim($("#user_email").val());
			var user_type = $.trim($("#user_type").val());
			var user_name = $.trim($("#user_name").val());
			var password = $.trim($("#password").val());
			
			if(full_name==""){
			   $("#full_name_error").html("Enter Name");
			   $("#full_name").focus();
			   return false;
			   }else
			   $("#full_name_error").html('');
			   
			if(user_email=='' || user_email==null)
             {
			 	$("#user_email_error").html("Enter Email");
				$("#user_email").focus();
                return false; 
             }else {
			 // validate email id
			 var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
			var valid = emailReg.test(user_email);

			if(!valid) {
			$("#user_email_error").html("Enter Valid Email ID...!");
			$("#user_email").focus();
			   return false;
			} else $("#user_email_error").html('');
			
			}
			
			if(user_type==""){
			   	$("#user_email_error").html("Select User Type");
			   $("#user_type").focus();
			   return false;
			   }else
			   $("#user_email_error").html('');
			
			if(user_name==""){
			   	$("#user_email_error").html("Enter User Name");
			   $("#user_name").focus();
			   return false;
			   }else
			   $("#user_name_error").html('');
			if(password==""){
			   	$("#password_error").html("Enter Password");
			   $("#password").focus();
			   return false;
			   }else
			   $("#password_error").html('');
							
			$("#frm_user").submit(); 
			
			
}); 
//***************************  End Create User ************************************//	


 });



function Validtn_empty(x, txtName, showerrhere) {
    var gettedval = x.value;
    var alphaExp = /^[a-z A-Z 0-9.]+$/;
    var txterr = document.getElementById(showerrhere);
    if (!gettedval.match(alphaExp) && (gettedval != ""))
    {
        txterr.innerHTML = "<font color='red'> Please Provide " + txtName + ". </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
    else if (gettedval.length == 0)
    {
        txterr.innerHTML = "<font color='red'> Please Provide " + txtName + ".</font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    } else {
        txterr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
}


function Validtn_url(x, txtName, showerrhere) {
    var gettedval = x.value;
    var alphaExp = /^[a-z A-Z 0-9.]+$/;
    var txterr = document.getElementById(showerrhere);
    if (gettedval.match(alphaExp) && (gettedval != ""))
    {
        txterr.innerHTML = "<font color='red'> Please Provide " + txtName + ". </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
    else if (gettedval.length == 0)
    {
        txterr.innerHTML = "<font color='red'> Please Provide " + txtName + ".</font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    } else {
        txterr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
}

function Validtn_date(x, txtName, showerrhere) {
    var gettedval = x.value;
    var alphaExp = /^[a-z A-Z 0-9.]+$/;
    var txterr = document.getElementById(showerrhere);
    if (gettedval.match(alphaExp) && (gettedval != ""))
    {
        txterr.innerHTML = "<font color='red'> Please Provide " + txtName + ". </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
    else if (gettedval.length == 0)
    {
        txterr.innerHTML = "<font color='red'> Please Provide " + txtName + ".</font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    } else {
        txterr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
}
 	
 	

function funResetRedirect(redirect_link)
{
	window.location=redirect_link;
}

//------------- Add new Property validation --------
function create_campaign_Validate()
{
	var campaign_name = document.getElementById("campaign_name");
	
	if (!Validtn_empty(campaign_name, 'Campaign Name', 'campaign_nameName_error')) {
		return false;
	}

	var campaign_start = document.getElementById("campaign_start");
	
	if (!Validtn_date(campaign_start, 'Campaign Start Date', 'campaign_startDate_error')) {
		return false;
	}
	var campaign_end = document.getElementById("campaign_end");
	
	if (!Validtn_date(campaign_end, 'Campaign End Date', 'campaign_endDate_error')) {
		return false;
	}

	/*var max_add_per_user = document.getElementById("max_add_per_user");
	
	if (!Validtn_date(max_add_per_user, 'Max Add / User /Day', 'max_add_per_user_error')) {
		return false;
	}*/

	var display_time = document.getElementById("display_time");
	
	if (!Validtn_date(display_time, 'Display Time', 'display_time_error')) {
		return false;
	}
	
	$("#frmcampaign").submit();
	
}

//------------- Add new App Info --------
function register_new_app()
{
	var company_name = document.getElementById("company_name");
	var app_name = document.getElementById("app_name");
	var google_play_link = document.getElementById("google_play");
	//var app_type = document.getElementById("campaign_name");
	
	if (!Validtn_empty(company_name, 'Company Name', 'company_nameName_error')) {
		return false;
	}

	if (!Validtn_empty(app_name, 'App Name', 'app_nameName_error')) {
		return false;
	}

	if (!Validtn_url(google_play, 'Google Play Link', 'google_playName_error')) {
		return false;
	}
	
	
	$("#frmcreateapp").submit();
	
}



//------------- Add new Property validation --------
function register_numer()
{
	var mobile_number = document.getElementById("mobile_number");

	if (!Validtn_empty(mobile_number, 'Mobile Number', 'mobile_Numbererror')) {
		return false;
	}
	
	$("#frm_regNumber").submit();
	
}



	// Create Assiciates
	$('#create').click(function (e){
            e.preventDefault();
           
		 var name=$.trim($("#name").val());
		 var email_id=$.trim($("#email_id").val());
         var phone_no=$.trim($("#phone_no").val());
		 var address=$.trim($("#address").val());
		 var city=$.trim($("#city").val());
		 var state=$.trim($("#state_list").val());
		 
		 
		 var skillList = "";
		$('#skill_list input[type=checkbox]').each(function () {
		
		if($(this).is(':checked'))
		skillList = "Checked";
		});
		
		
            if(name=='' || name==null)
             {
                $("#name").next('div.red').remove();
                $("#name").after('<div class="red">Enter Name...!</div>');
				$("#name").focus();
                return false; 
             }else $("#name").next('div.red').remove();


            
            if(email_id=='' || email_id==null)
             {
                $("#email_id").next('div.red').remove();
                $("#email_id").after('<div class="red">Enter Email ID...!</div>');
                $("#email_id").focus();
                return false; 
             }else {
			 // validate email id
			 var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
			var valid = emailReg.test(email_id);

			if(!valid) {
			
			   $("#email_id").next('div.red').remove();
			   $("#email_id").after('<div class="red">Enter Valid Email ID...!</div>');
			   $("#email_id").focus();
			   return false;
			} else $("#email_id").next('div.red').remove();
			
			}
			
			if(phone_no=='' || phone_no==null)
             {
                $("#phone_no").next('div.red').remove();
                $("#phone_no").after('<div class="red">Enter mobile number</div>');
                $("#phone_no").focus();
                return false; 
             } else $("#phone_no").next('div.red').remove();
			 
               if(address=='' || address==null)
             {
                $("#address").next('div.red').remove();
                $("#address").after('<div class="red">Enter Address</div>');
                $("#address").focus();
                return false; 
             }else $("#address").next('div.red').remove();
			 
			
			 if(state=='0' || state=='' || state==null)
             {
                $("#state_list").next('div.red').remove();
                $("#state_list").after('<div class="red">Select State</div>');
                $("#state_list").focus();
                return false; 
             }else $("#state_list").next('div.red').remove();
			 
			 
			  if(city=='0' || city=='' || city==null)
             {
                $("#city").next('div.red').remove();
                $("#city").after('<div class="red">Select City</div>');
                $("#city").focus();
                return false; 
             }else $("#city").next('div.red').remove();
			 
			  if(skillList=='' || skillList==null)
             {
				$('#skill_list').next('div.red').remove();
				$('#skill_list').after('<div class="red">Select At least One Checkbox</div>');
				
                return false; 
             }else $('#skill_list').next('div.red').remove();
			 
										   $("#reg_associate").submit();    

		/*
			$.ajax({
					url: "controller/ajax/ajaxController.php",

					data : {'action':'Validate_Captcha','captcha_val_enter' :code_captcha},

					success: function(result) 
					{
						//alert(result);

							if(result =='valid')
							{
							   $("#reg_associate").submit();    
							}
							else
							{
							   $('#refresh').next('div.red').remove();
							   $('#refresh').after('<div class="red">Invalid captcha value provided.</div>');
							   $("#code_captcha").focus();
								return false; 
							}

					}
			});

			*/
			
			
			
			 
});



	$( "#state_list" ).change(function() {
	
		var state_id =$(this).val();
		
		if(state_id=="" || state_id==null)
		{
		 return false;	
		}
		
		$.ajax({

			url: "controller/ajax/ajaxController.php",
			data : {'action':'city_list','state_id':state_id},
			success: function(result) 
			{
				console.log(result);
				
				if(result !=="")
				{
					
					$(".city_list_div").html(result);
				}
				
			}
		});
	
        });
	
	

	
	

