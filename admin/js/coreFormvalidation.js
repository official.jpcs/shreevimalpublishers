// ------------------Developer Name: MOHAN created on :19/10/2016 ------------------
function Validtn_empty(x, txtName, showerrhere) {
    var gettedval = x.value;
    var alphaExp = /^[a-z A-Z 0-9.]+$/;
    var txterr = document.getElementById(showerrhere);
    if (!gettedval.match(alphaExp) && (gettedval != ""))
    {
        txterr.innerHTML = "<font color='red'> Please provide " + txtName + ". </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
    else if (gettedval.length == 0)
    {
        txterr.innerHTML = "<font color='red'> Please provide " + txtName + ".</font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    } else {
        txterr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
}
/********************* Mohan Scripts**************/

// ------------------- Enter Only numbers ------------------
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        alert("Please Enter Numeric Value Only")
        return false;
    }
    return true;
}
//--------------Date Validate---------------
function gretarDate(x, dropname, showerrhere) {
    var gettedval = x.value;
    var selerr = document.getElementById(showerrhere);
    if (gettedval != "") {
        selerr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
    else
    {
        selerr.innerHTML = "<font color='red'> Please Select Provide " + dropname + " List. </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
}
// ------------------- Enter Only Characters ------------------
function isAlphaKey(e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    if ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8) {
        return true;
    }
    alert("Please Enter Characters only")
    return false;
}
// ------------------ Email ID ------------------
function Validtn_emailid(x, showerrhere) {
    var gettedval = x.value;
    var numericExpression = /^[a-zA-Z0-9-\_.]+@[a-zA-Z0-9-\_.]+\.[a-zA-Z0-9.]{2,5}$/;
    var emailerr = document.getElementById(showerrhere);
    if (gettedval != "") {
        if (gettedval.match(numericExpression))
        {
            emailerr.innerHTML = "";
            x.style.border = "1px solid green";
            return true;
        }
        else
        {
            emailerr.innerHTML = "<font color='red'>Please Enter Valid Email Id.</font>";
            x.value = '';
            x.style.border = "1px solid red";
            return false;
        }
    } else {
        emailerr.innerHTML = "<font color='red'>Please Enter Valid Email Id.</font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
}
//------------------------ 
function phonenumber(x, txtName, showerrhere) {
    var gettedval = x.value;
    var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
    var txterr = document.getElementById(showerrhere);
    if (!gettedval.match(phoneno) && (gettedval != ""))
    {
        txterr.innerHTML = "<font color='red'> Please provide valid " + txtName + ". </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
    else if (gettedval.length == 0 && gettedval.length < 10)
    {
        txterr.innerHTML = "<font color='red'> Please provide " + txtName + ".</font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    } else {
        txterr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
}

// ------------------ Select List ------------------
function Validtn_selection(x, dropname, showerrhere) {
    var gettedval = x.value;
    var selerr = document.getElementById(showerrhere);
    if (gettedval != "") {
        selerr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
    else
    {
        selerr.innerHTML = "<font color='red'> Please Select Value From " + dropname + " List. </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
}

//-------password checking-------
function chkfpswd()
{
    var namee = document.editalpass.txtpass.value;
    var nalt = document.getElementById('txtpass1');
    if (namee != "")
    {
        if (namee.length < 6) {
            nalt.innerHTML = "<font color='red'>Password Should be min 6 characters.</font>";
            document.editalpass.txtpass.focus();
            document.editalpass.txtpass.value = "";
            return false;
        } else {
            nalt.innerHTML = "";
            return true;
        }
    }
    else if (namee.length == 0)
    {
        nalt.innerHTML = "<font color='red'>Enter Password.</font>";
        document.editalpass.txtpass.focus();
        document.editalpass.txtpass.value = "";
        return false;
    }
}

// ------------------ Select List ------------------
function Validtn_selection(x, dropname, showerrhere) {
    var gettedval = x.value;
    var selerr = document.getElementById(showerrhere);
    if (gettedval != "") {
        selerr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;
    }
    else
    {
        selerr.innerHTML = "<font color='red'> Please Select Value From " + dropname + " List. </font>";
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    }
}

function Validtn_birthdt(x, txtName, showerrhere)
{
    var dob = x.value;
    var txterr = document.getElementById(showerrhere);
    if (dob.length == 0)
    {
        txterr.innerHTML = "<font color='red'> Please provide " + txtName + ". </font>";
        //alert("Please Choose Birth Date.");
        x.value = '';
        x.style.border = "1px solid red";
        return false;
    } else {
        txterr.innerHTML = "";
        x.style.border = "1px solid green";
        return true;

    }
}
//-------------- Doc file validatation ------------
function validtn_docs(){
   var myfile="";
   myfile= $( this ).val();
   var ext = myfile.split('.').pop();
   if(ext=="pdf" || ext=="docx" || ext=="doc"){
       alert(ext);
   } else{
       alert(ext);
   }
}