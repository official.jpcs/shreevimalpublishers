 $(function () {
$('.datepicker').datepicker({
			format: "dd/mm/yyyy",
	  todayHighlight: true,
	  autoclose: true
	  
		});      });
	


$(document).ready(function() {



     



// Search Panel	
	 $(".show").on( "click", function (e){
		e.preventDefault();
		$("#filter-panel-sub_daily").slideToggle("slower");
		$("#filter-panel-sub_alter").slideToggle("slower");
		$("#filter-panel-sub_weekly").slideToggle("slower");
		$("#filter-panel").slideToggle("slower");
		 });

// states and cities ajax



	$( "#state_list" ).on('change',function() {
	
		var state_id =$(this).val();
		
		if(state_id=="" || state_id==null)
		{
		 return false;	
		}
		
		$.ajax({

			url: "controller/ajax/ajaxController.php",
			data : {'action':'city_list','state_id':state_id},
			success: function(result) 
			{
				console.log(result);
				
				if(result !=="")
				{
					
					$(".city_list_div").html(result);
				}
				
			}
		});
	
});
	








 $("a.sub_menu_click").click(function()
  {
	 
	  var open_sub =$(this).attr("rel");
       $("#"+open_sub).slideToggle();
       return false;
 }); 
 

    //Report Issue Step1 validation
    $('.next_step').click(function() {
								   
        var next_step = $(this).attr("id");
		
		switch(next_step)
		{
		  case 'next_step2' :
		     
			 var is_validate =0;
			 var book_name=$("#book_name").val();
			 var author=$("#author").val();
			 var book_language=$("#book_language").val();
			 var book_mrp=$("#book_mrp").val();
			 var book_edition=$("#book_edition").val();

			if(book_name=='' || book_name==null)
			 {
				 $(".book_name").css("display","block");
				 $(".div_book_name").addClass("has-error");
				 is_validate =1;
			 }
	
			 if(author=='' || author==null)
			 {
				 $(".author").css("display","block");
				 is_validate =1; 
			 }

			 if(book_language=='' || book_language==null)
			 {
				 $(".book_language").css("display","block");
				 is_validate =1;
			 }


			 if(book_mrp=='' || book_mrp==null)
			 {
				 $(".book_mrp").css("display","block");
				 is_validate =1;
			 }


			 if(book_edition=='' || book_edition==null)
			 {
				 $(".book_edition").css("display","block");
				 is_validate =1; 
			 }
			
			
			if(is_validate==0)
			{
				$("#collapseOne").toggle();
				$("#collapseTwo").toggle();
			}
			else
			{
			  return false;
			}
		  break;

		  case 'next_step3' :
			var is_validate =0;
			var book_publisher=$("#book_publisher").val();
			
			if(book_publisher=='' || book_publisher==null)
			 {
				 $(".book_publisher").css("display","block");
				 $(".div_book_publisher").addClass("has-error");
				 return false;
			 }
			
			$("#collapseTwo").toggle();	
			$("#collapseThree").toggle();		  
		  break;
		  
		  case 'next_step4' :
		  var is_validate =0;
		  var isbn_no =$("#isbn_no").val();
		  var book_category =$("#book_category").val();
		  
		  var no_of_pages =$("#no_of_pages").val();
		  var binding_type =$("#binding_type").val();
		  var cover_type =$("#cover_type").val();
		  var copyright =$("#copyright").val();
		  

			if(isbn_no=='' || isbn_no==null)
			 {
				 $(".isbn_no").css("display","block");
				 $(".div_isbn_no").addClass("has-error");
				 is_validate =1;
			 }
	
			 if(book_category=='' || book_category==null)
			 {
				 $(".book_category").css("display","block");
				 $(".div_book_category").addClass("has-error");
				 is_validate =1; 
			 }

			 if(no_of_pages=='' || no_of_pages==null)
			 {
				 $(".no_of_pages").css("display","block");
				 $(".div_no_of_pages").addClass("has-error");
				 is_validate =1; 
			 }

			 if(binding_type=='' || binding_type==null)
			 {
				 $(".binding_type").css("display","block");
				 $(".div_binding_type").addClass("has-error");
				 is_validate =1; 
			 }

			 if(cover_type=='' || cover_type==null)
			 {
				 $(".cover_type").css("display","block");
				 $(".div_cover_type").addClass("has-error");
				 is_validate =1; 
			 }

			 if(copyright=='' || copyright==null)
			 {
				 $(".copyright").css("display","block");
				 $(".div_copyright").addClass("has-error");
				 is_validate =1; 
			 }

			if(is_validate==0)
			{
				$("#collapseThree").toggle();
				$("#collapseFour").toggle();
			}
			else
			{
			  return false;
			}

		  
		 			  
		  break;
		  
		  case 'next_step5' :
		 	$("#collapseFour").toggle();		  
		  break;
		}
        
    });

    //Add Book : Back Step
    $('.back_step').click(function() {

        var back_step = $(this).attr("id");
		
		//alert(back_step);
		switch(back_step)
		{
		  case 'back_step1' :
		   $("#collapseTwo").toggle();
		   $("#collapseOne").toggle();
		  break;

		  case 'back_step2' :
		   $("#collapseThree").removeClass("in");
		   $("#collapseTwo").addClass("in");
		  break;
		  
		  case 'back_step3' :
		   $("#collapseFour").removeClass("in");
		   $("#collapseThree").addClass("in");
		  break;
		  
		}

    });


	// Hide Alert message on Key Press for all Text Box 
	$( "input[type='text'],[type='password'],[type='select']" ).keypress(function() {
	   var field_id =$(this).attr("id");
	   $("."+field_id).css("display","none");
	   $(".div_"+field_id).removeClass("has-error");
	   
	});

	$( "select" )
	  .change(function () {
	   var field_id =$(this).attr("id");
	   $("."+field_id).css("display","none");
	   $(".div_"+field_id).removeClass("has-error");
	  })
	  .change();
 
   /* Common Click Event For Edit Info 
	* By : Prasad Bhale
	* Created On : 25-02-2015
	*/	
	$('.edit_info').click(function (){
		var id=$(this).attr("id");
		var pages=$(this).attr("pages");
		var submit_to =$(this).attr("rel");
		
		$("#sub_step").val("edit_info");
		$("#sub_step").attr("name","step");
		$("#update_id").val(id);
		$("#pages").val(pages);
		$("#frm_action").attr("action",submit_to);
		$("#frm_action").submit();
			
	});

   /* Common Click Event For Set Upcoming Book 
	* By : Prasad Bhale
	* Created On : 25-02-2015
	*/	
	$('.set_upcoming').click(function (){
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("set_upcoming");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
	});

   /* Common Click Event For Set Most Salable Book 
	* By : Prasad Bhale
	* Created On : 25-02-2015
	*/	
	$('.most_sale').click(function (){
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("most_sale");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
	});




	$( "#book_publisher" ).change(function() {

		var publisher_id =$(this).val();
		
		if(publisher_id=="" || publisher_id==null)
		{
		 return false;	
		}
		
		$.ajax({

			url: "controller/ajax/ajaxController.php",
			data : {'action':'load_publisher_info','publisher_id':publisher_id},
			success: function(result) 
			{
				
				if(result !=="")
				{
					var splt_info = result.split("~");
					
					$("#pub_addr").val(splt_info[1]);
					$("#pub_email").val(splt_info[0]);
					$("#pub_website").val(splt_info[2]);
					$("#pub_contact_no").val(splt_info[3]);
					
				}
				
			}
		});
	
   });



	/*********************************
	* Add Multiple Storage Locations
	* Created By : Prasad
	* Create Date : 03-03-2015	
	***********************************/
	var add_station_index = 0;
	
	$("#add_storage_location").click(function () 
	{  
		
		   add_station_index++;
		  
		   $("#clone").clone().insertBefore("div#insert_flag:last").attr("id","clone" + add_station_index);
		   $("#clone" + add_station_index + " :input").each(function(){
				   $(this).attr("id",$(this).attr("id") + add_station_index);
				   $(this).val(''); 
				   $("#add_storage_location"+add_station_index).hide();
				   $("#remove_station"+add_station_index).show();             
				});
	
			 $("#clone"+add_station_index).children().find("#fieldExistmsg").css("display","none"); 
	
	});

	/* function to remove dynamic div of station list */
	$('#frm_new_book').on('click','button:button[name^="remove_station"]',function(e){ 
		//$(this).parent().parent().remove();
	});



	/*********************************
	* Add New Book --Form Submit  
	* Created By : Prasad
	* Create Date : 03-05-2015	
	***********************************/
	
	$('#frm_login_btn').click(function (){
         
		 var password=$("#password").val();
		 var user_name=$("#user_name").val();
		 var step=$("#step").val();

		if(user_name=='' || user_name==null)
		 {
             $("#user_name_msg").css("display","block");
	         $("#user_name").focus();
			 return false; 
		 }

		 if(password=='' || password==null)
		 {
             $("#password_msg").css("display","block");
	         $("#password").focus();
			 return false; 
		 }

	 
         $("#frm_login").submit();

      });

	// Delete Info 
	$('.logout_clk').click(function ()
	{
		window.location="log_out.php";
	});


	/*********************************
	* View Book Detail 
	* Created By : Prasad
	* Create Date : 12-06-2015	
	***********************************/
	
	$('.view_book_detail').click(function (){
            
            var book_id =$(this).attr("lang");
            var book_title =$(this).attr("title");
            $('#book_detail').modal('show');
            $(".book_tab").removeClass("active");
            $("#tab_gen").addClass("active");
            $(".tab-pane").removeClass("active in");
            $("#general_info").addClass("fade active in");
            
            
            $.ajax({

                    url: "controller/ajax/ajaxController.php",
                    data : {'action':'book_detail','book_id':book_id},
                    
                    success: function(result) 
                    {
                        if(result !=="")
                        {
                           var parse_book_info = $.parseJSON(result);
                           
                           var book_thumbnail= "<img src='"+parse_book_info[0].book_thumbnail+"' width='120'>"; 
                           
                           $("#book_title").html(book_title+" (#"+parse_book_info[0].isbn_no+")");
                           $("#info_for_book").val(book_id);
                           
                           $(".book_thumbnail").html(book_thumbnail);
                           
                           $(".book_name").html(parse_book_info[0].book_title);
                           $(".book_author").html(parse_book_info[0].author_name);
                           $(".compile_by").html(parse_book_info[0].compile_by);
                           $(".translate-by").html(parse_book_info[0].translated_by);
                           $(".book_lng").html(parse_book_info[0].book_language);
                           $(".book_mrp").html(parse_book_info[0].book_mrp);
                           $(".book_edn").html(parse_book_info[0].book_edition);
                            
                           $(".pub_name").html(parse_book_info[0].pub_name);
                           $(".pub_addr").html(parse_book_info[0].pub_addr);
                           $(".pub_contact").html(parse_book_info[0].pub_contact);
                           $(".pub_email").html(parse_book_info[0].pub_email);
                           $(".pub_web").html(parse_book_info[0].pub_website);
                          
                          
                          
                          $(".book_isbn").html("#"+parse_book_info[0].isbn_no);
                          $(".book_cat").html(parse_book_info[0].category_title);
                          $(".dim_width").html(parse_book_info[0].dim_width);
                          $(".dim_heigh").html(parse_book_info[0].dim_height);
                          $(".dim_depth").html(parse_book_info[0].dim_depth);
                          $(".no_of_pages").html(parse_book_info[0].no_of_pages);
                          $(".binding_type").html(parse_book_info[0].binding_type);
                          $(".cover_type").html(parse_book_info[0].cover_type);
                          $(".weight").html(parse_book_info[0].book_weight);
                          $(".prod_cost").html(parse_book_info[0].production_cost);
                          $(".translated").html(parse_book_info[0].is_translated);
                          $(".trn_frm_book").html(parse_book_info[0].translated_from_book);
                          $(".trn_frm_lng").html(parse_book_info[0].translated_frm_lang);
                          $(".pub_date").html(parse_book_info[0].publication_date);
                          $(".co_auth").html(parse_book_info[0].co_author);
                          $(".contributor").html(parse_book_info[0].contributor);
                          $(".editor").html(parse_book_info[0].editor);
                          $(".copyright").html(parse_book_info[0].copyright);
                          

                        }
                    }
            });
            
            
	
      });

    
	/*********************************
	* View Storage Detail Info
	* Created By : Prasad
	* Create Date : 12-06-2015	
	***********************************/
	
	$('.storage_detail').click(function (){
            
            var book_id = $("#info_for_book").val();
            
            
            $.ajax({

                    url: "controller/ajax/ajaxController.php",
                    data : {'action':'book_storage_detail','book_id':book_id},
                    
                    success: function(result) 
                    {
                        if(result !=="")
                        {
                           $("#storage_list").html(result);
                           
                          

                        }
                    }
            });
            
            
	
      });
    

/************* Function / Events For Sales Module *****************/


    

	/*********************************
	* Key Up Events for Quantity & Discount text Box
	* Created By : Prasad
	* Create Date : 21-04-2016	
	***********************************/

	
	$('.calculate_amount').bind('change keyup', function() {	
														 
            var txt_box_nm = $(this).attr("name");	
            var price = $("#sel_book_price").val();				
            var quantity = $("#qualtity_txtbox").val();
            var discount_per = $("#sel_book_discount").val();


            if(discount_per=="" || discount_per==null)
            {
                    discount_per = 0;
            }

            if(quantity!="" || quantity!=0)
            {
                    var discount_amount = (parseFloat(price) * parseFloat(discount_per))/ 100 ;

                    var discounted_book_price = parseFloat(price) - parseFloat(discount_amount);

                    var amount = discounted_book_price * quantity;
                    $("#sel_book_amount").val(amount);
            }
            else
            {
                    //alert("Please Provide Quantity First");
                    //$("#qualtity_txtbox").focus();
                    return false;
            }
		
	});




	/*********************************
	* Click Event to Save Book Sale Entry
	* Created By : Prasad
	* Create Date : 26-04-2016	
	***********************************/

	 $('#frm_save_book_sale').click(function() {

            var book_val_set = $("#search_book_name").val();
            var sel_book_id = $("#sel_book_id").val();	
            var sel_book_title =  book_val_set;	

            var price = $("#sel_book_price").val();				
            var quantity = $("#qualtity_txtbox").val();

          //  var discount_per = $("#sel_book_discount").val();

            var book_amount = $("#sel_book_amount").val();
            
            var storage_loc = $("#storage_loc").val();
           
            if(book_val_set=="" || book_val_set==0)
            {
                $("#sel_book_msg").css("display","block");
                $("#search_book_name").focus();
                return false;
            }

            if(price=="" || price==0 || isNaN(price))
            {
                $("#sel_book_price_msg").css("display","block");
                $("#sel_book_price").focus();
                return false;
            }
             
            if(quantity=="" || quantity==0 || quantity==null)
            {
                $("#qualtity_txtbox_msg").css("display","block");
                $("#qualtity_txtbox").focus();
                return false;
            }

            if(book_amount=="" || book_amount==0 || book_amount==null)
            {
                $("#sel_book_amount_msg").css("display","block");
                $("#sel_book_amount").focus();
                return false;
            }

            if(storage_loc=="" || storage_loc==0 || storage_loc==null)
            {
                $("#storage_loc_msg").css("display","block");
                $("#storage_loc").focus();
                return false;
            }
             
             
 
          ////  if(discount_per=="" || discount_per==0)
          //  {
              var discount_per = funCalDiscoutnFromAmount(book_amount,price,quantity);
           // }
			

            $("#search_book_name").val("");

            $("#sel_book_price").val("");				
            $("#qualtity_txtbox").val("");
            $("#sel_book_discount").val("");
            $("#sel_book_amount").val("");

            var total_amount = $("#total_amount").val();	

            // Create New Row
            $('#saleModal').modal('hide');

            var total_content = $("#sale_entry_table").html();


            var sel_book_row = '<tr class="odd" id="row_'+sel_book_id+'"> ';
                            sel_book_row += '<td style="width:25%">'+sel_book_title+'&nbsp;<input type="hidden" name="p_book_id[]"  id="p_book_id_'+sel_book_id+'"  value='+sel_book_id+' /> </td>';
                            sel_book_row += '<td style="width:10%"><span class="rupyaINR">Rs</span> '+price +'&nbsp;<input type="hidden" name="p_book_price_id[]"  id="p_book_price_id_'+price+'"  value='+price+' /> </td>';
                            sel_book_row += '<td style="width:10%">'+discount_per +'%&nbsp;<input type="hidden" name="p_discount_per_id[]"  id="p_discount_per_id_'+discount_per+'"  value='+discount_per+' /></td>';
                            sel_book_row += '<td style="width:8%">'+quantity +'&nbsp;<input type="hidden" name="p_quantity_id[]"  id="p_quantity_id_'+quantity+'"  value='+quantity+' /></td>';
                            sel_book_row += '<td style="width:10%"><span class="rupyaINR">Rs</span>'+ book_amount +'&nbsp;<input type="hidden" name="p_book_amount[]"  id="p_book_amount_'+book_amount+'"  value='+book_amount+' /></td>';
                            sel_book_row += '<td style="width:10%">';
                            sel_book_row += '<input type="hidden" name="location_id[]"   value='+storage_loc+' />';
                            sel_book_row += '<a title="Delete" rel="'+book_amount+'"  class="delete_book_row" id="'+sel_book_id+'" >Remove</a>';																							
                            sel_book_row += '</td>';
                    sel_book_row += '</tr>';



            var final_amount =parseFloat(total_amount)+ parseFloat(book_amount);	
			final_amount =final_amount.toFixed(2);
			
            $("#total_amount").val(final_amount);	
			$("#final_amount").val(final_amount);	
            $("#total_amount_lbl").html("<span class='rupyaINR'>Rs</span> " + final_amount);	
			$("#final_amount_lbl").html("<span class='rupyaINR'>Rs</span> " + final_amount);	

			var total_row_count = $("#row_count").val();
			total_row_count = parseInt(total_row_count) + parseInt(1);
			$("#row_count").val(total_row_count);

            $("#sale_entry_table").html(total_content+sel_book_row);
			
	});
    


	/*********************************
	* Key Up Events to Calculate Final Amout by adding Service Tax / VAT
	* Created By : Prasad
	* Create Date : 01-05-2016	
	***********************************/

	
	$('#tax_amount').bind('change keyup', function() {	
					
            var tax_amount = $(this).val();	
            var tax_type = $('input[name=tax_type]:checked').val();;
            var total_amount = $("#total_amount").val();
			

            if(total_amount=="" || total_amount==0)
            {
				return false;
            }
			else
			{
				
				    var tax_amount_val = (parseFloat(total_amount) * parseFloat(tax_amount))/ 100 ;
					var final_amount = parseFloat(total_amount)+parseFloat(tax_amount_val);
					
					$("#final_amount").val(final_amount.toFixed(2));
					$("#final_amount_lbl").html("<span class='rupyaINR'>Rs</span> "+ final_amount.toFixed(2));
				 
			}		
	});
	

	$('.tax_type').click(function() {
	
            var tax_amount = $("#tax_amount").val();	
            var tax_type = $(this).val();
            var total_amount = $("#total_amount").val();
			
			
				
            if(total_amount=="" || total_amount==0)
            {
				return false;
            }
			else
			{
				    var tax_amount_val = (parseFloat(total_amount) * parseFloat(tax_amount))/ 100 ;
					var final_amount = parseFloat(total_amount)+parseFloat(tax_amount_val);
					
					$("#final_amount").val(final_amount.toFixed(2));
					$("#final_amount_lbl").html("<span class='rupyaINR'>Rs</span> "+ final_amount.toFixed(2));
				 
			}
			
		
	
	});

	$('#submit_sales_entry').click(function (){
		
		var final_amount = $("#final_amount").val();
		
		if(final_amount =="" || final_amount==null || final_amount==0)
		{
			return false;	
		}
		else
		{
		 $("#frm_new_sale").submit();
		}
		
	});



/************* Function / Events For Sales Module *****************/


/************* Function / Events For User Module *****************/
	// Add New User Registration section validation
     $('#frm_add_user_submit').click(function (){
         
         var name=$("#name").val();
         var contact_no=$("#contact_no").val();
		 var email_id=$("#email_id").val();
		 var password=$("#password").val();
		 var user_name=$("#user_name").val();
		 var step=$("#step").val();

		if(name=='' || name==null)
         {
            $("#name_msg").css("display","block");
			$("#name").focus();
			return false;
		 }
		 
		 if(contact_no=='' || contact_no==null)
		 {
             $("#contact_no_msg").css("display","block");
             $("#contact_no").focus();
			 return false;
		 }
		 
		 if(email_id=='' || email_id==null)
		 {
             $("#email_id_msg").css("display","block");
	         $("#email_id").focus();
			 return false; 
		 }

		 if(user_name=='' || user_name==null)
		 {
             $("#user_name_msg").css("display","block");
	         $("#user_name").focus();
			 return false; 
		 }

		 if(step!="update_user" && (password=='' || password==null) )
		 {	
            $("#password_msg").css("display","block");
            $("#password").focus();
			return false;
		 }

		 
         $("#frm_add_new_user").submit();

      });
	 

	// Edit User Info 
	  $('.edit_info').click(function (){
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("edit_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });

	// View Info 
	  $('.view_info').click(function (){
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("view_detail");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
			
      });

	// Delete Info 
	$('.delete_info').click(function ()
	{
	
		if (confirm("Do you really want to delete this record!") == true) 
		{
			
			var id=$(this).attr("id");
			var submit_to =$(this).attr("rel");
			
			$("#sub_step").val("delete_info");
			$("#sub_step").attr("name","step");
			$("#update_id").val(id);
			$("#frm_action").attr("action",submit_to);
			$("#frm_action").submit();
		}
		else
		{
		  return false;
		}
		
	});

/************* Function / Events For User Module *****************/
});




$(function(){
    $(document).on('click', '.delete_book_row', function(){
        var book_id = $(this).attr("id");
		var book_amount = $(this).attr("rel");
		
		var total_row_count = $("#row_count").val();
		
		$("#row_"+book_id).remove();
		
		var total_amount = $("#total_amount").val();	      
		
		var final_amount =parseFloat(total_amount)- parseFloat(book_amount);	
		$("#total_amount").val(final_amount);	
		$("#total_amount_lbl").html("<span class='rupyaINR'>Rs</span> " + final_amount);	

		total_row_count = parseInt(total_row_count) - parseInt(1);
		$("#row_count").val(total_row_count);


		var tax_amount = $("#tax_amount").val();	
		var tax_type = $('input[name=tax_type]:checked').val();;
		 total_amount = final_amount;
		

			
		var tax_amount_val = (parseFloat(total_amount) * parseFloat(tax_amount))/ 100 ;
		var final_amount = parseFloat(total_amount)+parseFloat(tax_amount_val);
		
		$("#final_amount").val(final_amount.toFixed(2));
		$("#final_amount_lbl").html("<span class='rupyaINR'>Rs</span> "+ final_amount.toFixed(2));


		if(total_row_count <=0)
		{
		 $("#tax_amount").val(0);
		}


    })
})



$(document).ready(function() {

	// Sales Entry Date

	if($("#saleDate").length) 
	{						   
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		

		var checkoutone = $('#saleDate').datepicker({
			onRender: function(date) {
			return date.valueOf() > now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
			checkoutone.hide();
		}).data('datepicker');


		$("#saleDatebtn").click(function(event) {
			event.preventDefault();
			$("#saleDate").focus();
		});


		$("#saleDate").click(function(event) {
			event.preventDefault();
			$("#saleDate").focus();
		});
	
	}


   $( "#saleDate" ).focusout(function() {
        focus++;
        $( "#focus-count" ).text( "focusout fired: " + focus + "x" );
    });      



});//closing parenthesis of .ready function 


$(document).ready(function() {

// Publication Date
 
	if($("#pubDate").length) 
	{		
	
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		
		
		var checkoutone = $('#pubDate').datepicker({
			onRender: function(date) {
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
			checkoutone.hide();
		}).data('datepicker');
		
		
		$("#pubDatebtn").click(function(event) {
			event.preventDefault();
			$("#pubDate").focus();
		});
		
		
		$("#pubDate").click(function(event) {
			event.preventDefault();
			$("#pubDate").focus();
		});
	
	}
	
	
	$( "#pubDate" ).focusout(function() {
		focus++;
		$( "#focus-count" ).text( "focusout fired: " + focus + "x" );
	});      


 //  Sales Report - From Date
 
 	if($("#sale_from_date").length) 
	{		
	
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		
		
		var checkoutone = $('#sale_from_date').datepicker({
			onRender: function(date) {
			//return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
			checkoutone.hide();
		}).data('datepicker');
		
		
		$("#saleFromDatebtn").click(function(event) {
			event.preventDefault();
			$("#sale_from_date").focus();
		});
		
		
		$("#sale_from_date").click(function(event) {
			event.preventDefault();
			$("#sale_from_date").focus();
		});
	
	}
	
	
	$( "#sale_from_date" ).focusout(function() {
		focus++;
		$( "#focus-count" ).text( "focusout fired: " + focus + "x" );
	});      



 //  Sales Report - To Date
 
 	if($("#saleToDate").length) 
	{		
	
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		
		
		var checkoutone = $('#saleToDate').datepicker({
			onRender: function(date) {
			//return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
			checkoutone.hide();
		}).data('datepicker');
		
		
		$("#saleToDatebtn").click(function(event) {
			event.preventDefault();
			$("#saleToDate").focus();
		});
		
		
		$("#saleToDate").click(function(event) {
			event.preventDefault();
			$("#saleToDate").focus();
		});
	
	}
	
	
	$( "#saleToDate" ).focusout(function() {
		focus++;
		$( "#focus-count" ).text( "focusout fired: " + focus + "x" );
	});      


$('#upload_campaign_file').on('click', function() {
    
    var file_data = $('#customFile').prop('files');  
    var book_id = $('#book_id').val();  
    var page_no = $('#page_no').val();  
    
    $('#overlay').fadeIn() ;   
    
    
    //console.log("length:"+file_data.length); 
    var form_data = new FormData();                  
    for(var i_file=0;i_file<=file_data.length-1;i_file++)
    {
      form_data.append('file['+i_file+']', file_data[i_file]);
    }

        
        $.ajax({
            url: 'upload_bookimages.php?book_id='+book_id, // point to server-side PHP script 
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(result){
                
                if(result =="success")
                {
                    
                        
                    $('#overlay').fadeOut();    
                    alert("Files Uploaded successfully.");

					window.location="edit_book_images.php?book_id="+book_id+"&step=book_images&pages="+page_no;

                    //window.location="campaign_files.php?campaign_id="+campaign_id+"&priority="+priority;
                      
                }
                else{
                	alert("Error while uploading files.")
                }
    
            }
         });
});



});//closing parenthesis of .ready function 




    /*********************************
    * Smart Search Book List
    * Created By : Prasad
    * Create Date : 21-04-2016	
    ***********************************/


    $(function(){
        
        
        $(document).on('click', '.select_book_click', function(){

            var book_val_set = $(this).attr("value");
            var splt_val_set = book_val_set.split("~");

     
            var book_id = splt_val_set[0];
            var book_title = splt_val_set[1];
            var book_price = splt_val_set[2];

            if(book_val_set!="" || book_val_set!=0)
            {
                $("#sel_book_msg").css("display","none");
            }
            else
            {
                return false;
            }
            setTimeout("$('#suggestions8').fadeOut();", 7);
            
            $("#search_book_name").val(book_title);
            $("#sel_book_id").val(book_id);		
            $("#sel_book_price").val(book_price);
            $("#qualtity_txtbox").val(1);

            var amount = book_price * 1;
            $("#sel_book_amount").val(amount);
            
        })
        
        
    	$( ".book_position" ).on('change',function() {
    	
        	var book_id = $(this).attr("book_id"); 
            var image_position = $(this).val(); 
        	var page_no = $('#page_no').val();  
        	var file_id = $(this).attr("file_id");

        	if(image_position==null || image_position=="")
        	{
        	    
        	 return false;   
        	}
        	else
        	{
        	    $('#overlay').fadeIn() ;   
        	
        		$.ajax({
        
        			url: "controller/ajax/ajaxController.php",
        			data : {'action':'update_image_position',"image_id":file_id,"position":image_position},
        			success: function(result) 
        			{
        				if(result=="success")
        				{
        				    $('#overlay').fadeOut();   
        					alert("Image position updated successfully !");
        
        					window.location="edit_book_images.php?book_id="+book_id+"&step=book_images&pages="+page_no;
        				}	
        				
        			}
        		});  
        	}	
        		
    	
        });
    
        
        
    })


function funCalDiscoutnFromAmount(book_amount,price,quantity)
{
	
  var single_book_amount  = parseFloat(book_amount)/ quantity;
  var  dicount_per = parseFloat(100)- ( (parseFloat(single_book_amount) * 100)/parseFloat(price)) ;
  return dicount_per.toFixed(2); 

}

/* functions for smart search auto suggest functionality */
function suggest(obj,tbl_name,field_name,list_id,listing_state,from_popup)
{

    var keyword  = $(obj).val();
        keyword  = keyword.trim();
    var field_id = $(obj).attr('id');

	$("#sel_book_price").val("");
	$("#sel_book_discount").val("");
	$("#qualtity_txtbox").val("");
	$("#sel_book_amount").val("");

    var path = "controller/ajax/ajaxController.php";
   
    if(keyword.length == 0)
     {
        $('#'+list_id).fadeOut();
     } 
     else 
     {
        $.ajax({
                url: path,
                data : {'action':'smartAutoSearch','keyword':keyword,'method':from_popup},
                    success: function(msg)
                    {

                        if(msg.length >0) 
                        {
                            $('#'+list_id).show();
                            $('#'+list_id+'List').html(msg);
                        }
                    }
              });
     }
}

function fill(list_id,thisValue,field_id) 
{
    $('#'+field_id).val(thisValue);

   // setTimeout("$('#"+list_id+"').fadeOut();", 7);
}

/**************************************
* Function Name : funDoSearchActionByFieldfunDoSearchActionByField
* Description : Perform the form submit action  
* Author : Benchmark 
* Develop By : Prasad
* Developed Date : 06/12/2013
**************************************/
function funDoSearchActionByField(step,post_to,frm_name,search_field)
{
	
 	var field_name;
	var field_val;
	var search_key;
	var key_val="";
	
	var search_field = search_field.split(",");
	var search_field_length = search_field.length;

	
	$("#step_search").attr("name","step");
	$("#step").val(step);
	
	for(var i_cnt=0;i_cnt<=search_field_length-1;i_cnt++)
	{
	
	   field_name=search_field[i_cnt];
	  
	   field_val=$("#"+search_field[i_cnt]).val();
		var input = field_name.trim();
	  //var stat = funCkeckFieldInput(field_val);
	// var stat = funAlphanumericPattenrChkForSearch(input);
		


	  var stat=1;
	  if(stat==0)
	  {
		  alert('Do not insert Special charecters in Field values !');
		 /* $("#js_message").html('<div id="message_div" class="alert alert-error" align="center">Do not insert Special charecters in Field values !</div>');
		   setTimeout('$("#js_message").fadeOut("100")',5000); */
		
		 return false;
		 
	  }
	  
		  if(field_val=="" || field_val==null || field_val=="Select Status" || field_val=="Select Contract Status" || field_val=="All")
		  {
		  }
		  else
		  {
		   field_val= field_val.trim();
		   key_val +="&"+field_name+"="+field_val;
		  }
	 
	}
	
	//var key_val=$("#search_company").val();
	var post_to_url=post_to+"?action="+step+key_val;

	window.location=post_to_url;
}


function funResetRedirect(redirect_link)
{
	window.location=redirect_link;
}

function deleteImage(book_id, image_id) 
{

	var page_no = $('#page_no').val();  

	if (confirm("Are you sure want to delete this image?") == true) 
	{
        $('#overlay').fadeIn() ;
        
		$.ajax({

			url: "controller/ajax/ajaxController.php",
			data : {'action':'delete_book_image','book_id':book_id,"image_id":image_id},
			success: function(result) 
			{
				if(result=="success")
				{
				    $('#overlay').fadeOut() ;
					alert("Image delete successfully !");

					window.location="edit_book_images.php?book_id="+book_id+"&step=book_images&pages="+page_no;
				}	
				
			}
		});


		// $.ajax({
		// 	url: 'index.php?route=checkout/cart/add',
		// 	type: 'post',
		// 	data: 'book_id=' + book_id + '&image_id=' + quantity,
		// 	dataType: 'json',
		// 	success: function(json) {
		// 		$('.success, .warning, .attention, .information, .error').remove();
				
		// 		if (json['redirect']) {
		// 			location = json['redirect'];
		// 		}
				
		// 		if (json['success']) {
		// 			$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
		// 			$('.success').fadeIn('slow');
					
		// 			$('#cart-total').html(json['total']);
					
		// 			$('html, body').animate({ scrollTop: 0 }, 'slow'); 
		// 		}	
		// 	}
		// });
	}
	else
	{
	return false;
	}

}



