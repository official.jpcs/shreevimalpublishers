<?php 
/***************************************************************
 *  File Name : Manage Book Categories
 *  Created Date: 27/02/2015
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Master Content";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("tbl_book_categories","reuse")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");


// Include Header Section
include(NAVIGATION_FILE . "header.php");


//Include Controller Section
include(CONTROLLER_PATH."CategoryController.php");

//Include View Section
include( VIEW_PATH."add_new_category_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
