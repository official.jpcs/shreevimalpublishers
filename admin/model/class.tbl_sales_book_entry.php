<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_sales_book_entry
* GENERATION DATE:  30.04.2016
* CLASS FILE:       D:\wamp\www\svppl\class_generator/generated_classes/class.tbl_sales_book_entry.php
* FOR MYSQL TABLE:  tbl_sales_book_entry
* FOR MYSQL DB:     svppl
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_sales_book_entry
{ // class : begin


	// **********************
	// ATTRIBUTE DECLARATION
	// **********************
	
	
	var $sale_book_entry_id;   // (normal Attribute)
	var $sales_maste_id;   // (normal Attribute)
	var $book_id;   // (normal Attribute)
	var $book_mrp;   // (normal Attribute)
	var $book_discount;   // (normal Attribute)
	var $book_sale_amount;   // (normal Attribute)
	var $book_quantity;   // (normal Attribute)
	var $location_id;   // (normal Attribute)
	
	var $database; // Instance of class database
	
	
	// **********************
	// CONSTRUCTOR METHOD
	// **********************
	
	function tbl_sales_book_entry()
	{
	
	$this->database = new Database();
	
	}
	
	
	// **********************
	// GETTER METHODS
	// **********************
	
	
	function getsale_book_entry_id()
	{
	return $this->sale_book_entry_id;
	}
	
	function getsales_maste_id()
	{
	return $this->sales_maste_id;
	}
	
	function getbook_id()
	{
	return $this->book_id;
	}
	
	function getbook_mrp()
	{
	return $this->book_mrp;
	}
	
	function getbook_discount()
	{
	return $this->book_discount;
	}
	
	function getbook_sale_amount()
	{
	return $this->book_sale_amount;
	}
	
	function getbook_quantity()
	{
	return $this->book_quantity;
	}
	
	function getlocation_id()
	{
	return $this->location_id;
	}
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setsale_book_entry_id($val)
	{
	$this->sale_book_entry_id =  $val;
	}
	
	function setsales_maste_id($val)
	{
	$this->sales_maste_id =  $val;
	}
	
	function setbook_id($val)
	{
	$this->book_id =  $val;
	}
	
	function setbook_mrp($val)
	{
	$this->book_mrp =  $val;
	}
	
	function setbook_discount($val)
	{
	$this->book_discount =  $val;
	}
	
	function setbook_sale_amount($val)
	{
	$this->book_sale_amount =  $val;
	}
	
	function setbook_quantity($val)
	{
	$this->book_quantity =  $val;
	}
	
	function setlocation_id($val)
	{
	$this->location_id =  $val;
	}
	
	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function select($id)
	{
	
		$sql =  "SELECT * FROM tbl_sales_book_entry WHERE  `sale_book_entry_id`=". $id;
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);
		
		
		$this->sale_book_entry_id = $row->sale_book_entry_id;
		
		$this->sales_maste_id = $row->sales_maste_id;
		
		$this->book_id = $row->book_id;
		
		$this->book_mrp = $row->book_mrp;
		
		$this->book_discount = $row->book_discount;
		
		$this->book_sale_amount = $row->book_sale_amount;
		
		$this->book_quantity = $row->book_quantity;
		
		$this->location_id = $row->location_id;
	
	}
	
	// **********************
	// DELETE
	// **********************
	
	function delete($id)
	{
	$sql = "DELETE FROM tbl_sales_book_entry WHERE  = $id;";
	$result = $this->database->query($sql);
	
	}
	
	// **********************
	// INSERT
	// **********************
	
	function insert()
	{
		$sql = "INSERT INTO tbl_sales_book_entry (sales_maste_id,book_id,book_mrp,book_discount,book_sale_amount,book_quantity,location_id ) VALUES ('$this->sales_maste_id','$this->book_id','$this->book_mrp','$this->book_discount','$this->book_sale_amount','$this->book_quantity','$this->location_id' )";
		$result = $this->database->query($sql);
		return mysqli_insert_id($this->database->link);
	}
	
	// **********************
	// UPDATE
	// **********************
	
	function update($id)
	{
		$sql = " UPDATE tbl_sales_book_entry SET  sale_book_entry_id = '$this->sale_book_entry_id',sales_maste_id = '$this->sales_maste_id',book_id = '$this->book_id',book_mrp = '$this->book_mrp',book_discount = '$this->book_discount',book_sale_amount = '$this->book_sale_amount',book_quantity = '$this->book_quantity',location_id = '$this->location_id' WHERE  = $id ";
	
		$result = $this->database->query($sql);
	}
	

	
	
 	// **********************
	// SELECT All Sales Info
	// **********************
	
	function getSalesBookEntryList($condition=null)
	{
	
	try
		{
			 $sql =  "SELECT
							tse.sale_book_entry_id,
							tse.sales_maste_id,
							tse.book_id, 	
							tse.book_mrp,
							tse.book_discount, 	
							tse.book_sale_amount,
							tse.book_quantity, 	
							tse.location_id,
							tb.book_title
							                        
						FROM `tbl_sales_book_entry` tse
						LEFT JOIN `tbl_books` tb ON tb.book_id=tse.book_id
						
					 	WHERE ". $condition ;
                        
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			 	 	 	  	 	 	 	 	 	 	 	
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
					$arr[$count]['sale_book_entry_id']=$row->sale_book_entry_id;
					$arr[$count]['sales_maste_id']=$row->sales_maste_id;
					$arr[$count]['book_id']=$row->book_id;
					$arr[$count]['book_title']=$row->book_title;
					$arr[$count]['book_mrp']=$row->book_mrp;
					$arr[$count]['book_discount']=$row->book_discount;
					$arr[$count]['book_sale_amount']=$row->book_sale_amount;
					$arr[$count]['book_quantity']=$row->book_quantity;
					$arr[$count]['location_id']=$row->location_id;

				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}



} // class : end

?>
<!-- end of generated class -->
