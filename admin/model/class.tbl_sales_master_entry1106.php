
<!-- begin of generated class -->
<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_sales_master_entry
* GENERATION DATE:  30.04.2016
* CLASS FILE:       D:\wamp\www\svppl\class_generator/generated_classes/class.tbl_sales_master_entry.php
* FOR MYSQL TABLE:  tbl_sales_master_entry
* FOR MYSQL DB:     svppl
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_sales_master_entry
{ // class : begin

	
	// **********************
	// ATTRIBUTE DECLARATION
	// **********************
	
	var $sales_maste_id;   // (normal Attribute)
	var $sale_date;   // (normal Attribute)
	var $user_id;   // (normal Attribute)
	var $sales_type;   // (normal Attribute)
	var $tax_type;   // (normal Attribute)
	var $total_amount;   // (normal Attribute)
	var $final_amount;   // (normal Attribute)
	var $customer_name;   // (normal Attribute)
	var $cust_contact_no;   // (normal Attribute)
	var $cust_address;   // (normal Attribute)
	var $cust_email_id;   // (normal Attribute)
	var $cust_note;   // (normal Attribute)
	var $created_date;   // (normal Attribute)
	var $is_deleted;   // (normal Attribute)
	var $tax_amount;
	
	var $database; // Instance of class database
	
	
	// **********************
	// CONSTRUCTOR METHOD
	// **********************
	
	function tbl_sales_master_entry()
	{
	
	$this->database = new Database();
	
	}
	
	
	// **********************
	// GETTER METHODS
	// **********************
	
	
	function getsales_maste_id()
	{
	return $this->sales_maste_id;
	}
	
	function getsale_date()
	{
	return $this->sale_date;
	}
	
	function getuser_id()
	{
	return $this->user_id;
	}
	
	function getsales_type()
	{
	return $this->sales_type;
	}
	
	function gettax_type()
	{
	return $this->tax_type;
	}
	
	function gettotal_amount()
	{
	return $this->total_amount;
	}
	
	function getfinal_amount()
	{
	return $this->final_amount;
	}
	
	function getcustomer_name()
	{
	return $this->customer_name;
	}
	
	function getcust_contact_no()
	{
	return $this->cust_contact_no;
	}
	
	function getcust_address()
	{
	return $this->cust_address;
	}
	
	function getcust_email_id()
	{
	return $this->cust_email_id;
	}
	
	function getcust_note()
	{
	return $this->cust_note;
	}
	
	function getcreated_date()
	{
	return $this->created_date;
	}
	
	function getis_deleted()
	{
	return $this->is_deleted;
	}

	function gettax_amount()
	{
	return $this->tax_amount;
	}
	
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setsales_maste_id($val)
	{
	$this->sales_maste_id =  $val;
	}
	
	function setsale_date($val)
	{
	$this->sale_date =  $val;
	}
	
	function setuser_id($val)
	{
	$this->user_id =  $val;
	}
	
	function setsales_type($val)
	{
	$this->sales_type =  $val;
	}
	
	function settax_type($val)
	{
	$this->tax_type =  $val;
	}
	
	function settotal_amount($val)
	{
	$this->total_amount =  $val;
	}
	
	function setfinal_amount($val)
	{
	$this->final_amount =  $val;
	}
	
	function setcustomer_name($val)
	{
	$this->customer_name =  $val;
	}
	
	function setcust_contact_no($val)
	{
	$this->cust_contact_no =  $val;
	}
	
	function setcust_address($val)
	{
	$this->cust_address =  $val;
	}
	
	function setcust_email_id($val)
	{
	$this->cust_email_id =  $val;
	}
	
	function setcust_note($val)
	{
	$this->cust_note =  $val;
	}
	
	function setcreated_date($val)
	{
	$this->created_date =  $val;
	}
	
	function setis_deleted($val)
	{
	$this->is_deleted =  $val;
	}

	function settax_amount($val)
	{
	$this->tax_amount =  $val;
	}
	
	
	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function select($id)
	{
	
	$sql =  "SELECT * FROM tbl_sales_master_entry WHERE  = $id;";
	$result =  $this->database->query($sql);
	$result = $this->database->result;
	$row = mysqli_fetch_object($result);
	
	
	$this->sales_maste_id = $row->sales_maste_id;
	
	$this->sale_date = $row->sale_date;
	
	$this->user_id = $row->user_id;
	
	$this->sales_type = $row->sales_type;
	
	$this->tax_type = $row->tax_type;
	
	$this->total_amount = $row->total_amount;
	
	$this->final_amount = $row->final_amount;
	
	$this->customer_name = $row->customer_name;
	
	$this->cust_contact_no = $row->cust_contact_no;
	
	$this->cust_address = $row->cust_address;
	
	$this->cust_email_id = $row->cust_email_id;
	
	$this->cust_note = $row->cust_note;
	
	$this->created_date = $row->created_date;
	
	$this->is_deleted = $row->is_deleted;
	
	}
	
	// **********************
	// DELETE
	// **********************
	
	function delete($id)
	{
	$sql = "DELETE FROM tbl_sales_master_entry WHERE  = $id;";
	$result = $this->database->query($sql);
	
	}
	
	// **********************
	// INSERT
	// **********************
	
	function insert()
	{
            $sql = "INSERT INTO tbl_sales_master_entry ( sales_maste_id,sale_date,user_id,sales_type,tax_type,tax_amount,total_amount,final_amount,customer_name,cust_contact_no,cust_address,cust_email_id,cust_note,created_date,is_deleted ) VALUES ( '$this->sales_maste_id','$this->sale_date','$this->user_id','$this->sales_type','$this->tax_type','$this->tax_amount','$this->total_amount','$this->final_amount','$this->customer_name','$this->cust_contact_no','$this->cust_address','$this->cust_email_id','$this->cust_note','$this->created_date','$this->is_deleted' )";
            $result = $this->database->query($sql);
            return mysqli_insert_id($this->database->link);
	}
	
	// **********************
	// UPDATE
	// **********************
	
	function update($id)
	{
		
		$sql = " UPDATE tbl_sales_master_entry SET  sales_maste_id = '$this->sales_maste_id',sale_date = '$this->sale_date',user_id = '$this->user_id',sales_type = '$this->sales_type',tax_type = '$this->tax_type',total_amount = '$this->total_amount',final_amount = '$this->final_amount',customer_name = '$this->customer_name',cust_contact_no = '$this->cust_contact_no',cust_address = '$this->cust_address',cust_email_id = '$this->cust_email_id',cust_note = '$this->cust_note',created_date = '$this->created_date',is_deleted = '$this->is_deleted' WHERE  = $id ";
		
		$result = $this->database->query($sql);
		
	}


	// **********************
	// SELECT All Sales Info
	// **********************
	
	function getSalesList($condition=null)
	{
	
		try
		{
			$sql =  "SELECT
							*                                        
					FROM `tbl_sales_master_entry`
			
			
			WHERE 
			`is_deleted`='0' ". $condition." ORDER BY sales_maste_id DESC";
                        
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			 	 	 	  	 	 	 	 	 	 	 	
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
					$arr[$count]['sales_maste_id']=$row->sales_maste_id;
					$arr[$count]['sale_date']=$row->sale_date;
					$arr[$count]['user_id']=$row->user_id;
					
					$arr[$count]['sales_type']=$row->sales_type;
					$arr[$count]['tax_type']=$row->tax_type;
					$arr[$count]['tax_amount']=$row->tax_amount;
					$arr[$count]['total_amount']=$row->total_amount;
					$arr[$count]['final_amount']=$row->final_amount;
					$arr[$count]['customer_name']=$row->customer_name;
					$arr[$count]['cust_contact_no']=$row->cust_contact_no;
					$arr[$count]['cust_address']=$row->cust_address;
					$arr[$count]['cust_email_id']=$row->cust_email_id;
					$arr[$count]['cust_note']=$row->cust_note;
					$arr[$count]['created_date']=$row->created_date;
					$arr[$count]['is_deleted']=$row->is_deleted;
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

        
	// **********************
	// Get Sales Master Detail
	// **********************
	
	function getSalesEntryMasterDetail($condition=null)
	{
            try
	    {
                $sql =  "SELECT
                            sm.sales_maste_id,
                            sm.sale_date,
                            sm.user_id,
                            sm.sales_type,
                            sm.tax_type,
                            sm.tax_amount,
                            sm.total_amount,
                            sm.final_amount,
                            sm.customer_name,
                            sm.cust_contact_no,
                            sm.cust_address,
                            sm.cust_email_id,
                            sm.cust_note,
                            sm.created_date,
                            tu.name as employee_name
                            
                        FROM `tbl_sales_master_entry` sm
                            LEFT JOIN `tbl_users` tu ON sm.user_id = tu.user_id
                        WHERE ". $condition;
                        
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
					$arr[$count]['sales_maste_id']=$row->sales_maste_id;
					$arr[$count]['sale_date']=$row->sale_date;
					$arr[$count]['user_id']=$row->user_id;
					$arr[$count]['sales_type']=$row->sales_type;
					$arr[$count]['tax_type']=$row->tax_type;
					$arr[$count]['tax_amount']=$row->tax_amount;
					$arr[$count]['total_amount']=$row->total_amount;
					$arr[$count]['final_amount']=$row->final_amount;
					$arr[$count]['customer_name']=$row->customer_name;
					$arr[$count]['cust_contact_no']=$row->cust_contact_no;
					$arr[$count]['cust_address']=$row->cust_address;
					$arr[$count]['cust_email_id']=$row->cust_email_id;
					$arr[$count]['cust_note']=$row->cust_note;
					$arr[$count]['created_date']=$row->created_date;
                                        $arr[$count]['employee_name']=$row->employee_name;
                                        
                                    
				}
			
				return $arr;
			}
		}
            catch(Exception $e)
            {
                    throw $e;
            }
	}
        



	// **********************
	// Generate Sales Report
	// **********************
	
	function getSalesReport($condition=null)
	{
	
		try
		{
			$sql =  "SELECT
						 tsm.sales_maste_id, 	
						 tsm.sale_date, 	
						 tsm.user_id, 	
						 tsm.sales_type,
						 tsm.tax_type, 
						 tsm.total_amount,
						 tsm.final_amount, 
						 tsm.customer_name,
						 tsm.cust_contact_no,
						 tsm.cust_address,
						 tsm.cust_email_id,
						 tsm.cust_note,
						 tsm.created_date,
						 tsm.is_deleted,
						 tse.book_id,  
						 tse.book_quantity,
						 tse.book_sale_amount,
						 tse.location_id,
						 tb.book_title,
						 tsl.location_addr                                      
					FROM 
						`tbl_sales_master_entry` tsm
					LEFT JOIN `tbl_sales_book_entry` tse ON tsm.sales_maste_id = tse.sales_maste_id
					LEFT JOIN `tbl_books` tb ON tb.book_id = tse.book_id
					LEFT JOIN `tbl_storage_locations` tsl ON tsl.location_id = tse.location_id
						
					WHERE 
						tsm.`is_deleted`='0' ".$condition ." ORDER BY tsm.`sale_date` DESC";
                        
							
				$result =  $this->database->query($sql);
				$result = $this->database->result;
				
				if((!$result) || (mysqli_num_rows($result) == 0))
				{
					return array();
				}
				else
				{
															
				
					for($count = 0; $row = mysqli_fetch_object($result); $count ++)
					{
						$arr[$count]['sales_maste_id']=$row->sales_maste_id;
						$arr[$count]['sale_date']=$row->sale_date;
						$arr[$count]['user_id']=$row->user_id;
						
						$arr[$count]['sales_type']=$row->sales_type;
						$arr[$count]['tax_type']=$row->tax_type;
						$arr[$count]['tax_amount']=$row->tax_amount;
						$arr[$count]['total_amount']=$row->total_amount;
						$arr[$count]['final_amount']=$row->final_amount;
						$arr[$count]['customer_name']=$row->customer_name;
						$arr[$count]['cust_contact_no']=$row->cust_contact_no;
						$arr[$count]['cust_address']=$row->cust_address;
						$arr[$count]['cust_email_id']=$row->cust_email_id;
						$arr[$count]['cust_note']=$row->cust_note;
						$arr[$count]['created_date']=$row->created_date;
						$arr[$count]['book_id']=$row->book_id;
						$arr[$count]['book_quantity']=$row->book_quantity;
						$arr[$count]['book_sale_amount']= $row->book_sale_amount;
						$arr[$count]['location_id']= $row->location_id;
						$arr[$count]['book_title']= $row->book_title;
						$arr[$count]['location_addr']= $row->location_addr;
						
						
					}
				
					return $arr;
				}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}



	

} // class : end

?>
<!-- end of generated class -->
