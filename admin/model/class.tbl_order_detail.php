<?php


include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_order_detail
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************



var $order_detail_id;   // (normal Attribute)
var $order_id;   // (normal Attribute)
var $book_id;   // (normal Attribute)
var $order_quantity;   // (normal Attribute)
var $book_price;   // (normal Attribute)
var $book_total_cost;   // (normal Attribute)
var $book_unit;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_order_detail()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getorder_detail_id()
{
return $this->order_detail_id;
}

function getorder_id()
{
return $this->order_id;
}

function getbook_id()
{
return $this->book_id;
}

function getorder_quantity()
{
return $this->order_quantity;
}

function getbook_price()
{
return $this->book_price;
}

function getbook_total_cost()
{
return $this->book_total_cost;
}

function getbook_unit()
{
return $this->book_unit;
}

function getis_deleted()
{
return $this->is_deleted;
}

// **********************
// SETTER METHODS
// **********************


function setorder_detail_id($val)
{
$this->order_detail_id =  $val;
}

function setorder_id($val)
{
$this->order_id =  $val;
}

function setbook_id($val)
{
$this->book_id =  $val;
}

function setorder_quantity($val)
{
$this->order_quantity =  $val;
}

function setbook_price($val)
{
$this->book_price =  $val;
}

function setbook_total_cost($val)
{
$this->book_total_cost =  $val;
}

function setbook_unit($val)
{
$this->book_unit =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_order_detail WHERE  order_detail_id= $id;";
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->order_detail_id = $row->order_detail_id;

$this->order_id = $row->order_id;

$this->book_id = $row->book_id;

$this->order_quantity = $row->order_quantity;

$this->book_price = $row->book_price;

$this->book_total_cost = $row->book_total_cost;

$this->book_unit = $row->book_unit;

$this->is_deleted = $row->is_deleted;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_order_detail WHERE  order_detail_id= $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{


$sql = "INSERT INTO tbl_order_detail (order_id,book_id,order_quantity,book_price,book_total_cost ) VALUES ( '$this->order_id','$this->book_id','$this->order_quantity','$this->book_price','$this->book_total_cost' )";
$result = $this->database->query($sql);
return $this->database->last_insertid;

}

// **********************
// UPDATE
// **********************

function update($id)
{



$sql = " UPDATE tbl_order_detail SET  order_id = '$this->order_id',book_id = '$this->book_id',order_quantity = '$this->order_quantity',book_price = '$this->book_price',book_total_cost = '$this->book_total_cost',book_unit = '$this->book_unit',is_deleted = '$this->is_deleted' WHERE  order_detail_id= $id ";

$result = $this->database->query($sql);



}


// **********************
// SELECT selectAllOrders
// **********************

function selectOrdersDetail($id)
{
		
		 $sql = "SELECT 
                        ot.order_id,
                        ot.customer_name,
                        ot.order_total_amount,
                        ot.discount,
                        ot.final_amount_paid,
                        ot.delivery_charges,
                        ot.order_date,
                        ot.order_status,
                        ot.payment_status,
                        ot.contact_no,
                        ot.customer_email,
                        ot.address_line1,
                        ot.address_line2,
                        ot.address_line3,
                        ot.landmark,
                        ot.town,
                        ot.taluka,
                        ot.district,
                        ot.pin_code,
                        ot.postage_charges,
                        ot.discount_amount,
                        ot.invoice_no



                       
			
		  FROM tbl_order ot 
		  
		WHERE  ot.is_deleted= '0' AND ot.order_id='$id'";
                     
                  

        $result =  $this->database->query($sql);
        $result = $this->database->result;
       
	   $arr=array();

        for($count = 0; $row = mysqli_fetch_object($result); $count ++)
			{
		
                 $arr[$count]['customer_name'] 			= $row->customer_name;
                 

                 $arr[$count]['address_line1']      = $row->address_line1;
                  $arr[$count]['address_line2']       = $row->address_line2;
                 $arr[$count]['address_line3']      = $row->address_line3;
                

                  $arr[$count]['mobile_no'] 			= $row->contact_no;
				  $arr[$count]['email_address']		    = $row->customer_email;
                  $arr[$count]['flat_plot_no'] 			= $row->address_line1;
                  $arr[$count]['building_name'] 		= $row->address_line2;
                  $arr[$count]['landmark'] 				= $row->landmark;
                  $arr[$count]['town'] 			= $row->town;
                  $arr[$count]['taluka']			    = $row->taluka;
                  $arr[$count]['district']			    = $row->district;
                  $arr[$count]['pin_code']			    = $row->pin_code;
                  $arr[$count]['postage_charges']			    = $row->postage_charges;
		
				  $arr[$count]['order_total_amount'] 	= $row->order_total_amount;
                  $arr[$count]['discount'] 				= $row->discount;
                  $arr[$count]['final_amount_paid'] 	= $row->final_amount_paid;
                  $arr[$count]['delivery_charges'] 	= $row->delivery_charges;
                  $arr[$count]['order_date'] 			= $row->order_date;
                  $arr[$count]['order_status'] 			= $row->order_status;
                  $arr[$count]['payment_status'] 		= $row->payment_status;
		
                  $arr[$count]['order_id'] 				= $row->order_id;

                  $arr[$count]['discount_amount']        = $row->discount_amount;

                  $arr[$count]['invoice_no']        = "W".sprintf("%04s", $row->invoice_no); 
                  
				  
                 
        }
		
		 return $arr;
        

}

   // **********************
    // SELECT selectOrdersProductDetail
    // **********************

    function selectOrdersProductDetail($id)
    {
			
                 $sql="SELECT
                           tod.`order_detail_id`, 
                           tod.`order_id`, 
                           tod.`book_id`, 
                           tod.`order_quantity`, 

                           tod.`book_total_cost`, 
                           
                           tp.`book_title`,
                           tp.`isbn_no`,
                                                     
                           


                          (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tod.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,                           
                           
                           tp.`book_mrp`

                     FROM 
                           `tbl_order_detail` tod
                            LEFT JOIN `tbl_books` tp ON tp.book_id=tod.`book_id`  
                     WHERE 
                           tod.`order_id` ='".$id."'"; 

                    $result =  $this->database->query($sql);
                    $result = $this->database->result;
           
		   $arr=array();

            for($count = 0; $row = mysqli_fetch_object($result); $count ++)
            {
                     

                $arr[$count]['order_status'] = $row->order_status;
                $arr[$count]['payment_status'] = $row->payment_status;

                $arr[$count]['order_detail_id'] = $row->order_detail_id;
                $arr[$count]['order_id'] = $row->order_id;
                $arr[$count]['product_id'] = $row->book_id;
                $arr[$count]['product_title'] = $row->book_title;
                $arr[$count]['product_image'] = $row->book_thumbnail;
                $arr[$count]['product_quantity'] = $row->order_quantity;
               
                $arr[$count]['product_price'] = $row->book_mrp;
                $arr[$count]['product_total_cost'] = $row->book_total_cost;
               
                 $arr[$count]['product_rate'] = $row->book_mrp;
                 $arr[$count]['discount_amount'] = $row->discount_amount;

                 $arr[$count]['hsn_code'] = $row->isbn_no;
            }
			
			 return $arr;
            

    }
    
	// **********************
	// Complete Orders
	// **********************

	function CompleteOrders($id)
	{

		$sql = " UPDATE tbl_order SET   order_status='1' WHERE  order_id= $id ";

		$result = $this->database->query($sql);

	}


} // class : end

?>