<?php


include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_books
{ // class : begin

// **********************
// ATTRIBUTE DECLARATION
// **********************
var $is_book_visible;
var $book_id;   // (normal Attribute)
var $book_title;   // (normal Attribute)
var $book_title_mr;   // (normal Attribute)
var $book_language;   // (normal Attribute)
var $author_id;   // (normal Attribute)
var $book_mrp;   // (normal Attribute)
var $compile_by;   // (normal Attribute)
var $translated_by;   // (normal Attribute)
var $book_edition;   // (normal Attribute)
var $book_description;   // (normal Attribute)
var $book_thumbnail;   // (normal Attribute)
var $publisher_id;   // (normal Attribute)
var $isbn_no;   // (normal Attribute)
var $category_id;   // (normal Attribute)
var $dim_width;   // (normal Attribute)
var $dim_height;   // (normal Attribute)
var $dim_depth;   // (normal Attribute)
var $no_of_pages;   // (normal Attribute)
var $binding_id;   // (normal Attribute)
var $cover_id;   // (normal Attribute)
var $book_weight;   // (normal Attribute)
var $production_cost;   // (normal Attribute)
var $is_translated;   // (normal Attribute)
var $translated_from_book;   // (normal Attribute)
var $translated_frm_lang;   // (normal Attribute)
var $publication_date;   // (normal Attribute)
var $co_author;   // (normal Attribute)
var $contributor;   // (normal Attribute)
var $editor;   // (normal Attribute)
var $copyright;   // (normal Attribute)
var $is_upcoming_book;   // (normal Attribute)
var $created_date;   // (normal Attribute)
var $created_by;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_books()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************

function getis_book_visible()
{
return $this->is_book_visible;
}

function getbook_id()
{
return $this->book_id;
}

function getbook_title()
{
return $this->book_title;
}

function getbook_title_mr()
{
return $this->book_title_mr;
}

function getbook_language()
{
return $this->book_language;
}

function getauthor_id()
{
return $this->author_id;
}

function getbook_mrp()
{
return $this->book_mrp;
}

function getcompile_by()
{
return $this->compile_by;
}

function gettranslated_by()
{
return $this->translated_by;
}

function getbook_edition()
{
return $this->book_edition;
}


function getbook_description()
{
return $this->book_description;
}

function getbook_thumbnail()
{
return $this->book_thumbnail;
}

function getpublisher_id()
{
return $this->publisher_id;
}

function getisbn_no()
{
return $this->isbn_no;
}

function getcategory_id()
{
return $this->category_id;
}

function getdim_width()
{
return $this->dim_width;
}

function getdim_height()
{
return $this->dim_height;
}

function getdim_depth()
{
return $this->dim_depth;
}

function getno_of_pages()
{
return $this->no_of_pages;
}

function getbinding_id()
{
return $this->binding_id;
}

function getcover_id()
{
return $this->cover_id;
}

function getbook_weight()
{
return $this->book_weight;
}

function getproduction_cost()
{
return $this->production_cost;
}

function getis_translated()
{
return $this->is_translated;
}

function gettranslated_from_book()
{
return $this->translated_from_book;
}

function gettranslated_frm_lang()
{
return $this->translated_frm_lang;
}

function getpublication_date()
{
return $this->publication_date;
}

function getco_author()
{
return $this->co_author;
}

function getcontributor()
{
return $this->contributor;
}

function geteditor()
{
return $this->editor;
}

function getcopyright()
{
return $this->copyright;
}

function getis_upcoming_book()
{
return $this->is_upcoming_book;
}


function getcreated_date()
{
return $this->created_date;
}

function getcreated_by()
{
return $this->created_by;
}

function getis_deleted()
{
return $this->is_deleted;
}

// **********************
// SETTER METHODS
// **********************

function setis_book_visible($val)
{
$this->is_book_visible =  $val;
}

function setbook_id($val)
{
$this->book_id =  $val;
}

function setbook_title($val)
{
$this->book_title =  $val;
}


function setbook_title_mr($val)
{
$this->book_title_mr =  $val;
}

function setbook_language($val)
{
$this->book_language =  $val;
}

function setauthor_id($val)
{
$this->author_id =  $val;
}

function setbook_mrp($val)
{
$this->book_mrp =  $val;
}

function setcompile_by($val)
{
$this->compile_by =  $val;
}

function settranslated_by($val)
{
$this->translated_by =  $val;
}

function setbook_edition($val)
{
$this->book_edition =  $val;
}

function setbook_description($val)
{
$this->book_description =  $val;
}

function setbook_thumbnail($val)
{
$this->book_thumbnail =  $val;
}

function setpublisher_id($val)
{
$this->publisher_id =  $val;
}

function setisbn_no($val)
{
$this->isbn_no =  $val;
}

function setcategory_id($val)
{
$this->category_id =  $val;
}

function setdim_width($val)
{
$this->dim_width =  $val;
}

function setdim_height($val)
{
$this->dim_height =  $val;
}

function setdim_depth($val)
{
$this->dim_depth =  $val;
}

function setno_of_pages($val)
{
$this->no_of_pages =  $val;
}

function setbinding_id($val)
{
$this->binding_id =  $val;
}

function setcover_id($val)
{
$this->cover_id =  $val;
}

function setbook_weight($val)
{
$this->book_weight =  $val;
}

function setproduction_cost($val)
{
$this->production_cost =  $val;
}

function setis_translated($val)
{
$this->is_translated =  $val;
}

function settranslated_from_book($val)
{
$this->translated_from_book =  $val;
}

function settranslated_frm_lang($val)
{
$this->translated_frm_lang =  $val;
}

function setpublication_date($val)
{
$this->publication_date =  $val;
}

function setco_author($val)
{
$this->co_author =  $val;
}

function setcontributor($val)
{
$this->contributor =  $val;
}

function seteditor($val)
{
$this->editor =  $val;
}

function setcopyright($val)
{
$this->copyright =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}


function setis_upcoming_book($val)
{
$this->is_upcoming_book =  $val;
}

function setcreated_by($val)
{
$this->created_by =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}







    // **********************
    // SELECT METHOD / LOAD
    // **********************
    
    function select($id)
    {
    
        $sql =  "SELECT * FROM tbl_books WHERE  = $id;";
        $result =  $this->database->query($sql);
        $result = $this->database->result;
        $row = mysqli_fetch_object($result);
        
        
        $this->book_id = $row->book_id;
        
        $this->book_title = $row->book_title;
        
        $this->book_title_mr = $row->book_title_mr;
        
        $this->book_language = $row->book_language;
        
        $this->author_id = $row->author_id;
        
        $this->book_mrp = $row->book_mrp;
        
        $this->compile_by = $row->compile_by;
        
        $this->translated_by = $row->translated_by;
        
        $this->book_edition = $row->book_edition;
        
        $this->book_description = $row->book_description;
        
        $this->book_thumbnail = $row->book_thumbnail;
        
        $this->publisher_id = $row->publisher_id;
        
        $this->isbn_no = $row->isbn_no;
        
        $this->category_id = $row->category_id;
        
        $this->dim_width = $row->dim_width;
        
        $this->dim_height = $row->dim_height;
        
        $this->dim_depth = $row->dim_depth;
        
        $this->no_of_pages = $row->no_of_pages;
        
        $this->binding_id = $row->binding_id;
        
        $this->cover_id = $row->cover_id;
        
        $this->book_weight = $row->book_weight;
        
        $this->production_cost = $row->production_cost;
        
        $this->is_translated = $row->is_translated;
        
        $this->translated_from_book = $row->translated_from_book;
        
        $this->translated_frm_lang = $row->translated_frm_lang;
        
        $this->publication_date = $row->publication_date;
        
        $this->co_author = $row->co_author;
        
        $this->contributor = $row->contributor;
        
        $this->editor = $row->editor;
        
        $this->copyright = $row->copyright;
        
        $this->is_upcoming_book = $row->is_upcoming_book;
        
        $this->created_date = $row->created_date;
        
        $this->created_by = $row->created_by;
        
        $this->is_deleted = $row->is_deleted;
        $this->is_book_visible = $row->is_book_visible;
    
    }







    
    
    // **********************
    // SELECT Single Book Info
    // **********************
    
    function selectSingleBook($id)
    {
     
        try
        {
         $sql =  "SELECT
                tb.`book_id`,
                tb.`book_title`,
                tb.`book_title_mr`,
                tb.`book_language`,
                tb.`author_id`,
                tb.`book_mrp`,
                tb.`compile_by`,
                tb.`book_edition`,
                (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                tb.`publisher_id`,
                tb.`isbn_no`,
                tb.`publication_date`,
                tb.`isbn_no`,
                ta.name,
                tb.translated_by,
                tb.book_edition,
                tb.book_description,
                tb.publisher_id,
                tb.isbn_no,
                tb.category_id,
                tb.dim_width,
                tb.dim_height,
                tb.dim_depth,
                tb.no_of_pages,
                tb.binding_id,
                tb.cover_id,
                tb.book_weight,
                tb.production_cost,
                tb.is_translated,
                tb.translated_from_book,
                tb.translated_frm_lang,
                tb.publication_date,
                tb.co_author,
                tb.contributor,
                tb.editor,
                tb.copyright,
                tb.is_upcoming_book,
                tb.created_date,
                tb.created_by,
                tp.name as pub_name , 
                tp.address as pub_addr , 
                tp.contact_no as pub_contact , 
                tp.email_id as pub_email , 
                tp.website as pub_website , 
                bt.title as binding_type,
                ct.title as cover_type,
                bc.category_title,
                tb.is_book_visible,
               
                (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                
            FROM `tbl_books` tb
            
            LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
            LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
            
            LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
            LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
            
            LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

            
            
            WHERE 
            tb.`is_deleted`='0' AND tb.`book_id`='$id' ";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;

                                    $arr[$count]['binding_type']=$row->binding_type;
                                    $arr[$count]['cover_type']=$row->cover_type;

                                    $arr[$count]['is_book_visible']=$row->is_book_visible;

                                   
                                    

                                    
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    





    
    
    // **********************
    // SELECT All Book Info getSearchBookList Count record
    // **********************
    
    function getSearchBookListCount($condition=null,$start=null,$per_page=null)
    {
     

    
        try
        {
          $sql =  "SELECT
                tb.`book_id`,
                tb.`book_title`,
                tb.`book_title_mr`,
                tb.`book_language`,
                tb.`author_id`,
                tb.`book_mrp`,
                tb.`compile_by`,
                tb.`book_edition`,
                (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                tb.`publisher_id`,
                tb.`isbn_no`,
                tb.`publication_date`,
                tb.`isbn_no`,
                ta.name,
                tb.translated_by,
                tb.book_edition,
                tb.book_description,
                
                tb.publisher_id,
                tb.isbn_no,
                tb.category_id,
                tb.dim_width,
                tb.dim_height,
                tb.dim_depth,
                tb.no_of_pages,
                tb.binding_id,
                tb.cover_id,
                tb.book_weight,
                tb.production_cost,
                tb.is_translated,
                tb.translated_from_book,
                tb.translated_frm_lang,
                tb.publication_date,
                tb.co_author,
                tb.contributor,
                tb.editor,
                tb.copyright,
                tb.is_upcoming_book,
                tb.created_date,
                tb.created_by,
                tp.name as pub_name , 
                tp.address as pub_addr , 
                tp.contact_no as pub_contact , 
                tp.email_id as pub_email , 
                tp.website as pub_website , 
                bt.title as binding_type,
                ct.title as cover_type,
                bc.category_title,
                tb.is_book_visible,
               
                (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                
            FROM `tbl_books` tb
            
            LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
            LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
            
            LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
            LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
            
            LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

            
            
            WHERE 
            tb.is_deleted='0' ". $condition." ORDER BY tb.book_id DESC";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            return mysqli_num_rows($result);
            
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }

    
    
    
    
    
    
    
// **********************
    // SELECT All Book Info getSearchBookList
    // **********************
    
    function getSearchBookList($condition=null,$start=null,$per_page=null)
    {
     if($start!=null || $per_page!=null)
            $limit="limit $start,$per_page";
            else
            $limit=null;

    
        try
        {
                                  $sql =  "SELECT
                                        tb.`book_id`,
                                        tb.`book_title`,
                                        tb.`book_title_mr`,
                                        tb.`book_language`,
                                        tb.`author_id`,
                                        tb.`book_mrp`,
                                        tb.`compile_by`,
                                        tb.`book_edition`,
                                        (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                                        tb.`publisher_id`,
                                        tb.`isbn_no`,
                                        tb.`publication_date`,
                                        tb.`isbn_no`,
                                        ta.name,
                                        tb.translated_by,
                                        tb.book_edition,
                                        tb.book_description,
                                        
                                        tb.publisher_id,
                                        tb.isbn_no,
                                        tb.category_id,
                                        tb.dim_width,
                                        tb.dim_height,
                                        tb.dim_depth,
                                        tb.no_of_pages,
                                        tb.binding_id,
                                        tb.cover_id,
                                        tb.book_weight,
                                        tb.production_cost,
                                        tb.is_translated,
                                        tb.translated_from_book,
                                        tb.translated_frm_lang,
                                        tb.publication_date,
                                        tb.co_author,
                                        tb.contributor,
                                        tb.editor,
                                        tb.copyright,
                                        tb.is_upcoming_book,
                                        tb.created_date,
                                        tb.created_by,
                                        tp.name as pub_name , 
                                        tp.address as pub_addr , 
                                        tp.contact_no as pub_contact , 
                                        tp.email_id as pub_email , 
                                        tp.website as pub_website , 
                                        bt.title as binding_type,
                                        ct.title as cover_type,
                                        bc.category_title,
                                        tb.is_book_visible,
                                       
                                        (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                                        
                                    FROM `tbl_books` tb
                                    
                                    LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                                    LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                                    
                                    LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                                    LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
                                    
                                    LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

                                    
                                    
                                    WHERE 
                                    tb.is_deleted='0' ". $condition." ORDER BY tb.book_id DESC $limit";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;

                                    $arr[$count]['binding_type']=$row->binding_type;
                                    $arr[$count]['cover_type']=$row->cover_type;

                                    $arr[$count]['is_book_visible']=$row->is_book_visible;

                                    
                                    

                                    
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }



    
    
    
    
    


    // **********************
    // SELECT All Book Info 
    // **********************
    
    function getBookList($start=null,$per_page=null)
    {
    $condition="";
     if($start!=null || $per_page!=null)
            $limit="limit $start,$per_page";
            else
            $limit=null;
            
// Search Books According Book Title or ISBN Author Name
            
    if($_REQUEST['book_title']!="" ) {
        $book_title = trim($_REQUEST['book_title']);
        $condition.=" AND (tb.book_title LIKE '%$book_title%')";
    }
    
    if($_REQUEST['isbn_no']!="" ) {
        
        $isbn_no         = trim($_REQUEST['isbn_no']);
        
        $condition.=" AND  (tb.isbn_no LIKE '%$isbn_no%' )";
    }

    if($_REQUEST['author_name']!="") {
        
        $author_name         = trim($_REQUEST['author_name']);
        $condition.=" AND (ta.name LIKE '%$author_name%')";
    }
    
    
             // $condition.="AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
    
        try
        {
           $sql =  "SELECT
                tb.`book_id`,
                tb.`book_title`,
                tb.`book_title_mr`,
                tb.`book_language`,
                tb.`author_id`,
                tb.`book_mrp`,
                tb.`compile_by`,
                tb.`book_edition`,
                (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                tb.`publisher_id`,
                tb.`isbn_no`,
                tb.`publication_date`,
                tb.`isbn_no`,
                ta.name,
                tb.translated_by,
                tb.book_edition,
                tb.book_description,
                
                tb.publisher_id,
                tb.isbn_no,
                tb.category_id,
                tb.dim_width,
                tb.dim_height,
                tb.dim_depth,
                tb.no_of_pages,
                tb.binding_id,
                tb.cover_id,
                tb.book_weight,
                tb.production_cost,
                tb.is_translated,
                tb.translated_from_book,
                tb.translated_frm_lang,
                tb.publication_date,
                tb.co_author,
                tb.contributor,
                tb.editor,
                tb.copyright,
                tb.is_upcoming_book,
                tb.created_date,
                tb.created_by,
                tp.name as pub_name , 
                tp.address as pub_addr , 
                tp.contact_no as pub_contact , 
                tp.email_id as pub_email , 
                tp.website as pub_website , 
                bt.title as binding_type,
                ct.title as cover_type,
                bc.category_title,
                tb.is_book_visible,
               
                (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity 
                
            FROM `tbl_books` tb
            
            LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
            LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
            
            LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
            LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
            
            LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

            
            
            WHERE 
            tb.is_deleted='0' $condition  ORDER BY tb.book_id DESC  $limit";
                         
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;

                                    $arr[$count]['binding_type']=$row->binding_type;
                                    $arr[$count]['cover_type']=$row->cover_type;

                                    $arr[$count]['is_book_visible']=$row->is_book_visible;

                                    
                                    

                                    
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }



    
    
    
    
    
    
    // **********************
    // Count All Books
    // **********************
    
    function CountBooks($condition=null)
    {
    
    
        try
        {
            
            
            if($_REQUEST['book_title']!="" ) {
                $book_title = trim($_REQUEST['book_title']);
                $condition.=" AND (tb.book_title LIKE '%$book_title%')";
            }
            
            if($_REQUEST['isbn_no']!="" ) {
                
                $isbn_no         = trim($_REQUEST['isbn_no']);
                
                $condition.=" AND  (tb.isbn_no LIKE '%$isbn_no%' )";
            }

            if($_REQUEST['author_name']!="") {
                
                $author_name         = trim($_REQUEST['author_name']);
                $condition.=" AND (ta.name LIKE '%$author_name%')";
            }
            
            
                                 $sql =  "SELECT
                                        tb.`book_id`,
                                        tb.`book_title`,
                                        tb.`book_title_mr`,
                                        tb.`book_language`,
                                        tb.`author_id`,
                                        tb.`book_mrp`,
                                        tb.`compile_by`,
                                        tb.`book_edition`,
                                        (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                                        tb.`publisher_id`,
                                        tb.`isbn_no`,
                                        tb.`publication_date`,
                                        tb.`isbn_no`,
                                        ta.name,
                                        tb.translated_by,
                                        tb.book_edition,
                                        tb.book_description,
                                        
                                        tb.publisher_id,
                                        tb.isbn_no,
                                        tb.category_id,
                                        tb.dim_width,
                                        tb.dim_height,
                                        tb.dim_depth,
                                        tb.no_of_pages,
                                        tb.binding_id,
                                        tb.cover_id,
                                        tb.book_weight,
                                        tb.production_cost,
                                        tb.is_translated,
                                        tb.translated_from_book,
                                        tb.translated_frm_lang,
                                        tb.publication_date,
                                        tb.co_author,
                                        tb.contributor,
                                        tb.editor,
                                        tb.copyright,
                                        tb.is_upcoming_book,
                                        tb.created_date,
                                        tb.created_by,
                                        tp.name as pub_name , 
                                        tp.address as pub_addr , 
                                        tp.contact_no as pub_contact , 
                                        tp.email_id as pub_email , 
                                        tp.website as pub_website , 
                                        bt.title as binding_type,
                                        ct.title as cover_type,
                                        bc.category_title,
                                        tb.is_book_visible,
                                       
                                        (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                                        
                                    FROM `tbl_books` tb
                                    
                                    LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                                    LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                                    
                                    LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                                    LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
                                    
                                    LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

                                    
                                    
                                    WHERE 
                                    tb.`is_deleted`='0' ". $condition." ORDER BY tb.book_id DESC";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            return mysqli_num_rows($result);
            
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }


    
    
    


        
    // **********************
    // Get Book Detail
    // **********************
    
    function getBookDetail($condition=null)
    {
            try
        {
                $sql =  "SELECT
                            tb.`book_id`,
                            tb.`book_title`,
                            tb.`book_title_mr`,
                           
                            tb.`author_id`,
                            tb.`book_mrp`,
                            tb.`compile_by`,
                            tb.`book_edition`,
                            (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                            tb.`publisher_id`,
                            tb.`publication_date`,
                            tb.`isbn_no`,
                            ta.name,
                            tb.translated_by,
                            tb.book_edition,
                            tb.book_description,
                            
                            tb.publisher_id,
                            tb.isbn_no,
                            tb.category_id,
                            tb.dim_width,
                            tb.dim_height,
                            tb.dim_depth,
                            tb.no_of_pages,
                            tb.binding_id,
                            tb.cover_id,
                            tb.book_weight,
                            tb.production_cost,
                            tb.is_translated,
                            tb.translated_from_book,
                            tb.translated_frm_lang,
                            tb.publication_date,
                            tb.co_author,
                            tb.contributor,
                            tb.editor,
                            tb.copyright,
                            tb.is_upcoming_book,
                            tb.created_date,
                            tb.created_by,
                            tp.name as pub_name , 
                            tp.address as pub_addr , 
                            tp.contact_no as pub_contact , 
                            tp.email_id as pub_email , 
                            tp.website as pub_website , 
                            bt.title as binding_type,
                            ct.title as cover_type,
                            bc.category_title,
                            tl.title as book_language,
                            tb.book_language sel_language_id,
                            tb.related_books,
                            tb.is_book_visible,

                            (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity

                        FROM `tbl_books` tb

                            LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                            LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id

                            LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                            LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id

                            LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id
                            
                            LEFT JOIN `tbl_languages` tl ON tl.language_id = tb.`book_language`
                        
                        WHERE 
                            tb.`is_deleted`='0' ". $condition." ORDER BY tb.book_id DESC";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                     $arr[$count]['category_id']=$row->category_id;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;
                                    
                                    $arr[$count]['binding_id']=$row->binding_id;
                                    $arr[$count]['binding_type']=$row->binding_type;
                                    
                                     $arr[$count]['cover_id']=$row->cover_id;
                                    $arr[$count]['cover_type']=$row->cover_type;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['sel_language_id']=$row->sel_language_id;
                                    $arr[$count]['related_books']=$row->related_books;

                                    $arr[$count]['is_book_visible']  =$row->is_book_visible;
                                    
                                    

                                    
                }
            
                return $arr;
            }
        }
            catch(Exception $e)
            {
                    throw $e;
            }
    }
        
        
        
    // **********************
    // Get Book Detail
    // **********************
    
    function getFroentBookDetail($condition=null)
    {
            try
        {
                 $sql =  "SELECT
                            tb.`book_id`,
                            tb.`book_title`,
                            tb.`book_title_mr`,
                           
                            tb.`author_id`,
                            tb.`book_mrp`,
                            tb.`compile_by`,
                            tb.`book_edition`,
                            (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                            tb.`publisher_id`,
                            tb.`publication_date`,
                            tb.`isbn_no`,
                            ta.name,
                            tb.translated_by,
                            tb.book_edition,
                            tb.book_description,
                            
                            tb.publisher_id,
                            tb.isbn_no,
                            tb.category_id,
                            tb.dim_width,
                            tb.dim_height,
                            tb.dim_depth,
                            tb.no_of_pages,
                            tb.binding_id,
                            tb.cover_id,
                            tb.book_weight,
                            tb.production_cost,
                            tb.is_translated,
                            tb.translated_from_book,
                            tb.translated_frm_lang,
                            tb.publication_date,
                            tb.co_author,
                            tb.contributor,
                            tb.editor,
                            tb.copyright,
                            tb.is_upcoming_book,
                            tb.created_date,
                            tb.created_by,
                            tp.name as pub_name , 
                            tp.address as pub_addr , 
                            tp.contact_no as pub_contact , 
                            tp.email_id as pub_email , 
                            tp.website as pub_website , 
                            bt.title as binding_type,
                            ct.title as cover_type,
                            bc.category_title,
                            tl.title as book_language,
                            tb.book_language sel_language_id,
                            tb.related_books,
                            tb.is_book_visible,
                            (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity


                        FROM `tbl_books` tb

                            LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                            LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id

                            LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                            LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id

                            LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id
                            
                            LEFT JOIN `tbl_languages` tl ON tl.language_id = tb.`book_language`
                        
                        WHERE 
                            tb.`is_deleted`='0' ". $condition." ORDER BY tb.book_id DESC";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                     $arr[$count]['category_id']=$row->category_id;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;
                                    
                                    $arr[$count]['binding_id']=$row->binding_id;
                                    $arr[$count]['binding_type']=$row->binding_type;
                                    
                                     $arr[$count]['cover_id']=$row->cover_id;
                                    $arr[$count]['cover_type']=$row->cover_type;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['sel_language_id']=$row->sel_language_id;
                                    $arr[$count]['related_books']=$row-> related_books;

                                    $arr[$count]['is_book_visible']=$row-> is_book_visible;
                                    
                                    
                                    

                                    
                }
            
                return $arr;
            }
        }
            catch(Exception $e)
            {
                    throw $e;
            }
    }
        

        
        
        
    // **********************
    // DELETE
    // **********************
    
    function delete($id)
    {
        $sql = "DELETE FROM tbl_books WHERE  = $id;";
        $result = $this->database->query($sql);
    }

    // **********************
    // INSERT
    // **********************
    
    function insert()
    {
        $sql = "INSERT INTO tbl_books ( book_title,book_title_mr,book_language,author_id,book_mrp,compile_by,translated_by,book_edition,book_description,book_thumbnail,publisher_id,isbn_no,category_id,dim_width,dim_height,dim_depth,no_of_pages,book_weight,production_cost,is_translated,translated_from_book,translated_frm_lang,publication_date,co_author,contributor,editor,copyright,is_upcoming_book,created_date,created_by,is_deleted,is_book_visible ) VALUES ('$this->book_title','$this->book_title_mr','$this->book_language','$this->author_id','$this->book_mrp','$this->compile_by','$this->translated_by','$this->book_edition','$this->book_description','$this->book_thumbnail','$this->publisher_id','$this->isbn_no','$this->category_id','$this->dim_width','$this->dim_height','$this->dim_depth','$this->no_of_pages','$this->book_weight','$this->production_cost','$this->is_translated','$this->translated_from_book','$this->translated_frm_lang','$this->publication_date','$this->co_author','$this->contributor','$this->editor','$this->copyright','$this->is_upcoming_book','$this->created_date','$this->created_by','$this->is_deleted','$this->is_book_visible' )";

           $result = $this->database->query($sql);

         return $this->database->last_insertid;
       
    }

    // **********************
    // UPDATE
    // **********************
    
    function update($id)
    {
        if($this->book_thumbnail=='')
        {
            $book_img ='';
        }
        else{
            $book_img ="`book_thumbnail` = '".$this->book_thumbnail."',";
        }
         $sql = " UPDATE 
                    tbl_books 
                  SET  
                    book_title = '$this->book_title',book_title_mr = '$this->book_title_mr',book_language = '$this->book_language',author_id = '$this->author_id',book_mrp = '$this->book_mrp',compile_by = '$this->compile_by',translated_by = '$this->translated_by',book_edition = '$this->book_edition',book_description = '$this->book_description',".
                    $book_img
                    ."publisher_id = '$this->publisher_id',isbn_no = '$this->isbn_no',category_id = '$this->category_id',dim_width = '$this->dim_width',dim_height = '$this->dim_height',dim_depth = '$this->dim_depth',no_of_pages = '$this->no_of_pages',binding_id = '$this->binding_id',cover_id = '$this->cover_id',book_weight = '$this->book_weight',production_cost = '$this->production_cost',is_translated = '$this->is_translated',translated_from_book = '$this->translated_from_book',translated_frm_lang = '$this->translated_frm_lang',publication_date = '$this->publication_date',co_author = '$this->co_author',contributor = '$this->contributor',editor = '$this->editor',copyright = '$this->copyright',is_upcoming_book = '$this->is_upcoming_book',copyright = '$this->copyright',is_book_visible='$this->is_book_visible' WHERE  book_id=". $id;

        $result = $this->database->query($sql);
    
    }
    


    // **********************
    // Get Book Detail
    // **********************
    
    function getBookSmartSearch($condition=null)
    {
        try
        {
            
            $condition.=" AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
    
    
                $sql =  "SELECT
                            tb.`book_id`,
                            tb.`book_title`,
                            tb.`book_title_mr`,
                            tb.`book_language`,
                            tb.`author_id`,
                            tb.`book_mrp`,
                            tb.`compile_by`,
                            tb.`book_edition`,
                            tb.`book_description`,
                            (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                            tb.`publisher_id`,
                            tb.`publication_date`,
                            tb.`isbn_no`,
                            ta.name,
                            tb.translated_by,
                            tb.book_edition,
                            
                            tb.publisher_id,
                            tb.isbn_no,
                            tb.category_id,
                            tb.dim_width,
                            tb.dim_height,
                            tb.dim_depth,
                            tb.no_of_pages,
                            tb.binding_id,
                            tb.cover_id,
                            tb.book_weight,
                            tb.production_cost,
                            tb.is_translated,
                            tb.translated_from_book,
                            tb.translated_frm_lang,
                            tb.publication_date,
                            tb.co_author,
                            tb.contributor,
                            tb.editor,
                            tb.copyright,
                            tb.is_upcoming_book,
                            tb.created_date,
                            tb.created_by,
                            tp.name as pub_name , 
                            tp.address as pub_addr , 
                            tp.contact_no as pub_contact , 
                            tp.email_id as pub_email , 
                            tp.website as pub_website , 
                            bc.category_title,
                            tq.location_id,
                            tq.book_quantity,
                            tb.is_book_visible
    
                        FROM `tbl_books` tb

                            LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                            LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                            LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id
                            LEFT JOIN `tbl_books_quantity_at_location` tq ON tb.book_id = tq.book_id
                        
                        WHERE 
                            tb.`is_deleted`='0' ". $condition." ORDER BY tb.book_id DESC";
                       
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                     $arr[$count]['category_id']=$row->category_id;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;
                                    
                                    $arr[$count]['binding_id']=$row->binding_id;
                                    $arr[$count]['binding_type']=$row->binding_type;
                                    
                                     $arr[$count]['cover_id']=$row->cover_id;
                                    $arr[$count]['cover_type']=$row->cover_type;
                                     $arr[$count]['book_quantity']=$row->book_quantity;

                                     $arr[$count]['is_book_visible']=$row->is_book_visible;
                                    

                                    
                }
            
                return $arr;
            }
        }
            catch(Exception $e)
            {
                    throw $e;
            }
    }

    
    
    



    // **********************
    // SELECT All Book of Most Salable Info
    // **********************
    
    function getBookListMostSalable($start=null,$per_page=null)
    {
    
     if($start!=null || $per_page!=null)
            $limit="limit $start,$per_page";
            else
            $limit=null;
        
        $condition="AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
    
        try
        {
                            $sql =  "SELECT
                                        count(sbe.`book_id`) as saleCount,
                                        tb.`book_id`,
                                        tb.`book_title`,
                                        tb.`book_title_mr`,
                                        tb.`book_language`,
                                        tb.`author_id`,
                                        tb.`book_mrp`,
                                        tb.`compile_by`,
                                        tb.`book_edition`,
                                        (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                                        tb.`publisher_id`,
                                        tb.`isbn_no`,
                                        tb.`publication_date`,
                                        tb.`isbn_no`,
                                        ta.name,
                                        tb.translated_by,
                                        tb.book_edition,
                                        tb.book_description,
                                        
                                        tb.publisher_id,
                                        tb.isbn_no,
                                        tb.category_id,
                                        tb.dim_width,
                                        tb.dim_height,
                                        tb.dim_depth,
                                        tb.no_of_pages,
                                        tb.binding_id,
                                        tb.cover_id,
                                        tb.book_weight,
                                        tb.production_cost,
                                        tb.is_translated,
                                        tb.translated_from_book,
                                        tb.translated_frm_lang,
                                        tb.publication_date,
                                        tb.co_author,
                                        tb.contributor,
                                        tb.editor,
                                        tb.copyright,
                                        tb.is_upcoming_book,
                                        tb.created_date,
                                        tb.created_by,
                                        tp.name as pub_name , 
                                        tp.address as pub_addr , 
                                        tp.contact_no as pub_contact , 
                                        tp.email_id as pub_email , 
                                        tp.website as pub_website , 
                                        bt.title as binding_type,
                                        ct.title as cover_type,
                                        bc.category_title,
                                        tb.is_book_visible,
                                       
                                        (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                                        
                                    FROM   tbl_sales_book_entry sbe 

                                    LEFT JOIN `tbl_books` tb ON tb.book_id = sbe.book_id 
                                    
                                    LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                                    LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                                    
                                    LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                                    LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
                                    
                                    LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

                                    WHERE 
                                    tb.`is_deleted`='0'". $condition. "  GROUP BY sbe.book_id  ORDER BY saleCount DESC $limit";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;

                                    $arr[$count]['binding_type']=$row->binding_type;
                                    $arr[$count]['cover_type']=$row->cover_type;

                                     $arr[$count]['is_book_visible']=$row->is_book_visible;
                                    

                                    
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }



    
    
    
    // **********************
    //  Count All Book of Most Salable Info
    // **********************
    
    function getBookListMostSalableCount()
    {
    
    
        try
        {
                            $sql =  "SELECT
                                        count(sbe.`book_id`) as saleCount,
                                        tb.`book_id`,
                                        tb.`book_title`,
                                        tb.`book_title_mr`,
                                        tb.`book_language`,
                                        tb.`author_id`,
                                        tb.`book_mrp`,
                                        tb.`compile_by`,
                                        tb.`book_edition`,
                                        (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                                        tb.`publisher_id`,
                                        tb.`isbn_no`,
                                        tb.`publication_date`,
                                        tb.`isbn_no`,
                                        ta.name,
                                        tb.translated_by,
                                        tb.book_edition,
                                        tb.book_description,
                                        
                                        tb.publisher_id,
                                        tb.isbn_no,
                                        tb.category_id,
                                        tb.dim_width,
                                        tb.dim_height,
                                        tb.dim_depth,
                                        tb.no_of_pages,
                                        tb.binding_id,
                                        tb.cover_id,
                                        tb.book_weight,
                                        tb.production_cost,
                                        tb.is_translated,
                                        tb.translated_from_book,
                                        tb.translated_frm_lang,
                                        tb.publication_date,
                                        tb.co_author,
                                        tb.contributor,
                                        tb.editor,
                                        tb.copyright,
                                        tb.is_upcoming_book,
                                        tb.created_date,
                                        tb.created_by,
                                        tp.name as pub_name , 
                                        tp.address as pub_addr , 
                                        tp.contact_no as pub_contact , 
                                        tp.email_id as pub_email , 
                                        tp.website as pub_website , 
                                        bt.title as binding_type,
                                        ct.title as cover_type,
                                        bc.category_title,
                                        tb.is_book_visible,
                                       
                                        (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                                        
                                    FROM   tbl_sales_book_entry sbe 

                                    LEFT JOIN `tbl_books` tb ON tb.book_id = sbe.book_id 
                                    
                                    LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                                    LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                                    
                                    LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                                    LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
                                    
                                    LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

                                    WHERE 
                                    tb.`is_deleted`='0'  GROUP BY sbe.book_id  ORDER BY saleCount DESC $limit";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
                return mysqli_num_rows($result);
            }
        
        catch(Exception $e)
        {
            throw $e;
        }
    }



    
    // **********************
    // SELECT All Book of Upcoming Book
    // **********************
    
    function getBookListUpcomingBook($condition=null)
    {
    
    
        try
        {

$condition.=" AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
    
        $sql =  "SELECT
                                        tb.`book_id`,
                                        tb.`book_title`,
                                        tb.`book_title_mr`,
                                        tb.`book_language`,
                                        tb.`author_id`,
                                        tb.`book_mrp`,
                                        tb.`compile_by`,
                                        tb.`book_edition`,
                                        (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                                        tb.`publisher_id`,
                                        tb.`isbn_no`,
                                        tb.`publication_date`,
                                        tb.`isbn_no`,
                                        ta.name,
                                        tb.translated_by,
                                        tb.book_edition,
                                        tb.book_description,
                                        
                                        tb.publisher_id,
                                        tb.isbn_no,
                                        tb.category_id,
                                        tb.dim_width,
                                        tb.dim_height,
                                        tb.dim_depth,
                                        tb.no_of_pages,
                                        tb.binding_id,
                                        tb.cover_id,
                                        tb.book_weight,
                                        tb.production_cost,
                                        tb.is_translated,
                                        tb.translated_from_book,
                                        tb.translated_frm_lang,
                                        tb.publication_date,
                                        tb.co_author,
                                        tb.contributor,
                                        tb.editor,
                                        tb.copyright,
                                        tb.is_upcoming_book,
                                        tb.created_date,
                                        tb.created_by,
                                        tp.name as pub_name , 
                                        tp.address as pub_addr , 
                                        tp.contact_no as pub_contact , 
                                        tp.email_id as pub_email , 
                                        tp.website as pub_website , 
                                        bt.title as binding_type,
                                        ct.title as cover_type,
                                        bc.category_title,
                                        tb.is_book_visible,
                                       
                                        (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                                        
                                    FROM `tbl_books` tb
                                    
                                    LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                                    LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                                    
                                    LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                                    LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
                                    
                                    LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

                                    
                                    
                                    WHERE 
                                    tb.`is_upcoming_book`='1' and tb.`is_deleted`='0' ". $condition." ORDER BY tb.is_upcoming_book DESC";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;

                                    $arr[$count]['binding_type']=$row->binding_type;
                                    $arr[$count]['cover_type']=$row->cover_type;

                                    $arr[$count]['is_book_visible']=$row->is_book_visible;
                                    

                                    
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }


// **********************
    // SELECT All Book Category Wise Count
    // **********************
    
    function getBookListCategoryWiseCount($condition=null)
    {
    
        try
        {
            
            
            
//$condition.=" AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
    
                                $sql =  "SELECT
                                        tb.`book_id`,
                                        tb.`book_title`,
                                        tb.`book_title_mr`,
                                        tb.`book_language`,
                                        tb.`author_id`,
                                        tb.`book_mrp`,
                                        tb.`compile_by`,
                                        tb.`book_edition`,
                                        (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                                        tb.`publisher_id`,
                                        tb.`isbn_no`,
                                        tb.`publication_date`,
                                        tb.`isbn_no`,
                                        ta.name,
                                        tb.translated_by,
                                        tb.book_edition,
                                        tb.book_description,
                                        
                                        tb.publisher_id,
                                        tb.isbn_no,
                                        tb.category_id,
                                        tb.dim_width,
                                        tb.dim_height,
                                        tb.dim_depth,
                                        tb.no_of_pages,
                                        tb.binding_id,
                                        tb.cover_id,
                                        tb.book_weight,
                                        tb.production_cost,
                                        tb.is_translated,
                                        tb.translated_from_book,
                                        tb.translated_frm_lang,
                                        tb.publication_date,
                                        tb.co_author,
                                        tb.contributor,
                                        tb.editor,
                                        tb.copyright,
                                        tb.is_upcoming_book,
                                        tb.created_date,
                                        tb.created_by,
                                        tp.name as pub_name , 
                                        tp.address as pub_addr , 
                                        tp.contact_no as pub_contact , 
                                        tp.email_id as pub_email , 
                                        tp.website as pub_website , 
                                        bt.title as binding_type,
                                        ct.title as cover_type,
                                        bc.category_title,
                                        tb.is_book_visible,
                                       
                                        (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                                        
                                    FROM `tbl_books` tb
                                    
                                    LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                                    LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                                    
                                    LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                                    LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
                                    
                                    LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

                                    
                                    
                                    WHERE 
                                     tb.`is_deleted`='0' ". $condition." ORDER BY tb.category_id DESC";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            return mysqli_num_rows($result);
            
            
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }

    
    
    
    // **********************
    // SELECT All Book Category Wise
    // **********************
    
    function getBookListCategoryWise($condition=null,$start=null,$per_page=null)
    {
     if($start!=null || $per_page!=null)
            $limit="limit $start,$per_page";
            else
            $limit=null;
    
    // $condition.=" AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
    
        try
        {
                                $sql =  "SELECT
                                        tb.`book_id`,
                                        tb.`book_title`,
                                        tb.`book_title_mr`,
                                        tb.`book_language`,
                                        tb.`author_id`,
                                        tb.`book_mrp`,
                                        tb.`compile_by`,
                                        tb.`book_edition`,
                                        (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                                        tb.`publisher_id`,
                                        tb.`isbn_no`,
                                        tb.`publication_date`,
                                        tb.`isbn_no`,
                                        ta.name,
                                        tb.translated_by,
                                        tb.book_edition,
                                        tb.book_description,
                                        
                                        tb.publisher_id,
                                        tb.isbn_no,
                                        tb.category_id,
                                        tb.dim_width,
                                        tb.dim_height,
                                        tb.dim_depth,
                                        tb.no_of_pages,
                                        tb.binding_id,
                                        tb.cover_id,
                                        tb.book_weight,
                                        tb.production_cost,
                                        tb.is_translated,
                                        tb.translated_from_book,
                                        tb.translated_frm_lang,
                                        tb.publication_date,
                                        tb.co_author,
                                        tb.contributor,
                                        tb.editor,
                                        tb.copyright,
                                        tb.is_upcoming_book,
                                        tb.created_date,
                                        tb.created_by,
                                        tp.name as pub_name , 
                                        tp.address as pub_addr , 
                                        tp.contact_no as pub_contact , 
                                        tp.email_id as pub_email , 
                                        tp.website as pub_website , 
                                        bt.title as binding_type,
                                        ct.title as cover_type,
                                        bc.category_title,
                                        tb.is_book_visible,
                                       
                                        (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity
                                        
                                    FROM `tbl_books` tb
                                    
                                    LEFT JOIN `tbl_authors_publishers` ta ON tb.author_id = ta.id
                                    LEFT JOIN `tbl_authors_publishers` tp ON tb.publisher_id = tp.id
                                    
                                    LEFT JOIN `tbl_binding_type` bt ON tb.binding_id = bt.binding_id
                                    LEFT JOIN `tbl_binding_type` ct ON tb.cover_id = ct.binding_id
                                    
                                    LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id

                                    
                                    
                                    WHERE 
                                     tb.`is_deleted`='0'  ".$condition."  ORDER BY tb.category_id DESC $limit";
                        
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                                    $arr[$count]['book_id']=$row->book_id;
                                    $arr[$count]['book_title']=$row->book_title;
                                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                                    $arr[$count]['book_language']=$row->book_language;
                                    $arr[$count]['author_id']=$row->author_id;
                                    $arr[$count]['book_mrp']=$row->book_mrp;
                                    $arr[$count]['compile_by']=$row->compile_by;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['author_name']=$row->name;
                                    $arr[$count]['translated_by']=$row->translated_by;
                                    $arr[$count]['book_edition']=$row->book_edition;
                                    $arr[$count]['book_description']=$row->book_description;
                                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                                    $arr[$count]['publisher_id']=$row->publisher_id;
                                    $arr[$count]['isbn_no']=$row->isbn_no;
                                    $arr[$count]['category_title']=$row->category_title;
                                    $arr[$count]['dim_width']=$row->dim_width;
                                    $arr[$count]['dim_height']=$row->dim_height;
                                    $arr[$count]['dim_depth']=$row->dim_depth;
                                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                                    $arr[$count]['book_weight']=$row->book_weight;
                                    $arr[$count]['production_cost']=$row->production_cost;
                                    $arr[$count]['is_translated']=$row->is_translated;
                                    $arr[$count]['translated_from_book']=$row->translated_from_book;
                                    $arr[$count]['translated_frm_lang']=$row->translated_frm_lang;
                                    $arr[$count]['publication_date']=$row->publication_date;
                                    $arr[$count]['co_author']=$row->co_author;
                                    $arr[$count]['contributor']=$row->contributor;
                                    $arr[$count]['editor']=$row->editor;
                                    $arr[$count]['copyright']=$row->copyright;
                                    $arr[$count]['is_upcoming_book']=$row->is_upcoming_book;
                                    $arr[$count]['created_date']=$row->created_date;
                                    $arr[$count]['created_by']=$row->created_by;
                                    $arr[$count]['quantity']=$row->quantity;
                                                              
                                    $arr[$count]['pub_name']=$row->pub_name;
                                    $arr[$count]['pub_addr']=$row->pub_addr;
                                    $arr[$count]['pub_contact']=$row->pub_contact;
                                    $arr[$count]['pub_email']=$row->pub_email;
                                    $arr[$count]['pub_website']=$row->pub_website;

                                    $arr[$count]['binding_type']=$row->binding_type;
                                    $arr[$count]['cover_type']=$row->cover_type;

                                    $arr[$count]['is_book_visible']=$row->is_book_visible;
                                    

                                    
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }

    // **********************
    // UPDATE General Info
    // **********************
    
    function updateGeneralInfo($id)
    {
        if($this->book_thumbnail=='')
        {
            $book_img ='';
        }
        else{
            $book_img ="`book_thumbnail` = '".$this->book_thumbnail."',";
        }
         $sql = " UPDATE 
                    tbl_books 
                  SET  
                    book_title = '$this->book_title',book_title_mr = '$this->book_title_mr',book_language = '$this->book_language',author_id = '$this->author_id',book_mrp = '$this->book_mrp',compile_by = '$this->compile_by',translated_by = '$this->translated_by',book_edition = '$this->book_edition',book_description = '$this->book_description',".
                    $book_img
                    ."is_upcoming_book = '$this->is_upcoming_book',is_book_visible='$this->is_book_visible'  WHERE  book_id=". $id;

        $result = $this->database->query($sql);
    
    }
    

    // **********************
    // UPDATE Publisher Info
    // **********************
    
    function updatePublisherInfo($id)
    {
        
         $sql = " UPDATE 
                    tbl_books 
                  SET  
                    publisher_id = '$this->publisher_id' 
                   WHERE  
                    book_id=". $id;

        $result = $this->database->query($sql);
    
    }
    

    // **********************
    // UPDATE Book Detail
    // **********************
    
    function updateBookDetail($id)
    {
        
         $sql = " UPDATE 
                    tbl_books 
                  SET  
                    isbn_no = '$this->isbn_no',category_id = '$this->category_id',dim_width = '$this->dim_width',dim_height = '$this->dim_height',dim_depth = '$this->dim_depth',no_of_pages = '$this->no_of_pages',binding_id = '$this->binding_id',cover_id = '$this->cover_id',book_weight = '$this->book_weight',production_cost = '$this->production_cost',is_translated = '$this->is_translated',translated_from_book = '$this->translated_from_book',translated_frm_lang = '$this->translated_frm_lang',publication_date = '$this->publication_date',co_author = '$this->co_author',contributor = '$this->contributor',editor = '$this->editor',copyright = '$this->copyright',copyright = '$this->copyright' WHERE  book_id=". $id;

        $result = $this->database->query($sql);
    
    }
    
    

    // **********************
    // UPDATE Book Detail
    // **********************
    
    function updateRelatedBooks($set_related_book,$update_for_book)
    {
        
         $sql = " UPDATE 
                    tbl_books 
                  SET  
                    `related_books` = '".$set_related_book."' WHERE  book_id IN(".$update_for_book.")";
                    
        $result = $this->database->query($sql);
    
    }
    
    
    // **********************
    // Get Related Books
    // **********************
    
    function getRelatedBookList($book_id)
    {
      $book_id = "'".$book_id."'";

      $condition =" AND  tb.book_id IN(".$book_id.")";
        try
        {
            $sql =  "SELECT
                            tb.`book_id`,
                            tb.`book_title`,
                            tb.`book_title_mr`,
                            tb.`book_mrp`,
                            tb.`compile_by`,
                            tb.`book_edition`,
                            (SELECT tbf.`input_file` FROM `tbl_book_files` tbf WHERE tbf.`book_id`=tb.`book_id` AND tbf.`position`='1' LIMIT 1) as book_thumbnail,
                            tb.`publisher_id`,
                            tb.`isbn_no`,
                            tb.`publication_date`,
                            tb.`isbn_no`,
                            
                            tb.`publisher_id`,
                            tb.`isbn_no`,

                            tb.`production_cost`,

                            tb.`is_upcoming_book`,
                            bc.`category_title`,
                            (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) as quantity

                           
                FROM `tbl_books` tb
                
                LEFT JOIN `tbl_book_categories` bc ON tb.category_id = bc.category_id
                
                WHERE 
                 tb.`is_deleted`='0'  ".$condition;
                
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                    $arr[$count]['quantity']=$row->quantity;
                    $arr[$count]['book_id']=$row->book_id;
                    $arr[$count]['book_title']=$row->book_title;
                    $arr[$count]['book_title_mr']=$row->book_title_mr;
                    $arr[$count]['book_language']=$row->book_language;
                    $arr[$count]['author_id']=$row->author_id;
                    $arr[$count]['book_mrp']=$row->book_mrp;
                    $arr[$count]['compile_by']=$row->compile_by;
                    $arr[$count]['publication_date']=$row->publication_date;
                    $arr[$count]['author_name']=$row->name;
                    $arr[$count]['book_description']=$row->book_description;
                    $arr[$count]['book_thumbnail']=BOOK_THUMBNAIL_FOLDER_HTTP.$row->book_thumbnail;
                    $arr[$count]['publisher_id']=$row->publisher_id;
                    $arr[$count]['isbn_no']=$row->isbn_no;
                    $arr[$count]['category_title']=$row->category_title;
                    $arr[$count]['dim_width']=$row->dim_width;
                    $arr[$count]['dim_height']=$row->dim_height;
                    $arr[$count]['dim_depth']=$row->dim_depth;
                    $arr[$count]['no_of_pages']=$row->no_of_pages;
                    $arr[$count]['book_weight']=$row->book_weight;
                    $arr[$count]['production_cost']=$row->production_cost;

                                    
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    
    
    // **********************
    // Get Related Books
    // **********************
    
    function getBookImages($book_id)
    {
     // $book_id = "'".$book_id."'";

     
        try
        {
            $sql="SELECT * FROM `tbl_book_files` WHERE `book_id`='".$book_id."' ORDER BY `position` ASC";
            
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
            if((!$result) || (mysqli_num_rows($result) == 0))
            {
                return array();
            }
            else
            {
            
                for($count = 0; $row = mysqli_fetch_object($result); $count ++)
                {
                    $arr[$count]['file_id']=$row->file_id;
                    $arr[$count]['book_id']=$row->book_id;
                    $arr[$count]['input_file']=$row->input_file ;
                    $arr[$count]['position']=$row->position ;
                   
                }
            
                return $arr;
            }
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
    
    
    // **********************
    // Delete Book Image
    // **********************
    
    function deleteBookImage($book_id,$file_id)
    {
     
        try
        {
            $sql="DELETE FROM `tbl_book_files` WHERE `book_id`='".$book_id."' AND `file_id`='".$file_id."'";
            
                        
            $result =  $this->database->query($sql);
            $result = $this->database->result;
            
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }



    // **********************
    // Update Book Image Position
    // **********************
    
    function updateBookImagePosition($image_id,$position)
    {
     
        try
        {
             $sqlUpdate="UPDATE `tbl_book_files` SET `position`='".$position."' WHERE `file_id`='".$image_id."'";
            
                        
            echo $result = $this->database->query($sqlUpdate);
                        
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }

    



} // class : end
?>
