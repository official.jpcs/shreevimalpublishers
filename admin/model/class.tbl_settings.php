
<?php

include_once(MODEL_DIR_PATH."class.database.php");
class tbl_settings
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************



var $setting_id;   					// (normal Attribute)
var $email_id_sending_email;   		// (normal Attribute)
var $books_in_stock_below_count;   // (normal Attribute)

var $database; 					 // Instance of class database



// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_settings()
{

$this->database = new Database();

}



// **********************
// GETTER METHODS
// **********************


function getsetting_id()
{
return $this->setting_id;
}

function getemail_id_sending_email()
{
return $this->email_id_sending_email;
}

function getbooks_in_stock_below_count()
{
return $this->books_in_stock_below_count;
}


// **********************
// SETTER METHODS
// **********************


function setsetting_id($val)
{
$this->setting_id =  $val;
}

function setemail_id_sending_email($val)
{
$this->email_id_sending_email =  $val;
}

function setbooks_in_stock_below_count($val)
{
$this->books_in_stock_below_count =  $val;
}



// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_settings WHERE  setting_id= $id;";
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->setting_id = $row->setting_id;

$this->email_id_sending_email = $row->email_id_sending_email;

$this->books_in_stock_below_count = $row->books_in_stock_below_count;

}


// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_settings WHERE  setting_id= $id;";
$result = $this->database->query($sql);

}


// **********************
// INSERT
// **********************

function insert()
{


$sql = "INSERT INTO tbl_settings ( email_id_sending_email,books_in_stock_below_count ) VALUES ( '$this->email_id_sending_email','$this->books_in_stock_below_count' )";
$result = $this->database->query($sql);
		return mysqli_insert_id($this->database->link);

}



// **********************
// UPDATE
// **********************

function update($id)
{

$sql = " UPDATE tbl_settings SET email_id_sending_email = '$this->email_id_sending_email',books_in_stock_below_count = '$this->books_in_stock_below_count' WHERE  setting_id= $id ";

$result = $this->database->query($sql);

}



} // class : end

?>