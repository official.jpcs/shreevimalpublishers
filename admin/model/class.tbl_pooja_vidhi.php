

<?php
//<!-- begin of generated class -->
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_pooja_vidhi
* GENERATION DATE:  27.12.2016
* CLASS FILE:       D:\wamp\www\Classgenerator/generated_classes/class.tbl_pooja_vidhi.php
* FOR MYSQL TABLE:  tbl_pooja_vidhi
* FOR MYSQL DB:     poorna_pooja
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_pooja_vidhi
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************


var $pooja_id;   // (normal Attribute)
var $category_id;   // (normal Attribute)

var $pooja_title_mr;   // (normal Attribute)
var $pooja_title_en;   // (normal Attribute)

var $pooja_desc_mr;   // (normal Attribute)
var $pooja_desc_en;   // (normal Attribute)

var $pooja_img;   // (normal Attribute)

var $pooja_dakshina_mr;   // (normal Attribute)
var $pooja_dakshina_en;   // (normal Attribute)

var $is_deleted;   // (normal Attribute)
var $created_date;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_pooja_vidhi()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************
function getpooja_dakshina_mr()
{
return $this->pooja_dakshina_mr;
}
function getpooja_dakshina_en()
{
return $this->pooja_dakshina_en;
}


function getpooja_id()
{
return $this->pooja_id;
}

function getcategory_id()
{
return $this->category_id;
}

function getpooja_title_mr()
{
return $this->pooja_title_mr;
}

function getpooja_title_en()
{
return $this->pooja_title_en;
}

function getpooja_desc_mr()
{
return $this->pooja_desc_mr;
}

function getpooja_desc_en()
{
return $this->pooja_desc_en;
}

function getpooja_img()
{
return $this->pooja_img;
}

function getis_deleted()
{
return $this->is_deleted;
}

function getcreated_date()
{
return $this->created_date;
}

// **********************
// SETTER METHODS
// **********************


function setpooja_id($val)
{
$this->pooja_id =  $val;
}

function setcategory_id($val)
{
$this->category_id =  $val;
}

function setpooja_title_mr($val)
{
$this->pooja_title_mr =  $val;
}

function setpooja_title_en($val)
{
$this->pooja_title_en =  $val;
}

function setpooja_desc_mr($val)
{
$this->pooja_desc_mr =  $val;
}

function setpooja_desc_en($val)
{
$this->pooja_desc_en =  $val;
}

function setpooja_img($val)
{
$this->pooja_img =  $val;
}

function setpooja_dakshina_mr($val)
{
$this->pooja_dakshina_mr =  $val;
}

function setpooja_dakshina_en($val)
{
$this->pooja_dakshina_en =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_pooja_vidhi WHERE  pooja_id=". $id;
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->pooja_id = $row->pooja_id;

$this->category_id = $row->category_id;

$this->pooja_title_mr = $row->pooja_title_mr;

$this->pooja_title_en = $row->pooja_title_en;

$this->pooja_desc_mr = $row->pooja_desc_mr;

$this->pooja_desc_en = $row->pooja_desc_en;

$this->pooja_img = $row->pooja_img;

$this->pooja_dakshina = $row->pooja_dakshina;

$this->is_deleted = $row->is_deleted;

$this->created_date = $row->created_date;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_pooja_vidhi WHERE  pooja_id=". $id;
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{
	$sql = "INSERT INTO tbl_pooja_vidhi (category_id,pooja_title_mr,pooja_title_en,pooja_desc_mr,pooja_desc_en,pooja_img,pooja_dakshina_mr,pooja_dakshina_en,is_deleted,created_date ) VALUES ( '$this->category_id','$this->pooja_title_mr','$this->pooja_title_en','$this->pooja_desc_mr','$this->pooja_desc_en','$this->pooja_img','$this->pooja_dakshina_mr','$this->pooja_dakshina_en','$this->is_deleted','$this->created_date' )";

	$result = $this->database->query($sql);
	return mysqli_insert_id($this->database->link);

}

// **********************
// UPDATE
// **********************

function update($id)
{
	$sql = " UPDATE tbl_pooja_vidhi SET  category_id = '$this->category_id',pooja_title_mr = '$this->pooja_title_mr',pooja_title_en = '$this->pooja_title_en',pooja_desc_mr = '$this->pooja_desc_mr',pooja_desc_en = '$this->pooja_desc_en',pooja_img = '$this->pooja_img',pooja_dakshina_mr = '$this->pooja_dakshina_mr',pooja_dakshina_en = '$this->pooja_dakshina_en' WHERE  pooja_id=". $id;

	$result = $this->database->query($sql);
}

	// Select All vendor List
	function selectAllPoojaVidhi()
	{
		
		try
		{
			$sql =  "SELECT * FROM tbl_pooja_vidhi WHERE  is_deleted = '0'";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_pooja_vidhi();
				  $arr[$count]->setpooja_id($row->pooja_id);
				  $arr[$count]->setcategory_id($row->category_id);
				  $arr[$count]->setpooja_title_mr($row->pooja_title_mr);
				  $arr[$count]->setpooja_title_en($row->pooja_title_en );
				  $arr[$count]->setpooja_img($row->pooja_img );
				  $arr[$count]->setpooja_dakshina_mr($row->pooja_dakshina_mr);
                                  $arr[$count]->setpooja_dakshina_en($row->pooja_dakshina_en);
				  
				  
				  
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
        
	// **********************
	// Delete 
	// **********************
	
	function deletePoojaVidhi($id)
	{
		
            $sql = " UPDATE tbl_pooja_vidhi SET  is_deleted = '1' WHERE  pooja_id=". $id;
            $result = $this->database->query($sql);
	}
        

	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function selectPoojaVidhiList($pooja_id,$category_id)
	{   
			if($pooja_id!="" || $pooja_id!=0)
			{
			   $condition1 =" AND pv.`pooja_id`=".$pooja_id; 
			}
			else
			{
			   $condition1 =""; 
			}
	
			if($category_id!="" || $category_id!=0)
			{
			   $condition2 =" AND pp.category_id=".$category_id; 
			}
			else
			{
			   $condition2 =""; 
			}


		$sql_1 =  "SELECT pv.*,pp.category_id,pp.category_title_mr,pp.category_title_en FROM tbl_pooja_vidhi pv LEFT JOIN tbl_pooja_prakar pp ON pp.category_id=pv.category_id WHERE pv.`is_deleted` ='0' ".$condition1 . $condition2;
		
                $result_1 =  $this->database->query($sql_1);
                $result_1 = $this->database->result;
		
                
		//var_dump($row);die;
                
		if((!$result_1) || (mysqli_num_rows($result_1) == 0))
		{
			return array();
		}
		else
		{
		
			for($count = 0; $row = mysqli_fetch_object($result_1); $count ++)
			{
				//var_dump($row);
				//$arr[$count] = new tbl_pooja_vidhi();

				$arr[$count]['pooja_id'] = $row->pooja_id;
				$arr[$count]['category_id'] = $row->category_id;
				$arr[$count]['category_title_mr'] = $row->category_title_mr;
				$arr[$count]['category_title_en'] = $row->category_title_en;
				$arr[$count]['pooja_title_mr'] = $row->pooja_title_mr;
				$arr[$count]['pooja_title_en'] = $row->pooja_title_en;
				$arr[$count]['pooja_img'] = $row->pooja_img;
				$arr[$count]['pooja_dakshina_mr'] = $row->pooja_dakshina_mr;
				$arr[$count]['pooja_dakshina_en'] = $row->pooja_dakshina_en;
				
				$arr[$count]['pooja_desc_mr'] = $row->pooja_desc_mr;
				$arr[$count]['pooja_desc_en'] = $row->pooja_desc_en;
				
			}
                        
                            return $arr;
		}	
	    
	}


} // class : end
//<!-- end of generated class -->
?>

