<?php
include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_authors_publishers
{ // class : begin
	
	
	// **********************
	// ATTRIBUTE DECLARATION
	// **********************
		
	var $id;   // (normal Attribute)
	var $user_type;   // (normal Attribute)
	var $name;   // (normal Attribute)
	var $name_mr;   // (normal Attribute)
	var $address;   // (normal Attribute)
	var $address_mr;   // (normal Attribute)
	var $contact_no;   // (normal Attribute)
	var $email_id;   // (normal Attribute)
	var $website;   // (normal Attribute)
	
	var $database; // Instance of class database
	var $created_date;
	var $is_deleted;
	
	
	// **********************
	// CONSTRUCTOR METHOD
	// **********************
	
	function tbl_authors_publishers()
	{
	
	$this->database = new Database();
	
	}
	
	
	// **********************
	// GETTER METHODS
	// **********************
	
	
	function getid()
	{
	return $this->id;
	}
	
	function getuser_type()
	{
	return $this->user_type;
	}
	
	function getname()
	{
	return $this->name;
	}
	
	function getname_mr()
	{
	return $this->name_mr;
	}
	
	function getaddress()
	{
	return $this->address;
	}
	
	function getaddress_mr()
	{
	return $this->address_mr;
	}
	
	function getcontact_no()
	{
	return $this->contact_no;
	}
	
	function getemail_id()
	{
	return $this->email_id;
	}
	
	function getwebsite()
	{
	return $this->website;
	}
	
	function gecreated_date()
	{
		return $this->created_date;
	}
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setid($val)
	{
	$this->id =  $val;
	}
	
	function setuser_type($val)
	{
	$this->user_type =  $val;
	}
	
	function setname($val)
	{
	$this->name =  $val;
	}
	function setname_mr($val)
	{
	$this->name_mr =  $val;
	}
	
	function setaddress($val)
	{
	$this->address =  $val;
	}
	
	function setaddress_mr($val)
	{
	$this->address_mr =  $val;
	}
	
	function setcontact_no($val)
	{
	$this->contact_no =  $val;
	}
	
	function setemail_id($val)
	{
	$this->email_id =  $val;
	}
	
	function setwebsite($val)
	{
	$this->website =  $val;
	}
	
	function setcreated_date()
	{
	  $this->created_date =  $val;
	}
	
	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function selectSinglePubAuthor($id)
	{
	
		$sql =  "SELECT * FROM tbl_authors_publishers WHERE id= $id"; 
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);
		
	
		$this->id = $row->id;
		
		$this->user_type = $row->user_type;
		
		$this->name = $row->name; 
		
		$this->name_mr = $row->name_mr;
		
		$this->address = $row->address;
		
		$this->address_mr = $row->address_mr;
		
		$this->contact_no = $row->contact_no;
		
		$this->email_id = $row->email_id;
		
		$this->website = $row->website;
	
	}

	
	
			
//**********************
	// Count AuthorPublishers List
	//**********************
	
	function CountAuthorPublishers($user_type=null)
    {
	 

		try
		{
			$sql =  "SELECT * FROM tbl_authors_publishers WHERE `is_deleted`='0' AND `user_type`='".$user_type."' ORDER BY id DESC";

			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			return mysqli_num_rows($result);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	//**********************
	// SELECT All Author/Publisher List
	//**********************
	
	function select_author_publishers($user_type=null,$start=null,$per_page=null)
    {
	 if($start!=null || $per_page!=null)
            $limit="limit $start,$per_page";
            else
            $limit=null;

	
		try
		{
			$sql =  "SELECT * FROM tbl_authors_publishers WHERE `is_deleted`='0' AND `user_type`='".$user_type."' ORDER BY id DESC $limit";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_authors_publishers();
				  $arr[$count]->setid($row->id);
				  $arr[$count]->setuser_type($row->user_type);
				  
				  $arr[$count]->setname($row->name);
				  $arr[$count]->setname_mr($row->name_mr);
				  $arr[$count]->setaddress($row->address);
				  $arr[$count]->setaddress_mr($row->address_mr);
				  $arr[$count]->setcontact_no($row->contact_no);
				  
				  $arr[$count]->setemail_id($row->email_id);
				  $arr[$count]->setwebsite($row->website);
				  $arr[$count]->setcreated_date($row->created_date);
				  
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	
	// **********************
	// DELETE
	// **********************
	
	function delete($id)
	{
		$sql = "DELETE FROM tbl_authors_publishers WHERE id = $id;";
		$result = $this->database->query($sql);
		
	}
	
	// **********************
	// INSERT
	// **********************
	
	function insert()
	{
		$sql = "INSERT INTO tbl_authors_publishers (user_type,name,name_mr,address,address_mr,contact_no,email_id,website,created_date,is_deleted) VALUES ('$this->user_type','$this->name','$this->name_mr','$this->address','$this->address_mr','$this->contact_no','$this->email_id','$this->website','$this->created_date','0' )";
		$result = $this->database->query($sql);
		return $new_id = mysqli_insert_id($this->database->link);
		
	}
	
	// **********************
	// UPDATE
	// **********************
	
	function update($id)
	{
		$sql = " UPDATE tbl_authors_publishers SET  name = '$this->name',name_mr = '$this->name_mr',address = '$this->address',address_mr = '$this->address_mr',contact_no = '$this->contact_no',email_id = '$this->email_id',website = '$this->website' WHERE  id= $id ";
		
		$result = $this->database->query($sql);
	}
	

} // class : end

?>

