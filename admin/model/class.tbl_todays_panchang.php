<?php

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_todays_panchang
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************



var $panchang_content_id;   // (normal Attribute)
var $for_month;   // (normal Attribute)
var $panchang_date;   // (normal Attribute)
var $content_mr;   // (normal Attribute)
var $content_er;   // (normal Attribute)
var $created_date;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_todays_panchang()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getpanchang_content_id()
{
return $this->panchang_content_id;
}

function getfor_month()
{
return $this->for_month;
}

function getpanchang_date()
{
return $this->panchang_date;
}

function getcontent_mr()
{
return $this->content_mr;
}

function getcontent_er()
{
return $this->content_er;
}

function getcreated_date()
{
return $this->created_date;
}

// **********************
// SETTER METHODS
// **********************


function setpanchang_content_id($val)
{
$this->panchang_content_id =  $val;
}

function setfor_month($val)
{
$this->for_month =  $val;
}

function setpanchang_date($val)
{
$this->panchang_date =  $val;
}

function setcontent_mr($val)
{
$this->content_mr =  $val;
}

function setcontent_er($val)
{
$this->content_er =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($start_date,$end_date)
{
try
		{
 $sql =  "SELECT * FROM tbl_todays_panchang WHERE panchang_date BETWEEN '$start_date' AND '$end_date' ORDER BY panchang_date ASC";
//$sql =  "SELECT * FROM tbl_todays_panchang WHERE  panchang_content_id= $id;";
$result =  $this->database->query($sql);
$result = $this->database->result;

if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
		     	
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_todays_panchang();
				 
				  $arr[$count]->setpanchang_content_id($row->panchang_content_id);
				  $arr[$count]->setfor_month($row->for_month);
				  $arr[$count]->setpanchang_date($row->panchang_date);
				  $arr[$count]->setcontent_mr($row->content_mr);
				  $arr[$count]->setcontent_er($row->content_er);
				  $arr[$count]->setcreated_date($row->created_date);
				
				}
			
				return $arr;
			}
}
		catch(Exception $e)
		{
			throw $e;
		}
}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_todays_panchang WHERE  = $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert($sql)
{


//$sql = "INSERT INTO tbl_todays_panchang ( for_month,panchang_date,content_mr,content_er,created_date ) VALUES ( '$this->for_month','$this->panchang_date','$this->content_mr','$this->content_er','$this->created_date' )";
$result = $this->database->query($sql);
return  mysqli_insert_id($this->database->link);

}

// **********************
// UPDATE
// **********************

function update($sql)
{



//$sql = " UPDATE tbl_todays_panchang SET  for_month = '$this->for_month',panchang_date = '$this->panchang_date',content_mr = '$this->content_mr',content_er = '$this->content_er',created_date = '$this->created_date' WHERE  panchang_content_id= $id ";

$result = $this->database->query($sql);



}


} // class : end

?>

