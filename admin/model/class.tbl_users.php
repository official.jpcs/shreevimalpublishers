<!-- begin of generated class -->
<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_users
* GENERATION DATE:  03.02.2015
* FOR MYSQL TABLE:  tbl_users
* FOR MYSQL DB:     esquire
* -------------------------------------------------------
*
*/

include_once("class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_users
{ // class : begin


	// **********************
	// ATTRIBUTE DECLARATION
	// **********************
	
	
	
	var $user_id;   // (normal Attribute)
	var $user_type;   // (normal Attribute)
	var $name;   // (normal Attribute)
	var $address;   // (normal Attribute)
	var $contact_no;   // (normal Attribute)
	var $username;   // (normal Attribute)
	var $email_id;   // (normal Attribute)
	var $password;   // (normal Attribute)
	var $created_at;   // (normal Attribute)
	var $is_deleted;   // (normal Attribute)
	
	var $location_id;   // (normal Attribute)
	
	var $database; // Instance of class database
	
	
	// **********************
	// CONSTRUCTOR METHOD
	// **********************
	
	function tbl_users()
	{
	
	$this->database = new Database();
	
	}
	
	
	// **********************
	// GETTER METHODS
	// **********************
	
	
	function getuser_id()
	{
	return $this->user_id;
	}
	
	function getuser_type()
	{
	return $this->user_type;
	}
	
	function getname()
	{
	return $this->name;
	}
	
	function getaddress()
	{
	return $this->address;
	}
	
	function getcontact_no()
	{
	return $this->contact_no;
	}
	
	function getemail_id()
	{
	return $this->email_id;
	}
	
	function getusername()
	{
	return $this->username;
	}
	
	function getpassword()
	{
	return $this->password;
	}
	
	function getcreated_at()
	{
	return $this->created_at;
	}
	
	function getis_deleted()
	{
	return $this->is_deleted;
	}
	function getuser_list($val)
	{
	return $this->user_list;
	}

	function getlocation_id($val)
	{
	return $this->location_id;
	}
	
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setuser_id($val)
	{
	$this->user_id =  $val;
	}
	
	function setuser_type($val)
	{
	$this->user_type =  $val;
	}
	
	function setname($val)
	{
	$this->name =  $val;
	}
	
	function setaddress($val)
	{
	$this->address =  $val;
	}
	
	function setcontact_no($val)
	{
	$this->contact_no =  $val;
	}
	
	function setemail_id($val)
	{
	$this->email_id =  $val;
	}

	function setusername($val)
	{
	$this->username =  $val;
	}
	
	
	function setpassword($val)
	{
	$this->password =  $val;
	}
	
	function setcreated_at($val)
	{
	$this->created_at =  $val;
	}
	
	function setis_deleted($val)
	{
	$this->is_deleted =  $val;
	}

	function setuser_list($val)
	{
	$this->user_list =  $val;
	}

	function setlocation_id($val)
	{
	$this->location_id =  $val;
	}
	


	function selectSingleUser($id="",$user_name="",$password="")
	{
		if($id=="" && ($user_name!="" && $password!=""))
		{
		 $condition=" WHERE `is_deleted`='0' AND `username` ='".$user_name."' AND `password` ='".$password."'";	
		}
		else
		{
		 $condition=" WHERE user_id = $id;";		 	
		}
		
		$sql =  "SELECT * FROM tbl_users ".$condition;  
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);
		
		$arr=array();
		
		if($row->user_id!="")
		{
			$arr[0]['user_id'] = $row->user_id;
			$arr[0]['user_type'] = $row->user_type;
			$arr[0]['name'] = $row->name;
			$arr[0]['username'] = $row->username;
			$arr[0]['address'] = $row->address;
			$arr[0]['contact_no'] = $row->contact_no;
			$arr[0]['email_id'] = $row->email_id;
            $arr[0]['location_id'] = $row->location_id;
			
			return $arr;
		}
		else
		{
		 	return $arr;
		}			
	}
	
	// **********************
	// SELECT All User Info
	// **********************
	
	function select_user($condition=null)
	{
	
		try
		{
			$sql =  "SELECT * FROM tbl_users WHERE is_deleted='0' AND user_type!='4'".$condition;
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_users();
				  $arr[$count]->setuser_id($row->user_id);
				  $arr[$count]->setuser_type($row->user_type);
				  $arr[$count]->setname($row->name);
				  $arr[$count]->setusername($row->username);
				  $arr[$count]->setusername($row->username);
				  $arr[$count]->setaddress($row->address);
				  $arr[$count]->setcontact_no($row->contact_no);
				  $arr[$count]->setemail_id($row->email_id);
                                  $arr[$count]->setlocation_id($row->location_id);
                                  
				
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	


 	// **********************
	// SELECT All Sales Info
	// **********************
	
	function getUserList($condition=null)
	{
	
		try
		{
		
			 $sql =  "SELECT
							tu.user_id,
							tu.user_type,
							tu.name,
							tu.address,
							tu.contact_no,
							tu.email_id,
							tu.username,
							tu.password,
							tu.location_id,
							tu.created_at,
							tu.is_deleted,
							loc.location_addr 
							                        
						FROM `tbl_users` tu
						LEFT JOIN `tbl_storage_locations` loc ON tu.location_id=loc.location_id
						
					 	WHERE tu.is_deleted='0' AND tu.user_type!='4'".$condition;
                        
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			 	 	 	  	 	 	 	 	 	 	 	
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
					$arr[$count]['user_id']=$row->user_id;
					$arr[$count]['user_type']=$row->user_type;
					$arr[$count]['name']=$row->name;
					$arr[$count]['address']=$row->address;
					$arr[$count]['contact_no']=$row->contact_no;
					$arr[$count]['email_id']=$row->email_id;
					$arr[$count]['username']=$row->username;
					$arr[$count]['location_id']=$row->location_id;
					$arr[$count]['location_addr']=$row->location_addr;

				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
	// **********************
	// DELETE
	// **********************
	
	function delete($id)
	{
		$sql = "DELETE FROM tbl_users WHERE  = $id;";
		$result = $this->database->query($sql);
	
	}
	
	// **********************
	// INSERT
	// **********************
	
	function insert()
	{
		//$this-> = ""; // clear key for autoincrement
		
		$sql = "INSERT INTO tbl_users ( user_id,user_type,name,address,contact_no,email_id,username,password,location_id,created_at,is_deleted ) VALUES ( '$this->user_id','$this->user_type','$this->name','$this->address','$this->contact_no','$this->email_id','$this->username','$this->password','$this->location_id','$this->created_at','$this->is_deleted' )";
		$result = $this->database->query($sql);
		$result = mysqli_insert_id($this->database->link);
	
	}
	
	// **********************
	// UPDATE
	// **********************
	
	function update($id)
	{
		$user_type=$this->user_type;
		$name=$this->name;
		$address=$this->address;
		$contact_no=$this->contact_no;
		$email_id=$this->email_id;
		$username =$this->username;
		$user_type =$this->user_type;
        $location_id =$this->location_id;
		 
		
		$password=$this->password;
		if($password!="")
		{
		 $password_fields =",password = '$password'" ;
		}
		else
		{
		 $password_fields="";
		}
	
		$sql = " UPDATE tbl_users SET  name = '$name',address = '$address',contact_no = '$contact_no',email_id = '$email_id',user_type = '$user_type',username = '$username',location_id ='$location_id' ".$password_fields." WHERE  `user_id`='$id' ";
		$result = $this->database->query($sql);
	}

	// **********************
	// Delete 
	// **********************
	
	function deleteUser($id)
	{
		
	    $sql = " UPDATE tbl_users SET  is_deleted = '1'  WHERE  `user_id`='$id' ";
		$result = $this->database->query($sql);
	}


} // class : end

?>
<!-- end of generated class -->
