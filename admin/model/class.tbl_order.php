<?php

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_order
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $courier_company;
var $dispatch_date;
var $tracking_id;
var $invoice_file_name; 
var $updated_date;


var $discount_amount;
var $razor_response_payload;
var $order_id;   // (normal Attribute)
var $customer_name;   // (normal Attribute)
var $contact_no;   // (normal Attribute)
var $customer_email;   // (normal Attribute)
var $address_line1;   // (normal Attribute)
var $address_line2;   // (normal Attribute)
var $address_line3;   // (normal Attribute)
var $landmark;   // (normal Attribute)
var $town;   // (normal Attribute)
var $taluka;   // (normal Attribute)
var $district;   // (normal Attribute)
var $pin_code;   // (normal Attribute)
var $order_total_amount;   // (normal Attribute)
var $discount;   // (normal Attribute)
var $postage_charges;   // (normal Attribute)
var $final_amount_paid;   // (normal Attribute)
var $order_date;   // (normal Attribute)
var $order_status;   // (normal Attribute)
var $payment_status;   // (normal Attribute)
var $order_delivery_date;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)
var $created_date;   // (normal Attribute)
var $delivery_charges;   // (normal Attribute)
var $transaction_id;

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_order()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************



function getcourier_company()
{
return $this->courier_company;
}

function getdispatch_date()
{
return $this->dispatch_date;
}

function gettracking_id()
{
return $this->tracking_id;
}

function getinvoice_file_name()
{
return $this->invoice_file_name;
}

function getupdated_date()
{
return $this->updated_date;
}


function gettransaction_id()
{
return $this->transaction_id;
}


function getrazor_response_payload()
{
return $this->razor_response_payload;
}

function getorder_id()
{
return $this->order_id;
}

function getcustomer_name()
{
return $this->customer_name;
}

function getcontact_no()
{
return $this->contact_no;
}

function getcustomer_email()
{
return $this->customer_email;
}

function getaddress_line1()
{
return $this->address_line1;
}

function getaddress_line2()
{
return $this->address_line2;
}

function getaddress_line3()
{
return $this->address_line3;
}

function getlandmark()
{
return $this->landmark;
}

function gettown()
{
return $this->town;
}

function gettaluka()
{
return $this->taluka;
}

function getdistrict()
{
return $this->district;
}

function getpin_code()
{
return $this->pin_code;
}

function getorder_total_amount()
{
return $this->order_total_amount;
}

function getdiscount()
{
return $this->discount;
}

function getpostage_charges()
{
return $this->postage_charges;
}

function getfinal_amount_paid()
{
return $this->final_amount_paid;
}

function getorder_date()
{
return $this->order_date;
}

function getorder_status()
{
return $this->order_status;
}

function getpayment_status()
{
return $this->payment_status;
}

function getorder_delivery_date()
{
return $this->order_delivery_date;
}

function getis_deleted()
{
return $this->is_deleted;
}

function getcreated_date()
{
return $this->created_date;
}

function getdelivery_charges()
{
return $this->delivery_charges;
}

// **********************
// SETTER METHODS
// **********************



function setcourier_company($val)
{
$this->courier_company =  $val;
}

function setdispatch_date($val)
{
$this->dispatch_date =  $val;
}

function settracking_id($val)
{
$this->tracking_id =  $val;
}

function setinvoice_file_name($val)
{
$this->invoice_file_name =  $val;
}

function setupdated_date($val)
{
$this->updated_date =  $val;
}




function setdiscount_amount($val)
{
$this->discount_amount =  $val;
}


function settransaction_id($val)
{
$this->transaction_id =  $val;
}


function setrazor_response_payload($val)
{
$this->razor_response_payload =  $val;
}

function setorder_id($val)
{
$this->order_id =  $val;
}

function setcustomer_name($val)
{
$this->customer_name =  $val;
}

function setcontact_no($val)
{
$this->contact_no =  $val;
}

function setcustomer_email($val)
{
$this->customer_email =  $val;
}

function setaddress_line1($val)
{
$this->address_line1 =  $val;
}

function setaddress_line2($val)
{
$this->address_line2 =  $val;
}

function setaddress_line3($val)
{
$this->address_line3 =  $val;
}

function setlandmark($val)
{
$this->landmark =  $val;
}

function settown($val)
{
$this->town =  $val;
}

function settaluka($val)
{
$this->taluka =  $val;
}

function setdistrict($val)
{
$this->district =  $val;
}

function setpin_code($val)
{
$this->pin_code =  $val;
}

function setorder_total_amount($val)
{
$this->order_total_amount =  $val;
}

function setdiscount($val)
{
$this->discount =  $val;
}

function setpostage_charges($val)
{
$this->postage_charges =  $val;
}

function setfinal_amount_paid($val)
{
$this->final_amount_paid =  $val;
}

function setorder_date($val)
{
$this->order_date =  $val;
}

function setorder_status($val)
{
$this->order_status =  $val;
}

function setpayment_status($val)
{
$this->payment_status =  $val;
}

function setorder_delivery_date($val)
{
$this->order_delivery_date =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

function setdelivery_charges($val)
{
$this->delivery_charges =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_order WHERE  order_id= $id;";
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->order_id = $row->order_id;

$this->customer_name = $row->customer_name;

$this->contact_no = $row->contact_no;

$this->customer_email = $row->customer_email;

$this->address_line1 = $row->address_line1;

$this->address_line2 = $row->address_line2;

$this->address_line3 = $row->address_line3;

$this->landmark = $row->landmark;

$this->town = $row->town;

$this->taluka = $row->taluka;

$this->district = $row->district;

$this->pin_code = $row->pin_code;

$this->order_total_amount = $row->order_total_amount;

$this->discount = $row->discount;

$this->postage_charges = $row->postage_charges;

$this->final_amount_paid = $row->final_amount_paid;

$this->order_date = $row->order_date;

$this->order_status = $row->order_status;

$this->payment_status = $row->payment_status;

$this->order_delivery_date = $row->order_delivery_date;

$this->is_deleted = $row->is_deleted;

$this->created_date = $row->created_date;

$this->delivery_charges = $row->delivery_charges;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_order WHERE  order_id= $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{

	  $sql = "INSERT INTO tbl_order (customer_name,contact_no,customer_email,address_line1,address_line2,address_line3,landmark,town,taluka,district,pin_code,order_total_amount,postage_charges,final_amount_paid,order_date,order_status,payment_status,order_delivery_date,is_deleted,created_date,discount,delivery_charges,razor_response_payload,discount_amount) VALUES ( '$this->customer_name','$this->contact_no','$this->customer_email','$this->address_line1','$this->address_line2','$this->address_line3','$this->landmark','$this->town','$this->taluka','$this->district','$this->pin_code','$this->order_total_amount','$this->postage_charges','$this->final_amount_paid','$this->order_date','$this->order_status','$this->payment_status','$this->order_delivery_date','$this->is_deleted','$this->created_date','$this->discount','0','','$this->discount_amount' )";
	 
	$result = $this->database->query($sql);


	return $this->database->last_insertid;

}

// **********************
// UPDATE
// **********************

function update($id)
{

	$sql = " UPDATE tbl_order SET  customer_name = '$this->customer_name',contact_no = '$this->contact_no',customer_email = '$this->customer_email',address_line1 = '$this->address_line1',address_line2 = '$this->address_line2',address_line3 = '$this->address_line3',landmark = '$this->landmark',town = '$this->town',taluka = '$this->taluka',district = '$this->district',pin_code = '$this->pin_code',order_total_amount = '$this->order_total_amount',discount = '$this->discount',postage_charges = '$this->postage_charges',final_amount_paid = '$this->final_amount_paid',order_date = '$this->order_date',order_status = '$this->order_status',payment_status = '$this->payment_status',order_delivery_date = '$this->order_delivery_date',is_deleted = '$this->is_deleted',created_date = '$this->created_date',delivery_charges = '$this->delivery_charges' WHERE  order_id= $id ";

	$result = $this->database->query($sql);
}


// **********************
// Get Order Detail
// **********************
	
function getOrderDetail($id)
{
 
	try
	{
     $sql =  "SELECT
            
				order_id,customer_name,contact_no,customer_email,address_line1,address_line2,address_line3,landmark,town,taluka,district,pin_code,order_total_amount,postage_charges,final_amount_paid
			  FROM `tbl_order` 
		        
		      WHERE `order_id`='$id' ";
                    
                    
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		
		if((!$result) || (mysqli_num_rows($result) == 0))
		{
			return array();
		}
		else
		{
		
			for($count = 0; $row = mysqli_fetch_object($result); $count ++)
			{
                $arr[$count]['order_id']=$row->order_id;
                $arr[$count]['customer_name']=$row->customer_name;
                $arr[$count]['contact_no']=$row->contact_no;
                $arr[$count]['customer_email']=$row->customer_email;
                $arr[$count]['address_line1']=$row->address_line1;
                $arr[$count]['landmark']=$row->landmark;
                $arr[$count]['town']=$row->town;
                $arr[$count]['taluka']=$row->taluka;
                $arr[$count]['district']=$row->district;
                
                
                $arr[$count]['final_amount_paid']=$row->final_amount_paid;
                               
			}
		
			return $arr;
		}
	}
	catch(Exception $e)
	{
		throw $e;
	}
}

// function updatePaymentStatus($id)
// {

//  $sql = " UPDATE 
// 			tbl_order 
// 		  SET  
// 			order_status = '$this->order_status',payment_status = '$this->payment_status',razor_response_payload = '$this->razor_response_payload' WHERE  order_id=". $id;

// $result = $this->database->query($sql);

// }



    // **********************

    // SELECT selectAllOrders

    // **********************



    function selectAllOrders($order_status=null,$start=null,$per_page=null)

    {

			if($start!=null || $per_page!=null)

            $limit="limit $start,$per_page";

            else

            $limit=null;

			if($order_status!=null)

            $order_status="AND order_status='$order_status'";

            else

            $order_status=null;


	 

	 if($_REQUEST['order_date_from']!="" && $_REQUEST['order_date_to']!="") {

			

				$order_date_from = trim($_REQUEST['order_date_from']);

				$order_date_from = str_replace('/', '-', $order_date_from);

				$order_date_from=date('Y-m-d', strtotime($order_date_from));

				

				$order_date_to = trim($_REQUEST['order_date_to']);

				$order_date_to = str_replace('/', '-', $order_date_to);

				$order_date_to=date('Y-m-d', strtotime($order_date_to));



				$condition=" AND date(tbl_order.order_date) BETWEEN '$order_date_from' AND  '$order_date_to' ";

			}

			 $sql = "SELECT * FROM tbl_order  WHERE  tbl_order.is_deleted= '0' AND `payment_status` ='1' ".$condition.$order_status." ORDER BY tbl_order.order_id DESC $limit;";

            $result =  $this->database->query($sql);

            $result = $this->database->result;

			$arr=array();

			for($count = 0; $row = mysqli_fetch_object($result); $count ++)

				{

				  $arr[$count]['order_id'] = $row->order_id;

				  $arr[$count]['customer_name'] = $row->customer_name;

				  $arr[$count]['contact_no'] = $row->contact_no;

				  $arr[$count]['customer_email'] = $row->customer_email;

				  $arr[$count]['contact_no']= $row->contact_no;

				  $arr[$count]['address_line1'] = $row->address_line1;

				  $arr[$count]['address_line2'] = $row->address_line2;

				  $arr[$count]['landmark'] 		= $row->landmark;

				  $arr[$count]['town'] = $row->town;

				  $arr[$count]['taluka'] = $row->taluka;

				  $arr[$count]['district']  = $row->district;

				  $arr[$count]['pin_code'] = $row->pin_code;

				  $arr[$count]['discount'] = $row->discount;

				  $arr[$count]['final_amount_paid'] = $row->final_amount_paid;

				  $arr[$count]['order_date'] = $row->order_date;

				  $arr[$count]['order_status'] = $row->order_status;

				  $arr[$count]['payment_status'] = $row->payment_status;

				  $arr[$count]['is_deleted'] = $row->is_deleted;

				  $arr[$count]['created_date'] = $row->created_date;

				  $arr[$count]['order_delivery_date'] = $row->order_delivery_date;

				  $arr[$count]['delivery_charges'] = $row->delivery_charges;

				  $arr[$count]['discount_amount'] = $row->discount_amount;

				  
				}

			 return $arr;

    }


  // **********************
  // Count Total Complete Orders
  // **********************

    function countCompleteOrders()
    {
			
		if($_REQUEST['order_date_from']!="" && $_REQUEST['order_date_to']!="") 
		{
		$order_date_from = trim($_REQUEST['order_date_from']);

		$order_date_from = str_replace('/', '-', $order_date_from);

		$order_date_from=date('Y-m-d', strtotime($order_date_from));



		$order_date_to = trim($_REQUEST['order_date_to']);

		$order_date_to = str_replace('/', '-', $order_date_to);

		$order_date_to=date('Y-m-d', strtotime($order_date_to));



		$condition=" AND date(tbl_order.order_date) BETWEEN '$order_date_from' AND  '$order_date_to' ";

		}



		$sql = "SELECT * FROM tbl_order  

		WHERE  tbl_order.is_deleted= '0'  AND order_status='1' ".$condition." ORDER BY tbl_order.order_id DESC";


		$result =  $this->database->query($sql);

		$result = $this->database->result;

		return mysqli_num_rows($result);

    }

	

	 // **********************

    // Count Total Incomplete Orders

    // **********************



    function countIncompleteOrders()

    {

	 if($_REQUEST['order_date_from']!="" && $_REQUEST['order_date_to']!="") {

			

			$order_date_from = trim($_REQUEST['order_date_from']);

			$order_date_from = str_replace('/', '-', $order_date_from);

			$order_date_from=date('Y-m-d', strtotime($order_date_from));

			

			$order_date_to = trim($_REQUEST['order_date_to']);

			$order_date_to = str_replace('/', '-', $order_date_to);

			$order_date_to=date('Y-m-d', strtotime($order_date_to));



			$condition=" AND date(tbl_order.order_date) BETWEEN '$order_date_from' AND  '$order_date_to' ";

			}

			

	 

			$sql = "SELECT * FROM tbl_order  WHERE  tbl_order.is_deleted= '0' AND `payment_status` ='1' AND order_status='0' ".$condition." ORDER BY tbl_order.order_id DESC ";



			$result =  $this->database->query($sql);

            $result = $this->database->result;

           return mysqli_num_rows($result);

    }


	// **********************
	// Upload Profile Photo
	// **********************

	function updatePaymentStatus($id,$status)
	{


	  $sql = " UPDATE tbl_order SET  transaction_id = '$this->transaction_id',payment_status='$status',invoice_no=(SELECT * FROM (SELECT MAX(`invoice_no`)+1 FROM `tbl_order`) AS X) WHERE  order_id= '$id' ";

	$result = $this->database->query($sql);

	}



	function getOrderStatus($order_id)
	{

		$sql =  "SELECT * FROM tbl_order WHERE  order_id= $order_id;";
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);


		$this->order_id = $row->order_id;

		$this->customer_name = $row->customer_name;

	
		$this->order_status = $row->order_status;

		$this->payment_status = $row->payment_status;

		$this->order_delivery_date = $row->order_delivery_date;

		$this->transaction_id = $row->transaction_id;

		return $this;

	}


	function updateOrderDispatchDetailnStatus($order_id)
	{

	   $sql = " UPDATE tbl_order SET  courier_company = '$this->courier_company',dispatch_date='$this->dispatch_date',tracking_id='$this->tracking_id',invoice_file_name='$this->invoice_file_name',order_status='1',updated_date='$this->updated_date' WHERE  order_id= '$order_id' ";

	$result = $this->database->query($sql);

	}







} // class : end

?>