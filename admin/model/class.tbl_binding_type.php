<?php


include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

	class tbl_binding_type
	{ // class : begin


	// **********************
	// ATTRIBUTE DECLARATION
	// **********************


		var $binding_id;   // (normal Attribute)
		var $title;   // (normal Attribute)
		var $database; // Instance of class database
	 	var $created_date;

	// **********************
	// CONSTRUCTOR METHOD
	// **********************

		function tbl_binding_type()
		{

			$this->database = new Database();

		}


		// **********************
		// GETTER METHODS
		// **********************


		function getbinding_id()
		{
			return $this->binding_id;
		}

		function gettitle()
		{
			return $this->title;
		}

		function gecreated_date()
		{
			return $this->created_date;
		}
		function getype()
		{
			return $this->type;
		}



		// **********************
		// SETTER METHODS
		// **********************


		function setbinding_id($val)
		{
			$this->binding_id =  $val;
		}

		function settitle($val)
		{
			$this->title =  $val;
		}

		function setcreated_date()
		{
		  $this->created_date =  $val;
		}

		function setype()
		{
		  $this->type =  $val;
		}


		// **********************
		// SELECT METHOD / LOAD
		// **********************

		function selectSingleBindingType($id)
		{

			$sql =  "SELECT * FROM tbl_binding_type WHERE  binding_id= $id;";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			$row = mysqli_fetch_object($result);
			$this->binding_id = $row->binding_id;

			$this->title = $row->title;

		}

		
		
		
//**********************
	// Count BindingTypes List
	//**********************
	
	function CountBindingTypes($cover_type=null)
	{
	
		try
		{
			$sql =  "SELECT * FROM tbl_binding_type WHERE `is_deleted`='0' AND `type`= '".$cover_type."' ORDER BY binding_id DESC";

			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			return mysqli_num_rows($result);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	// **********************
	// SELECT All Bindig Types
	// **********************

	function select_binding_types($cover_type=null,$start=null,$per_page=null)
    {
	 if($start!=null || $per_page!=null)
            $limit="limit $start,$per_page";
            else
            $limit=null;


		try
		{
			$sql =  "SELECT * FROM tbl_binding_type WHERE `is_deleted`='0' AND `type`= '".$cover_type."' ORDER BY binding_id DESC $limit";
			$result =  $this->database->query($sql);
			$result = $this->database->result;

			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{

				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_binding_type();
				  $arr[$count]->setbinding_id($row->binding_id);
				  $arr[$count]->settitle($row->title);
				}

				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}



		// **********************
		// DELETE
		// **********************

		function delete($id)
		{
			$sql = "DELETE FROM tbl_binding_type WHERE  = $id;";
			$result = $this->database->query($sql);

		}

		// **********************
		// INSERT
		// **********************

		function insert()
		{
			$sql = "INSERT INTO tbl_binding_type ( title,created_date,type ) VALUES ( '$this->title','$this->created_date','$this->type')";
			$result = $this->database->query($sql);
			$last_id=  mysqli_insert_id($this->database->link);
			return $last_id;

		}

		// **********************
		// UPDATE
		// **********************

		function update($id)
		{
			$sql = " UPDATE tbl_binding_type SET  title = '$this->title' WHERE  binding_id= $id ";

			$result = $this->database->query($sql);
		}


	} // class : end

?>
