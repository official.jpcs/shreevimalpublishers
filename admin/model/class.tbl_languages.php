<!-- begin of generated class -->
<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_languages
* GENERATION DATE:  19.05.2016
* CLASS FILE:       D:\wamp\www\svppl\class_generator/generated_classes/class.tbl_languages.php
* FOR MYSQL TABLE:  tbl_languages
* FOR MYSQL DB:     svppl
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_languages
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************
var $language_id;   // (normal Attribute)
var $title;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_languages()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


	function getlanguage_id()
	{
	return $this->language_id;
	}
	
	function gettitle()
	{
	return $this->title;
	}
	
	function getis_deleted()
	{
	return $this->is_deleted;
	}
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setlanguage_id($val)
	{
	$this->language_id =  $val;
	}
	
	function settitle($val)
	{
	$this->title =  $val;
	}
	
	function setis_deleted($val)
	{
	$this->is_deleted =  $val;
	}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

	$sql =  "SELECT * FROM tbl_languages WHERE  = $id;";
	$result =  $this->database->query($sql);
	$result = $this->database->result;
	$row = mysqli_fetch_object($result);
	
	
	$this->language_id = $row->language_id;
	
	$this->title = $row->title;
	
	$this->is_deleted = $row->is_deleted;

}

// **********************
// DELETE
// **********************

function delete($id)
{
	$sql = "DELETE FROM tbl_languages WHERE  = $id;";
	$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{
	
	$sql = "INSERT INTO tbl_languages (title,is_deleted ) VALUES ( '$this->title','$this->is_deleted' )";
	$result = $this->database->query($sql);
	return mysqli_insert_id($this->database->link);
	
}

// **********************
// UPDATE
// **********************

	function update($id)
	{
	$sql = " UPDATE tbl_languages SET title = '$this->title' WHERE  language_id=". $id;
	
	$result = $this->database->query($sql);
	}



		// **********************
		// SELECT METHOD / LOAD
		// **********************
		
		function selectSingleLanguage($id)
		{
		
			$sql =  "SELECT * FROM tbl_languages WHERE  language_id=". $id;
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			$row = mysqli_fetch_object($result);
			$this->language_id = $row->language_id;
			
			$this->title = $row->title;
		
		}
	

	// **********************
	// SELECT All Bindig Types
	// **********************
	
	function select_language_list()
	{
	
		try
		{
			$sql =  "SELECT * FROM tbl_languages WHERE `is_deleted`='0'  ORDER BY language_id ASC";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_languages();
				  $arr[$count]->setlanguage_id($row->language_id);
				  $arr[$count]->settitle($row->title);
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}



} // class : end

?>
<!-- end of generated class -->
