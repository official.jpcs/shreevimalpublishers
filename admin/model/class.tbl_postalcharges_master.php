<?php

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_postalcharges_master
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $id;   // (normal Attribute)
var $range_from;   // (normal Attribute)
var $range_to;   // (normal Attribute)
var $amount;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_postalcharges_master()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getid()
{
return $this->id;
}

function getrange_from()
{
return $this->range_from;
}

function getrange_to()
{
return $this->range_to;
}

function getamount()
{
return $this->amount;
}

// **********************
// SETTER METHODS
// **********************


function setid($val)
{
$this->id =  $val;
}

function setrange_from($val)
{
$this->range_from =  $val;
}

function setrange_to($val)
{
$this->range_to =  $val;
}

function setamount($val)
{
$this->amount =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_postalcharges_master WHERE  id= $id;";
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->id = $row->id;

$this->range_from = $row->range_from;

$this->range_to = $row->range_to;

$this->amount = $row->amount;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_postalcharges_master WHERE  id= $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{


$sql = "INSERT INTO tbl_postalcharges_master ( id,range_from,range_to,amount ) VALUES ( '$this->id','$this->range_from','$this->range_to','$this->amount' )";
$result = $this->database->query($sql);
return mysqli_insert_id($this->database->link);

}

// **********************
// UPDATE
// **********************

function update($id)
{

$sql = " UPDATE tbl_postalcharges_master SET  id = '$this->id',range_from = '$this->range_from',range_to = '$this->range_to',amount = '$this->amount' WHERE  = $id ";
$result = $this->database->query($sql);

}



// **********************
// calculatePostalCharges
// **********************

function calculatePostalCharges($total_weight)
{
 
	try
	{
     $sql =  "select `range_from`,`range_to`,`amount` from tbl_postalcharges_master where ".$total_weight." between `range_from` and `range_to` ";
                    
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		
		if((!$result) || (mysqli_num_rows($result) == 0))
		{
			return array();
		}
		else
		{
		
			for($count = 0; $row = mysqli_fetch_object($result); $count ++)
			{
                $arr[$count]['range_from']=$row->range_from;
                $arr[$count]['range_to']=$row->range_to;
                $arr[$count]['amount']=$row->amount;
			}
		
			return $arr[0]['amount'];
		}
	}
	catch(Exception $e)
	{
		throw $e;
	}
}
	



} // class : end

?>