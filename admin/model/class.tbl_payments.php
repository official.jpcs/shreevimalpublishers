<?php

include_once("class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_payments
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************



var $payment_id;   // (normal Attribute)
var $invoice_id;   // (normal Attribute)
var $jobseeker_id;   // (normal Attribute)
var $payment_amount;   // (normal Attribute)
var $payment_mode;   // (normal Attribute)
var $card_name;   // (normal Attribute)
var $status_code;   // (normal Attribute)
var $status_message;   // (normal Attribute)
var $payment_status;   // (normal Attribute)
var $payment_date;   // (normal Attribute)
var $tracking_id;   // (normal Attribute)
var $bank_ref_no;   // (normal Attribute)
var $order_status;   // (normal Attribute)
var $failure_message;   // (normal Attribute)
var $created_date;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_payments()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getpayment_id()
{
return $this->payment_id;
}

function getinvoice_id()
{
return $this->invoice_id;
}

function getjobseeker_id()
{
return $this->jobseeker_id;
}

function getpayment_amount()
{
return $this->payment_amount;
}

function getpayment_mode()
{
return $this->payment_mode;
}

function getcard_name()
{
return $this->card_name;
}

function getstatus_code()
{
return $this->status_code;
}

function getstatus_message()
{
return $this->status_message;
}

function getpayment_status()
{
return $this->payment_status;
}

function getpayment_date()
{
return $this->payment_date;
}

function gettracking_id()
{
return $this->tracking_id;
}

function getbank_ref_no()
{
return $this->bank_ref_no;
}

function getorder_status()
{
return $this->order_status;
}

function getfailure_message()
{
return $this->failure_message;
}

function getcreated_date()
{
return $this->created_date;
}

// **********************
// SETTER METHODS
// **********************


function setpayment_id($val)
{
$this->payment_id =  $val;
}

function setinvoice_id($val)
{
$this->invoice_id =  $val;
}

function setjobseeker_id($val)
{
$this->jobseeker_id =  $val;
}

function setpayment_amount($val)
{
$this->payment_amount =  $val;
}

function setpayment_mode($val)
{
$this->payment_mode =  $val;
}

function setcard_name($val)
{
$this->card_name =  $val;
}

function setstatus_code($val)
{
$this->status_code =  $val;
}

function setstatus_message($val)
{
$this->status_message =  $val;
}

function setpayment_status($val)
{
$this->payment_status =  $val;
}

function setpayment_date($val)
{
$this->payment_date =  $val;
}

function settracking_id($val)
{
$this->tracking_id =  $val;
}

function setbank_ref_no($val)
{
$this->bank_ref_no =  $val;
}

function setorder_status($val)
{
$this->order_status =  $val;
}

function setfailure_message($val)
{
$this->failure_message =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_payments WHERE  payment_id= $id;";
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysql_fetch_object($result);


$this->payment_id = $row->payment_id;

$this->invoice_id = $row->invoice_id;

$this->jobseeker_id = $row->jobseeker_id;

$this->payment_amount = $row->payment_amount;

$this->payment_mode = $row->payment_mode;

$this->card_name = $row->card_name;

$this->status_code = $row->status_code;

$this->status_message = $row->status_message;

$this->payment_status = $row->payment_status;

$this->payment_date = $row->payment_date;

$this->tracking_id = $row->tracking_id;

$this->bank_ref_no = $row->bank_ref_no;

$this->order_status = $row->order_status;

$this->failure_message = $row->failure_message;

$this->created_date = $row->created_date;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_payments WHERE  payment_id= $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{


 $sql = "INSERT INTO tbl_payments (order_id,payment_amount,payment_status,payment_date,tracking_id,created_date ) VALUES ( '$this->order_id','$this->payment_amount','$this->payment_status','$this->payment_date','$this->tracking_id','$this->created_date' )";
$result = $this->database->query($sql);
return $this->database->last_insertid;

}

// **********************
// UPDATE
// **********************

function update($id)
{



$sql = " UPDATE tbl_payments SET  payment_id = '$this->payment_id',invoice_id = '$this->invoice_id',jobseeker_id = '$this->jobseeker_id',payment_amount = '$this->payment_amount',payment_mode = '$this->payment_mode',card_name = '$this->card_name',status_code = '$this->status_code',status_message = '$this->status_message',payment_status = '$this->payment_status',payment_date = '$this->payment_date',tracking_id = '$this->tracking_id',bank_ref_no = '$this->bank_ref_no',order_status = '$this->order_status',failure_message = '$this->failure_message',created_date = '$this->created_date' WHERE  payment_id= $id ";

$result = $this->database->query($sql);



}


} // class : end

?>