<!-- begin of generated class -->
<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_upcoming_event
* GENERATION DATE:  20.03.2017
* CLASS FILE:       D:\wamp\www\Classgenerator/generated_classes/class.tbl_upcoming_event.php
* FOR MYSQL TABLE:  tbl_upcoming_event
* FOR MYSQL DB:     poorna_pooja
* -------------------------------------------------------
* CODE GENERATED BY:
* MY PHP-MYSQL-CLASS GENERATOR
* from: >> www.voegeli.li >> (download for free!)
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_upcoming_event
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************


var $event_id;   // (normal Attribute)
var $event_date;   // (normal Attribute)
var $event_title_mr;   // (normal Attribute)
var $event_title_en;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)
var $created_date;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_upcoming_event()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getevent_id()
{
return $this->event_id;
}

function getevent_date()
{
return $this->event_date;
}

function getevent_title_mr()
{
return $this->event_title_mr;
}

function getevent_title_en()
{
return $this->event_title_en;
}

function getis_deleted()
{
return $this->is_deleted;
}

function getcreated_date()
{
return $this->created_date;
}

// **********************
// SETTER METHODS
// **********************


function setevent_id($val)
{
$this->event_id =  $val;
}

function setevent_date($val)
{
$this->event_date =  $val;
}

function setevent_title_mr($val)
{
$this->event_title_mr =  $val;
}

function setevent_title_en($val)
{
$this->event_title_en =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_upcoming_event WHERE  event_id= $id;";
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->event_id = $row->event_id;

$this->event_date = $row->event_date;

$this->event_title_mr = $row->event_title_mr;

$this->event_title_en = $row->event_title_en;

$this->is_deleted = $row->is_deleted;

$this->created_date = $row->created_date;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_upcoming_event WHERE  event_id= $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{

$sql = "INSERT INTO tbl_upcoming_event ( event_date,event_title_mr,event_title_en,is_deleted,created_date ) VALUES ( '$this->event_date','$this->event_title_mr','$this->event_title_en','$this->is_deleted','$this->created_date' )";
$result = $this->database->query($sql);
return mysqli_insert_id($this->database->link);

}

// **********************
// UPDATE
// **********************

function update($id)
{



$sql = " UPDATE tbl_upcoming_event SET  event_date = '$this->event_date',event_title_mr = '$this->event_title_mr',event_title_en = '$this->event_title_en',is_deleted = '$this->is_deleted',created_date = '$this->created_date' WHERE  event_id= $id ";

$result = $this->database->query($sql);



}



// Select All Upcoming Event List
	function selectAllUpcomingEventList()
	{
		
		try
		{
			$sql =  "SELECT * FROM tbl_upcoming_event WHERE  is_deleted = '0'";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
		
    
				  $arr[$count] = new tbl_upcoming_event();
				  $arr[$count]->setevent_id($row->event_id);
				  $arr[$count]->setevent_date($row->event_date);
				  $arr[$count]->setevent_title_mr($row->event_title_mr);
				  $arr[$count]->setevent_title_en($row->event_title_en );
				  $arr[$count]->setis_deleted($row->is_deleted );
				  $arr[$count]->setcreated_date($row->created_date );
				  
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
     

} // class : end

?>
<!-- end of generated class -->
