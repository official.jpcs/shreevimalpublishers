
<?php
//<!-- begin of generated class -->
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_pooja_prakar
* GENERATION DATE:  27.12.2016
* CLASS FILE:       D:\wamp\www\Classgenerator/generated_classes/class.tbl_pooja_prakar.php
* FOR MYSQL TABLE:  tbl_pooja_prakar
* FOR MYSQL DB:     poorna_pooja
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");;

// **********************
// CLASS DECLARATION
// **********************

class tbl_pooja_prakar
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************


var $category_id;   // (normal Attribute)
var $category_title_mr;   // (normal Attribute)
var $category_title_en;   // (normal Attribute)
var $category_image;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)
var $created_date;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_pooja_prakar()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getcategory_id()
{
return $this->category_id;
}

function getcategory_title_mr()
{
return $this->category_title_mr;
}

function getcategory_title_en()
{
return $this->category_title_en;
}

function getcategory_image()
{
return $this->category_image;
}

function getis_deleted()
{
return $this->is_deleted;
}

function getcreated_date()
{
return $this->created_date;
}

// **********************
// SETTER METHODS
// **********************


function setcategory_id($val)
{
$this->category_id =  $val;
}

function setcategory_title_mr($val)
{
$this->category_title_mr =  $val;
}

function setcategory_title_en($val)
{
$this->category_title_en =  $val;
}

function setcategory_image($val)
{
$this->category_image =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

$sql =  "SELECT * FROM tbl_pooja_prakar WHERE  category_id=". $id;
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->category_id = $row->category_id;

$this->category_title_mr = $row->category_title_mr;

$this->category_title_en = $row->category_title_en;

$this->category_image = $row->category_image;

$this->is_deleted = $row->is_deleted;

$this->created_date = $row->created_date;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_pooja_prakar WHERE  category_id=". $id;
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{
    
//mysql_set_charset('utf8');    
//mysql_query("SET CHARACTER SET utf8");
$sql = "INSERT INTO tbl_pooja_prakar (category_title_mr,category_title_en,category_image,is_deleted,created_date ) VALUES ('$this->category_title_mr','$this->category_title_en','$this->category_image','$this->is_deleted','$this->created_date' )";
$result = $this->database->query($sql);

return mysqli_insert_id($this->database->link);

}

// **********************
// UPDATE
// **********************

function update($id)
{
$sql = " UPDATE tbl_pooja_prakar SET  category_title_mr = '$this->category_title_mr',category_title_en = '$this->category_title_en' WHERE  category_id=". $id;

$result = $this->database->query($sql);

}


	// Select All vendor List
	function selectAllPoojaPrakar()
	{
		
		try
		{
			$sql =  "SELECT * FROM tbl_pooja_prakar WHERE  is_deleted = '0'";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_pooja_prakar();
				  $arr[$count]->setcategory_id($row->category_id);
				  $arr[$count]->setcategory_title_mr($row->category_title_mr);
				  $arr[$count]->setcategory_title_en($row->category_title_en);
				  $arr[$count]->setcategory_image($row->category_image );
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
        
	// **********************
	// Delete 
	// **********************
	
	function deletePoojaPrakar($id)
	{
		
            $sql = " UPDATE tbl_pooja_prakar SET  is_deleted = '1' WHERE  category_id=". $id;
            $result = $this->database->query($sql);
	}
        

	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function selectSinglePoojaPrakar($category_id)
	{
	
		$sql =  "SELECT * FROM tbl_pooja_prakar WHERE `category_id`=".$category_id;
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);
		
		
		$arr=array();
		
		if($row->category_id!="")
		{
                    $arr[$count] = new tbl_pooja_prakar();

                    $arr[0]['category_id'] = $row->category_id;
                    $arr[0]['category_title_mr'] = $row->category_title_mr;
                    $arr[0]['category_title_en'] = $row->category_title_en;
                    $arr[0]['category_image'] = $row->category_image;
                    return $arr;
		}
		else
		{
                    return $arr;
		}			
			
	    
	}
        

} // class : end
//<!-- end of generated class -->
?>

