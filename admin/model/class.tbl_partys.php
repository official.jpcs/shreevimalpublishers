<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_partys
* GENERATION DATE:  24.02.2015
* CLASS FILE:       class.tbl_partys.php
* FOR MYSQL TABLE:  tbl_partys
* FOR MYSQL DB:     svppl
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_partys
{ // class : begin
	
	
	// **********************
	// ATTRIBUTE DECLARATION
	// **********************
		
	var $party_id;   // (normal Attribute)
	var $name;   // (normal Attribute)
	var $address;   // (normal Attribute)
	var $contact_no;   // (normal Attribute)
	var $email_id;   // (normal Attribute)
	var $website;   // (normal Attribute)
	
	var $database; // Instance of class database
	var $created_date;
	var $is_deleted;
	
	
	// **********************
	// CONSTRUCTOR METHOD
	// **********************
	
	function tbl_partys()
	{
	
	$this->database = new Database();
	
	}
	
	
	// **********************
	// GETTER METHODS
	// **********************
	
	
	function getparty_id()
	{
	return $this->party_id;
	}
	
	
	function getname()
	{
	return $this->name;
	}
	
	function getaddress()
	{
	return $this->address;
	}
	
	function getcontact_no()
	{
	return $this->contact_no;
	}
	
	function getemail_id()
	{
	return $this->email_id;
	}
	
	function getwebsite()
	{
	return $this->website;
	}
	
	function gecreated_date()
	{
		return $this->created_date;
	}
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setparty_id($val)
	{
	$this->party_id =  $val;
	}
	
	
	function setname($val)
	{
	$this->name =  $val;
	}
	
	function setaddress($val)
	{
	$this->address =  $val;
	}
	
	function setcontact_no($val)
	{
	$this->contact_no =  $val;
	}
	
	function setemail_id($val)
	{
	$this->email_id =  $val;
	}
	
	function setwebsite($val)
	{
	$this->website =  $val;
	}
	
	function setcreated_date()
	{
	  $this->created_date =  $val;
	}
	
	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function selectSingleParty($party_id)
	{
	
		$sql =  "SELECT * FROM tbl_partys WHERE party_id= $party_id"; 
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);
		
	
		$this->party_id = $row->party_id;
		
		$this->name = $row->name;
		
		$this->address = $row->address;
		
		$this->contact_no = $row->contact_no;
		
		$this->email_id = $row->email_id;
		
		$this->website = $row->website;
	
	}


	//**********************
	// SELECT All Author/Publisher List
	//**********************
	
	function select_partys()
	{
	
		try
		{
			$sql =  "SELECT * FROM tbl_partys WHERE `is_deleted`='0' ORDER BY party_id DESC";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_partys();
				  $arr[$count]->setparty_id($row->party_id);
				  $arr[$count]->setname($row->name);
				  $arr[$count]->setaddress($row->address);
				  $arr[$count]->setcontact_no($row->contact_no);
				  
				  $arr[$count]->setemail_id($row->email_id);
				  $arr[$count]->setwebsite($row->website);
				  $arr[$count]->setcreated_date($row->created_date);
				  
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	
	// **********************
	// DELETE
	// **********************
	
	function delete($party_id)
	{
		$sql = "DELETE FROM tbl_partys WHERE party_id = $party_id;";
		$result = $this->database->query($sql);
		
	}
	
	// **********************
	// INSERT
	// **********************
	
	function insert()
	{
		$sql = "INSERT INTO tbl_partys (name,address,contact_no,email_id,website,created_date,is_deleted) VALUES ('$this->name','$this->address','$this->contact_no','$this->email_id','$this->website','$this->created_date','0' )";
		$result = $this->database->query($sql);
		return $new_id = mysqli_insert_id($this->database->link);
		
	}
	
	// **********************
	// UPDATE
	// **********************
	
	function update($party_id)
	{
		$sql = " UPDATE tbl_partys SET  name = '$this->name',address = '$this->address',contact_no = '$this->contact_no',email_id = '$this->email_id',website = '$this->website' WHERE  party_id= $party_id ";
		
		$result = $this->database->query($sql);
	}
	

} // class : end

?>

