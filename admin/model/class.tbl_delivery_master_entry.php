<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_delivery_master_entry
* GENERATION DATE:  17.06.2016
* CLASS FILE:       D:\wamp\www\svppl\class_generator/generated_classes/class.tbl_delivery_master_entry.php
* FOR MYSQL TABLE:  tbl_delivery_master_entry
* FOR MYSQL DB:     svppl
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_delivery_master_entry
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************

var $delivery_maste_id;   // (normal Attribute)
var $delivery_date;   // (normal Attribute)
var $user_id;   // (normal Attribute)
var $total_amount;   // (normal Attribute)
var $final_amount;   // (normal Attribute)
var $party_id;   // (normal Attribute)
var $cust_note;   // (normal Attribute)
var $created_date;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_delivery_master_entry()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getdelivery_maste_id()
{
return $this->delivery_maste_id;
}

function getdelivery_date()
{
return $this->delivery_date;
}

function getuser_id()
{
return $this->user_id;
}

function gettotal_amount()
{
return $this->total_amount;
}

function getfinal_amount()
{
return $this->final_amount;
}

function getparty_id()
{
return $this->party_id;
}

function getcust_note()
{
return $this->cust_note;
}

function getcreated_date()
{
return $this->created_date;
}

function getis_deleted()
{
return $this->is_deleted;
}

// **********************
// SETTER METHODS
// **********************


function setdelivery_maste_id($val)
{
$this->delivery_maste_id =  $val;
}

function setdelivery_date($val)
{
$this->delivery_date =  $val;
}

function setuser_id($val)
{
$this->user_id =  $val;
}

function settotal_amount($val)
{
$this->total_amount =  $val;
}

function setfinal_amount($val)
{
$this->final_amount =  $val;
}

function setparty_id($val)
{
$this->party_id =  $val;
}

function setcust_note($val)
{
$this->cust_note =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

	$sql =  "SELECT * FROM tbl_delivery_master_entry WHERE  = $id;";
	$result =  $this->database->query($sql);
	$result = $this->database->result;
	$row = mysqli_fetch_object($result);
	
	
	$this->delivery_maste_id = $row->delivery_maste_id;
	
	$this->delivery_date = $row->delivery_date;
	
	$this->user_id = $row->user_id;
	
	$this->total_amount = $row->total_amount;
	
	$this->final_amount = $row->final_amount;
	
	$this->party_id = $row->party_id;
	
	$this->cust_note = $row->cust_note;
	
	$this->created_date = $row->created_date;
	
	$this->is_deleted = $row->is_deleted;

}

// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_delivery_master_entry WHERE  = $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{

$sql = "INSERT INTO tbl_delivery_master_entry (delivery_date,user_id,total_amount,final_amount,party_id,cust_note,created_date,is_deleted ) VALUES ('$this->delivery_date','$this->user_id','$this->total_amount','$this->final_amount','$this->party_id','$this->cust_note','$this->created_date','$this->is_deleted' )";
$result = $this->database->query($sql);
return mysqli_insert_id($this->database->link);

}

// **********************
// UPDATE
// **********************

function update($id)
{
	$sql = " UPDATE tbl_delivery_master_entry SET  delivery_date = '$this->delivery_date',user_id = '$this->user_id',total_amount = '$this->total_amount',final_amount = '$this->final_amount',party_id = '$this->party_id',cust_note = '$this->cust_note',created_date = '$this->created_date',is_deleted = '$this->is_deleted' WHERE  delivery_maste_id=".$id ;
	
	$result = $this->database->query($sql);
}


	// **********************
	// SELECT All delivery Info
	// **********************
	
	function getDeliveryList($condition=null)
	{
	
		try
		{
			$sql =  "SELECT
							*                                        
					FROM `tbl_delivery_master_entry`
			
			
			WHERE 
			`is_deleted`='0' ". $condition." ORDER BY delivery_maste_id DESC";
                        
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			 	 	 	  	 	 	 	 	 	 	 	
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
					$arr[$count]['delivery_maste_id']=$row->delivery_maste_id;
					$arr[$count]['delivery_date']=$row->delivery_date;
					$arr[$count]['user_id']=$row->user_id;
					
					
					
					
					$arr[$count]['total_amount']=$row->total_amount;
					$arr[$count]['final_amount']=$row->final_amount;
					$arr[$count]['customer_name']=$row->customer_name;
					$arr[$count]['cust_contact_no']=$row->cust_contact_no;
					$arr[$count]['cust_address']=$row->cust_address;
					$arr[$count]['cust_email_id']=$row->cust_email_id;
					$arr[$count]['cust_note']=$row->cust_note;
					$arr[$count]['created_date']=$row->created_date;
					$arr[$count]['is_deleted']=$row->is_deleted;
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	// **********************
	// Get delivery Master Detail
	// **********************
	
	function getDeliveryEntryMasterDetail($condition=null)
	{
            try
	    {
             $sql =  "SELECT
                            sm.delivery_maste_id,
                            sm.delivery_date,
                            sm.user_id,
                            sm.total_amount,
                            sm.final_amount,
                            tu.name as employee_name
                            
                        FROM `tbl_delivery_master_entry` sm
                            LEFT JOIN `tbl_users` tu ON sm.user_id = tu.user_id
                        WHERE ". $condition;
                        
                
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
					$arr[$count]['delivery_maste_id']=$row->delivery_maste_id;
					$arr[$count]['delivery_date']=$row->delivery_date;
					$arr[$count]['user_id']=$row->user_id;
					
					$arr[$count]['total_amount']=$row->total_amount;
					$arr[$count]['final_amount']=$row->final_amount;
					$arr[$count]['customer_name']=$row->customer_name;
					$arr[$count]['cust_contact_no']=$row->cust_contact_no;
					$arr[$count]['cust_address']=$row->cust_address;
					$arr[$count]['cust_email_id']=$row->cust_email_id;
					$arr[$count]['cust_note']=$row->cust_note;
					$arr[$count]['created_date']=$row->created_date;
                                        $arr[$count]['employee_name']=$row->employee_name;
                                        
                                    
				}
			
				return $arr;
			}
		}
            catch(Exception $e)
            {
                    throw $e;
            }
	}
        



	// **********************
	// Generate delivery Report
	// **********************
	
	function getDeliveryReport($condition=null)
	{
	
		try
		{
			$sql =  "SELECT
						 tsm.delivery_maste_id, 	
						 tsm.delivery_date, 	
						 tsm.user_id, 	
						 tsm.delivery_type,
						 tsm.tax_type, 
						 tsm.total_amount,
						 tsm.final_amount, 
						 tsm.customer_name,
						 tsm.cust_contact_no,
						 tsm.cust_address,
						 tsm.cust_email_id,
						 tsm.cust_note,
						 tsm.created_date,
						 tsm.is_deleted,
						 tse.book_id,  
						 tse.book_quantity,
						 tse.book_delivery_amount,
						 tse.location_id,
						 tb.book_title,
						 tsl.location_addr,
						 tb.book_mrp,
						 tse.book_discount						                                       
					FROM 
						`tbl_delivery_master_entry` tsm
					LEFT JOIN `tbl_delivery_book_entry` tse ON tsm.delivery_maste_id = tse.delivery_maste_id
					LEFT JOIN `tbl_books` tb ON tb.book_id = tse.book_id
					LEFT JOIN `tbl_storage_locations` tsl ON tsl.location_id = tse.location_id
						
					WHERE 
						tsm.`is_deleted`='0' ".$condition ." ORDER BY tsm.`delivery_date` DESC";
                        
							
				$result =  $this->database->query($sql);
				$result = $this->database->result;
				
				if((!$result) || (mysqli_num_rows($result) == 0))
				{
					return array();
				}
				else
				{
															
				
					for($count = 0; $row = mysqli_fetch_object($result); $count ++)
					{
						$arr[$count]['delivery_maste_id']=$row->delivery_maste_id;
						$arr[$count]['delivery_date']=$row->delivery_date;
						$arr[$count]['user_id']=$row->user_id;
						
						
						
						
						$arr[$count]['total_amount']=$row->total_amount;
						$arr[$count]['final_amount']=$row->final_amount;
						$arr[$count]['customer_name']=$row->customer_name;
						$arr[$count]['cust_contact_no']=$row->cust_contact_no;
						$arr[$count]['cust_address']=$row->cust_address;
						$arr[$count]['cust_email_id']=$row->cust_email_id;
						$arr[$count]['cust_note']=$row->cust_note;
						$arr[$count]['created_date']=$row->created_date;
						$arr[$count]['book_id']=$row->book_id;
						$arr[$count]['book_quantity']=$row->book_quantity;
						$arr[$count]['book_delivery_amount']= $row->book_delivery_amount;
						$arr[$count]['location_id']= $row->location_id;
						$arr[$count]['book_title']= $row->book_title;
						$arr[$count]['location_addr']= $row->location_addr;
						

						$arr[$count]['book_mrp']= $row->book_mrp;
						$arr[$count]['book_discount']= $row->book_discount;
						
					}
				
					return $arr;
				}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


} // class : end

?>
<!-- end of generated class -->
