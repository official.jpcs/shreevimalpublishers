<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_storage_locations
* GENERATION DATE:  24.02.2015
* CLASS FILE:       F:\wamp\www\class_generator/generated_classes/class.tbl_storage_locations.php
* FOR MYSQL TABLE:  tbl_storage_locations
* FOR MYSQL DB:     svppl
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_storage_locations
{ // class : begin

	
	// **********************
	// ATTRIBUTE DECLARATION
	// **********************
	
	var $location_id;   // (normal Attribute)
	var $location_addr;   // (normal Attribute)
	var $location_keyword;   // (normal Attribute)
	
	var $database; // Instance of class database
	
	
	// **********************
	// CONSTRUCTOR METHOD
	// **********************
	
	function tbl_storage_locations()
	{
	
	$this->database = new Database();
	
	}
	
	
	// **********************
	// GETTER METHODS
	// **********************
	
	
	function getlocation_id()
	{
	return $this->location_id;
	}
	
	function getlocation_addr()
	{
	return $this->location_addr;
	}
	
	function getlocation_keyword()
	{
	return $this->location_keyword;
	}
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setlocation_id($val)
	{
	$this->location_id =  $val;
	}
	
	function setlocation_addr($val)
	{
	$this->location_addr =  $val;
	}
	
	function setlocation_keyword($val)
	{
	$this->location_keyword =  $val;
	}

	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function selectSinglelocation($id)
	{
	
		$sql =  "SELECT * FROM tbl_storage_locations WHERE  location_id= $id;";
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);
		
		
		$this->location_id = $row->location_id;
		
		$this->location_addr = $row->location_addr;
		
		$this->location_keyword = $row->location_keyword;
	
	}




	// **********************
	// SELECT All Location List
	// **********************
	
	function selectLocationslist()
	{
	
		try
		{
			$sql =  "SELECT * FROM tbl_storage_locations WHERE is_deleted='0' ORDER BY location_id DESC";
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_storage_locations();
				  $arr[$count]->setlocation_id($row->location_id);
				  $arr[$count]->setlocation_addr($row->location_addr);
				  $arr[$count]->setlocation_keyword($row->location_keyword);
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	// **********************
	// DELETE
	// **********************
	
	function delete($id)
	{
		$sql = "DELETE FROM tbl_storage_locations WHERE  location_id= $id;";
		$result = $this->database->query($sql);
	
	}

	// **********************
	// INSERT
	// **********************
	
	function insert()
	{
	
		$sql = "INSERT INTO tbl_storage_locations ( location_addr,location_keyword,is_deleted ) VALUES ( '$this->location_addr','$this->location_keyword','0' )";
		$result = $this->database->query($sql);
		return mysqli_insert_id($this->database->link);
	
	}

	// **********************
	// UPDATE
	// **********************
	
	function update($id)
	{
	
		$sql = " UPDATE tbl_storage_locations SET  location_addr = '$this->location_addr',location_keyword = '$this->location_keyword' WHERE  location_id= $id ";
		
		$result = $this->database->query($sql);
	}


} // class : end

?>
