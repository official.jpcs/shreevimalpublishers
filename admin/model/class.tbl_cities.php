	<?php
/*
*
* -------------------------------------------------------
* CLASSNAME:        tbl_cities
* GENERATION DATE:  12.01.2016
* CLASS FILE:       G:\wamp\www\match_schedular\admin\class_generator/generated_classes/class.tbl_cities.php
* FOR MYSQL TABLE:  cities
* FOR MYSQL DB:     match_schedular
* -------------------------------------------------------
* CODE GENERATED BY:
* MY PHP-MYSQL-CLASS GENERATOR
* from: >> www.voegeli.li >> (download for free!)
* -------------------------------------------------------
*
*/

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_cities
{ // class : begin


	// **********************
	// ATTRIBUTE DECLARATION
	// **********************
	
	
	var $id;   // (normal Attribute)
	var $name;   // (normal Attribute)
	var $state_id;   // (normal Attribute)
	
	var $database; // Instance of class database


	// **********************
	// CONSTRUCTOR METHOD
	// **********************
	
	function tbl_cities()
	{
	
	$this->database = new Database();
	
	}


	// **********************
	// GETTER METHODS
	// **********************
	
	
	function getid()
	{
	return $this->id;
	}
	
	function getname()
	{
	return $this->name;
	}
	
	function getstate_id()
	{
	return $this->state_id;
	}
	
	// **********************
	// SETTER METHODS
	// **********************
	
	
	function setid($val)
	{
	$this->id =  $val;
	}
	
	function setname($val)
	{
	$this->name =  $val;
	}
	
	function setstate_id($val)
	{
	$this->state_id =  $val;
	}

	// **********************
	// SELECT METHOD / LOAD
	// **********************
	
	function select($id)
	{
		$sql =  "SELECT * FROM cities WHERE  `state_id`= $id";
		$result =  $this->database->query($sql);
		$result = $this->database->result;
		$row = mysqli_fetch_object($result);
		
		$this->id = $row->id;
		
		$this->name = $row->name;
		
		$this->state_id = $row->state_id;
	}

	// **********************
	// DELETE
	// **********************
	
	function delete($id)
	{
		$sql = "DELETE FROM cities WHERE  `state_id`= $id";
		$result = $this->database->query($sql);
	}

	// **********************
	// INSERT
	// **********************
	
	function insert()
	{
		$sql = "INSERT INTO cities ( id,name,state_id ) VALUES ( '$this->id','$this->name','$this->state_id' )";
		$result = $this->database->query($sql);
		return mysqli_insert_id($this->database->link);
	}

	// **********************
	// UPDATE
	// **********************
	
	function update($id)
	{
		$sql = " UPDATE cities SET  id = '$this->id',name = '$this->name',state_id = '$this->state_id' WHERE  `state_id`= $id ";
		
		$result = $this->database->query($sql);
	}


	//**********************
	// SELECT All City List
	//**********************
	
	function select_city_list($state_id)
	{
	
		try
		{
			$sql =  "SELECT * FROM tbl_cities WHERE `state_id`='".$state_id."' ORDER BY name ASC";
			
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
			
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
				  $arr[$count] = new tbl_cities();

				  $arr[$count]->setid($row->id);
				  $arr[$count]->setname($row->name);
				  $arr[$count]->setstate_id($row->state_id);
				}
			
				return $arr;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


} // class : end

?>

