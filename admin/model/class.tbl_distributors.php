<?php

include_once(MODEL_DIR_PATH."class.database.php");

// **********************
// CLASS DECLARATION
// **********************

class tbl_distributors
{ // class : begin


// **********************
// ATTRIBUTE DECLARATION
// **********************


var $distributor_id;   // (normal Attribute)
var $distributor_name;   // (normal Attribute)
var $distributor_contact_no;   // (normal Attribute)
var $distributor_emailid;   // (normal Attribute)
var $distributor_website;   // (normal Attribute)
var $distributor_address;   // (normal Attribute)
var $distributor_state_id;   // (normal Attribute)
var $distributor_city_id;   // (normal Attribute)
var $zip_code;   // (normal Attribute)
var $is_deleted;   // (normal Attribute)
var $created_date;   // (normal Attribute)

var $database; // Instance of class database


// **********************
// CONSTRUCTOR METHOD
// **********************

function tbl_distributors()
{

$this->database = new Database();

}


// **********************
// GETTER METHODS
// **********************


function getdistributor_id()
{
return $this->distributor_id;
}

function getdistributor_name()
{
return $this->distributor_name;
}

function getdistributor_contact_no()
{
return $this->distributor_contact_no;
}

function getdistributor_emailid()
{
return $this->distributor_emailid;
}

function getdistributor_website()
{
return $this->distributor_website;
}

function getdistributor_address()
{
return $this->distributor_address;
}

function getdistributor_state_id()
{
return $this->distributor_state_id;
}

function getdistributor_city_id()
{
return $this->distributor_city_id;
}

function getzip_code()
{
return $this->zip_code;
}

function getis_deleted()
{
return $this->is_deleted;
}

function getcreated_date()
{
return $this->created_date;
}

// **********************
// SETTER METHODS
// **********************


function setdistributor_id($val)
{
$this->distributor_id =  $val;
}

function setdistributor_name($val)
{
$this->distributor_name =  $val;
}

function setdistributor_contact_no($val)
{
$this->distributor_contact_no =  $val;
}

function setdistributor_emailid($val)
{
$this->distributor_emailid =  $val;
}

function setdistributor_website($val)
{
$this->distributor_website =  $val;
}

function setdistributor_address($val)
{
$this->distributor_address =  $val;
}

function setdistributor_state_id($val)
{
$this->distributor_state_id =  $val;
}

function setdistributor_city_id($val)
{
$this->distributor_city_id =  $val;
}

function setzip_code($val)
{
$this->zip_code =  $val;
}

function setis_deleted($val)
{
$this->is_deleted =  $val;
}

function setcreated_date($val)
{
$this->created_date =  $val;
}

// **********************
// SELECT METHOD / LOAD
// **********************

function select($id)
{

 $sql =  "SELECT * FROM tbl_distributors WHERE  distributor_id=". $id;
$result =  $this->database->query($sql);
$result = $this->database->result;
$row = mysqli_fetch_object($result);


$this->distributor_id = $row->distributor_id;

$this->distributor_name = $row->distributor_name;

$this->distributor_contact_no = $row->distributor_contact_no;

$this->distributor_emailid = $row->distributor_emailid;

$this->distributor_website = $row->distributor_website;

$this->distributor_address = $row->distributor_address;

$this->distributor_state_id = $row->distributor_state_id;

$this->distributor_city_id = $row->distributor_city_id;

$this->zip_code = $row->zip_code;

$this->is_deleted = $row->is_deleted;

$this->created_date = $row->created_date;
return $this;
}





// **********************
	// Select Single Distributor List for view distributor
	// **********************
	
	function selectSingleDistributor($id)
    {
	 
            try
	    {
		 $sql =  "SELECT *,tbl_cities.name as city_name,tbl_states.name as state_name FROM tbl_distributors 
		LEFT JOIN 	tbl_cities ON tbl_cities.id=tbl_distributors.distributor_city_id
		LEFT JOIN 	tbl_states ON tbl_states.id=tbl_distributors.distributor_state_id
		WHERE tbl_distributors.is_deleted='0' AND tbl_distributors.distributor_id='$id'";
                
                    
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
		
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
                    $arr[$count]['distributor_id']=$row->distributor_id;
                    $arr[$count]['distributor_name']=$row->distributor_name;
                    $arr[$count]['distributor_contact_no']=$row->distributor_contact_no;
                    $arr[$count]['distributor_emailid']=$row->distributor_emailid;
                    $arr[$count]['distributor_website']=$row->distributor_website;
                    $arr[$count]['distributor_address']=$row->distributor_address;
                    $arr[$count]['distributor_state_id']=$row->state_name;
                    $arr[$count]['distributor_city_id']=$row->city_name;
                    $arr[$count]['zip_code']=$row->zip_code;
                    $arr[$count]['is_deleted']=$row->is_deleted;
                    $arr[$count]['created_date']=created_date;
                   
				}
			
				return $arr;
			}
		}
            catch(Exception $e)
            {
                    throw $e;
            }
	}
	
	
	
// **********************
// DELETE
// **********************

function delete($id)
{
$sql = "DELETE FROM tbl_distributors WHERE  distributor_id= $id;";
$result = $this->database->query($sql);

}

// **********************
// INSERT
// **********************

function insert()
{


$sql = "INSERT INTO tbl_distributors ( distributor_name,distributor_contact_no,distributor_emailid,distributor_website,distributor_address,distributor_state_id,distributor_city_id,zip_code,is_deleted,created_date ) VALUES ( '$this->distributor_name','$this->distributor_contact_no','$this->distributor_emailid','$this->distributor_website','$this->distributor_address','$this->distributor_state_id','$this->distributor_city_id','$this->zip_code','$this->is_deleted','$this->created_date' )";
$result = $this->database->query($sql);
return mysqli_insert_id($this->database->link);

}

// **********************
// UPDATE
// **********************

function update($id)
{



$sql = " UPDATE tbl_distributors SET  distributor_name = '$this->distributor_name',distributor_contact_no = '$this->distributor_contact_no',distributor_emailid = '$this->distributor_emailid',distributor_website = '$this->distributor_website',distributor_address = '$this->distributor_address',distributor_state_id = '$this->distributor_state_id',distributor_city_id = '$this->distributor_city_id',zip_code = '$this->zip_code' WHERE  distributor_id= $id ";

$result = $this->database->query($sql);



}


//**********************
	// Count Distributors List
	//**********************
	
	function CountDistributors()
	{
	
		try
		{
			$sql =  "SELECT * FROM tbl_distributors WHERE `is_deleted`='0'";

			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			return mysqli_num_rows($result);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

    
	// **********************
	// All Dstributor List
	// **********************
	
	function AllDistributor($start=null,$per_page=null)
    {
	 if($start!=null || $per_page!=null)
            $limit="limit $start,$per_page";
            else
            $limit=null;

            try
	    {
		 $sql =  "SELECT *,tbl_cities.name as city_name,tbl_states.name as state_name FROM tbl_distributors 
		LEFT JOIN 	tbl_cities ON tbl_cities.id=tbl_distributors.distributor_city_id
		LEFT JOIN 	tbl_states ON tbl_states.id=tbl_distributors.distributor_state_id
		WHERE tbl_distributors.is_deleted='0' $limit";
                
                    
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
		
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
                    $arr[$count]['distributor_id']=$row->distributor_id;
                    $arr[$count]['distributor_name']=$row->distributor_name;
                    $arr[$count]['distributor_contact_no']=$row->distributor_contact_no;
                    $arr[$count]['distributor_emailid']=$row->distributor_emailid;
                    $arr[$count]['distributor_website']=$row->distributor_website;
                    $arr[$count]['distributor_address']=$row->distributor_address;
                    $arr[$count]['distributor_state_id']=$row->state_name;
                    $arr[$count]['distributor_city_id']=$row->city_name;
                    $arr[$count]['zip_code']=$row->zip_code;
                    $arr[$count]['is_deleted']=$row->is_deleted;
                    $arr[$count]['created_date']=created_date;

                                    
				}
			
				return $arr;
			}
		}
            catch(Exception $e)
            {
                    throw $e;
            }
	}
        
     //select dt.`distributor_name`,dt.`distributor_address`,ct.name as `distributor_city_name` from tbl_distributors dt LEFT JOIN tbl_cities ct ON dt.`distributor_city_id`= ct.id ORDER BY ct.name   


    
	// **********************
	// All Dstributor List City Wise
	// **********************
	
	function AllDistributorCitywise($condition=null)
	{
            try
	    {
		
		$sql =  "select dt.`distributor_name`,dt.`distributor_address`,ct.name as `distributor_city_name`,dt.distributor_contact_no from tbl_distributors dt LEFT JOIN tbl_cities ct ON dt.`distributor_city_id`= ct.id WHERE `is_deleted`='0' ORDER BY ct.name";
                
                      
                        
			$result =  $this->database->query($sql);
			$result = $this->database->result;
			
			if((!$result) || (mysqli_num_rows($result) == 0))
			{
				return array();
			}
			else
			{
		
				for($count = 0; $row = mysqli_fetch_object($result); $count ++)
				{
                    
                    $arr[$count]['distributor_name']=$row->distributor_name;
                    $arr[$count]['distributor_address']=$row->distributor_address;
                    $arr[$count]['distributor_city_name']=$row->distributor_city_name;
                    $arr[$count]['distributor_contact_no']=$row->distributor_contact_no;
					
					
                                    
				}
			
				return $arr;
			}
		}
            catch(Exception $e)
            {
                    throw $e;
            }
	}


} // class : end

?>
<!-- end of generated class -->
