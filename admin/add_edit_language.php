<?php 
/***************************************************************
 *  File Name : Add / Edit Language
 *  Created Date: 19/05/2016
 *  Created By: Prasad
 ************************************************************** */


/* Including Globally Declared Variables */
include("config/config.php");


$tab="Master Content";

$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("tbl_languages")
					  );

// Include Common Files
include_once(CONFIG_CLASS_PATH ."class.php");

$cover_type=1;
// Include Header Section
include(NAVIGATION_FILE . "header.php");


//Include Controller Section
include(CONTROLLER_PATH."LanguageController.php");

//Include View Section
include( VIEW_PATH."add_new_language_view.php");

//Include Footer Section
include(NAVIGATION_FILE . "footer.php");

?>
