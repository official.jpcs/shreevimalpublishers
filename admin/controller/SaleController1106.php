<?php
$step=isset($_POST['step']) ? $_POST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	case 'add_sale_entry':
	//case 'update_book':
	 
            $sales_master_entry = new tbl_sales_master_entry();
            $sales_book_entry = new tbl_sales_book_entry();
            $re    = new reuseFunction();
		
            $sales_master_entry->user_id 			=  $_SESSION['user']['user_id']; 
            $sales_master_entry->sale_date 			=  date("Y-m-d", strtotime(mysqli_real_escape_string($_POST['sale_date']))); 

            $sales_master_entry->sales_type 		=  mysqli_real_escape_string($_POST['sale_type']); 
            $sales_master_entry->tax_type 			=  mysqli_real_escape_string($_POST['tax_type']); 
            $sales_master_entry->tax_amount 		=  mysqli_real_escape_string($_POST['tax_amount']); 
            $sales_master_entry->total_amount 		=  mysqli_real_escape_string($_POST['total_amount']); 
            $sales_master_entry->final_amount 		=  mysqli_real_escape_string($_POST['final_amount']); 

            $sales_master_entry->customer_name 		=  mysqli_real_escape_string($_POST['customer_name']); 
            $sales_master_entry->cust_contact_no 	=  mysqli_real_escape_string($_POST['contact_no']); 
            $sales_master_entry->cust_address 		=  mysqli_real_escape_string($_POST['cust_address']); 
            $sales_master_entry->cust_email_id 		=  mysqli_real_escape_string($_POST['cust_email_id']); 
            $sales_master_entry->cust_note 			=  mysqli_real_escape_string($_POST['cust_note']); 
            $sales_master_entry->created_date 		=  date("Y-m-d h:i:s"); 
            $sales_master_entry->is_deleted 		=  0; 

            $sales_master_entry_id=$sales_master_entry->insert();
            

			
			
            // Add Sales Book in Sale Entry tables
            $sales_book_count=count($_POST['p_book_id']);

            if($sales_book_count >0)
            {
                for($i_sale_book=0;$i_sale_book<=$sales_book_count-1;$i_sale_book++)
                {
                    $p_book_id          = $_POST['p_book_id'][$i_sale_book];
                    $p_book_price_id    = $_POST['p_book_price_id'][$i_sale_book];
                    $p_discount_per_id  = $_POST['p_discount_per_id'][$i_sale_book];
                    $p_quantity_id      = $_POST['p_quantity_id'][$i_sale_book];
                    $p_book_amount      = $_POST['p_book_amount'][$i_sale_book];
                    $location_id        = $_POST['location_id'][$i_sale_book];
                    
                    $sales_book_entry->sales_maste_id = $sales_master_entry_id;
                    $sales_book_entry->book_id = $p_book_id;
                    $sales_book_entry->book_mrp =$p_book_price_id;
                    $sales_book_entry->book_discount = $p_discount_per_id;
                    $sales_book_entry->book_sale_amount = $p_book_amount;
                    $sales_book_entry->book_quantity = $p_quantity_id;
                    $sales_book_entry->location_id = $location_id;


                    $sales_book_entry->insert();
					
					// Deduct the Book Quantity From Location
					
					$condition ="`book_id`='".$p_book_id."' AND `location_id`='".$location_id."'";
					
					$value_update = "book_quantity -".$p_quantity_id;
					
					$parameter= array("tbl_books_quantity_at_location","book_quantity",$condition,$value_update);
					
					 $re->funMinusTheBookQuantityFromLocation($parameter);
	

                }
            }
            
			
            
            $_SESSION['Message']=200;


            echo "<script>window.location='new_sales_entry.php';</script>";
	 break;

	 case 'edit_info':
	    
		$book_id=$_POST['update_id'];
		$condition =" AND tb.book_id =".$book_id;
		
		// Get Book Detail
		$books = new tbl_books();    
		$result_book_info = $books->getBookDetail($condition);
		$total_books = count($result_book_info);
		
		// Get Book Storage Location Detail 
		$location_detail = new tbl_book_quantity_at_location();    
		$location_info = $location_detail->selectStorageInfo($book_id);

	 break;

	 case 'delete_info':
	    
		$sale_entry_id=$_POST['update_id']; 
		$parameter=array('tbl_sales_master_entry','is_deleted','sales_maste_id',$sale_entry_id,'1');
		$re->funDoGenericAction($parameter);
		
		$_SESSION['Message']=202;
		echo "<script>window.location='manage_sales.php';</script>";
	
	 break;
	  	 
	 case 'view_detail':
	    
		$sale_entry_id=$_POST['update_id']; 
	 
		$sales_master_entry = new tbl_sales_master_entry();
		$sales_book_entry = new tbl_sales_book_entry();
		
		$condition= "sm.sales_maste_id=".$sale_entry_id;
		
		$sale_master_entry_detail = $sales_master_entry->getSalesEntryMasterDetail($condition);
		
		// Sales Book List
		$condition_sl =" tse.sales_maste_id=".$sale_entry_id;
		$sales_book_entry_list = $sales_book_entry->getSalesBookEntryList($condition_sl);
		
	 break;
	  	 
	 default:
	 
	  $sale_entry = new tbl_sales_master_entry();
	   
	  $user_id =  $_SESSION['user']['user_id']; 
	  $user_type =  $_SESSION['user']['user_type'];
	   
	   if($user_type==2)
	   {
	     $condition =" AND `user_id`=".$user_id;
	   }
	   else{
	    $condition ="";
	   }
	   
	   $result_sales_list = $sale_entry->getSalesList($condition);
	   $total_sale_entry = count($result_sales_list);
	 break;
 
}

?>