<?php
$step=isset($_POST['step']) ? $_POST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	 case 'add_binding_type':
	 case 'update_binding_type':
	 
	 
		$binding_type = new tbl_binding_type();
		$binding_type->title 	=  mysqli_real_escape_string($db_con->link,$_POST['binding_type']); 
		$binding_type->created_date =date("Y-m-d h:i:s"); 
		
		if($step=='update_binding_type')
		{
		 $binding_id 		= $_POST['binding_id'];
		 $binding_type->update($binding_id);       // update
	 	}
		else
		{
			$binding_type->is_deleted	= 0;
			$binding_type->type=$cover_type;
			$binding_type->insert();
			$_SESSION['Message']=1;

		}	
		if($cover_type==1)
		{
	 	 echo "<script>window.location='manage_binding_types.php';</script>";
	  	}
		else
		{
		 echo "<script>window.location='manage_coverage_types.php';</script>";
		}
	 break;

	 case 'edit_info':
	    
		$binding_id=$_POST['update_id'];
		$binding_type = new tbl_binding_type();
		$binding_type->selectSingleBindingType($binding_id);
	
	 break;

	 case 'delete_info':
	    
		$binding_id=$_POST['update_id']; 
		$parameter=array('tbl_binding_type','is_deleted','binding_id',$binding_id,'1');
		$re->funDoGenericAction($parameter);

		if($cover_type==1)
		{
	 	 echo "<script>window.location='manage_binding_types.php';</script>";
	  	}
		else
		{
		 echo "<script>window.location='manage_coverage_types.php';</script>";
		}
	
	 break;
	  	 
	 default:
		$binding_type = new tbl_binding_type();
		$total_record = $binding_type->CountBindingTypes($cover_type);
			
			$pages=$_REQUEST['pages'];
			$per_page = ITEMS_PER_PAGE;
			$page = 1;
			if($_REQUEST['pages']!="") {
				$page=$_REQUEST['pages'];
			}
			$start = ($page-1)*$per_page;

	 	$result_binding_list = $binding_type->select_binding_types($cover_type,$start,$per_page);
		
		$total_record = count($result_binding_list);
	 break;
 
}

?>