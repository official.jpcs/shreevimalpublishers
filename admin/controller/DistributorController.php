<?php
$step=isset($_POST['step']) ? $_POST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	 case 'update_distributor':
	 case 'add_distributor':
	 
	 
		$distributors = new tbl_distributors();
		
		$distributors->distributor_name =  mysqli_real_escape_string($db_con->link,$_POST['distributor_name']); 
		$distributors->distributor_contact_no 	=  mysqli_real_escape_string($db_con->link,$_POST['distributor_contact_no']); 
		$distributors->distributor_emailid 	=  mysqli_real_escape_string($db_con->link,$_POST['distributor_emailid']); 
		$distributors->distributor_website 	=  mysqli_real_escape_string($db_con->link,$_POST['distributor_website']); 
		$distributors->distributor_address 	=  mysqli_real_escape_string($db_con->link,$_POST['distributor_address']); 
		$distributors->distributor_state_id 	=  mysqli_real_escape_string($db_con->link,$_POST['distributor_state_id']); 
		$distributors->distributor_city_id 	=  mysqli_real_escape_string($db_con->link,$_POST['distributor_city_id']); 
		$distributors->zip_code 	=  mysqli_real_escape_string($db_con->link,$_POST['zip_code']); 
		$distributors->is_deleted ='0'; 
		
	
		
		
		if($step=='update_distributor')
		{
		 $distributor_id 	=  mysqli_real_escape_string($db_con->link,$_POST['distributor_id']); 
		 $distributors->update($distributor_id);       // update
	 	}
		else
		{
		$distributors->created_date =date("Y-m-d h:i:s"); 
			$distributors->insert();
			$_SESSION['Message']=1;

		}	
		
		
		 echo "<script>window.location='manage_distributors.php';</script>";
		
	 break;

	 case 'edit_info':
	    
		$distributor_id=$_POST['update_id'];
		$distributors = new tbl_distributors();
		//for Edit distributor
		$distributors->select($distributor_id);
		
		//for view distributor
		$distributors_view=$distributors->selectSingleDistributor($distributor_id);
		
	 break;

	 case 'delete_info':
	    
		$distributor_id=$_POST['update_id'];
		$distributors = new tbl_distributors();
		$distributors->delete($distributor_id);
		
	$_SESSION['Message']=3;

		 echo "<script>window.location='manage_distributors.php';</script>";
		
	 break;
	  	 
	 default:
		$distributors = new tbl_distributors();
		$total_record = $distributors->CountDistributors();
			
			$pages=$_REQUEST['pages'];
			$per_page = ITEMS_PER_PAGE;
			$page = 1;
			if($_REQUEST['pages']!="") {
				$page=$_REQUEST['pages'];
			}
			$start = ($page-1)*$per_page;

	 	$result_distributors_list = $distributors->AllDistributor($start,$per_page);
		$total_record = count($result_distributors_list);
		
	 break;
 
}

?>