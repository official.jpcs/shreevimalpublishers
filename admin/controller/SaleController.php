<?php
$step=isset($_POST['step']) ? $_POST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	case 'add_sale_entry':
	 
            $sales_master_entry = new tbl_sales_master_entry();
            $sales_book_entry = new tbl_sales_book_entry();
            $re    = new reuseFunction();
		
            $sales_master_entry->user_id 			=  $_SESSION['user']['user_id']; 
            $sales_master_entry->sale_date 			=  date("Y-m-d", strtotime(mysqli_real_escape_string($db_con->link,$_POST['sale_date']))); 

            $sales_master_entry->sales_type 		=  mysqli_real_escape_string($db_con->link,$_POST['sale_type']); 
            $sales_master_entry->tax_type 			=  mysqli_real_escape_string($db_con->link,$_POST['tax_type']); 
            $sales_master_entry->tax_amount 		=  mysqli_real_escape_string($db_con->link,$_POST['tax_amount']); 
            $sales_master_entry->total_amount 		=  mysqli_real_escape_string($db_con->link,$_POST['total_amount']); 
            $sales_master_entry->final_amount 		=  mysqli_real_escape_string($db_con->link,$_POST['final_amount']); 

            $sales_master_entry->customer_name 		=  mysqli_real_escape_string($db_con->link,$_POST['customer_name']); 
            $sales_master_entry->cust_contact_no 	=  mysqli_real_escape_string($db_con->link,$_POST['contact_no']); 
            $sales_master_entry->cust_address 		=  mysqli_real_escape_string($db_con->link,$_POST['cust_address']); 
            $sales_master_entry->cust_email_id 		=  mysqli_real_escape_string($db_con->link,$_POST['cust_email_id']); 
            $sales_master_entry->cust_note 			=  mysqli_real_escape_string($db_con->link,$_POST['cust_note']); 
            $sales_master_entry->created_date 		=  date("Y-m-d h:i:s"); 
            $sales_master_entry->is_deleted 		=  0; 

            $sales_master_entry_id=$sales_master_entry->insert();
            

			
			
            // Add Sales Book in Sale Entry tables
            $sales_book_count=count($_POST['p_book_id']);

            if($sales_book_count >0)
            {
                for($i_sale_book=0;$i_sale_book<=$sales_book_count-1;$i_sale_book++)
                {
                    $p_book_id          = $_POST['p_book_id'][$i_sale_book];
                    $p_book_price_id    = $_POST['p_book_price_id'][$i_sale_book];
                    $p_discount_per_id  = $_POST['p_discount_per_id'][$i_sale_book];
                    $p_quantity_id      = $_POST['p_quantity_id'][$i_sale_book];
                    $p_book_amount      = $_POST['p_book_amount'][$i_sale_book];
                    $location_id        = $_POST['location_id'][$i_sale_book];
                    
                    $sales_book_entry->sales_maste_id = $sales_master_entry_id;
                    $sales_book_entry->book_id = $p_book_id;
                    $sales_book_entry->book_mrp =$p_book_price_id;
                    $sales_book_entry->book_discount = $p_discount_per_id;
                    $sales_book_entry->book_sale_amount = $p_book_amount;
                    $sales_book_entry->book_quantity = $p_quantity_id;
                    $sales_book_entry->location_id = $location_id;


                    $sales_book_entry->insert();
					
					// Deduct the Book Quantity From Location
					
					$condition ="`book_id`='".$p_book_id."' AND `location_id`='".$location_id."'";
					
					$value_update = "book_quantity -".$p_quantity_id;
					
					$parameter= array("tbl_books_quantity_at_location","book_quantity",$condition,$value_update);
					
					 $re->funMinusTheBookQuantityFromLocation($parameter);
	

                }
            }
            
			
            
            $_SESSION['Message']=200;


            echo "<script>window.location='new_sales_entry.php';</script>";
	 break;

	 case 'edit_info':
	    
		$sale_entry_id=$_POST['update_id']; 
	 
		$sales_master_entry = new tbl_sales_master_entry();
		$sales_book_entry = new tbl_sales_book_entry();
		
		$condition= "sm.sales_maste_id=".$sale_entry_id;
		
		$sale_master_entry_detail = $sales_master_entry->getSalesEntryMasterDetail($condition);
		
		// Sales Book List
		$condition_sl =" tse.sales_maste_id=".$sale_entry_id;
		$sales_book_entry_list = $sales_book_entry->getSalesBookEntryList($condition_sl);

	 break;

	case 'update_sales_entry':
	 
            $sales_master_entry = new tbl_sales_master_entry();
            $sales_book_entry = new tbl_sales_book_entry();
            $re    = new reuseFunction();
			$total_amount =0;
		
            $sales_master_entry->user_id 			=  $_SESSION['user']['user_id']; 
			$sales_master_entry_id = $_POST["sales_maste_id"];

			// Revert Back Book Quantity
			$condition_sl =" tse.sales_maste_id=".$sales_master_entry_id;
			$sales_book_entry_list = $sales_book_entry->getSalesBookEntryList($condition_sl);
			
			if(count($sales_book_entry_list) >0)
			{
				for($i_sale_entry = 0;$i_sale_entry <=count($sales_book_entry_list)-1;$i_sale_entry++)
				{	
					$book_id = $sales_book_entry_list[$i_sale_entry]["book_id"];
					$book_quantity = $sales_book_entry_list[$i_sale_entry]["book_quantity"];
					$location_id = $sales_book_entry_list[$i_sale_entry]["location_id"];
					
					$condition ="`book_id`='".$book_id."' AND `location_id`='".$location_id."'";
					$value_update = "book_quantity +".$book_quantity;
					$parameter= array("tbl_books_quantity_at_location","book_quantity",$condition,$value_update);
					$re->funMinusTheBookQuantityFromLocation($parameter);
				}
				
				// Delete Previous Book Sale Entry From Table
				$parameter=array('tbl_sales_book_entry','sales_maste_id',$sales_master_entry_id);
				$re->funDoGenericActionDelete($parameter);
				
			}	
		

            // Add Sales Book in Sale Entry tables
            $sales_book_count=count($_POST['p_book_id']);

            if($sales_book_count >0)
            {

                for($i_sale_book=0;$i_sale_book<=$sales_book_count-1;$i_sale_book++)
                {
                    $p_book_id          = $_POST['p_book_id'][$i_sale_book];
                    $p_book_price_id    = $_POST['p_book_price_id'][$i_sale_book];
                    $p_discount_per_id  = $_POST['p_discount_per_id'][$i_sale_book];
                    $p_quantity_id      = $_POST['p_quantity_id'][$i_sale_book];
                    $p_book_amount      = $_POST['p_book_amount'][$i_sale_book];
                    $location_id        = $_POST['location_id'][$i_sale_book];
                    $total_amount    	= $total_amount + $p_book_amount;
					
					
                    $sales_book_entry->sales_maste_id = $sales_master_entry_id;
                    $sales_book_entry->book_id = $p_book_id;
                    $sales_book_entry->book_mrp =$p_book_price_id;
                    $sales_book_entry->book_discount = $p_discount_per_id;
                    $sales_book_entry->book_sale_amount = $p_book_amount;
                    $sales_book_entry->book_quantity = $p_quantity_id;
                    $sales_book_entry->location_id = $location_id;
						

                    $sales_book_entry->insert();
					
					// Deduct the Book Quantity From Location
					
					$condition ="`book_id`='".$p_book_id."' AND `location_id`='".$location_id."'";
					
					$value_update = "book_quantity -".$p_quantity_id;
					
					$parameter= array("tbl_books_quantity_at_location","book_quantity",$condition,$value_update);
					
					 $re->funMinusTheBookQuantityFromLocation($parameter);
					
                }
				
					
					$sales_master_entry->sale_date 			=  date("Y-m-d", strtotime($_POST['sale_date'])); 
					$sales_master_entry->sales_type 		=  $_POST['sale_type']; 
					$sales_master_entry->tax_type 			=  $_POST['tax_type']; 
					$sales_master_entry->tax_amount 		=  $_POST['tax_amount']; 
					$sales_master_entry->total_amount 		=  $total_amount; 
					$sales_master_entry->final_amount 		=  $re->calSalesFinalAmout($total_amount,$_POST['tax_amount']); 
		
					$sales_master_entry->customer_name 		=  $_POST['customer_name']; 
					$sales_master_entry->cust_contact_no 	=  $_POST['contact_no']; 
					$sales_master_entry->cust_address 		=  $_POST['cust_address']; 
					$sales_master_entry->cust_email_id 		=  $_POST['cust_email_id']; 
					$sales_master_entry->cust_note 			=  $_POST['cust_note']; 
					$sales_master_entry->created_date 		=  date("Y-m-d h:i:s"); 
					$sales_master_entry->is_deleted 		=  0; 
		
					$sales_master_entry_id=$sales_master_entry->update($sales_master_entry_id);
					
					$_SESSION['Message']=203;
					echo "<script>window.location='manage_sales.php';</script>";
            }
            
	 break;


	 case 'delete_info':
	    
		$sale_entry_id=$_POST['update_id']; 
		$parameter=array('tbl_sales_master_entry','is_deleted','sales_maste_id',$sale_entry_id,'1');
		$re->funDoGenericAction($parameter);
		
		$_SESSION['Message']=202;
		echo "<script>window.location='manage_sales.php';</script>";
	
	 break;
	  	 
	 case 'view_detail':
	    
		$sale_entry_id=$_POST['update_id']; 
	 
		$sales_master_entry = new tbl_sales_master_entry();
		$sales_book_entry = new tbl_sales_book_entry();
		
		$condition= "sm.sales_maste_id=".$sale_entry_id;
		
		$sale_master_entry_detail = $sales_master_entry->getSalesEntryMasterDetail($condition);
		
		// Sales Book List
		$condition_sl =" tse.sales_maste_id=".$sale_entry_id;
		$sales_book_entry_list = $sales_book_entry->getSalesBookEntryList($condition_sl);
		
	 break;
	  	 
	 default:
	 
	  $sale_entry = new tbl_sales_master_entry();
	   
	  $user_id =  $_SESSION['user']['user_id']; 
	  $user_type =  $_SESSION['user']['user_type'];
	  
	   
	   if($user_type==2)
	   {
	     $condition =" AND `user_id`=".$user_id;
	   }
	   else{
	    $condition =NULL;
	   }
	   
	   
	   
	   $total_record = $sale_entry->CountSalesMasterBookEntry();
			
			$pages=$_REQUEST['pages'];
			$per_page = ITEMS_PER_PAGE;
			$page = 1;
			if($_REQUEST['pages']!="") {
				$page=$_REQUEST['pages'];
			}
			$start = ($page-1)*$per_page;

	   $result_sales_list = $sale_entry->getSalesList($condition,$start,$per_page);
	  // $result_sales_list = $sale_entry->getSalesList($start,$per_page);
	   $total_sale_entry = count($result_sales_list);
	 break;
 
}

?>