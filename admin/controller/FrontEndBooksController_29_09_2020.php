<?php
$step=isset($_REQUEST['step']) ? $_REQUEST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	
	case "add_to_cart":
	$_POST["quantity"] =1; 
	$book_id= $_REQUEST['code'];

		if(!empty($book_id)) 
		{

			// Get Book Detail
			$condition =" AND tb.book_id =".$book_id;
			$books1 = new tbl_books();    
			$result_book_info = $books1->getFroentBookDetail($condition);

			if($result_book_info[0]['book_thumbnail']!=BOOK_THUMBNAIL_FOLDER_HTTP)
			$book_img = $result_book_info[0]['book_thumbnail'];
			else
			$book_img = BOOK_THUMBNAIL_FOLDER_HTTP."default.png";			

			$itemArray = array($result_book_info[0]["book_id"]=>array('name'=>$result_book_info[0]['book_title'], 'code'=>$result_book_info[0]['book_id'], 'quantity'=>'1', 'price'=>$result_book_info[0]['book_mrp'], 'image'=>$book_img));
			
			
			if(!empty($_SESSION["cart_item"])) 
			{
				if(in_array($result_book_info[0]["book_id"],array_keys($_SESSION["cart_item"]))) {
					foreach($_SESSION["cart_item"] as $k => $v) {
							if($result_book_info[0]["book_id"] == $k) {
								if(empty($_SESSION["cart_item"][$k]["quantity"])) {
									$_SESSION["cart_item"][$k]["quantity"] = 0;
								}
								$_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
							}
					}
				} else {
					$_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
				}
			} else {
				$_SESSION["cart_item"] = $itemArray;
			}

			header("Location: category_detail.php?catid=".$_REQUEST['catid']); 
		}
	break;

	case "remove_from_cart":
		if(!empty($_SESSION["cart_item"])) {
			foreach($_SESSION["cart_item"] as $k => $v) {
					if($_GET["code"] == $k)
						unset($_SESSION["cart_item"][$k]);				
					if(empty($_SESSION["cart_item"]))
						unset($_SESSION["cart_item"]);
			}
		}
	break;
	case "empty_cart":
		unset($_SESSION["cart_item"]);
	break;	

	 default:
	 
			$pages=$_REQUEST['pages'];
			$per_page = ITEMS_PER_PAGE;
			$page = 1;
			if($_REQUEST['pages']!="") {
				$page=$_REQUEST['pages'];
			}
			$start = ($page-1)*$per_page;
	 
	 
	 	//echo $_REQUEST['search']; die;
		// Search Book
		if($_REQUEST['search']!=""){
		
		$search_book=trim($_REQUEST['search']);
		$condition=" AND `book_title` LIKE '%$search_book%'";
		
		$condition.="AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
	
		$books = new tbl_books();//getBookList
		$search_result_book_list = $books->getSearchBookList($condition,$start,$per_page);
		$totalCountSearch = $books->getSearchBookListCount($condition);
	 	
		}
		
		$books = new tbl_books();//getBookList
		$result_book_list = $books->getBookList();
	 	$total_books = count($result_book_list);
		
		
		$booksMostSalable = new tbl_books();//getBookListMostSalable
		
		$total_MostSalableCount = $booksMostSalable->getBookListMostSalableCount();
			
			

			
		
		
		$result_booksMostSalable_list = $booksMostSalable->getBookListMostSalable($start,$per_page);
		$total_booksMostSalable = count($result_booksMostSalable_list);
		
		
		
		
		
		$booksUpcomingBook = new tbl_books();//getBookListUpcomingBook
		$result_booksUpcomingBook_list = $booksUpcomingBook->getBookListUpcomingBook();
		$total_booksUpcomingBook = count($result_booksUpcomingBook_list);
		
		
		$bookCategory = new tbl_book_categories();//getBookCategoryList
		$result_bookCategory_list = $bookCategory->select_categories();
		$total_result_bookCategory = count($result_bookCategory_list);
		
		
		if(isset($_GET['catid']) && $_GET['catid']!=""){
		$bookListCategoryWise = new tbl_books();// getBookList Category Wise
		$category_id=$_GET['catid'];
		$condition=" AND tb.category_id=$category_id";
		$totalCountCategory=$bookListCategoryWise->getBookListCategoryWiseCount($condition);
		$bookListCategoryWise=$bookListCategoryWise->getBookListCategoryWise($condition,$start,$per_page);
		
		}
		
		if(isset($_GET['id']) && $_GET['id']!=""){
		$book_id=$_GET['id'];
		$condition =" AND tb.book_id =".$book_id;
		
		$condition.="AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
	
		
		// Get Book Detail

		$books1 = new tbl_books();    
		$result_book_info = $books1->getFroentBookDetail($condition);
		
		
		$bookListCategoryWise = new tbl_books();// getBookList Category Wise
		$category_id=$result_book_info[0]['category_id'];
		$condition=" AND tb.category_id=$category_id";
		
		$condition.="AND (SELECT SUM(tq.book_quantity) FROM tbl_books_quantity_at_location tq WHERE tq.book_id=tb.`book_id`) > (SELECT books_in_stock_below_count FROM tbl_settings)";
	
		//$bookListCategoryWise=$bookListCategoryWise->getBookListCategoryWise($condition);
		
		}
		
		
		$distributors = new tbl_distributors();//getALL Distributors List
		$result_distributors_list = $distributors->AllDistributor();
		$total_distributors = count($result_distributors_list);
		
		$distributors = new tbl_distributors();//getALL Distributors List city wise
		$result_distributors_list_citywise = $distributors->AllDistributorCitywise();
		$total_distributors_citywise = count($result_distributors_list);
		
		
	 break;
 
}

?>