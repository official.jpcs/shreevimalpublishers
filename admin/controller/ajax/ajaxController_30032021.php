<?php
$step=$_REQUEST['action'] ;

/* Including Globally Declared Variables */
$page_loc ="Front_end";

include("../../config/config.php");

switch($step)
{
	 case 'add_contact':
	
		$include_files =array("js"=>array() ,
					  "css" =>array() ,
					  "model"=>array("tbl_contact")
					  );
		include_once(CONFIG_CLASS_PATH . "class.php");
		
		$contact = new tbl_contact();
		$contact->contact_name 		=  mysqli_real_escape_string($_REQUEST['contact_name']); 
		$contact->contact_no 		=  mysqli_real_escape_string($_REQUEST['contact_no']); 
		$contact->contact_email 	=  mysqli_real_escape_string($_REQUEST['contact_email']); 
		$contact->contact_address 	=  mysqli_real_escape_string($_REQUEST['contact_address']); 
		$contact->contact_message 	=  mysqli_real_escape_string($_REQUEST['contact_message']); 
		$contact->is_deleted		= 0;
		$contact->created_date		=  date("Y-m-d h:i:s"); 
		$res=$contact->insert();
		
	
			// ********* Send Email To SVVPL **********//
			
				$subject ="Contact Information";
				$email_message = '<html>
										<body>
										Hi '.$_REQUEST['contact_name'].',<br><br>
										 Thanks for Mail,
										<br><br>

										<table>
											<tr><td style="vertical-align:top;">Name</td> 
											<td style="vertical-align:top;">Name</td>
											</tr>
											<tr><td style="vertical-align:top;">Email</td> 
											<td style="vertical-align:top;">'.$_REQUEST['contact_email'].'</td>
											</tr>
											<tr><td style="vertical-align:top;">Contact No</td> 
											<td style="vertical-align:top;">'.$_REQUEST['contact_no'].'</td>
											</tr>
											<tr><td style="vertical-align:top;">Address</td> 
											<td style="vertical-align:top;">'.$_REQUEST['contact_address'].'</td>
											</tr>
											<tr><td style="vertical-align:top;">Message</td> 
											<td style="vertical-align:top;">'.$_REQUEST['contact_message'].'</td>
											</tr>
											
										</table>
										<br>
										Thanks,<br>
										Team Vimal Publisher
										</body>
								</html>';
				
				
				
            $to      = "info@shreevimalpublishers.com";
            $subject = "Contact Information";

            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

            // Additional headers
            $headers .= 'To: '.$to. "\r\n";
            $headers .= 'From: '.$_REQUEST['contact_email']. "\r\n";
			$headers.= "X-Priority: 1\r\n"; 
			
            // Mail it
			 mail($to, $subject, $email_message, $headers);
			
			// ********* Send Email To Associate **********//

		if($res!=0)
		$contact_reply=array("contact_reply"=>"success");
		else
		$contact_reply=array("contact_reply"=>"Failed");
		echo json_encode($contact_reply);

	 break;
	 case 'load_publisher_info':
		
		$include_files =array(
                                        "model"=>array("tbl_authors_publishers")
                                        );
		
		include_once(CONFIG_CLASS_PATH . "class.php");

		$publisher_id=$_REQUEST['publisher_id'];
	
			// Get Stations List
		$publisher_list = new tbl_authors_publishers();
	    $publisher_list->selectSinglePubAuthor($publisher_id);
		
		echo $publisher_list->email_id."~".$publisher_list->address."~".$publisher_list->website."~".$publisher_list->contact_no;
	 break;

	 case 'party_connected_train':
		$AddClass = array('reuse','tbl_party_connected_train');
		include_once(CONFIG_CLASS_PATH . "class.php");

		$party_id=$_REQUEST['party_id'];

		$tbl_party_connected_train = new tbl_party_connected_train();
			
		// Get Stations List
		$result_train_list = $tbl_party_connected_train->selectAllConnectedTrain($party_id);
		$total_record = count($result_train_list);
		$opt= "<option value=''>-- Select Train --</option>";		
		 if($total_record >0)
		 {
		  	
			for($i_train=0;$i_train <=$total_record-1;$i_train++)
			{
			 $opt.= "<option value='".$result_train_list[$i_train]['train_id']."'>".$result_train_list[$i_train]['train_name']."(#".$result_train_list[$i_train]['train_no'].")</option>";
			}
		  
		 }			
		
		echo $opt;	  
	 break;

	 case 'edit_info':
	    
		$user_id=$_REQUEST['update_id'];
		$user = new tbl_users();
		$user->selectSingleUser($user_id,'','');
	
	 break;

	 case 'delete_info':
	    
		$user_id=$_REQUEST['update_id']; 
		$user = new tbl_users();
		$user->deleteUser($user_id);
		echo "<script>window.location='manage_users.php';</script>";
	
	 break;

	 case 'user_login':
	    
		$user_name=$_REQUEST['user_name']; 
		$password=md5($_REQUEST['password']); 
		
		$user = new tbl_users();
		$login_status = $user->selectSingleUser('',$user_name,$password);
		
		if($login_status=='0')
		{
		$_SESSION['Message']=1;
		echo "<script>window.location='index.php';</script>";
		}
		else
		{
		 
		echo "<script>window.location='manage_party.php';</script>";
		}
	
	 break;

	 case 'book_detail':
		
            $include_files =array(
                                    "model"=>array("tbl_authors_publishers","tbl_books","tbl_book_quantity_at_location")
                                    );

            include_once(CONFIG_CLASS_PATH . "class.php");

            $book_id=$_REQUEST['book_id'];
            
            $condition =" AND tb.book_id =".$book_id;
            
            // Get Book Detail
            $books = new tbl_books();    
            $result_book_info = $books->getBookDetail($condition);
            $total_books = count($result_book_info);
            
           echo json_encode($result_book_info);
            
	 break;
         
	 case 'book_storage_detail':
		
            $include_files =array(
                                    "model"=>array("tbl_books","tbl_book_quantity_at_location")
                                    );

            include_once(CONFIG_CLASS_PATH . "class.php");

            $book_id=$_REQUEST['book_id'];
            
            // Get Book Storage Location Detail 
            $location_detail = new tbl_book_quantity_at_location();    
            $location_info = $location_detail->selectStorageInfo($book_id);
             
           for($i_loc=0;$i_loc <=count($location_info)-1;$i_loc++)
            {
              
                $storage_list.='<div class="form-group">';	
                $storage_list.='<div class="col-md-6 book_info_lbl">'.$location_info[$i_loc]['location'].' :</div>';
                $storage_list.=  '<div class="col-md-8">'.$location_info[$i_loc]['quantity'].'</div>';
                $storage_list.='</div>';
                $storage_list.= '<div class="clear"></div>';
                
            }
           echo $storage_list;
            
	 break;

	 case 'autosearch_book_list':
		
            $include_files =array(
                                    "model"=>array("tbl_books","reuse")
                                    );

            include_once(CONFIG_CLASS_PATH . "class.php");

            echo $book_title=$_REQUEST['title'];
            die;
            // Auto Search Book Title 
            $result = $re->funSmartSearch($book_title);             
           
		   var_dump($result);die; 
	 break;
     
        case 'smartAutoSearch': 
           
           
            $include_files =array(
                                    "model"=>array("tbl_books","reuse")
                                    );

            include_once(CONFIG_CLASS_PATH . "class.php");
		
                
                $book_title       = $_GET["keyword"];
                $shop_locatnion_id  =  $_SESSION['user']['location_id']; 
			    
			   
                $condition =" AND tq.location_id ='".$shop_locatnion_id."' AND tb.book_title LIKE '%".$book_title."%' AND tq.book_quantity >0";

                // Get Book Detail
                $books = new tbl_books();    
                $result_data = $books->getBookSmartSearch($condition);
                $total_books = count($result_data);

           		//var_dump($result_data);
                
                
      
                $string = '';
                $string.= '<ul class="smart_search_ul_class autoSearchbook" style="">';
                    if(count($result_data) > 0) 
                    {

                    foreach ($result_data as $key => $value) 
                      {
                        

                        $string.= '<li class="smart_search_li_class select_book_click" value="'.$value["book_id"]."~".$value["book_title"]."~".$value["book_mrp"].'">'.$value["book_title"]." , <span class='author_class'> By ".$value["author_name"] .'</span></li>';

                      }
                      
                    } 
                    else 
                    {
                    $string.= '<li class="smart_search_li_class">No Record found</li>';
                    }
                    $string.= '</ul>';

                    echo $string;
                    
                     

	break;
	case 'Newsletter' :
	
	 $include_files =array(
                                    "model"=>array("tbl_newsletter_subscriber")
                                    );

            include_once(CONFIG_CLASS_PATH . "class.php");
		
	//print($_REQUEST['email']);
	$newsletter_subscriber = new tbl_newsletter_subscriber();
	$newsletter_subscriber->subscriber_email=$_REQUEST['email'];
	$newsletter_subscriber->subscription_date= date("Y-m-d h:i:s"); 

	$is_exist =  $newsletter_subscriber->chkAlreadySubscribe();

	if($is_exist > 0)
	{
	  $subscriber=array("subscription"=>"error","message"=>"Subscription for this email id already exist.");			
	}
	else
	{
		$subscriber_id=$newsletter_subscriber->insert();
		
		if($subscriber_id>0)
		$subscriber=array("subscription"=>"success");
		else
		$subscriber=array("subscription"=>"error");
	}

	echo json_encode($subscriber);

	break;
     
   	 default:
		$user = new tbl_users();
	 	$result_user_list = $user->select_user();
		$total_record = count($result_user_list);
	 break;
 
	case 'city_list':
	
		$include_files =array(
					"model"=>array("tbl_cities")
		  );

		include_once(CONFIG_CLASS_PATH . "class.php");

		$state_id=$_REQUEST['state_id'];

		$city_list = new tbl_cities();

		// Get Stations List
		$result_city_list = $city_list->select_city_list($state_id);

		$total_record = count($result_city_list);

		$opt= "<select name='distributor_city_id' id='distributor_city_id' class='form-control' >";
		$opt.= "<option value='0'>--------- Select City ---------</option>";		
		if($total_record >0)
		{

		for($i_city=0;$i_city <=$total_record-1;$i_city++)
		{
		$opt.= "<option value='".$result_city_list[$i_city]->id."'>".$result_city_list[$i_city]->name."</option>";
		}

		}			
		$opt.="</select>";

		echo $opt;	  
		
	break;

	case 'delete_book_image':

		$book_id=$_REQUEST['book_id'];
		$image_id=$_REQUEST['image_id'];

		$include_files =array(
			 "model"=>array("tbl_books")
		  );

		include_once(CONFIG_CLASS_PATH . "class.php");

		$books = new tbl_books();
		$books->deleteBookImage($book_id,$image_id);
		echo "success";

	break;

	case 'update_image_position':

		$image_id=$_REQUEST['image_id'];
		$position=$_REQUEST['position'];

		$include_files =array(
			 "model"=>array("tbl_books")
		  );

		include_once(CONFIG_CLASS_PATH . "class.php");

		$books = new tbl_books();
		$books->updateBookImagePosition($image_id,$position);
		echo "success";

	break;

	case 'delete_book_image':

		$book_id=$_REQUEST['book_id'];
		$image_id=$_REQUEST['image_id'];

		$include_files =array(
			 "model"=>array("tbl_books")
		  );

		include_once(CONFIG_CLASS_PATH . "class.php");

		$books = new tbl_books();
		$books->deleteBookImage($book_id,$image_id);
		echo "success";

	break;

	case 'update_image_position':

		$book_id=$_REQUEST['book_id'];
		$image_id=$_REQUEST['image_id'];
		$position=$_REQUEST['position'];

		$include_files =array(
			 "model"=>array("tbl_books")
		  );

		include_once(CONFIG_CLASS_PATH . "class.php");

		$books = new tbl_books();
		$books->updateBookImagePosition($image_id,$position);
		echo "success";

	break;



}

?>