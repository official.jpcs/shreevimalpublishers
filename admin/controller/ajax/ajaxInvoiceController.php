<?php
$step=$_REQUEST['action'] ;

/* Including Globally Declared Variables */
$page_loc ="Front_end";

include("../../config/config.php");

include_once("../../pdf_generate/mpdf60/mpdf.php");


$include_files =array(
			  "model"=>array("tbl_order_detail","tbl_order")
			  );
include_once(CONFIG_CLASS_PATH . "class.php");


$order = new tbl_order();


$order_id=$_REQUEST['order_id'];
$courier_company=$_REQUEST['courier_company'];
$dispatch_date=$_REQUEST['dispatch_date'];
$tracking_id=$_REQUEST['tracking_id'];

$hsn_code ="49011010";




/************ Generate Invoice PDF *****************/

$mpdf=new mPDF('c','A2');
$invoicepdf= "";
$message ="";

$total_qty =0;
$total_rate=0;
$total_discount = 0; 
$total_taxable_amount = 0; 



$order_detail = new tbl_order_detail();
$order_product_detail = new tbl_order_detail();
$order_detail = $order_detail->selectOrdersDetail($order_id);

$order_product_detail = $order_product_detail->selectOrdersProductDetail($order_id);


$to_customer=$order_detail[0]['email_address'];  
$customer_name = $order_detail[0]['customer_name'];

$discount_amount= $order_detail[0]['discount_amount'];


 $invoicepdf='
<div style="width:100%;"> 

<!-- Main content -->
<section Style="min-height: 250px; padding: 5px;padding-left: 5px;padding-right: 5px;">

<!-- row -->
<div class="margin-right: 5px;margin-left: 5px;width:100%">

<!--col-sm-12 -->
<div style="width: 100%;">

<div style="width: 100%;text-align:center">
	<span style="color:blue;font-size:25px;text-align:center">
		Tax Invoice
	</span>
</div>

<!--box -->
<div style="position: relative;border-radius: 3px;background: #ffffff;margin-bottom: 5px;width: 100%;">

<!--box-body -->
<div style="border-top-left-radius: 0;border-top-right-radius: 0; border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;padding: 15px;">


<!-- row -->
<div class="margin-right: -15px;margin-left: -15px;">

<!--col-sm-12 -->
<div style="width: 100%;">
<!--col-sm-9 -->
<div style="position:absolute">
	<img src="http://shreevimalpublishers.com/admin/images/logo.png" width="120px" style="text-align:left">
</div>
<div style="width: 100%;float: right;text-align:center;line-height: 1.5;">

<b>SHREE VIMAL PUBLISHERS PRIVATE LIMITED </b><br/>
 Opposite to Poornawad Bhavan, 
Parner-414302, Dist.- Ahmednagar, Maharashtra<br/>
Mob No.: 9226885522 , sales@shreevimalpublishers.com , www.shreevimalpublishers.com<br/>
GSTIN: 27AARCS6147G2ZU , 
PAN No. AARCS6147G , CIN: U22219PN2012PTC143528
<br>

</div>';

$invoicepdf.='<div style="float:none;clear:both;margin-top:10px;"><br/>

<hr style="margin-top: 5px;margin-bottom: 5px;border: 0; border-top: 1px solid gray;" />

<table id="example2" style=" width: 100%;">



</tr>


<tr>
	<td style="line-height: 1.5;"><b>Bill To,</b> <br/><b>'.$order_detail[0]['customer_name'].'</b><br/>'.$order_detail[0]['address_line1']. ",".$order_detail[0]['address_line2'].'<br/>'.$order_detail[0]['town'].",".$order_detail[0]['taluka']."-".$order_detail[0]['pin_code'].", ".$order_detail[0]['district'].'<br/>'.$order_detail[0]['mobile_no'].'</td>
	<td style="line-height: 1.5;text-align:right">Place of supply: 27-Maharashtra <br/>INVOICE NO.: '.$order_detail[0]['invoice_no'].'<br/>DATE : <b>'.date("d-m-Y", strtotime($order_detail[0]['order_date'])).'</b><br/></td>
</tr>

</table>

<br>
<div style="width: 100%; display: inline-block;float:left;"></div>

</div>


</div><!-- /.row -->


<div style="float:none;clear:both;">


<!-------------------- Row 2 start --------------->
<hr style="margin-top: 10px;margin-bottom: 10px;border: 0; border-top: 1px solid gray">';

'<!-- row -->
<div class="margin-right: -15px;margin-left: -15px;">

<!--col-sm-12 -->
<div style="width: 100%;">';



$invoicepdf.='<table id="example2"  class=" table-responsive table table-bordered table-hover" border="1" style="border:1px solid;"  >
				
			<thead>
			  <tr>
				<th colspan="12" style="background-color: rgba(47, 79, 79, 0.21);text-align:left;padding:5px;">Order Detail</th>
			  </tr>
			<tr>
				<th style="text-align:left;width:20px;">#</th>
				<th style="text-align:left;width:220px;">Item Name</th>
				<th style="text-align:left;width:80px;" >HSN/SAC</th>				
				<th style="text-align:center;width:50px;">Quantity</th>
				<th style="text-align:center;width:100px;">Price/ Unit</th>
				<th style="text-align:center;width:100px;">Discount</th>
				<th style="text-align:center;width:100px;">Taxable Amount</th>
				<th style="text-align:center;width:100px;">CGST</th>
				<th style="text-align:center;width:100px;">SGST</th>
				<th style="text-align:center;width:100px;">Amount</th>
				
				
			</tr>
             </thead>              
			 <tbody>';			
			 
            for($i=0;$i<count($order_product_detail);$i++)
            {
            	$sr =$i+1;


            	if($discount_amount==null || discount_amount=="")
            	{
	            	$disc_per = 0;  //0% Discount
            	}
            	else
            	{
	            	$disc_per = 15;  //15% Discount
            	}

            	
            	$book_quantity = $order_product_detail[$i]['product_quantity'];
            	$book_rate = $order_product_detail[$i]['product_rate'];

            	$book_n_qty_amount =$book_rate*$book_quantity;

            	$book_discount = ($book_n_qty_amount * $disc_per)/100;

            	$book_taxable_amt = $book_n_qty_amount - $book_discount;

            	
            	## Total

            	$total_qty = $total_qty+$book_quantity; 

            	$total_discount = $total_discount+$book_discount; 
            	$total_taxable_amount = $total_taxable_amount+$book_taxable_amt; 

            	


            	
			$invoicepdf.='<tr>
				<td>'.$sr.'</td>

				<td >'.$order_product_detail[$i]['product_title'].'</td>
				<td>'.$hsn_code.'</td>
				
				<td style="text-align:center">'.$book_quantity.'</td>
				<td style="text-align:center"> Rs. '.sprintf("%01.2f",$book_rate).'</td>
				<td style="text-align:center"> Rs. '.sprintf("%01.2f",$book_discount).' ('.$disc_per.'%)'.'</td>
				<td style="text-align:center"> Rs. '.sprintf("%01.2f",$book_taxable_amt).'</td>
				<td style="text-align:center"> Rs. 0.00 (0%)</td>
				<td style="text-align:center"> Rs. 0.00 (0%)</td>
				<td style="text-align:center"> Rs. '.sprintf("%01.2f",$book_taxable_amt).'</td>
			</tr>';
			
            }


$invoicepdf.='<tr>
				<th style="text-align:center;width:20px;font-weight:bold">#</th>
				<th style="text-align:center;width:220px;font-weight:bold">Total</th>
				<th style="text-align:center;width:80px;font-weight:bold" >&nbsp;</th>				
				<th style="text-align:center;width:100px;font-weight:bold">'.$total_qty.'</th>
				<th style="text-align:center;width:100px;font-weight:bold">&nbsp;</th>
				<th style="text-align:center;width:100px;font-weight:bold">  Rs.'.sprintf("%01.2f",$total_discount).'</th>
				<th style="text-align:center;width:100px;font-weight:bold"> Rs.'.sprintf("%01.2f",$total_taxable_amount).'</th>
				<th style="text-align:center;width:100px;font-weight:bold"> Rs.0.00</th>
				<th style="text-align:center;width:100px;font-weight:bold"> Rs.0.00</th>
				<th style="text-align:center;width:100px;font-weight:bold">  Rs.'.sprintf("%01.2f",$total_taxable_amount).'</th>
				
			</tr>';
 						
			
		 $invoicepdf.=' </tbody>
		  </table>';

		$invoicepdf.='<table style="width: 100%">
                    <tr>
                        
                        <td colspan="4"  style="text-align: left;">';
                            
                             if($order_detail[0]['payment_status']==1)
                             {
                             
                            $invoicepdf.='<span style="color:green;font-weight:bold">Your Payment in Completed </span>';
                             }
                             else
                             {
                             
                             $invoicepdf.='<span class="text-red text-bold">Your Payment in Pending </span>'; 
                             }
                           
                        $invoicepdf.='</td>
                        <td colspan="3"  style="text-align: right;"><br>Sub Total 	: Rs. '. sprintf("%01.2f",$total_taxable_amount).' /- </td>

                    </tr>
                  
                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    	<td colspan="3">&nbsp;</td>      
						<td colspan="4" style="text-align: right;"><br>Postal Charges 	:  Rs.'.sprintf("%01.2f", $order_detail[0]['postage_charges']).' /- </td>

                    </tr>

                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    	<td colspan="3">&nbsp;</td>      
						<td colspan="4" style="text-align: right;color:blue;font-size:16px;"><br><b>Final Amount Paid 	:  Rs. '.sprintf("%01.2f", $order_detail[0]['final_amount_paid']).' /-</b></td>

                    </tr>

                </table><hr/>';		  

	$invoicepdf.='<table id="example2" border="1" class=" table-responsive table table-bordered table-hover" width="100%" style="width:100%" >
					
				<thead>
				  <tr>
					<th colspan="9" style="background-color: rgba(47, 79, 79, 0.21);text-align:left;padding:5px;">Dispatch Detail</th>
				  </tr>
				<tr>
					<th width="30%" style="text-align:left">Courier Company</th>
					<th width="30%" style="text-align:left">Dispatch Date</th>
					<th width="30%" style="text-align:left">Tracking Id</th>
				</tr>
	             </thead>              
				 <tbody>	
				  <tr>
					<td width="30%">'.$courier_company.'</td>
					<td width="30%">'.$dispatch_date.'</td>
					<td width="30%">'.$tracking_id.'</td>
				  </tr>
			    </tbody>
			  </table>';
	$invoicepdf.='<div style="float:none;clear:both;">



<div style="float:none;clear:both;">

<div style="float:none;clear:both;">


<!-------------------- Row 2 start --------------->
<hr style="margin-top: 20px;margin-bottom: 20px;border: 0; border-top: 1px solid #1f1b1b;">

<!-- row -->
<div class="margin-right: -15px;margin-left: -15px;">

<!--col-sm-12 -->

<div style="width: 100%; display: inline-block;float:left;text-align:center">
<p>SUBJECT TO PUNE JURISDICTION</p>
<p>This is a Computer Generated Invoice</p>
<p>Declaration : We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</p>
<br><br>
</div>

</div><!--/.row -->

</div><!--/.box-body -->
</div><!-- /.box -->



</div><!-- /.col-sm-12 -->
</div><!-- /.row -->
</section><!-- /.Main content -->
</div><!-- /.content-wrapper -->';

//echo $invoicepdf; die;

$mpdf->WriteHTML($invoicepdf);
ob_clean(); 
$file_name= $invoice_for_month."/".$invoice_Customer_Detail['first_name']."_".$invoice_Customer_Detail['last_name']."_".$invoice_Customer_Detail['mobile_no']."_Invoice.pdf";
$invoice_file_name= $invoice_Customer_Detail['first_name']."_".$invoice_Customer_Detail['last_name']."_".$invoice_Customer_Detail['mobile_no']."_Invoice.pdf";

//Create Folder if Not Exist
if (!file_exists("../../save_invoice_pdf/".$invoice_for_month)) 
{
	mkdir("../../save_invoice_pdf/".$invoice_for_month, 0777, true);
}

$invoice_pdf = "../../save_invoice_pdf/".$invoice_file_name;
$mpdf->Output($invoice_pdf,'F'); 

unset($mpdf);

/************ Generate Invoice PDF *****************/


/******** Update Invoice & Courier Detail ********/

$order->courier_company=$courier_company;
$order->dispatch_date=$dispatch_date;
$order->tracking_id=$tracking_id;
$order->invoice_file_name=$invoice_file_name;
$order->updated_date=date("Y-m-d h:i:s");


$order->updateOrderDispatchDetailnStatus($order_id);


/******** Update Invoice & Courier Detail End ********/


/************ Send Email To Customer *****************/
$subject ="Shree Vimal Publishers - You Order Invoice";

//$message = funSetEmailContent($customer_name,$invoice_for_month);
$message = "Hi, Your Invoice for Order";

/**** Get Email Content *****/
$file_content= file_get_contents("../includes/email_content/email_content.html");
$bodymessage =$message;
//$bodymessage = str_replace(DESCRIPTION, $message, $file_content);


require("../../includes/sendmail.php");

/************ Send Email To Customer *****************/


?>