<?php
$search_action=$_REQUEST["action"];


$key=$_REQUEST["key"];

$db_con = new Database();
$db_con->OpenLink();

switch($search_action)
{

	 case 'search':
	 case 'export':
	    
		  if($_REQUEST["sale_from_date"] !="" && $_REQUEST["saleToDate"]!="")
		  {
		  	$condition .= " AND  tsm.`sale_date` >='". date("Y-m-d" ,strtotime($_REQUEST["sale_from_date"])) ."' AND tsm.`sale_date` <='". date("Y-m-d" ,strtotime($_REQUEST["saleToDate"])) ."' ";
		  }
		  else if($_REQUEST["sale_from_date"]!="")
		  {
		    $condition .= " AND  tsm.`sale_date` >='". date("Y-m-d" ,strtotime($_REQUEST["sale_from_date"])) ."'";
		  }
		  else if($_REQUEST["saleToDate"]!="")
		  {
		     $condition .= " AND  tsm.`sale_date` <='". date("Y-m-d" ,strtotime($_REQUEST["saleToDate"])) ."'";
		  }

		  
	
		  if($_REQUEST["sel_location"]!="")
		  {
		  	$condition .= " AND  tse.location_id=".$_REQUEST['sel_location'];
		  }
	
		  if($_REQUEST["search_book_name"]!="")
		  {
		   $condition .= " AND  tb.`book_title` ='". mysqli_real_escape_string($db_con->link,$_REQUEST['search_book_name']) ."'";
		  }
	
		$sale_entry = new tbl_sales_master_entry();
		
		$salesReportList = $sale_entry->getSalesReport($condition);
		$report_count = count($salesReportList);
		
		if($search_action=="export")
		{
					$objPHPExcel = new PHPExcel();
					
					$exportDirPath = EXPORT_DIR_PATH;
					$path = $exportDirPath.$filename;
					
					// create a new workbook For error report
					$workbook = new PHPExcel();
					
					// creating the Orders worksheet
					
					$file_title = "Sales Report";
					$workbook->setActiveSheetIndex(0);
					$worksheet = $workbook->getActiveSheet();
					$worksheet->setTitle($file_title);
					$worksheet->freezePaneByColumnAndRow( 1, 2 );

			

                    // Set the column widths
                    $j = 0;
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(strlen('Sr No')+1);
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('Date'),32)+1);
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('Location'),32)+1);
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('Book Title'),12)+1);
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('Price'),12)+1);
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('Discount'),12)+1);										
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('Quantity'),35)+1);
                    $worksheet->getColumnDimensionByColumn($j++)->setWidth(max(strlen('Total Amount'),12)+1);


                    // The heading row
                    $k = 1;
                    $j = 0;
                    $re->setCell( $worksheet, $k, $j++, 'Sr No', $boxFormat );
                    $re->setCell( $worksheet, $k, $j++, 'Date', $boxFormat );
                    $re->setCell( $worksheet, $k, $j++, 'Location', $boxFormat );
                    $re->setCell( $worksheet, $k, $j++, 'Book Title', $boxFormat );

                    $re->setCell( $worksheet, $k, $j++, 'Price', $boxFormat );
                    $re->setCell( $worksheet, $k, $j++, 'Discount', $boxFormat );
					
                    $re->setCell( $worksheet, $k, $j++, 'Quantity', $boxFormat );
                    $re->setCell( $worksheet, $k, $j++, 'Total Amount', $boxFormat );


                    $worksheet->getRowDimension($k)->setRowHeight(25);

                   	if($report_count >0)
                    {     
					   for($i_report=0;$i_report <=$report_count-1;$i_report++)
					   {
					   
							//Code for Set row for write data into excel
							$k += 1;    
							$j = 0;
							$worksheet->getRowDimension($k)->setRowHeight(13);
							$sr=$i_report+1;

							// Code for Prepair each column values to write into excel 
							$sr_no = $sr;
							$sale_date= date("d-M-Y", strtotime($salesReportList[$i_report]['sale_date']));
							$location_addr = $salesReportList[$i_report]['location_addr'];
							$book_title = $salesReportList[$i_report]['book_title'];
							
							$book_mrp = $salesReportList[$i_report]['book_mrp'];							
							$book_discount = $salesReportList[$i_report]['book_discount'];							
							
							$book_quantity = $salesReportList[$i_report]['book_quantity'];
							$book_sale_amount = $salesReportList[$i_report]['book_sale_amount'];
							

							// Code for Set each cell value
							$re->setCell( $worksheet, $k, $j++, $sr_no);
							$re->setCell( $worksheet, $k, $j++, $sale_date);
							$re->setCell( $worksheet, $k, $j++, $location_addr);
							$re->setCell( $worksheet, $k, $j++, $book_title);
							
							$re->setCell( $worksheet, $k, $j++, $book_mrp);
							$re->setCell( $worksheet, $k, $j++, $book_discount);													
								
							$re->setCell( $worksheet, $k, $j++, $book_quantity);
							$re->setCell( $worksheet, $k, $j++, $book_sale_amount);
	
					   }
					   

                           // Write workbook book data into csv file
                            $workbook->setActiveSheetIndex(0);

                            // Instantiate a Writer to create an OfficeOpenXML Excel .xls file
                            $objWriter = PHPExcel_IOFactory::createWriter($workbook, 'CSV');
                            $exportDirPath = EXPORT_DIR_PATH;
                            $filename = "SalesReport_".date("d_m_Y").".csv";
                            $objWriter->save($exportDirPath.$filename);

                            // Download the file
                            $path = $exportDirPath.$filename;
                            header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
                            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
                            header('Accept-Ranges: bytes');  // For download resume
                            header('Content-Length: ' . filesize($path));  // File size
                            header('Content-Encoding: none');
                            header('Content-Type: application/vnd.ms-excel');  // Change this mime type if the file is not PDF
                            header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
                           // readfile($path);  //this is necessary in order to get it to actually download the file, otherwise it will be 0Kb

                           // exit;	


					}


			
		}
		
	 break;
}

?>