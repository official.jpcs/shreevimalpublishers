<?php
$step=isset($_POST['step']) ? $_POST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	 case 'add_book':
	 case 'update_book':
	 
		$books = new tbl_books();
        $book_quantity =new tbl_book_quantity_at_location();
		
		$books->book_title 			=  mysqli_real_escape_string($_POST['book_name']); 
		$books->book_title_mr 			=  mysqli_real_escape_string($_POST['book_name_mr']); 
		$books->book_language 			=  mysqli_real_escape_string($_POST['book_language']); 
		$books->author_id 			=  $_POST['author']; 
		$books->book_mrp 			=  mysqli_real_escape_string($_POST['book_mrp']); 
		$books->compile_by 			=  mysqli_real_escape_string($_POST['compile_by']); 
		$books->translated_by 			=  mysqli_real_escape_string($_POST['translate_by']); 
		$books->book_edition 			=  mysqli_real_escape_string($_POST['book_edition']); 
		$books->book_description 		=  mysqli_real_escape_string($_POST['book_description']); 
		
		$books->publisher_id 			=  mysqli_real_escape_string($_POST['book_publisher']); 
		$books->isbn_no 			=  mysqli_real_escape_string($_POST['isbn_no']); 
		$books->category_id 			=  $_POST['book_category']; 
		$books->dim_width 			=  mysqli_real_escape_string($_POST['dim_width']); 
		$books->dim_height 			=  mysqli_real_escape_string($_POST['dim_height']); 
		$books->dim_depth 			=  mysqli_real_escape_string($_POST['dim_depth']); 
		$books->no_of_pages 			=  mysqli_real_escape_string($_POST['no_of_pages']); 
		$books->binding_id 			=  $_POST['binding_type']; 
		$books->cover_id 			=  $_POST['cover_type']; 
		$books->book_weight 			=  mysqli_real_escape_string($_POST['weight']); 
		$books->production_cost 		=  mysqli_real_escape_string($_POST['prod_cost']); 
		$books->is_translated 			=  mysqli_real_escape_string($_POST['translated']); 
		$books->translated_from_book    =  mysqli_real_escape_string($_POST['trans_frm_book']); 
		$books->translated_frm_lang     =  mysqli_real_escape_string($_POST['transl_frm_lng']); 
		$books->publication_date 		=  mysqli_real_escape_string($_POST['pub_date']); 
		$books->co_author 			=  mysqli_real_escape_string($_POST['co_author']); 
		$books->contributor 			=  mysqli_real_escape_string($_POST['contributor']); 
		$books->editor 				=  mysqli_real_escape_string($_POST['editor']); 
		$books->copyright 			=  mysqli_real_escape_string($_POST['copyright']);
		$pages 			=  mysqli_real_escape_string($_POST['pages']);

		if(!isset($_POST['is_upcoming_book']))		$_POST['is_upcoming_book']=0;
		$books->is_upcoming_book 		=  mysqli_real_escape_string($_POST['is_upcoming_book']); 
		$books->created_by 			=  mysqli_real_escape_string($_POST['created_by']); 
		
		
                
		// Book Image
		if($_FILES["book_thumbnail"]["name"]!="")
		{
		
			// Get Extention of file
			$explode = explode(".",$_FILES["book_thumbnail"]["name"]);
			$extension = end($explode);
				
			$book_thumbnail = rand(99,999)."_".$_FILES["book_thumbnail"]["name"]; 
			$fileTmpLoc = $_FILES["book_thumbnail"]["tmp_name"];
			// Path and file name
			 $pathAndName = BOOK_THUMBNAIL_FOLDER_PATH.$book_thumbnail;
			// Run the move_uploaded_file() function here
			$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName) ;

			$books->book_thumbnail =$book_thumbnail;
			
			
			
		}        
		
		if($step=='update_book')
		{
		  $book_id =$_POST['book_id'];	
          $books->update($book_id);
		  $_SESSION['Message']=102;
		  
		  $book_quantity->delete_location($book_id);
	 	}
		else
		{
		
		
		    		$books->created_date 	=  date("Y-m-d h:i:s"); 
                    $books->is_deleted	= 0;
                    $book_id=$books->insert();
                    $_SESSION['Message']=100;
		}
					
		// Add Book Quantity at Storage Location
		
		
		$location_count=count($_POST['sel_location']);
		
		if($location_count >0)
		{
			for($i_location=0;$i_location<=$location_count-1;$i_location++)
			{
				$location_id=$_POST['sel_location'][$i_location];
				$quantity=$_POST['quantity'][$i_location];
			 
				
				$book_quantity->book_id = $book_id;
				$book_quantity->location_id = $location_id;
				$book_quantity->book_quantity = $quantity;
				
				$book_quantity->insert();
				
				
			}
		}
                    
			
		
	 	 echo "<script>window.location='manage_books.php?pages=$pages';</script>";
	 break;
	 case 'edit_info':
	    
		 $book_id=$_POST['update_id'];
		$condition =" AND tb.book_id =".$book_id;
		
		// Get Book Detail for edit
		$books = new tbl_books();    
		$result_book_info = $books->getBookDetail($condition);
		$total_books = count($result_book_info);
		
		// Get Book Storage Location Detail 
		$location_detail = new tbl_book_quantity_at_location();    
		$location_info = $location_detail->selectStorageInfo($book_id);
		
		//View Book Detail with Publisher
		$result_book_list = $books->selectSingleBook($book_id);
	 	
		
	 break;

	 case 'delete_info':
	    
		$book_id=$_POST['update_id']; 
		$parameter=array('tbl_books','is_deleted','book_id',$book_id,'1');
		$re->funDoGenericAction($parameter);

		if($cover_type==1)
		{
	 	 echo "<script>window.location='manage_books.php';</script>";
	  	}
		else
		{
		 echo "<script>window.location='manage_books.php';</script>";
		}
	
	 break;
	  	 
	 default:
		$books = new tbl_books();//getBookList
		$total_record = $books->countBooks();
			
			$pages=$_REQUEST['pages'];
			$per_page = ITEMS_PER_PAGE;
			$page = 1;
			if($_REQUEST['pages']!="") {
				$page=$_REQUEST['pages'];
			}
			$start = ($page-1)*$per_page;

			
		$result_book_list = $books->getBookList($start,$per_page);
	 	$total_books = count($result_book_list);
		
		
		
	 break;
 
}

?>