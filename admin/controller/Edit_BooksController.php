<?php
$step=isset($_REQUEST['step']) ? $_REQUEST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	 case 'add_book':
	 case 'update_book':
	 
		$books = new tbl_books();
        $book_quantity =new tbl_book_quantity_at_location();
		
		$books->book_title 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['book_name']); 
		$books->book_title_mr 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['book_name_mr']); 
		$books->book_language 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['book_language']); 
		$books->author_id 			=  $_POST['author']; 
		$books->book_mrp 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['book_mrp']); 
		$books->compile_by 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['compile_by']); 
		$books->translated_by 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['translate_by']); 
		$books->book_edition 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['book_edition']); 
		$books->book_description 		=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['book_description']); 
		
		$books->publisher_id 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['book_publisher']); 
		$books->isbn_no 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['isbn_no']); 
		$books->category_id 			=  $_POST['book_category']; 
		$books->dim_width 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['dim_width']); 
		$books->dim_height 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['dim_height']); 
		$books->dim_depth 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['dim_depth']); 
		$books->no_of_pages 			=  mysqli_real_escape_string($db_con->link,$db_con->link,_POST['no_of_pages']); 
		$books->binding_id 			=  $_POST['binding_type']; 
		$books->cover_id 			=  $_POST['cover_type']; 
		$books->book_weight 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['weight']); 
		$books->production_cost 		=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['prod_cost']); 
		$books->is_translated 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['translated']); 
		$books->translated_from_book    =  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['trans_frm_book']); 
		$books->translated_frm_lang     =  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['transl_frm_lng']); 
		$books->publication_date 		=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['pub_date']); 
		$books->co_author 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['co_author']); 
		$books->contributor 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['contributor']); 
		$books->editor 				=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['editor']); 
		$books->copyright 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['copyright']);
		$pages 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['pages']);

		if(!isset($_POST['is_upcoming_book']))		$_POST['is_upcoming_book']=0;
		$books->is_upcoming_book 		=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['is_upcoming_book']); 
		$books->created_by 			=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['created_by']); 
		$books->is_book_visible 		=  mysqli_real_escape_string($db_con->link,$db_con->link,$_POST['is_book_visible']);
		
                
		// Book Image
		if($_FILES["book_thumbnail"]["name"]!="")
		{
		
			// Get Extention of file
			$explode = explode(".",$_FILES["book_thumbnail"]["name"]);
			$extension = end($explode);
				
			$book_thumbnail = rand(99,999)."_".$_FILES["book_thumbnail"]["name"]; 
			$fileTmpLoc = $_FILES["book_thumbnail"]["tmp_name"];
			// Path and file name
			 $pathAndName = BOOK_THUMBNAIL_FOLDER_PATH.$book_thumbnail;
			// Run the move_uploaded_file() function here
			$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName) ;

			$books->book_thumbnail =$book_thumbnail;
			
			
			
		}        
		
		if($step=='update_book')
		{
		  $book_id =$_POST['book_id'];	
          $books->update($book_id);
		  $_SESSION['Message']=102;
		  
		  $book_quantity->delete_location($book_id);
	 	}
		else
		{
		
		
		    		$books->created_date 	=  date("Y-m-d h:i:s"); 
                    $books->is_deleted	= 0;
                    $book_id=$books->insert();
                    $_SESSION['Message']=100;
		}
					
		// Add Book Quantity at Storage Location
		
		
		$location_count=count($_POST['sel_location']);
		
		if($location_count >0)
		{
			for($i_location=0;$i_location<=$location_count-1;$i_location++)
			{
				$location_id=$_POST['sel_location'][$i_location];
				$quantity=$_POST['quantity'][$i_location];
			 
				
				$book_quantity->book_id = $book_id;
				$book_quantity->location_id = $location_id;
				$book_quantity->book_quantity = $quantity;
				
				$book_quantity->insert();
				
				
			}
		}
                    
			
		
	 	 echo "<script>window.location='manage_books.php?pages=$pages';</script>";
	 break;
	 
	 case 'general_info':
	    
		 $book_id=$_REQUEST['book_id'];
		$condition =" AND tb.book_id =".$book_id;
		
		// Get Book Detail for edit
		$books = new tbl_books();    
		$result_book_info = $books->getBookDetail($condition);
		$total_books = count($result_book_info);
	
	 break;


	 case 'publisher_info':
	    $books = new tbl_books();    
		$book_id=$_REQUEST['book_id'];
		$condition =" AND tb.book_id =".$book_id;
		
		$result_book_info = $books->getBookDetail($condition);

		//View Book Detail with Publisher
		$result_book_list = $books->selectSingleBook($book_id);
		
		//var_dump($result_book_info[0]['pub_addr']); die;
	 break;
	 
	 case 'book_detail':
	    
		$book_id=$_REQUEST['book_id'];
		$condition =" AND tb.book_id =".$book_id;
		
		// Get Book Detail for edit
		$books = new tbl_books();    
		$result_book_info = $books->getBookDetail($condition);
		$total_books = count($result_book_info);
	 break;
	 
	 case 'storage_detail':
	    $books = new tbl_books();   
		$book_id=$_REQUEST['book_id'];
		$condition =" AND tb.book_id =".$book_id;
		
		$result_book_info = $books->getBookDetail($condition);
		
		// Get Book Storage Location Detail 
		$location_detail = new tbl_book_quantity_at_location();    
		$location_info = $location_detail->selectStorageInfo($book_id);
		
	 break;
	 
	 case 'related_books':
	    $books = new tbl_books();   
		$book_id=$_REQUEST['book_id'];
		$condition =" AND tb.book_id =".$book_id;

		$result_book_list = $books->getBookList($start,$per_page);
		
		$result_book_info = $books->getBookDetail($condition);

		//var_dump($result_book_info[0]["related_books"]); die;

	 break;

	 case 'book_images':
	    $books = new tbl_books();   
		$book_id=$_REQUEST['book_id'];
		$condition =" AND tb.book_id =".$book_id;

		//$result_book_list = $books->getBookList($start,$per_page);
		
		$result_book_info = $books->getBookDetail($condition);

		//var_dump($result_book_info[0]["related_books"]); die;

		$book_images = $books->getBookImages($book_id);

		//var_dump($book_images); die;


	 break;
	 
	 case 'update_generalinfo':
	 
		$books = new tbl_books();
		
		$books->book_title 			=  mysqli_real_escape_string($db_con->link,$_POST['book_name']); 
		$books->book_title_mr 			=  mysqli_real_escape_string($db_con->link,$_POST['book_name_mr']); 
		$books->book_language 			=  mysqli_real_escape_string($db_con->link,$_POST['book_language']); 
		$books->author_id 			=  $_POST['author']; 
		$books->book_mrp 			=  mysqli_real_escape_string($db_con->link,$_POST['book_mrp']); 
		$books->compile_by 			=  mysqli_real_escape_string($db_con->link,$_POST['compile_by']); 
		$books->translated_by 			=  mysqli_real_escape_string($db_con->link,$_POST['translate_by']); 
		$books->book_edition 			=  mysqli_real_escape_string($db_con->link,$_POST['book_edition']); 
		$books->book_description 		=  mysqli_real_escape_string($db_con->link,$_POST['book_description']); 
		

		if(!isset($_POST['is_upcoming_book']))		$_POST['is_upcoming_book']=0;
		$books->is_upcoming_book 		=  mysqli_real_escape_string($db_con->link,$_POST['is_upcoming_book']); 
		$books->created_by 			=  mysqli_real_escape_string($db_con->link,$_POST['created_by']); 

		$books->is_book_visible 			=  $_POST['is_book_visible'] ? '1' :'0'; 


		
                
		// Book Image
		if($_FILES["book_thumbnail"]["name"]!="")
		{
		
			// Get Extention of file
			$explode = explode(".",$_FILES["book_thumbnail"]["name"]);
			$extension = end($explode);
				
			$book_thumbnail = rand(99,999)."_".$_FILES["book_thumbnail"]["name"]; 
			$fileTmpLoc = $_FILES["book_thumbnail"]["tmp_name"];
			// Path and file name
			 $pathAndName = BOOK_THUMBNAIL_FOLDER_PATH.$book_thumbnail;
			// Run the move_uploaded_file() function here
			$moveResult = move_uploaded_file($fileTmpLoc, $pathAndName) ;

			$books->book_thumbnail =$book_thumbnail;
			
		}        
		
		  $book_id =$_POST['book_id'];	
          $books->updateGeneralInfo($book_id);
		  $_SESSION['Message']=102;
		  
		
	 	 echo "<script>window.location='edit_book_general_info.php?pages=$pages&step=general_info&book_id=$book_id';</script>";
	 break;
	 
	case 'update_publisher_info':
	
		$books = new tbl_books();

		$books->publisher_id =  mysqli_real_escape_string($db_con->link,$_POST['book_publisher']); 

		$book_id =$_POST['book_id'];	
		$books->updatePublisherInfo($book_id);
		$_SESSION['Message']=108;
		
	
	 	 echo "<script>window.location='edit_book_publisher_info.php?pages=$pages&step=publisher_info&book_id=$book_id';</script>";	
	break;

	case 'update_book_detail':
	
		$books = new tbl_books();
		$books->isbn_no 			=  mysqli_real_escape_string($db_con->link,$_POST['isbn_no']); 
		$books->category_id 			=  $_POST['book_category']; 
		$books->dim_width 			=  mysqli_real_escape_string($db_con->link,$_POST['dim_width']); 
		$books->dim_height 			=  mysqli_real_escape_string($db_con->link,$_POST['dim_height']); 
		$books->dim_depth 			=  mysqli_real_escape_string($db_con->link,$_POST['dim_depth']); 
		$books->no_of_pages 			=  mysqli_real_escape_string($db_con->link,$_POST['no_of_pages']); 
		$books->binding_id 			=  $_POST['binding_type']; 
		$books->cover_id 			=  $_POST['cover_type']; 
		$books->book_weight 			=  mysqli_real_escape_string($db_con->link,$_POST['weight']); 
		$books->production_cost 		=  mysqli_real_escape_string($db_con->link,$_POST['prod_cost']); 
		$books->is_translated 			=  mysqli_real_escape_string($db_con->link,$_POST['translated']); 
		$books->translated_from_book    =  mysqli_real_escape_string($db_con->link,$_POST['trans_frm_book']); 
		$books->translated_frm_lang     =  mysqli_real_escape_string($db_con->link,$_POST['transl_frm_lng']); 
		$books->publication_date 		=  mysqli_real_escape_string($db_con->link,$_POST['pub_date']); 
		$books->co_author 			=  mysqli_real_escape_string($db_con->link,$_POST['co_author']); 
		$books->contributor 			=  mysqli_real_escape_string($db_con->link,$_POST['contributor']); 
		$books->editor 				=  mysqli_real_escape_string($db_con->link,$_POST['editor']); 
		$books->copyright 			=  mysqli_real_escape_string($db_con->link,$_POST['copyright']);
		$pages 			=  mysqli_real_escape_string($db_con->link,$_POST['pages']);

		$book_id =$_POST['book_id'];	
		
		$books->updateBookDetail($book_id);
		$_SESSION['Message']=109;
		
		
	 	 echo "<script>window.location='edit_book_detail.php?pages=$pages&step=book_detail&book_id=$book_id';</script>";	
	
	break;

	case 'update_storage_detail':
	
	$book_id =$_POST['book_id'];
	 $book_quantity =new tbl_book_quantity_at_location();	
	 $book_quantity->delete_location($book_id);
		 
		// Add Book Quantity at Storage Location
		$location_count=count($_POST['sel_location']);
		
		if($location_count >0)
		{
			for($i_location=0;$i_location<=$location_count-1;$i_location++)
			{
				$location_id=$_POST['sel_location'][$i_location];
				$quantity=$_POST['quantity'][$i_location];
			 
				
				$book_quantity->book_id = $book_id;
				$book_quantity->location_id = $location_id;
				$book_quantity->book_quantity = $quantity;
				
				$book_quantity->insert();
				
				
			}
		}
		 $_SESSION['Message']=110;
	
	 	 echo "<script>window.location='edit_book_storage_detail.php?pages=$pages&step=storage_detail&book_id=$book_id';</script>";	
	
	break;
	
	case 'update_related_books' :

		$books = new tbl_books();

		$book_id =$_POST['book_id'];
		$related_book_list =$_POST['related_books'];

		if(count($related_book_list) >0)
		{
			//$set_related_book = implode(",", $related_book_list).",".$book_id;
			$set_related_book = implode(",", $related_book_list);
			
			$update_for_book = "'".str_replace(",","','",$set_related_book)."'";
			
			$books->updateRelatedBooks($set_related_book,$book_id);

			$_SESSION['Message']=111; 	
        }
        else
        {
        	$_SESSION['Message']=112;
        }

	 	 echo "<script>window.location='edit_related_books.php?pages=$pages&step=related_books&book_id=$book_id';</script>";	

	break;
}

?>