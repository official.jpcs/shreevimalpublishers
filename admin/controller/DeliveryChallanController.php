<?php
$step=isset($_POST['step']) ? $_POST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	case 'add_delivery_entry':
	//case 'update_book':
	 
            $delivery_master_entry = new tbl_delivery_master_entry();
            $delivery_book_entry = new tbl_delivery_book_entry();
            $re    = new reuseFunction();
		
            $delivery_master_entry->user_id 			=  $_SESSION['user']['user_id']; 
            $delivery_master_entry->delivery_date 			=  date("Y-m-d", strtotime(mysqli_real_escape_string($db_con->link,$_POST['delivery_date']))); 

            $delivery_master_entry->total_amount 		=  mysqli_real_escape_string($db_con->link,$_POST['total_amount']); 
            $delivery_master_entry->final_amount 		=  mysqli_real_escape_string($db_con->link,$_POST['final_amount']); 

            $delivery_master_entry->customer_name 		=  mysqli_real_escape_string($db_con->link,$_POST['customer_name']); 
             $delivery_master_entry->created_date 		=  date("Y-m-d h:i:s"); 
            $delivery_master_entry->is_deleted 		=  0; 

            $delivery_master_entry_id=$delivery_master_entry->insert();
            

			
			
            // Add delivery Book in delivery Entry tables
            $delivery_book_count=count($_POST['p_book_id']);

            if($delivery_book_count >0)
            {
                for($i_delivery_book=0;$i_delivery_book<=$delivery_book_count-1;$i_delivery_book++)
                {
                    $p_book_id          = $_POST['p_book_id'][$i_delivery_book];
                    $p_book_price_id    = $_POST['p_book_price_id'][$i_delivery_book];
                    $p_discount_per_id  = $_POST['p_discount_per_id'][$i_delivery_book];
                    $p_quantity_id      = $_POST['p_quantity_id'][$i_delivery_book];
                    $p_book_amount      = $_POST['p_book_amount'][$i_delivery_book];
                    $location_id        = $_POST['location_id'][$i_delivery_book];
                    
                    $delivery_book_entry->delivery_maste_id = $delivery_master_entry_id;
                    $delivery_book_entry->book_id = $p_book_id;
                    $delivery_book_entry->book_mrp =$p_book_price_id;
                    $delivery_book_entry->book_discount = $p_discount_per_id;
                    $delivery_book_entry->book_delivery_amount = $p_book_amount;
                    $delivery_book_entry->book_quantity = $p_quantity_id;
                    $delivery_book_entry->location_id = $location_id;


                    $delivery_book_entry->insert();
					
					// Deduct the Book Quantity From Location
					
					$condition ="`book_id`='".$p_book_id."' AND `location_id`='".$location_id."'";
					
					$value_update = "book_quantity -".$p_quantity_id;
					
					$parameter= array("tbl_books_quantity_at_location","book_quantity",$condition,$value_update);
					
					 $re->funMinusTheBookQuantityFromLocation($parameter);
	

                }
            }
            
			
            
            $_SESSION['Message']=200;


            echo "<script>window.location='new_delivery_entry.php';</script>";
	 break;

	 case 'edit_info':
	    
		$book_id=$_POST['update_id'];
		$condition =" AND tb.book_id =".$book_id;
		
		// Get Book Detail
		$books = new tbl_books();    
		$result_book_info = $books->getBookDetail($condition);
		$total_books = count($result_book_info);
		
		// Get Book Storage Location Detail 
		$location_detail = new tbl_book_quantity_at_location();    
		$location_info = $location_detail->selectStorageInfo($book_id);

	 break;

	 case 'delete_info':
	    
		$delivery_entry_id=$_POST['update_id']; 
		$parameter=array('tbl_delivery_master_entry','is_deleted','delivery_maste_id',$delivery_entry_id,'1');
		$re->funDoGenericAction($parameter);
		
		$_SESSION['Message']=202;
		echo "<script>window.location='manage_delivery.php';</script>";
	
	 break;
	  	 
	 case 'view_detail':
	    
		$delivery_entry_id=$_POST['update_id']; 
	 
		$delivery_master_entry = new tbl_delivery_master_entry();
		$delivery_book_entry = new tbl_delivery_book_entry();
		
		$condition= "sm.delivery_maste_id=".$delivery_entry_id;
		
		$delivery_master_entry_detail = $delivery_master_entry->getdeliveryEntryMasterDetail($condition);
		
		// delivery Book List
		$condition_sl =" tse.delivery_maste_id=".$delivery_entry_id;
		$delivery_book_entry_list = $delivery_book_entry->getdeliveryBookEntryList($condition_sl);
		
	 break;
	  	 
	 default:
	 
	  $delivery_entry = new tbl_delivery_master_entry();
	   
	  $user_id =  $_SESSION['user']['user_id']; 
	  $user_type =  $_SESSION['user']['user_type'];
	   
	   if($user_type==2)
	   {
	     $condition =" AND `user_id`=".$user_id;
	   }
	   else{
	    $condition ="";
	   }
	   
	   $result_delivery_list = $delivery_entry->getdeliveryList($condition);
	   $total_delivery_entry = count($result_delivery_list);
	 break;
 
}

?>