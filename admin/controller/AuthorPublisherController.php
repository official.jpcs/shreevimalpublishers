<?php
$step=isset($_POST['step']) ? $_POST['step'] : '';
$db_con = new Database();
$db_con->OpenLink();

switch($step)
{
	 case 'add_author_publisher':
	 case 'update_author_publisher':
	 
	 
		$publisher_list = new tbl_authors_publishers();
		
		$publisher_list->name 	= mysqli_real_escape_string($db_con->link,$_POST['publisher_name']);
		$publisher_list->name_mr 	= mysqli_real_escape_string($db_con->link,$_POST['publisher_name_mr']);
		$publisher_list->address =  mysqli_real_escape_string($db_con->link,$_POST['address']);
		$publisher_list->address_mr =  mysqli_real_escape_string($db_con->link,$_POST['address_mr']);
		$publisher_list->contact_no =  $_POST['contact_no'];
		$publisher_list->email_id 	=  $_POST['pub_email_id'];
		
		$publisher_list->website =  $_POST['website'];
		$publisher_list->user_type 	=  $user_type;
		
		
		$publisher_list->created_date =date("Y-m-d h:i:s"); 
		
		if($step=='update_author_publisher')
		{
		 $pub_auth_id 		= $_POST['pub_auth_id'];
		 $publisher_list->update($pub_auth_id);       // update
	 	}
		else
		{
			$publisher_list->is_deleted	= 0;
			$publisher_list->insert();
			$_SESSION['Message']=1;

		}
		
		if($user_type==1)
		{	
	 	 echo "<script>window.location='manage_publishers.php';</script>";
	    }
		else
		{
	 	 echo "<script>window.location='manage_authors.php';</script>";
		
		} 
	 break;

	 case 'edit_info':
	    
		$pub_auth_id=$_POST['update_id'];
		$publisher_list = new tbl_authors_publishers();
		$publisher_list->selectSinglePubAuthor($pub_auth_id);
	
	 break;

	 case 'delete_info':
	    
		$pub_auth_id=$_POST['update_id']; 
		$parameter=array('tbl_authors_publishers','is_deleted','id',$pub_auth_id,'1');
		$re->funDoGenericAction($parameter);
		if($user_type==1)
		{	
	 	 echo "<script>window.location='manage_publishers.php';</script>";
	    }
		else
		{
	 	 echo "<script>window.location='manage_authors.php';</script>";
		
		} 
	
	 break;
	  	 
	 default:
	 	$user_type=$user_type;
		$authpublist = new tbl_authors_publishers();
			$total_record = $authpublist->CountAuthorPublishers($user_type);
			
			$pages=$_REQUEST['pages'];
			$per_page = ITEMS_PER_PAGE;
			$page = 1;
			if($_REQUEST['pages']!="") {
				$page=$_REQUEST['pages'];
			}
			$start = ($page-1)*$per_page;

	 	$result_authpub_list = $authpublist->select_author_publishers($user_type,$start,$per_page);
		
		//$total_record = count($result_authpub_list);
	 break;
 
}

?>