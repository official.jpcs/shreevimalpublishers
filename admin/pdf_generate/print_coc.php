<?php 
include("authorizeServices/config.php");
setlocale(LC_MONETARY, 'en_US');

$data = json_decode(file_get_contents("php://input"));
$token = $data->token;
$coc_id = $data->coc_id;

/**
* Authenicate and get back token
*/
$curl = curl_init(SUGAR_SERVER_PATH."rest/v10/oauth2/token");
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// Set the POST arguments to pass to the Sugar server"client_secret":"","current_language":"en_us",
$rawPOSTdata = array(
    "grant_type" => "password",
    "username" => API_TOKEN_USERNAME,
    "password" => API_TOKEN_PASSWORD,
    "client_id" => "sugar",
    "platform" => 'base',
    "client_secret" => '',
    "current_language"=>'en_us'
);
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($rawPOSTdata));
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
// Make the REST call, returning the result
$response = curl_exec($curl);           
if (!$response) {
    die("Connection Failure.\n");
}
// Convert the result from JSON format to a PHP array
$result = json_decode($response);
curl_close($curl);
if (isset($result->error)) 
{
    die($result->error_message . "\n");
}
$token = $result->access_token;

/**
 * Get coc information with coc id
 */
$ch = curl_init(SUGAR_SERVER_PATH . 'rest/v10/portal/Lab_COC/' . $coc_id);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'oauth-token: ' . $token)
);
// execute!
$response_curl = curl_exec($ch);

$coc_data = json_decode($response_curl);


/**
 * Get coc sample with coc id
 */
$ch_sample = curl_init(SUGAR_SERVER_PATH .'rest/v10/portal/Lab_COC/'.$coc_id.'/link/lab_coc_lab_sample?max_num=500&view=list&fields=id,description,sample_type,water_system,test_code,notes,date_collected,sample_number,acceptable,comment_c&order_by=sample_number:ASC');
curl_setopt($ch_sample, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch_sample, CURLOPT_HTTPHEADER, array(
    'oauth-token: ' . $token)
);
// execute!
$response_curl_sample = curl_exec($ch_sample);
$coc_sample_data = json_decode($response_curl_sample);
$coc_sample_data_res = $coc_sample_data->records;
/****** HTML Template For PDF ********/

$html='<html>
       <head> 
    <style>
  @page *{
    margin:0px ;
}
h3{margin: 0px;font-size:13px}
td{margin: 0px;padding: 5px; vertical-align: top;}

table {
  border-collapse: collapse;
}
table td, table th {
  border: 1px solid black;
}
table tr:first-child th {
  border-top: 0;
}
table tr:last-child td {
  border-bottom: 0;
}
table tr td:first-child,
table tr th:first-child {
  border-left: 0;
}
table tr td:last-child,
table tr th:last-child {
  border-right: 0;
}

.wrapper {
	width:90%;
	max-width:800px;
	margin:0px auto;
	font-family:Arial, Verdana, Helvetica, sans-serif;
	font-size:11px;
        color:#535353;
               
	}
.bold {
	font-weight:bold;
	}
.row {
	width:100%;
	float:left;
	padding:4px 0;
        
	}
@media print{
	input {
	border:0px none;
	}
.pagebreak{page-break-after: always;}
}
.w50 {
	width:50%;
	float:left;
	}
.w50.left-form {
	border:1px solid #000;
	float:left;
	width:313px;
	padding:10px 5px;
	height:107px;
	}
.w50.right-form{
	border:1px solid #000;
	float:left;
	width:314px;
	padding:10px 5px;
	border-left:0px none;
	}
.submit_btn{
        border :1px solid;
        background-color:buttonhighlight; 
        font-weight: bold;
        height: 30px;
        width: 40%;
            cursor:pointer;
        }       
.no_border{ border-bottom: none;} 



.mt20{ margin-top: 20px;}
.mt10{ margin-top: 10px;}
.mb20{ margin-bottom: 20px;}
.mb10{ margin-bottom: 10px;}
.member_heading_color{ color :#3BACE4 !important; }

ol { margin-top: 2px;padding-left: 18px;}
.ol_li_class { padding-bottom: 10px;color:#6e6a6b;font-size: 15px;}

.member_synopsis { width:100%;float: left;line-height: 20px;text-spacing: 4px;}
.footer_div { width:99%;float:left;padding:25px;padding-bottom:10px; }
.footer_lable { font-size: 14px;font-weight: bold }
.footer_content { color:#6e6a6b;font-size: 15px; }
</style>   
</head>

<body>
    <div class="wrapper">';


$html .='<p><strong>SPL ID #</strong>&nbsp;'.$coc_data->spl_id.' &nbsp;</p>
<table style="width: 100%;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:11px;margin:0px"  cellspacing="0" cellpadding="15" align="left">
<tbody>
<tr>
<td style="background-color: #ccc;" colspan="4">
<h3>&nbsp;Client Information</h3>
</td>
</tr>
<tr>
<td><strong>&nbsp;Account No.</strong>&nbsp;<br />&nbsp;'.$coc_data->account_no_c.'</td>
<td><strong>&nbsp;PO Number</strong>&nbsp;<br />&nbsp;'.$coc_data->po_number.'</td>     
<td><strong>&nbsp;Submitting Company</strong>&nbsp;<br />&nbsp;'.$coc_data->accounts_lab_coc_1_name.'</td>
<td><strong>&nbsp;Address</strong>&nbsp;<br />&nbsp;'.$coc_data->address.', '.$coc_data->city.',<br />&nbsp;'.$coc_data->state.',&nbsp;'.$coc_data->zip.'</td>
</tr>
<tr>
<td style="background-color: #ccc;" colspan="4">
<h3>&nbsp;Sample Information</h3>
</td>
</tr>
<tr>
<td><strong>&nbsp;Project Identifier</strong>&nbsp;<br />&nbsp;'.$coc_data->project_identifier.'</td>
<td><strong>&nbsp;Sample By</strong>&nbsp;<br />&nbsp;'.$coc_data->sampled_by.'</td>
<td><strong>&nbsp;From State of New York</strong>&nbsp;<br />&nbsp;'.$coc_data->is_from_state_newyork_c.'</td>   
<td><strong>&nbsp;Number of Sample</strong>&nbsp;<br />&nbsp;'.$coc_data->no_of_samples.'</td>
</tr>
<tr>
<td colspan="2"><strong>&nbsp;Are these samples for compliance?</strong>&nbsp;<br />&nbsp;'.$coc_data->samples_for_compliance_c.'</td>
<td colspan="2"><strong>&nbsp;PWSID Number</strong>&nbsp;<br />&nbsp;'.$coc_data->pwsid_number_c.'</td> 
</tr>
<tr>
<td colspan="4"><strong>&nbsp;Is primary biocide for cooling tower an oxidizing agent ?</strong>&nbsp;<br />&nbsp;'.$coc_data->is_biocide_cooling_agent_c.'</td>
</tr>
<tr>
<td style="background-color: #ccc;" colspan="4">
<h3>Report Contact Information</h3>
</td>
</tr>
<tr>
<td><strong>&nbsp;Report Contact</strong><br />&nbsp;'.$coc_data->report_contact.'</td>
<td><strong>&nbsp;Phone</strong>&nbsp;<br />&nbsp;'.'('.substr($coc_data->phone,0,3).')'.substr($coc_data->phone,3,3).'-'.substr($coc_data->phone,6).'</td>
<td><strong>&nbsp;Fax</strong>&nbsp;<br />&nbsp;'.'('.substr($coc_data->fax,0,3).')'.substr($coc_data->fax,3,3).'-'.substr($coc_data->fax,6).'</td>
<td><strong>&nbsp;Email</strong>&nbsp;<br />&nbsp;'.$coc_data->email.'</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table style="width: 100%; font-family:Arial, Verdana, Helvetica, sans-serif;font-size:11px;margin:0px"  cellspacing="0" cellpadding="5">
<tbody>
<tr>
<td style="border-right: none;" colspan="10"><strong>&nbsp;Sample Information</strong><strong>&nbsp;Volumes: 120 ml required for all tests; 500 ml for Waterborne Pathogens Panel</strong></td>
</tr>
<tr>
<td width="10%"><strong>Sample No.</strong></td>
<td width="10%"><strong>Sample Description</strong></td>
<td width="10%"><strong>Sample Type</strong></td>
<td width="10%"><strong>Test Codes</strong></td>
<td width="10%"><strong>Water System</strong></td>
<td width="10%"><strong>Notes</strong></td>
<td width="10%"><strong>Date Collected</strong></td>
<td width="10%"><strong>Time Collected</strong></td>
<td width="10%">SPL USE ONLY<br /> <strong>&nbsp;Sample Conditions &nbsp;Acceptable?</strong></td>
<td width="10%">SPL USE ONLY<br /> <strong>&nbsp;Comments</strong></td>
</tr>';
if (isset($coc_sample_data_res)) {

    foreach ($coc_sample_data_res as $coc_sample_item) {
        $html.='
<tr>
<td >&nbsp;'.$coc_sample_item->sample_number.'</td>
<td >&nbsp;'.$coc_sample_item->description.'</td>
<td >&nbsp;'.$coc_sample_item->sample_type.'</td>
<td >&nbsp;'.implode(',',$coc_sample_item->test_code).'</td>
<td>&nbsp;'.$coc_sample_item->water_system.'</td>
<td >&nbsp;'.$coc_sample_item->notes.'</td>
<td >&nbsp;'.($coc_sample_item->date_collected!='' ? date("m/d/Y",  strtotime($coc_sample_item->date_collected)) : '').'</td>
<td >&nbsp;'.($coc_sample_item->date_collected!='' ? date("H:i A",  strtotime($coc_sample_item->date_collected)) : '').'</td>
<td >&nbsp;'. ucfirst($coc_sample_item->acceptable).'</td>
<td >&nbsp;'.$coc_sample_item->comment_c.'</td>
</tr>';
    }
} else {
    $html.='<tr><td colspan="6">No record found</td></tr>';
}
$html.='</tbody>
</table>

<p>&nbsp;</p>
<table style="width: 100%;font-family:Arial, Verdana, Helvetica, sans-serif;font-size:11px;margin:0px"  cellspacing="0" cellpadding="5">
<tbody>
<tr>
<td colspan="4" width="100%"><strong>&nbsp;Submission Notes</strong></td>
</tr>
<tr>
<td colspan="4" width="100%">&nbsp;'.$coc_data->description.'</td>
</tr>
</tbody>
</table>

<p>&nbsp;</p>
<table style="width: 100%; font-family:Arial, Verdana, Helvetica, sans-serif;font-size:11px;"  cellspacing="0" cellpadding="5">
<tbody>
<tr>
<td><strong>&nbsp;Relinquished by</strong></td>
<td><strong>&nbsp;Relinquished Date</strong></td>
<td><strong>&nbsp;Relinquished Time</strong></td>
<td><strong>&nbsp;Received by</strong></td>
<td><strong>&nbsp;Received Date</strong></td>
<td><strong>&nbsp;Received Time</strong></td>
</tr>
<tr>
<td>&nbsp;'.$coc_data->reliquished_by.'</td>
<td>&nbsp;'.($coc_data->reliquished_date!='' ? date("m/d/Y",  strtotime($coc_data->reliquished_date)) : '').'</td>
<td>&nbsp;'.($coc_data->reliquished_date!='' ? date("H:i A",  strtotime($coc_data->reliquished_date)) : '').'</td>
<td>&nbsp;'.$coc_data->received_by.'</td>
<td>&nbsp;'.($coc_data->received_date!='' ? date("m/d/Y",  strtotime($coc_data->received_date)) : '').'</td>
<td>&nbsp;'.($coc_data->received_date!='' ? date("H:i A",  strtotime($coc_data->received_date)) : '').'</td>
</tr>
</tbody>
</table>
<p>&nbsp;* Sample bottles provided upon request.</p>';

$html.='</div>
</body>
</html>';

//echo $html;die; 
include("mpdf60/mpdf.php");

$mpdf=new mPDF('c'); 
$mpdf->mPDF('utf-8','A4','','','0','0','35','10'); 

//$header_content ='<div style="width:100%;float:left;padding-top:-35px;"> <img src="pdf_header.png" alt="Header Banner" height="400"  /></div>';
// Define the Header/Footer before writing anything so they appear on the first page
//$mpdf->SetHTMLHeader($header_content);

//$mpdf->SetWatermarkText('CONFIDENTIAL', 0.1);
//$mpdf->watermark_font = 'ubuntu';
//$mpdf->showWatermarkText = true;


$mpdf->WriteHTML($html);

//$footer_text ="<div class='footer_div'><span class='footer_lable'>DISCLAIMER:</span><span class='footer_content'>This medical opinion does not constitute a physician - patient relationship, and is not intended as a solicitation of individuals to become patients or clients of the physician. No diagnosis or treatment is being provided. Any suggested changes in treatment should be discussed with patient's treating physician.</span></div>";

//$mpdf->SetHTMLFooter($header_content);
//$mpdf->Output(); 


$mpdf->Output('coc_print/coc_details.pdf','F');
//header("Content-Disposition: attachment; filename=invoice_receipt.pdf"); 
//readfile ("invoice_receipt.pdf");

$file_path =$site_root."coc_print/coc_details.pdf";
echo $file_path;
exit;

?>


