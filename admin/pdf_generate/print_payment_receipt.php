<?php 
include("authorizeServices/config.php");
setlocale(LC_MONETARY,"en_US");


$data = json_decode(file_get_contents("php://input"));
$token = $data->token;
$payment_id = $data->payment_id;
$card_no = $data->card_no;

/**
* Authenicate and get back token
*/

$curl = curl_init(SUGAR_SERVER_PATH."rest/v10/oauth2/token");
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
// Set the POST arguments to pass to the Sugar server"client_secret":"","current_language":"en_us",
$rawPOSTdata = array(
    "grant_type" => "password",
    "username" => API_TOKEN_USERNAME,
    "password" => API_TOKEN_PASSWORD,
    "client_id" => "sugar",
    "platform" => 'base',
    "client_secret" => '',
    "current_language"=>'en_us'
);
curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($rawPOSTdata));
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
// Make the REST call, returning the result
$response = curl_exec($curl);           
if (!$response) {
    die("Connection Failure.\n");
}
// Convert the result from JSON format to a PHP array
$result = json_decode($response);
curl_close($curl);
if (isset($result->error)) 
{
    die($result->error_message . "\n");
}
$token = $result->access_token;

/**
 * Get invoice related to payment
 */
$ch = curl_init(SUGAR_SERVER_PATH . 'rest/v10/portal/Qb_Payment/' . $payment_id);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'oauth-token: ' . $token)
);
// execute!
$response_curl = curl_exec($ch);
$payment_data = json_decode($response_curl);

/**
 * Get Payment related invoice data 
 */
$ch = curl_init(SUGAR_SERVER_PATH . 'rest/v10/portal/Qb_Payment/' . $payment_id.'/link/qb_payment_qb_invoice_1?view=subpanel-for-qb_payment-qb_payment_qb_invoice_1');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'oauth-token: ' . $token)
);
// execute!
$response_curl = curl_exec($ch);

$payment_invoice_obj = json_decode($response_curl);
$payment_invoice_data = $payment_invoice_obj->records;

/****** HTML Template For PDF ********/

$html='<html>
       <head> 
    
<style>
  @page *{
    margin:0px ;
}
.wrapper {
	width:90%;
	max-width:800px;
	margin:0px auto;
	font-family:forza;
	font-size:12px;
        color:#535353;
               
	}
.bold {
	font-weight:bold;
	}
.row {
	width:100%;
	float:left;
	padding:4px 0;
        
	}
@media print{
	input {
	border:0px none;
	}
.pagebreak{page-break-after: always;}
}
.w50 {
	width:50%;
	float:left;
	}
.w50.left-form {
	border:1px solid #000;
	float:left;
	width:313px;
	padding:10px 5px;
	height:107px;
	}
.w50.right-form{
	border:1px solid #000;
	float:left;
	width:314px;
	padding:10px 5px;
	border-left:0px none;
	}
.submit_btn{
        border :1px solid;
        background-color:buttonhighlight; 
        font-weight: bold;
        height: 30px;
        width: 40%;
            cursor:pointer;
        }       
.no_border{ border-bottom: none;} 



.mt20{ margin-top: 20px;}
.mt10{ margin-top: 10px;}
.mb20{ margin-bottom: 20px;}
.mb10{ margin-bottom: 10px;}
.member_heading_color{ color :#3BACE4 !important; }

ol { margin-top: 2px;padding-left: 18px;}
.ol_li_class { padding-bottom: 10px;color:#6e6a6b;font-size: 15px;}

.member_synopsis { width:100%;float: left;line-height: 20px;text-spacing: 4px;}
.footer_div { width:99%;float:left;padding:25px;padding-bottom:10px; }
.footer_lable { font-size: 14px;font-weight: bold }
.footer_content { color:#6e6a6b;font-size: 15px; }
</style>
</head>

<body>
    <div class="wrapper">';

$html.='
        <table style="width: 800px; margin:0 auto 30px;font-family:arial;font-size:12px" border="0" cellspacing="0" cellpadding="0" align="center">
            <tbody>
                <tr>
                    <td style="width:30%">
                        <img src="'.$site_root.'spl_logo.jpg" alt="" width="150"/>
                    </td>
                </tr>
        </table>';


$html.='<p style="width: 800px; margin:0 auto 10px;font-family:arial;font-size:15px;font-weight:bold; border-bottom:1px solid #000;padding-bottom:5px; text-align:left;">Payment Receipt</p>';





$html.='<table style="width: 800px; margin:0 auto 50px;font-family:arial;font-size:12px"  cellspacing="0" cellpadding="5" align="center">
        <tbody>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr style="color: #000;font-family:arial;font-size:12px" >
                <td width="25%"><strong>&nbsp;Card Number:</strong></td>
                <td width="30%">XXXX-XXXX-XXXX-'.$card_no.'</td>
                <td width="25%"><strong>Payment Date:</strong></td>
                <td width="30%">'.($payment_data->payment_date!='' ? date("m/d/Y",  strtotime($payment_data->payment_date)) : '').'</td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>           

        </tbody>
    </table>';


$html.='<table style="width: 800px; margin:0 auto 30px;font-family:arial;font-size:12px" border="1" cellspacing="0" cellpadding="5" align="center">
        <tbody>
            <tr style="color: #000;font-family:arial;font-size:12px" bgcolor="#CCCCCC">
                <td width="30%"><strong>&nbsp;Invoice Number</strong></td>
                <td width="30%"><strong>Amount</strong></td>
               
            </tr>';
if(isset($payment_invoice_data))
    {
  foreach($payment_invoice_data as $payment_invoice_data_item) {           
           $html.= '<tr style="font-family:arial;font-size:12px">
                <td>&nbsp;'.$payment_invoice_data_item->name.'&nbsp;</td>
                <td>&nbsp;$'.number_format($payment_invoice_data_item->data_paid_amount,2).'</td>
                
            </tr>';
            }
    }
    else
    {
      $html.='<tr><td colspan="6">No record found</td></tr>';   
    }
      $html.='</tbody>
    </table>';


$html.='<table style="width: 800px; margin:0 auto 30px;font-family:arial;font-size:12px"  cellspacing="0" cellpadding="5" align="center">
        <tbody>
            <tr style="color: #000;font-family:arial;font-size:12px" >
                
                <td width="30%" style="text-align:right"><strong>Total Amount ($):</strong></td>
                <td width="30%">&nbsp;$'.number_format($payment_data->amount,2).'</td>
            </tr>
             


        </tbody>
    </table>';


        
$html.='</div>
</body>
</html>';

//echo $html;
//die;
include("mpdf60/mpdf.php");

$mpdf=new mPDF('c'); 
$mpdf->mPDF('utf-8','A4','','','0','0','35','10'); 

//$header_content ='<div style="width:100%;float:left;padding-top:-35px;"> <img src="pdf_header.png" alt="Header Banner" height="400"  /></div>';
// Define the Header/Footer before writing anything so they appear on the first page
//$mpdf->SetHTMLHeader($header_content);

//$mpdf->SetWatermarkText('CONFIDENTIAL', 0.1);
//$mpdf->watermark_font = 'ubuntu';
//$mpdf->showWatermarkText = true;


$mpdf->WriteHTML($html);

//$footer_text ="<div class='footer_div'><span class='footer_lable'>DISCLAIMER:</span><span class='footer_content'>This medical opinion does not constitute a physician - patient relationship, and is not intended as a solicitation of individuals to become patients or clients of the physician. No diagnosis or treatment is being provided. Any suggested changes in treatment should be discussed with patient's treating physician.</span></div>";

//$mpdf->SetHTMLFooter($header_content);
//$mpdf->Output(); 


$mpdf->Output('payment_invoice_print/payment_receipt.pdf','F');
//header("Content-Disposition: attachment; filename=invoice_receipt.pdf"); 
//readfile ("invoice_receipt.pdf");

$file_path =$site_root."payment_invoice_print/payment_receipt.pdf";
echo $file_path;
exit;

?>


