<!--Header-->
<?php include('header.php'); 
if(isset($_GET['msg']))
{
$msg =$_GET['msg'];
}
else
{
	$msg ="";
}
?>
        <!--Start breadcrumb area-->
        <section class="breadcrumb-area style2" style="background-image: url(images/resources/3d.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="inner-content-box clearfix">
                            <div class="title-s2 text-center">
                                <span></span>
                              
								<h2><?php echo  "<script>window.history.back();</script>
            <div class='alert alert-success alert-dismissible'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h6><i class='icon fa fa-check'></i> $msg </h6>
              "; ?></h2>
     </div>
                            <div class="breadcrumb-menu float-left">
                               <!--	<ul class="clearfix">
                                    <li><a href="index-2.html">Home</a></li>
                                    <li><a href="project.html">Projects</a></li>
                                    <li class="active">Classic View V1</li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End breadcrumb area-->

        <!--Start Main project area-->
        <section class="main-project-area">
            <div class="container-fluid">
               <!-- <ul class="project-filter post-filter has-dynamic-filters-counter">
                    <li data-filter=".filter-item" class="active"><span class="filter-text">All Projects</span></li>
                    <li data-filter=".mod"><span class="filter-text">Modern</span></li>
                    <li data-filter=".contem"><span class="filter-text">Contemporary</span></li>
                    <li data-filter=".trad"><span class="filter-text">Traditional</span></li>
                    <li data-filter=".ret"><span class="filter-text">Retreat</span></li>
                </ul> -->
                <div class="row filter-layout masonary-layout">
                <div class="col-xl-12 col-md-12 col-sm-10 ml-3">
				<div class="row">
					<?php
						//connection
						include('includes/dbcon.php'); 
			
						if(isset($_GET['bk']))
					{
						$id=$_GET['bk'];
			
						$query = "SELECT * from tbl_books INNER JOIN tbl_book_categories ON 
						(tbl_books.category_id=tbl_book_categories.category_id)
						INNER JOIN tbl_languages ON
                        (tbl_books.book_language=tbl_languages.language_id)
						WHERE tbl_books.category_id=$id AND is_book_visible=2";
						$fire = mysqli_query($mysqli,$query) or die("can not fetch the data." .mysqli_error($mysqli));
						if(mysqli_num_rows($fire)>0)
										{
											while($user = mysqli_fetch_assoc($fire))
										{
						
					?>
                    <!--Start single project item-->
					<form action="manage_cart.php" method="POST">
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6  filter-item contem ret">
                        <div class="single-project-style4">
                            <div class="img-holder">
                                <div class="inner">
                                    <img src="admin/images/book_thumbnails/<?php echo $user['book_thumbnail']?>" alt="Awesome Image">
                                   
                                </div>
                                <div class="overlay-content">
                                    <div class="title">
                                        <div><h3><a href="Book_Category_Single.php?bk=<?php echo $user['book_id']?>"><?php echo $user['book_title']?></a></h3></div>
                                    </div>
                                </div>
                            </div>
                        <br>
						
								<div class="text-center"><span><b><?php echo $user['category_title']?>&nbsp;|&nbsp;<?php echo $user['title']?>&nbsp;|&nbsp;Rs.&nbsp;<?php echo $user['book_mrp']?></b></span></div>
								<button class="btn-sm btn-block" id="cat" name="add_to_cart_category" style="background-color:#ff7b00; color:#fff">ADD TO CART</button>
										<input type="hidden" name="book_name" value="<?php echo $user['book_title']?>">
										<input type="hidden" name="₹" value="<?php echo $user['book_mrp']?>">
						</div>	
                    </div>
					</form>
                    <!--End single project item-->
					<?php
										}
										}
					}
					
					?>
				</div>
				</div>
				</div>
            </div>
        </section>
        <!--End Main project area-->

<!--Footer-->
<?php include('footer.php'); ?>